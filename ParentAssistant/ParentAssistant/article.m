//
//  article.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "article.h"
#import "articleQuestion.h"

@interface article()

@property (nonatomic, strong) NSString *testpaper_id;
@property (nonatomic, assign) int type;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *brief;
@property  (nonatomic, assign) BOOL forwho;
@property (nonatomic, strong) NSString *creator;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) NSString *course_id;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSString *createtime;
@end
@implementation article

- (id)init
{
    self = [super init];
    if (self) {
        self.previewd = NO;
    }
    return self;
}

- (void)updateListWithDic:(NSDictionary*)dic
{
    self.testpaper_id = [dic objectForKey:@"testpaper_id"];
    self.type = [[dic objectForKey:@"type"]intValue];
    self.title = [dic objectForKey:@"title"];
    self.brief = [dic objectForKey:@"brief"];
    self.forwho = [[dic objectForKey:@"forwho"]boolValue];
    self.creator = [dic objectForKey:@"creator"];
    self.source = [dic objectForKey:@"source"];
    self.course_id = [dic objectForKey:@"course_id"];
    self.createtime = [dic objectForKey:@"createtime"];
}
- (void)updateQuestionWithArr:(NSArray*)arr
{
    self.items = [NSMutableArray array];
    for (NSDictionary *dic in arr) {
        articleQuestion *a = [[articleQuestion alloc]init];
        [a updateWithDic:dic];
        [self.items addObject:a];
    }
}
@end
