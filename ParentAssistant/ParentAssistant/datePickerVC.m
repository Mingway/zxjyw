//
//  datePickerVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-16.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "datePickerVC.h"
#import "Util.h"

//判断是否为4寸屏
#define inch4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

@interface datePickerVC ()
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIImageView *datePickerImageView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, strong) NSDate *date;
- (IBAction)pressBtn:(UIButton *)sender;

@end

@implementation datePickerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        [self.datePicker setFrame:CGRectOffset(self.datePicker.frame, 0, 480-568)];
        [self.datePickerImageView setFrame:CGRectOffset(self.datePickerImageView.frame, 0, 480-568)];
        [self.cancelButton setFrame:CGRectOffset(self.cancelButton.frame, 0, 480-568)];
        [self.doneButton setFrame:CGRectOffset(self.doneButton.frame, 0, 480-568)];
    }
    // Do any additional setup after loading the view from its nib.
    self.datePicker.minimumDate = [[NSDate alloc]initWithTimeInterval:86400 sinceDate:[Util getCurrentDate]];
    //10天
    self.datePicker.maximumDate = [[NSDate alloc]initWithTimeInterval:864000 sinceDate:self.datePicker.minimumDate];
    [self.datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    self.date = self.datePicker.date;
    UIControl *remove = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height - self.datePicker.frame.size.height)];
    [remove addTarget:self action:@selector(remove) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:remove];
    [self.view sendSubviewToBack:remove];
}
-(void)remove
{
    [self.view removeFromSuperview];
}
- (void)dateChanged:(UIDatePicker*)sender
{
    self.date = self.datePicker.date;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //取消
            [self.delegate cancelSelectDate];
        }
            break;
        case 1:
        {
            //完成
            [self.delegate selectDoneWithDate:self.date];
        }
            break;
        default:
            break;
    }
}
@end
