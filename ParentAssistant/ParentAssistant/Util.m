//
//  Util.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-12.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "Util.h"
#import "CommonCrypto/CommonDigest.h"
#import "keychainUtils.h"
#import "user.h"

static NSString * const KEY_IN_KEYCHAIN = @"com.zxjyw.parentassistant.allinfo";
static NSString * const KEY_TOKEN = @"com.zxjyw.parentassistant.access_token";
@implementation Util
+(void)saveToken:(NSString *)access_token
{
    NSMutableDictionary *usernamepasswordKVPairs = [NSMutableDictionary dictionary];
    [usernamepasswordKVPairs setObject:access_token forKey:KEY_TOKEN];
    [keychainUtils save:KEY_IN_KEYCHAIN data:usernamepasswordKVPairs];
}
+(id)read
{
    NSMutableDictionary *usernamepasswordKVPair = (NSMutableDictionary *)[keychainUtils load:KEY_IN_KEYCHAIN];
    return usernamepasswordKVPair;
}

+(void)deleteUser
{
    [keychainUtils delete:KEY_IN_KEYCHAIN];
    [[user shareUser].bindStudentMulArr  removeAllObjects];
    [user shareUser].currentStudent = nil;
}

+(NSString *) md5: (NSString *) inPutText
{
    const char *cStr = [inPutText UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, strlen(cStr), result);
    return [[NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
             result[0], result[1], result[2], result[3],
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]
             ] lowercaseString];
}

+(void)showAlertViewWithTitle:(NSString*)title Message:(NSString*)messgae
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:messgae delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
}

+ (NSDate*)getCurrentDate
{
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date];
    NSDate *locale = [date  dateByAddingTimeInterval: interval];
    return locale;
}

+ (NSString*)getCurrentDay
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger day = [components day];
//    NSInteger month= [components month];
//    NSInteger year= [components year];
    if (day <= 9) {
         return [NSString stringWithFormat:@"0%li",(long)day];
    }else{
        return [NSString stringWithFormat:@"%li",(long)day];
    }
}

+(NSString*)convertDateToString:(NSDate*)date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    NSInteger day = [components day];
    NSInteger month= [components month];
    NSInteger year= [components year];
    return [NSString stringWithFormat:@"%i/%i/%i",year,month,day];
}

+ (BOOL)checkTel:(NSString *)str
{
    if ([str length] == 0) {
        [Util showAlertViewWithTitle:@"提示" Message:@"请输入手机号码！"];
        return NO;
    }
    //1[0-9]{10}
    //^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$
    //    NSString *regex = @"[0-9]{11}";
    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:str];
    if (!isMatch) {
        [Util showAlertViewWithTitle:@"提示" Message:@"请输入正确的手机号"];
        return NO;
    }
    return YES;
}

@end
