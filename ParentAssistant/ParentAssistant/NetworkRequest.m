//
//  NetworkRequest.m
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "NetworkRequest.h"

@interface NetworkRequest()

+ (NSOperationQueue *)getQueue;

@end

@implementation NetworkRequest
-(id)copyWithZone:(NSZone *)zone
{
    NetworkRequest *request = [[[self class]allocWithZone:zone]init];
    request.url = [self.url copyWithZone:zone];
    if (!self.parameters) {
        request.parameters = [self.parameters copyWithZone:zone];
    }else{
        request.parameters = [NSMutableDictionary dictionary];
    }
    return request;
}
+ (NSOperationQueue *)getQueue
{
    static NSOperationQueue *queue = nil;
    if (!queue) {
        queue = [[NSOperationQueue alloc] init];
    }
    return queue;
}

- (BOOL)sendAsyncRequestWithCompletionHandler:(void (^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler
{
    if (!completionHandler) {
        return NO;
    }
    //后台进程
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSURL *url = [[NSURL alloc] initWithString:self.url];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setTimeoutInterval:20.0f];
        [request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
        NSString *HTTPBodyString = [self HTTPBodyWithParameters:self.parameters];
        [request setHTTPBody:[HTTPBodyString dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPMethod:@"post"];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NetworkRequest getQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                   BOOL isSuccess = NO;
                                   NSDictionary *result = nil;
                                   NSString *errorMessage = nil;
                                   if (!connectionError ) {
                                       NSError *error = nil;
//                                       NSLog(@"data = %@",[[NSString alloc] initWithBytes:data.bytes length:[data length] encoding:NSUTF8StringEncoding]);
                                       result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                       if (error) {
                                           errorMessage = @"解析失败";
                                       } else {
                                           isSuccess = YES;
                                       }
                                   } else {
                                       errorMessage = @"联网失败";
                                   }
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       completionHandler(isSuccess, result, errorMessage);
                                   });
                               }];
    });
    return YES;
}
- (NSString *)HTTPBodyWithParameters:(NSDictionary *)parameters
{
    NSMutableArray *parametersArray = [[NSMutableArray alloc]init];
    for (NSString *key in [parameters allKeys]) {
        id value = [parameters objectForKey:key];
        [parametersArray addObject:[NSString stringWithFormat:@"%@=%@",key,value]];
    }
    return [parametersArray componentsJoinedByString:@"&"];
}

@end
