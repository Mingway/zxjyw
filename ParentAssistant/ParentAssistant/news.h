//
//  news.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//


//新闻
#import <Foundation/Foundation.h>

@interface news : NSObject
@property (nonatomic, readonly) NSString *news_title;
@property (nonatomic, readonly) NSString *news_source;
@property (nonatomic, readonly) NSString *news_updatetime;
@property (nonatomic, readonly) NSString *news_content;
@property (nonatomic, readonly) NSString *news_id;
@property (nonatomic, readonly) BOOL isSingleLine;
@property (nonatomic, assign) BOOL previewd;

- (void)updateWithDic:(NSDictionary *)dic;

@end
