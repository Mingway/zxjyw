//
//  repo.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-16.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

//错题难题
#import <Foundation/Foundation.h>

@interface repo : NSObject
@property (nonatomic, readonly)int itemtype;
@property (nonatomic, readonly)NSString *testitem_id;
@property (nonatomic, readonly)NSString *itemtype_str;
@property (nonatomic, readonly)NSString *stem;
@property (nonatomic, readonly)int error_time;
@property (nonatomic, readonly)NSString *status_date;

- (void)updateWithDic:(NSDictionary*)dic;

@end
