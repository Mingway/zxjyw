//
//  student.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-14.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

//绑定学生信息
#import <Foundation/Foundation.h>

@class course;
@interface student : NSObject
@property (nonatomic, readonly)NSString *studentId;
@property (nonatomic, readonly)NSString *name;
@property (nonatomic, readonly)NSString *account;//邮箱或手机号码
@property (nonatomic, readonly)NSString *gender;
@property (nonatomic, readonly)NSURL *avator;//头像
@property (nonatomic, readonly) NSString *school_name;
@property (nonatomic, readonly) NSString *grade_name;
@property (nonatomic, readonly) NSString *class_name;
@property (nonatomic, readonly) NSString *class_id;
@property (nonatomic, readonly) NSString *grade_id;
@property (nonatomic, readonly) NSString *term_year;
@property (nonatomic, readonly) NSString *school_id;
@property (nonatomic, strong) course *currentCourse;//某个学生当前选定科目
@property (nonatomic, readonly) NSString *term;
@property (nonatomic, strong) NSMutableArray *courseListMulArr;//科目列表
- (void)updateWithDic:(NSDictionary*)dic;
@end
