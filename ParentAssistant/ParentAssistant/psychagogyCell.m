//
//  psychagogyCell.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "psychagogyCell.h"
#import "article.h"

@interface psychagogyCell()
@property (weak, nonatomic) IBOutlet UIImageView *ForTheCrowdImageView;
@property (weak, nonatomic) IBOutlet UILabel *ForTheCrowdLabel;
@property (weak, nonatomic) IBOutlet UIButton *previewButton;

@end


@implementation psychagogyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)configure
{
    if (self.currentArticle.previewd) {
        [self.previewButton setImage:[UIImage imageNamed:@"educationNewsVC_previewd.png"] forState:UIControlStateNormal];
    }
    if (self.isSuitParent) {
        self.ForTheCrowdImageView.image = [UIImage imageNamed:@"orangeBlock.png"];
        self.ForTheCrowdLabel.text = @"家";
    }else{
        self.ForTheCrowdImageView.image = [UIImage imageNamed:@"greenBlock.png"];
        self.ForTheCrowdLabel.text = @"孩";
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
