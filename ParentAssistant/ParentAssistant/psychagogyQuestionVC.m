//
//  psychagogyQuestionVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-12.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "psychagogyQuestionVC.h"
#import "article.h"
#import "articleQuestion.h"
#import "task.h"
#import "reportVC.h"
#import "NetworkManager.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

#define OPTION_BUTTON_TAG_BASE 1000
#define OPTION_LABEL_TAG_BASE  2000

@interface psychagogyQuestionVC ()
@property (weak, nonatomic) IBOutlet UILabel *articleTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionCountLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *questionListScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *singleQuestionScrollView;
@property (nonatomic, assign)int currentIndex;//当前问题索引
@property (nonatomic, assign)int optionCount;//记录选项个数
@property (nonatomic, strong)NSMutableArray *answerOptionMulArr;//记录选项的数组
- (IBAction)pressBtn:(UIButton *)sender;
- (IBAction)pressBarButton:(UIBarButtonItem *)sender;
@property (nonatomic, strong) NetworkManager *network;
@end

@implementation psychagogyQuestionVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	// Do any additional setup after loading the view.
    self.network = [[NetworkManager alloc]init];
    self.currentIndex = 0;
    self.answerOptionMulArr = [NSMutableArray array];
    [self loadData];
}

- (void)loadData
{
    self.articleTitleLabel.text = self.currentArticle.title;
    int count = self.currentArticle.items.count;
    self.questionCountLabel.text = [NSString stringWithFormat:@"%i",count];
    //加载题目数的按钮，初始默认不可点，灰色背景
    int i = 0;
    int j = 0;
    //题目按钮的tag值以1为起始
    for (; i < count; i++) {
        UIButton *questionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [questionButton setFrame:CGRectMake(10+35*(i%8), 10 + 35*j, 29, 29)];
        [questionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [questionButton setTitle:[NSString stringWithFormat:@"%i",i+1] forState:UIControlStateNormal];
        [questionButton setBackgroundImage:[UIImage imageNamed:@"psychagogyQuestionVC_grayBlock.png"] forState:UIControlStateNormal];
        questionButton.tag = i+1;
        questionButton.enabled  =NO;
        [questionButton addTarget:self action:@selector(selectQuestion:) forControlEvents:UIControlEventTouchUpInside];
        [self.questionListScrollView addSubview:questionButton];
        if ((i+1)%8 ==0) {
            j++;
        }
    }
    [self.questionListScrollView setContentSize:CGSizeMake(self.questionListScrollView.contentSize.width, 10 +35 *j +30)];
    [self updateButtonStatus:self.currentIndex];
}
- (void)selectQuestion:(UIButton*)button
{
    self.currentIndex = button.tag -1;
    [self showContentWithQuestionIndex:self.currentIndex];
}
- (void)updateButtonStatus:(int)currentIndex
{
    //改变currentIndex前面按钮的状态
    for (int i = 0; i <= currentIndex; i++) {
        UIButton *tempButton = (UIButton*)[self.questionListScrollView viewWithTag:i+1];
        [tempButton setBackgroundImage:[UIImage imageNamed:@"psychagogyQuestionVC_greenBlock.png"] forState:UIControlStateNormal];
        tempButton.enabled = YES;
    }
    //显示第currentIndex题目的内容
    [self showContentWithQuestionIndex:currentIndex];
}
- (void)showContentWithQuestionIndex:(int)currentIndex
{
    //需要先清空ScrollView上的内容
    for (id key in self.singleQuestionScrollView.subviews) {
        [key removeFromSuperview];
    }
    articleQuestion * currentQuestion = [self.currentArticle.items objectAtIndex:currentIndex];
    UILabel *questionLabel = [[UILabel alloc]init];
    questionLabel.numberOfLines = 0;
    questionLabel.text = currentQuestion.question;
    CGSize size = CGSizeMake(265, 2000);
    CGSize questionLabelSize =  [questionLabel.text sizeWithFont:questionLabel.font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    [questionLabel setFrame:CGRectMake(10, 10, questionLabelSize.width, questionLabelSize.height)];
    [self.singleQuestionScrollView addSubview:questionLabel];
    float height = 0;
    self.optionCount = currentQuestion.options.count;
    int selectedIndex = -1;
    NSLog(@"%@",self.answerOptionMulArr);
    if (self.answerOptionMulArr.count > self.currentIndex) {
        if ([self.answerOptionMulArr objectAtIndex:self.currentIndex]!= nil) {
            selectedIndex = [[self.answerOptionMulArr objectAtIndex:self.currentIndex] intValue];
        }
    }
    //选项按钮的tag值以0为起始
    for (int i = 0; i < self.optionCount; i++) {
        UIButton *optionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if (i == 0) {
            [optionButton setFrame:CGRectMake(10, CGRectGetMaxY(questionLabel.frame) + 20 + 35 * i, 15, 15)];
        }else{
            UILabel *optionLabel = (UILabel*)[self.singleQuestionScrollView viewWithTag:i + OPTION_LABEL_TAG_BASE - 1];
            [optionButton setFrame:CGRectMake(10, CGRectGetMaxY(optionLabel.frame) +10 , 15, 15)];
        }
        [optionButton setImage:[UIImage imageNamed:@"psychagogyQuestionVC_grayOption.png"] forState:UIControlStateNormal];
        [optionButton setImage:[UIImage imageNamed:@"psychagogyQuestionVC_greenOption.png"] forState:UIControlStateSelected];
        optionButton.tag = i+OPTION_BUTTON_TAG_BASE;
        [optionButton addTarget:self action:@selector(selectOption:) forControlEvents:UIControlEventTouchUpInside];
        [self.singleQuestionScrollView addSubview:optionButton];
        
        if (i+OPTION_BUTTON_TAG_BASE == selectedIndex) {
            [optionButton setSelected:YES];
        }
        
        UILabel *optionLabel = [[UILabel alloc]init];
        optionLabel.tag = i + OPTION_LABEL_TAG_BASE;
        optionLabel.numberOfLines = 0;
        optionLabel.text = [currentQuestion.options objectAtIndex:i];
        CGSize size = CGSizeMake(230, 2000);
        CGSize optionLabelSize =  [optionLabel.text sizeWithFont:optionLabel.font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
        [optionLabel setFrame:CGRectMake(35, optionButton.frame.origin.y, optionLabelSize.width, optionLabelSize.height)];
        [self.singleQuestionScrollView addSubview:optionLabel];
        height = CGRectGetMaxY(optionLabel.frame);
        [self.singleQuestionScrollView setContentSize:CGSizeMake(self.singleQuestionScrollView.contentSize.width, height)];
    }
}
- (void)selectOption:(UIButton*)button
{
    for (int  i = 0; i < self.optionCount; i++) {
        UIButton *optionButton = (UIButton *)[self.singleQuestionScrollView viewWithTag:i+OPTION_BUTTON_TAG_BASE];
        [optionButton setSelected:NO];
    }
    [button setSelected:!button.isSelected];
    if (self.answerOptionMulArr.count >= self.currentIndex) {
        [self.answerOptionMulArr addObject:[NSString stringWithFormat:@"%i",button.tag]];
    }else{
        [self.answerOptionMulArr replaceObjectAtIndex:self.currentIndex withObject:[NSString stringWithFormat:@"%i",button.tag]];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            if (self.currentIndex < self.currentArticle.items.count -1&& self.answerOptionMulArr.count >self.currentIndex) {
                self.currentIndex ++;
                [self updateButtonStatus:self.currentIndex];
            }else if(self.answerOptionMulArr.count  <= self.currentIndex){
                NSLog(@"这题没答!");
            }else{
                NSLog(@"这是最后一题!");
            }
        }
            break;
        default:
            break;
    }
}

- (IBAction)pressBarButton:(UIBarButtonItem *)sender {
    switch (sender.tag) {
        case 0:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}
@end
