//
//  student.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-14.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "student.h"
#import "define.h"

@interface student ()
@property (nonatomic, strong)NSString *studentId;
@property (nonatomic, strong)NSString *name;
@property (nonatomic, strong)NSString *account;
@property (nonatomic, strong)NSURL *avator;//头像
@property (nonatomic, strong)NSString *gender;
@property (nonatomic, strong) NSString *school_name;
@property (nonatomic, strong) NSString *grade_name;
@property (nonatomic, strong) NSString *class_name;
@property (nonatomic, strong) NSString *class_id;
@property (nonatomic, strong) NSString *grade_id;
@property (nonatomic, strong) NSString *term_year;
@property (nonatomic, strong) NSString *school_id;
@property (nonatomic, strong) NSString *term;
@end

@implementation student
- (void)updateWithDic:(NSDictionary*)dic
{
    if ([dic objectForKey:@"id"] != [NSNull null]) {
        self.studentId = [dic objectForKey:@"id"];
    }
    if ([dic objectForKey:@"name"] != [NSNull null]) {
        self.name = [dic objectForKey:@"name"];
    }
    if ([dic objectForKey:@"account"] != [NSNull null]) {
        self.account = [dic objectForKey:@"account"];
    }
    if ([dic objectForKey:@"avator"] != [NSNull null]) {
        self.avator = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ResourceBaseUrl,[dic objectForKey:@"avator"]]];
    }
    if ([dic objectForKey:@"gender"] != [NSNull null]) {
        self.gender = [dic objectForKey:@"gender"];
    }
    if ([dic objectForKey:@"school_name"] != [NSNull null]) {
        self.school_name = [dic objectForKey:@"school_name"];
    }
    if ([dic objectForKey:@"grade_name"] != [NSNull null]) {
        self.grade_name = [dic objectForKey:@"grade_name"];
    }
    if ( [dic objectForKey:@"class_name"] != [NSNull null]) {
        self.class_name = [dic objectForKey:@"class_name"];
    }
    if ([dic objectForKey:@"class_id"] != [NSNull null]) {
        self.class_id = [dic objectForKey:@"class_id"];
    }
    if ( [dic objectForKey:@"grade_id"] != [NSNull null]) {
        self.grade_id = [dic objectForKey:@"grade_id"];
    }
    if ( [dic objectForKey:@"term_year"] != [NSNull null]) {
        self.term_year = [dic objectForKey:@"term_year"];
    }
    if ( [dic objectForKey:@"school_id"] != [NSNull null]) {
        self.school_id = [dic objectForKey:@"school_id"];
    }
    if ( [dic objectForKey:@"term"] != [NSNull null]) {
        self.term = [dic objectForKey:@"term"];
    }
    self.courseListMulArr = [NSMutableArray array];
}
@end
