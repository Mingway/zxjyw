//
//  kpoint_stat.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface kpoint_stat : NSObject
@property (nonatomic, readonly) NSString *kpoint_id;
@property (nonatomic, readonly) NSString *kpoint_str;
@property (nonatomic, readonly) int count;
@property (nonatomic, readonly) int error_score;

- (void)updateWithDic:(NSDictionary*)dic;

@end
