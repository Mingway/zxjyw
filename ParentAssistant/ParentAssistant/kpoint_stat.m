//
//  kpoint_stat.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "kpoint_stat.h"

@interface kpoint_stat ()
@property (nonatomic, strong) NSString *kpoint_id;
@property (nonatomic, strong) NSString *kpoint_str;
@property (nonatomic, assign) int count;
@property (nonatomic, assign) int error_score;
@end

@implementation kpoint_stat
- (void)updateWithDic:(NSDictionary*)dic;
{
    self.kpoint_id = [dic objectForKey:@"kpoint_id"];
    self.kpoint_str = [dic objectForKey:@"kpoint_str"];
    self.count = [[dic objectForKey:@"count"]intValue];
    self.error_score = [[dic objectForKey:@"error_score"]intValue];
}
@end
