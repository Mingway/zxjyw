//
//  bindChildrenVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "bindChildrenVC.h"
#import "NetworkManager.h"
#import "waitView.h"
#import "Util.h"
#import "task.h"
#import "reportVC.h"
#import "bindChildrenDetailVC.h"
#import "student.h"
#import "user.h"
#import "course.h"
#import "define.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface bindChildrenVC ()<UITextFieldDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *sstudentPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *studentAccountTextField;
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong) user *userModel;
- (IBAction)pressBtn:(UIButton *)sender;

@end

@implementation bindChildrenVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	// Do any additional setup after loading the view.
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    self.nameTextField.delegate = self;
    self.studentAccountTextField.delegate = self;
    self.sstudentPasswordTextField.delegate = self;
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.frame];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
    
    UIButton* backBtn;
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}
- (void)resign
{
    [self.nameTextField resignFirstResponder];
    [self.sstudentPasswordTextField resignFirstResponder];
    [self.studentAccountTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.nameTextField.text = @"";
    self.sstudentPasswordTextField.text  = @"";
    self.studentAccountTextField.text = @"";
    if (![self.userModel.name isEqualToString:@""]) {
        self.nameTextField.text = self.userModel.name;
        self.nameTextField.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            if ([self.nameTextField.text isEqualToString:@""]) {
                [Util showAlertViewWithTitle:@"提示" Message:@"请输入您的真实姓名"];
            }else if([self.sstudentPasswordTextField.text isEqualToString:@""]){
                [Util showAlertViewWithTitle:@"提示" Message:@"请输入您需要绑定的子女的邮箱"];
            }else if([self.studentAccountTextField.text isEqualToString:@""]){
                [Util showAlertViewWithTitle:@"提示" Message:@"请输入您需要绑定的子女的账号密码"];
            }else{
                [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createBindingRequestWithName:self.nameTextField.text StudentPassword:self.sstudentPasswordTextField.text StudentAccount:self.studentAccountTextField.text] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView]stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if([[data objectForKey:@"error_code"]intValue] == 0 ){
                            student *s = [[student alloc]init];
                            [s updateWithDic:[data objectForKey:@"student"]];
                            [self.userModel.bindStudentMulArr addObject:s];
                            [self getProgress:s];
                        }else{
                            [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                        }
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                    }
                }];
            }

        }
            break;
            
        default:
            break;
    }
}
- (void)getProgress:(student*)tempStudent
{
        [self.network sendRequest:[self.network createGetProgressRequestWithStudentAccount:tempStudent.account] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            [[waitView getWaitView]stopAnimation];
            [[waitView getWaitView]removeFromSuperview];
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue]  == 0) {
                    NSArray *arr = [data objectForKey:@"data"];
                    NSMutableArray *tempArr = [NSMutableArray array];
                    for (NSDictionary *key in arr) {
                        course *c = [[course alloc]init];
                        [c updateWithDic:key];
                        [tempArr addObject:c];
                    }
                    tempStudent.courseListMulArr = [NSMutableArray arrayWithArray:tempArr];
                    if (tempStudent.courseListMulArr.count > 0) {
                        tempStudent.currentCourse = [tempStudent.courseListMulArr objectAtIndex:0];
                    }
                    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    bindChildrenDetailVC*next = [board instantiateViewControllerWithIdentifier:@"bindChildrenDetailVC"];
                    next.currentStudent = tempStudent;
                    [self.navigationController pushViewController:next animated:YES];
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                }
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
            }
        }];
}

#pragma  mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
