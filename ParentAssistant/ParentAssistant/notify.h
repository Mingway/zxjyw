//
//  notify.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-20.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface notify : NSObject
@property (nonatomic, readonly) NSString *notifyId;
@property (nonatomic, readonly) int type;
@property (nonatomic, readonly) NSString *task_id;
@property (nonatomic, readonly) NSString *ntime;//通知时间
@property (nonatomic, readonly) NSString *rtime;//设为已读的时间
@property (nonatomic, readonly) int status;
@property (nonatomic, readonly) NSString *student_account;
@property (nonatomic, readonly) NSString *student_name;

- (void)updateWithDic:(NSDictionary*)dic;

@end
