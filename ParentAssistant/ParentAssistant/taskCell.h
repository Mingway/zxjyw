//
//  taskCell.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class task;
@protocol taskCellDelegate <NSObject>
- (void)checkReportByTaskId:(task*)task;

@end

@interface taskCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *createDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *paperNameLabel;
@property (nonatomic, strong) task *currentTask;
@property (nonatomic)id <taskCellDelegate> delegate;
@property (nonatomic, assign)int status;

- (IBAction)pressBtn:(UIButton *)sender;

- (void)configure;

@end
