//
//  psychagogyCell.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class article;
@interface psychagogyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *articleTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *articleBriefLabel;
@property (weak, nonatomic) IBOutlet UILabel *articleSourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *articleDateLabel;
@property (nonatomic, strong) article *currentArticle;
@property (nonatomic, assign) BOOL isSuitParent;

- (void)configure;

@end
