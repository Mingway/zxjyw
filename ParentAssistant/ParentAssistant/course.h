//
//  course.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//


//科目进度信息
#import <Foundation/Foundation.h>

@interface course : NSObject
@property (nonatomic, readonly) NSString *book_id;
@property (nonatomic, readonly) NSString *book_name;
@property (nonatomic, readonly) NSString *course_id;
@property (nonatomic, readonly) NSString *course_name;
@property (nonatomic, readonly) NSString *current_chapter_id;
@property (nonatomic, readonly) NSString *current_chapter_name;
@property (nonatomic, readonly) NSString *update_chapter_time;
@property (nonatomic, readonly) int status;
@property (nonatomic, readonly) NSString *courseclass_id;

- (void)updateWithDic:(NSDictionary *)dic;

@end
