//
//  define.h
//  ParentAssistant
//
//  Created by Mingway on 13-12-23.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#ifndef ParentAssistant_define_h
#define ParentAssistant_define_h

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

//判断系统版本是否是iOS7
#define IOS7_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending )

//判断是否为4寸屏
#define inch4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define ResourceBaseUrl   @"http://demodev.stcrowd.com/res/get_res?res_id="

#define LOGOUT_TIPS @"退出则不在接受推送，是否确定退出？"

#endif
