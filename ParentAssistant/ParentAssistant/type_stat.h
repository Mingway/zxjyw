//
//  type_stat.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface type_stat : NSObject
@property (nonatomic, readonly) int itemtype;
@property (nonatomic, readonly) NSString *itemtypestr;
@property (nonatomic, readonly) int count;
@property (nonatomic, readonly) int correct;
@property (nonatomic, readonly) int error;

- (void)updateWithDic:(NSDictionary*)dic;

@end
