//
//  difficulty.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "difficulty.h"

@interface difficulty ()
@property (nonatomic, assign) int difficulty;
@property (nonatomic, assign) int testitem_count;
@property (nonatomic, assign) int testitem_error;
@end

@implementation difficulty
- (void)updateWithDic:(NSDictionary*)dic;
{
    self.difficulty = [[dic objectForKey:@"difficulty"]intValue];
    self.testitem_count = [[dic objectForKey:@"testitem_count"]intValue];
    self.testitem_error = [[dic objectForKey:@"testitem_error"]intValue];
}
@end
