//
//  courseStatusVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-13.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "courseStatusVC.h"
#import "course.h"
#import "student.h"
#import "user.h"
#import "Util.h"
#import "task.h"
#import "reportVC.h"
#import "paperAssignmentVC.h"
#import "NetworkManager.h"
#import "stat_kpoint.h"
#import "kpoint_statCell.h"
#import "UIButton+Extensions.h"
#import "waitView.h"
#import "define.h"
#import  "chartWebViewVC.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface courseStatusVC ()<UIWebViewDelegate,UITableViewDataSource,UITableViewDelegate,kpoint_statCellDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseNameLabel;
@property (nonatomic, strong)user *userModel;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIWebView *chartWebView;
@property (weak, nonatomic) IBOutlet UITableView *statStudentLsitTableView;
@property (nonatomic, strong) NetworkManager *network;
@property (weak, nonatomic) IBOutlet UITableView *studentListTableView;
@property (weak, nonatomic) IBOutlet UITableView *courseListTableView;
@property (weak, nonatomic) IBOutlet UIButton *studentDropListButton;
@property (weak, nonatomic) IBOutlet UIButton *courseDropListButton;
@property (nonatomic, strong) NSMutableArray *statKpointMulArr;//学期失分点可变数组
@end

@implementation courseStatusVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
    self.statKpointMulArr = [NSMutableArray array];
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    self.studentNameLabel.text = self.userModel.currentStudent.name;
    self.courseNameLabel.text = self.userModel.currentStudent.currentCourse.course_name;
    self.progressLabel.text = [NSString stringWithFormat:@"%@  更新时间：%@",self.userModel.currentStudent.currentCourse.current_chapter_name,self.userModel.currentStudent.currentCourse.update_chapter_time];
    self.chartWebView.delegate = self;
    [self loadChartWebViewHtml];
    self.statStudentLsitTableView.delegate = self;
    self.statStudentLsitTableView.dataSource = self;
    self.studentListTableView.delegate = self;
    self.studentListTableView.dataSource = self;
    self.courseListTableView.delegate = self;
    self.courseListTableView.dataSource = self;
    [self getStatStudent];
    [self setButtonRect];
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.frame];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    self.chartWebView.scrollView.showsVerticalScrollIndicator = NO;
    self.chartWebView.scrollView.showsHorizontalScrollIndicator = NO;
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
    
    UIButton* backBtn;
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}
-(void)loadChartWebViewHtml
{
    //
    [self.chartWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://demo703.tongyouxuetang.com/student-report.html#/?token=%@&course_id=%@&saccount=%@&weekno=%@",self.userModel.access_token,self.userModel.currentStudent.currentCourse.course_id,self.userModel.currentStudent.account,self.weekno]]]];
}
- (void)resign
{
    if (!self.studentListTableView.hidden) {
        self.studentListTableView.hidden = YES;
        [self rotate:self.studentDropListButton];
    }
    if (!self.courseListTableView.hidden) {
        self.courseListTableView.hidden = YES;
        [self rotate:self.courseDropListButton];
    }
}
-(void)setButtonRect
{
    [self.studentDropListButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -30, -5, -5)];
    [self.courseDropListButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -30, -5, -5)];
}
-(void)getStatStudent
{
    [self.network sendRequest:[self.network createGetStatStudentRequestWithCourseId:self.userModel.currentStudent.currentCourse.course_id StudentAccount:self.userModel.currentStudent.account] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                NSArray *tempArr = [data objectForKey:@"kpoints"];
                for (NSDictionary *dic in tempArr) {
                    stat_kpoint *s = [[stat_kpoint alloc]init];
                    [s updateWithDic:dic];
                    [self.statKpointMulArr addObject:s];
                }
                [self.statStudentLsitTableView reloadData];
            }
        }
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)rotate:(UIButton*)sender
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    CGAffineTransform t = CGAffineTransformRotate(sender.transform, -M_PI);
    sender.transform = t;
    [UIView commitAnimations];
}
- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            paperAssignmentVC *next = [board instantiateViewControllerWithIdentifier:@"paperAssignmentVC"];
            [self.navigationController pushViewController:next animated:YES];
        }
            break;
        case 1:
        {
            [self rotate:sender];
            if (!self.courseListTableView.hidden) {
                self.courseListTableView.hidden = YES;
                [self rotate:self.courseDropListButton];
            }
            if (self.studentListTableView.hidden) {
                self.studentListTableView.hidden = NO;
            }else{
                self.studentListTableView.hidden = YES;
            }
        }
            break;
        case 2:
        {
            [self rotate:sender];
            if (!self.studentListTableView.hidden) {
                self.studentListTableView.hidden = YES;
                [self rotate:self.studentDropListButton];
            }
            if (self.courseListTableView.hidden) {
                self.courseListTableView.hidden = NO;
            }else{
                self.courseListTableView.hidden = YES;
            }
        }
            break;
        case 3:
        {
            //横屏显示
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            chartWebViewVC *chart = [storyboard instantiateViewControllerWithIdentifier:@"chartWebViewVC"];
            chart.weekno = self.weekno;
            [self.navigationController pushViewController:chart animated:YES];
        }
            break;
        default:
            break;
    }
}

#pragma mark UIWebViewDlelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if(navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSLog(@"clicked");
//        if(overrideLinksSwitch.on == TRUE)
//        {
//            
//            return YES;
//        }
//        else
//        {
//            return YES;
//        }
    }
    return YES;
}
#pragma mark UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 0) {
        return self.statKpointMulArr.count;
    }else if(tableView.tag == 1){
        return self.userModel.bindStudentMulArr.count;
    }else{
        return self.userModel.currentStudent.courseListMulArr.count;
    }
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0) {
        static NSString *stat_kpointCellIdentifier = @"kpoint_statCell";
        kpoint_statCell *cell = [tableView dequeueReusableCellWithIdentifier:stat_kpointCellIdentifier];
        if (!cell) {
            NSArray *kpointNib = [[NSBundle mainBundle]loadNibNamed:stat_kpointCellIdentifier owner:Nil options:nil];
            if ([kpointNib count]>0) {
                cell = [kpointNib objectAtIndex:0];
            }
            cell.delegate = self;
        }
        stat_kpoint *tempStat = [self.statKpointMulArr objectAtIndex:indexPath.row];
        cell.currentStat_kpoint = tempStat;
        cell.indexLabel.text = [NSString stringWithFormat:@"%i.",(int)indexPath.row+1];
        cell.kpointStatLabel.text = tempStat.kpoint_str;
        return cell;
    }else if (tableView.tag == 1){
        static NSString *cell_identifier = @"studentCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
            cell.textLabel.font = [UIFont systemFontOfSize:10.f];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [[self.userModel.bindStudentMulArr objectAtIndex:indexPath.row] name];
        return cell;
    }else{
        static NSString *cell_identifier = @"studentCourseCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
            cell.textLabel.font = [UIFont systemFontOfSize:10.f];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [[self.userModel.currentStudent.courseListMulArr objectAtIndex:indexPath.row] course_name];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0) {
        
    }else if (tableView.tag == 1){
        [self rotate:self.studentDropListButton];
        self.studentListTableView.hidden = YES;
        student *tempStudent = [self.userModel.bindStudentMulArr objectAtIndex:indexPath.row];
        self.studentNameLabel.text = [tempStudent name];
        self.userModel.currentStudent = tempStudent;
        self.courseNameLabel.text = self.userModel.currentStudent.currentCourse.course_name;
        if (tempStudent.courseListMulArr.count > 0) {
            self.userModel.currentStudent.currentCourse = [tempStudent.courseListMulArr objectAtIndex:0];
        }
          self.progressLabel.text = [NSString stringWithFormat:@"%@  更新时间：%@",self.userModel.currentStudent.currentCourse.current_chapter_name,self.userModel.currentStudent.currentCourse.update_chapter_time];
        [self.courseListTableView reloadData];
        [self.statKpointMulArr removeAllObjects];
        if (self.userModel.currentStudent.courseListMulArr.count >0) {
            [self getStatStudent];
        }else{
            [self.statStudentLsitTableView reloadData];
        }
        [self loadChartWebViewHtml];
    }else{
        [self rotate:self.courseDropListButton];
        self.courseListTableView.hidden = YES;
        course *tempCourse = [self.userModel.currentStudent.courseListMulArr objectAtIndex:indexPath.row];
        self.userModel.currentStudent.currentCourse = tempCourse;
        self.courseNameLabel.text = [tempCourse course_name];
        [self.statKpointMulArr removeAllObjects];
          self.progressLabel.text = [NSString stringWithFormat:@"%@  更新时间：%@",self.userModel.currentStudent.currentCourse.current_chapter_name,self.userModel.currentStudent.currentCourse.update_chapter_time];
        [self loadChartWebViewHtml];
        [self getStatStudent];
    }
}

#pragma mark kpoint_statCellDelegate
- (void)pressStrengthButtonWithStat_kpoint:(stat_kpoint *)currentStat_kpoint
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    paperAssignmentVC *next = [board instantiateViewControllerWithIdentifier:@"paperAssignmentVC"];
    next.currentStat_kpoint = currentStat_kpoint;
    
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
