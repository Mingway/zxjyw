//
//  weight.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "weight.h"

@interface weight ()
@property (nonatomic, assign) int weight;
@property (nonatomic, assign) int testitem_count;
@property (nonatomic, assign) int testitem_error;
@end

@implementation weight
- (void)updateWithDic:(NSDictionary*)dic;
{
    self.weight = [[dic objectForKey:@"weight"]intValue];
    self.testitem_count = [[dic objectForKey:@"testitem_count"]intValue];
    self.testitem_error = [[dic objectForKey:@"testitem_error"]intValue];
}
@end
