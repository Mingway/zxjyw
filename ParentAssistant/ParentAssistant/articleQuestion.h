//
//  articleQuestion.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//


//心理测试题目
#import <Foundation/Foundation.h>

@interface articleQuestion : NSObject
@property (nonatomic, readonly) NSString *testitem_id;
@property (nonatomic, readonly) int idx;
@property (nonatomic, readonly) NSString *question;
@property (nonatomic, readonly) int complex;
@property (nonatomic, readonly) float score;
@property (nonatomic, readonly) int repo_err;
@property (nonatomic, readonly) int repo_dif;
@property (nonatomic, readonly) int repo_good;
@property (nonatomic, readonly) NSMutableArray *options;
- (void)updateWithDic:(NSDictionary*)dic;

@end
