//
//  datePickerVC.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-16.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol datePickerDelegate <NSObject>
- (void)cancelSelectDate;
- (void)selectDoneWithDate:(NSDate*)date;
@end

@interface datePickerVC : UIViewController
@property (nonatomic)id<datePickerDelegate> delegate;

@end
