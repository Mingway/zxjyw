//
//  repo.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-16.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "repo.h"

@interface repo ()
@property (nonatomic, assign)int itemtype;
@property (nonatomic, strong)NSString *testitem_id;
@property (nonatomic, strong)NSString *itemtype_str;
@property (nonatomic, strong)NSString *stem;
@property (nonatomic, assign)int error_time;
@property (nonatomic, strong)NSString *status_date;
@end

@implementation repo
- (void)updateWithDic:(NSDictionary*)dic
{
    self.itemtype = [[dic objectForKey:@"itemtype"]intValue];
    self.testitem_id = [dic objectForKey:@"testitem_id"];
    self.itemtype_str = [dic objectForKey:@"itemtype_str"];
    NSString* new_questionContent = [NSString stringWithFormat:@"<html> <head><title>问题内容</title><script type = \"text/javascript\"src = \"MathJax.js\"></script> </head><body>%@</body>  </html>",[dic objectForKey:@"stem"]];
    new_questionContent = [self convertImgUrlWithString:new_questionContent];
    if([self.itemtype_str isEqualToString:@"单选题"] || [self.itemtype_str isEqualToString:@"多选题"]){
        new_questionContent = [new_questionContent stringByReplacingOccurrencesOfString:@"\\a" withString:@"<br/>A."];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>A." toStr2:@"<br/>B." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>B." toStr2:@"<br/>C." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>C." toStr2:@"<br/>D." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>D." toStr2:@"<br/>E." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>E." toStr2:@"<br/>F." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>F." toStr2:@"<br/>G." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>G." toStr2:@"<br/>H." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>H." toStr2:@"<br/>I." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>I." toStr2:@"<br/>J." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>J." toStr2:@"<br/>K." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>K." toStr2:@"<br/>L." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>L." toStr2:@"<br/>M." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>M." toStr2:@"<br/>N." htmlString:new_questionContent];
    }else{
        new_questionContent = [new_questionContent stringByReplacingOccurrencesOfString:@"\\a" withString:@"_____"];
    }
    self.stem = new_questionContent;
    self.error_time = [[dic objectForKey:@"error_time"]intValue];
    self.status_date = [dic objectForKey:@"status_date"];
}
-(NSString*)updateOptionsWithStr1:(NSString*)str1 toStr2:(NSString*)str2 htmlString:(NSString*)html_str;
{
    NSRange range = [html_str rangeOfString:str1];
    html_str = [html_str stringByReplacingOccurrencesOfString:str1 withString:str2 options:NSBackwardsSearch range:NSMakeRange(range.location + range.length, html_str.length-range.location - range.length)];
    return html_str;
}
- (NSString*)convertImgUrlWithString:(NSString*)string
{
    NSScanner *scanner = [NSScanner scannerWithString:string];
    while ([scanner scanUpToString:@"<img" intoString:NULL]) {
        NSString *ImgTagContents;
        if ([scanner scanUpToString:@">" intoString:&ImgTagContents]) {
            // Do something with tag contents
            NSScanner *scanner1 = [NSScanner scannerWithString:ImgTagContents];
            while ([scanner1 scanUpToString:@"src=\"" intoString:NULL]) {
                NSString *srcTagContents;
                if ([scanner1 scanUpToString:@"/" intoString:&srcTagContents]) {
                    string  = [string stringByReplacingOccurrencesOfString:srcTagContents withString: [NSString stringWithFormat:@"src=\"http://demodev.stcrowd.com%@",[srcTagContents substringFromIndex:5]]];
                }
            }
        }
        else {
            // Do nothing? I think this would be hit on the last time through the loop
        }
    }
    return string;
}
@end
