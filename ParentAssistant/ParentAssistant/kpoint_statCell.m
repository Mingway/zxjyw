
//
//  kpoint_statCell.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "kpoint_statCell.h"

@implementation kpoint_statCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            if (self.currentKpoint_stat) {
                [self.delegate pressStrengthButtonWithKpoint:self.currentKpoint_stat andNotify:self.currentNotify];
            }else{
                [self.delegate pressStrengthButtonWithStat_kpoint:self.currentStat_kpoint];
            }
        }
            break;
            
        default:
            break;
    }
}
@end
