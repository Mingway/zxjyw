//
//  course.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "course.h"

@interface course ()
@property (nonatomic, strong) NSString *book_id;
@property (nonatomic, strong) NSString *book_name;
@property (nonatomic, strong) NSString *course_id;
@property (nonatomic, strong) NSString *course_name;
@property (nonatomic, strong) NSString *current_chapter_id;
@property (nonatomic, strong) NSString *current_chapter_name;
@property (nonatomic, strong) NSString *update_chapter_time;
@property (nonatomic, assign) int status;
@property (nonatomic, strong) NSString *courseclass_id;
@end

@implementation course
- (void)updateWithDic:(NSDictionary *)dic
{
    self.book_id = [dic objectForKey:@"book_id"];
    self.book_name = [dic objectForKey:@"book_name"];
    self.course_id = [dic objectForKey:@"course_id"];
    self.course_name = [dic objectForKey:@"course_name"];
    self.current_chapter_id = [dic objectForKey:@"current_chapter_id"];
    self.current_chapter_name = [dic objectForKey:@"current_chapter_name"];
    self.update_chapter_time = [dic objectForKey:@"update_chapter_time"];
    self.status = [[dic objectForKey:@"status"]intValue];
    self.courseclass_id = [dic objectForKey:@"courseclass_id"];
}
@end
