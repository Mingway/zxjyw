//
//  courseCell.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIUnderlinedButton.h"

@class course;
@protocol courseCellDelegate <NSObject>
- (void)pressCourseButtonWithCourse:(course*)currentCourse;
- (void)pressStrengthenButtonWithCourse:(course*)currentCourse;
@end

@interface courseCell : UITableViewCell
@property (nonatomic, strong) course *currentCourse;
@property (weak, nonatomic) IBOutlet UIUnderlinedButton *courseNameButton;
@property (weak, nonatomic) IBOutlet UILabel *currentChapterNameLabel;
@property (nonatomic)id<courseCellDelegate> delegate;
- (IBAction)pressBtn:(UIButton *)sender;


@end
