//
//  reportVC.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class task;
@class notify;
@interface reportVC : UIViewController
@property (nonatomic, strong) task *currentTask;
@property (nonatomic, strong) notify *currentNotify;
@end
