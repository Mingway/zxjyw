//
//  psychagogyPaperAssignmentVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-12.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "psychagogyPaperAssignmentVC.h"
#import "article.h"
#import "Util.h"
#import "datePickerVC.h"
#import "waitView.h"
#import "NetworkManager.h"
#import "user.h"
#import "student.h"
#import "course.h"
#import "Util.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface psychagogyPaperAssignmentVC ()<datePickerDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *articleTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *articleBriefTextView;
@property (weak, nonatomic) IBOutlet UILabel *assignedChildrenLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentDayLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectDateButton;
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong) NSString *selectDateStr;
@property (nonatomic, strong) datePickerVC *datePicker;
@property (nonatomic, strong) user *userModel;
@property (nonatomic, strong) student *currentStudent;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *studentListTableView;
- (IBAction)pressBarButton:(UIBarButtonItem *)sender;
@end

@implementation psychagogyPaperAssignmentVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	// Do any additional setup after loading the view.
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    self.studentListTableView.delegate = self;
    self.studentListTableView.dataSource = self;
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.frame];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
}
- (void)resign
{
    self.studentListTableView.hidden = YES;
}
- (datePickerVC *)datePicker
{
    if (_datePicker == Nil) {
        _datePicker = [[datePickerVC alloc]init];
        _datePicker.delegate = self;
    }
    return _datePicker;
}

- (void)loadData
{
    self.articleTitleLabel.text = self.currentArticle.title;
    self.articleBriefTextView.text = self.currentArticle.brief;
    self.currentDayLabel.text = [Util getCurrentDay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //显示绑定子女列表
            if (self.studentListTableView.hidden) {
                self.studentListTableView.hidden = NO;
            }else{
                self.studentListTableView.hidden = YES;
            }
        }
            break;
        case 1:
        {
            //显示分配日期picker
             [[UIApplication sharedApplication].keyWindow addSubview:self.datePicker.view];
        }
            break;
        case 2:
        {
            //点击确认分配按钮
            if (!self.currentStudent) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"确认分配该任务吗？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                alert.tag = 1;
                [alert show];
                alert = nil;
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:@"请选择需要分配的子女"];
            }
        }
            break;
        default:
            break;
    }
}

- (IBAction)pressBarButton:(UIBarButtonItem *)sender {
    switch (sender.tag) {
        case 0:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark datePickerDelegate

- (void)cancelSelectDate
{
    [self.datePicker.view removeFromSuperview];
    self.datePicker = nil;
}
- (void)selectDoneWithDate:(NSDate *)date
{
    [self.selectDateButton setTitle:[Util convertDateToString:date] forState:UIControlStateNormal];
    self.selectDateStr = [date description];
    [self.datePicker.view removeFromSuperview];
    self.datePicker = nil;
}

#pragma mark UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.userModel.bindStudentMulArr.count;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cell_identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
    }
    cell.textLabel.text = [[self.userModel.bindStudentMulArr objectAtIndex:indexPath.row] name];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.studentListTableView.hidden = YES;
    self.currentStudent = [self.userModel.bindStudentMulArr objectAtIndex:indexPath.row];
    self.assignedChildrenLabel.text = [ [self.userModel.bindStudentMulArr objectAtIndex:indexPath.row] name];
}
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        switch (buttonIndex) {
            case 0:
            {
                
            }
                break;
            case 1:
            {
                [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createTaskRequestWithTaskType:0 Title:[NSString stringWithFormat:@"分配给%@的任务",self.userModel.currentStudent.name]  TestPaperId:self.currentArticle.testpaper_id Students:[NSArray arrayWithObjects:self.userModel.currentStudent.studentId, nil] DeadLine:self.selectDateStr] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                            [[waitView getWaitView]stopAnimation];
                            [[waitView getWaitView]removeFromSuperview];
                            if (isSuccess) {
                                if ([[data objectForKey:@"error_code"]intValue]  == 0) {
                                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"分配任务成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                                    alert.tag = 2;
                                    [alert show];
                                    alert = nil;
                                }else{
                                    [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                                    }
                            }else{
                                [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                        }
                    
                }];
            }
                break;
            default:
                break;
        }
    }else if(alertView.tag == 2){
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
