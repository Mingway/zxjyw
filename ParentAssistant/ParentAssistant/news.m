//
//  news.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "news.h"

@interface news()
@property (nonatomic, strong) NSString *news_title;
@property (nonatomic, strong) NSString *news_source;
@property (nonatomic, strong) NSString *news_updatetime;
@property (nonatomic, strong) NSString *news_content;
@property (nonatomic, strong) NSString *news_id;
@property (nonatomic, assign) BOOL isSingleLine;
@end

@implementation news

- (id)init
{
    self = [super init];
    if (self) {
        self.previewd = NO;
        self.isSingleLine = YES;
    }
    return self;
}
- (void)updateWithDic:(NSDictionary *)dic
{
    if ([dic objectForKey:@"title"]) {
        self.news_title = [dic objectForKey:@"title"];
    }else{
        self.news_title = [dic objectForKey:@"news_title"];
    }
    if ([dic objectForKey:@"updatetime"]) {
        self.news_updatetime = [dic objectForKey:@"updatetime"];
    }else{
        self.news_updatetime = [dic objectForKey:@"news_updatetime"];
    }
    if ([dic objectForKey:@"id"]) {
         self.news_id = [dic objectForKey:@"id"];
    }else{
         self.news_id = [dic objectForKey:@"news_id"];
    }
    if ([dic objectForKey:@"source"]) {
        self.news_source = [dic objectForKey:@"source"];
    }else{
        self.news_source = [dic objectForKey:@"news_source"];
    }
    if ([dic objectForKey:@"news_content"]) {
        self.news_content = [dic objectForKey:@"news_content"];
    }
    CGSize size = {1000,1000};
    CGSize titleSize = [self.news_title sizeWithFont:[UIFont systemFontOfSize:14.f] constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    if (titleSize.width > 285) {
        self.isSingleLine = NO;
    }
}

@end
