//
//  waitView.m
//  zxjyw
//
//  Created by HelyData on 13-11-13.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "waitView.h"
#import <QuartzCore/QuartzCore.h>

//判断是否为4寸屏
#define inch4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

static waitView * defaulView = nil;

@interface waitView()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end

@implementation waitView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
+ (waitView*)getWaitView
{
    if (defaulView == nil) {
        NSArray *waitViewArr = [[NSBundle mainBundle]loadNibNamed:@"waitView" owner:self options:nil];
        defaulView = [waitViewArr  objectAtIndex:0];
        defaulView.bgView.layer.cornerRadius = 5;
        float offset;
        if (!inch4) {
            offset = -50;
            [defaulView.bgView setFrame:CGRectOffset(defaulView.bgView.frame, 0, offset)];
            [defaulView.activityIndicatorView setFrame:CGRectOffset(defaulView.activityIndicatorView.frame, 0, offset)];
            [defaulView.statusLabel setFrame:CGRectOffset(defaulView.statusLabel.frame, 0, offset)];
        }else{
        }
    }
    return defaulView;
}
- (void)startAnimation
{
    [self.activityIndicatorView startAnimating];
}
- (void)stopAnimation
{
    [self.activityIndicatorView stopAnimating];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
