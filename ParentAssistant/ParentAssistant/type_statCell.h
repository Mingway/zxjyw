//
//  type_statCell.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class type_stat;

@interface type_statCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *typeStatLabel;
@property (nonatomic, strong) type_stat *currentType_stat;

@end
