//
//  bindChildrenSecondVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-14.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "bindChildrenSecondVC.h"
#import "student.h"
#import "EGOImageView.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface bindChildrenSecondVC ()

@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentSchoolLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentClassLabel;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet EGOImageView *avatorImageView;

@end

@implementation bindChildrenSecondVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	[self loadData];
}

- (void)loadData
{
    if (self.currentStudent.avator != nil) {
        self.avatorImageView.imageURL = self.currentStudent.avator;
    }
    self.studentNameLabel.text = self.currentStudent.name;
    self.studentSchoolLabel.text = self.currentStudent.school_name;
    self.studentClassLabel.text = self.currentStudent.class_name;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
             [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"1"];
        }
            break;
        default:
            break;
    }
}
@end
