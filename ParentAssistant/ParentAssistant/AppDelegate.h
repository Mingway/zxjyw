//
//  AppDelegate.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-6.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"
#import "user.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate , UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NetworkManager *network;
@property (strong, nonatomic) user *userModel;

@end
