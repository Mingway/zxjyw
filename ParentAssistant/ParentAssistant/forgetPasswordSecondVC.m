//
//  forgetPasswordSecondVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-6.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "forgetPasswordSecondVC.h"
#import "waitView.h"
#import "Util.h"
#import "NetworkManager.h"
#import "user.h"
#import "loginVC.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface forgetPasswordSecondVC ()<UITextFieldDelegate>
{
    NSDate* startDate;
    NSTimer* timer;
}
@property (weak, nonatomic) IBOutlet UITextField *captchaTextField;
@property (nonatomic, strong) NSString *userName;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong) user *userModel;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UIButton *reSendCaptchaButton;

@end

@implementation forgetPasswordSecondVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
    self.userModel = [user shareUser];
    self.network = [[NetworkManager alloc]init];
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
    self.captchaTextField.delegate = self;
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.frame];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    self.phoneNumberLabel.text = self.phoneNumber;
    startDate = [Util getCurrentDate];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
}
-(void)updateCount_down//更新倒计时
{
    self.reSendCaptchaButton.enabled = NO;
    static int total_time = 60;
    NSDate* now = [Util getCurrentDate];
    int count_down = [now timeIntervalSinceDate:startDate];
    if (count_down < total_time) {
        count_down = total_time - count_down;
        self.timerLabel.text = [NSString stringWithFormat:@"%i秒后重发到验证码",count_down];
        self.timerLabel.textColor = [UIColor grayColor];
    }
    else
    {
        self.timerLabel.text = @"可点击重发验证码";
        self.timerLabel.textColor = [UIColor blackColor];
        self.reSendCaptchaButton.enabled = YES;
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [timer invalidate];
//    timer = nil;
}
- (void)resign
{
    [self.captchaTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //重发验证码
            [timer invalidate];
            timer = nil;
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createSendcaptchaRequestWithMobile:self.phoneNumber] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        startDate = [Util getCurrentDate];
                        timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];

        }
            break;
        case 1:
        {
            //修改密码 // 判断新密码长度
            if (![self.captchaTextField.text isEqualToString:@""]) {
                if (![self.passwordTextField.text isEqualToString:@""] && self.passwordTextField.text.length >=6 &&self.passwordTextField.text.length <= 32) {
                    if ([self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
                        [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                        [[waitView getWaitView]startAnimation];
                        [self.network sendRequest:[self.network createChangePassRequestWithAccount:self.phoneNumber OldPass:nil NewPass:self.passwordTextField.text Captcha:self.captchaTextField.text  ] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                            if (isSuccess) {
                                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                                    //切换到主界面
                                    //重新登录
                                    [self.network logonAccount:self.phoneNumber password:self.passwordTextField.text comletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                                        [[waitView getWaitView]stopAnimation];
                                        [[waitView getWaitView]removeFromSuperview];
                                        if (isSuccess) {
                                            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                                                self.userModel.account = self.phoneNumber;
                                                self.userModel.password = self.passwordTextField.text;
                                                [self.userModel updateWithDic:data];
                                                [Util saveToken:self.userModel.access_token];
                                                [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"1"];
                                            }else{
                                                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                                            }
                                        }else{
                                            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                                        }
                                    }];
                                }else{
                                    [[waitView getWaitView]stopAnimation];
                                    [[waitView getWaitView]removeFromSuperview];
                                    [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                                }
                            }else{
                                [[waitView getWaitView]stopAnimation];
                                [[waitView getWaitView]removeFromSuperview];
                                 [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                            }
                        }];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:@"两次密码不一致"];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:@"新密码(6-32字符)"];
                }
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:@"验证码不能为空"];
            }
        }
            break;
        case 2:
        {
            //换手机号
            [timer invalidate];
            timer = nil;
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 3:
        {
            //继续登录
            for (UIViewController *viewController in self.navigationController.viewControllers) {
                if ([viewController isKindOfClass:[loginVC class]]) {
                    [self.navigationController popToViewController:viewController animated:YES];
                }
            }
        }
        default:
            break;
    }
}
#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
