//
//  updatePasswordVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-14.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "updatePasswordVC.h"
#import "user.h"
#import "task.h"
#import "reportVC.h"
#import "NetworkManager.h"
#import "waitView.h"
#import "Util.h"
#import "define.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface updatePasswordVC ()<UITextFieldDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (nonatomic, strong) user *userModel;
@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (nonatomic, strong) NetworkManager *network;
- (IBAction)pressBtn:(UIButton *)sender;
@end

@implementation updatePasswordVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
    self.userModel = [user shareUser];
    self.network = [[NetworkManager alloc]init];
    self.nameLabel.text = self.userModel.name;
    self.accountLabel.text = self.userModel.account;
    self.oldPasswordTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.frame];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
    
    UIButton* backBtn;
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}
- (void)resign
{
    [self.oldPasswordTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            if (![self.oldPasswordTextField.text isEqualToString:@""] && ![self.passwordTextField.text isEqualToString:@""] && ![self.confirmPasswordTextField.text isEqualToString:@""]) {
                //确认修改
                [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                if ([self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
                    [self.network sendRequest:[self.network createChangePassRequestWithAccount:nil OldPass:self.oldPasswordTextField.text NewPass:self.passwordTextField.text Captcha:nil] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                        [[waitView getWaitView]stopAnimation];
                        [[waitView getWaitView]removeFromSuperview];
                        if (isSuccess) {
                            if ([[data objectForKey:@"error_code"]intValue]  == 0) {
                                [Util deleteUser];
                                [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                                [Util showAlertViewWithTitle:@"提示" Message:@"修改成功！"];
                            }else
                            {
                                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                            }
                        }else{
                            [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:errorMessage]];
                        }
                    }];
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:@"新密码和确认密码不匹配"];
                }
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:@"请填写完全"];
            }

        }
            break;
            
        default:
            break;
    }
}
#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
