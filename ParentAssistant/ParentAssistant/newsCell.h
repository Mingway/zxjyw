//
//  newsCell.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class news;
@interface newsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *newsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsSourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsDateLabel;
@property (nonatomic, strong) news *currentNews;
- (void)configure;
@end
