//
//  loginVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-6.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "loginVC.h"
#import "NetworkManager.h"
#import "Util.h"
#import "waitView.h"
#import "user.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface loginVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (nonatomic, strong)NetworkManager *network;
@property (nonatomic, strong) user *userModel;
- (IBAction)pressBtn:(UIButton *)sender;

@end

@implementation loginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    self.phoneNumberTextField.delegate = self;
    self.passwordTextField.delegate = self;
	// Do any additional setup after loading the view.
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.frame];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
}
- (void)resign
{
    [self.phoneNumberTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)login
{
    [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
    [[waitView getWaitView]startAnimation];
    [self.network logonAccount:self.phoneNumberTextField.text password:self.passwordTextField.text comletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [[waitView getWaitView]stopAnimation];
        [[waitView getWaitView]removeFromSuperview];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                self.userModel.account = self.phoneNumberTextField.text;
                self.userModel.password = self.passwordTextField.text;
                [self.userModel updateWithDic:data];
                [Util saveToken:self.userModel.access_token];
                //切换到主界面
                [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"1"];
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
            }
        }else{
            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
        }
    }];
}
- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //判断用户名密码
            if ([Util checkTel:self.phoneNumberTextField.text]) {
                if ([self.passwordTextField.text isEqualToString:@""]) {
                    [Util showAlertViewWithTitle:@"提示" Message:@"请输入密码"];
                }else{
                    [self login];
                }
            }
        }
            break;
    }
}
#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
