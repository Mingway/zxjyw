//
//  taskCenterVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-26.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "taskCenterVC.h"
#import "PullTableView.h"
#import "user.h"
#import "NetworkManager.h"
#import "student.h"
#import "course.h"
#import "task.h"
#import "Util.h"
#import "taskCell.h"
#import "repoListVC.h"
#import "reportVC.h"
#import "UIButton+Extensions.h"
#import <QuartzCore/QuartzCore.h>
#import "waitView.h"
#import "define.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
    self.modalPresentationCapturesStatusBarAppearance =NO;\
    self.edgesForExtendedLayout = UIRectEdgeNone;}

#define PageSize 10

@interface taskCenterVC ()<UITableViewDataSource,UITableViewDelegate,PullTableViewDelegate,taskCellDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *studentDropListButton;
@property (weak, nonatomic) IBOutlet UITableView *studentListTableView;
@property (weak, nonatomic) IBOutlet PullTableView *taskListTableView;
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong) user *userModel;
@property (nonatomic, strong) NSArray *taskStatusArr;
@property (nonatomic, strong) NSMutableArray *taskListMulArr;
@property (nonatomic, assign) int currentSelectedStatus;
@property (weak, nonatomic) IBOutlet UIButton *statusDropListButton;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *taskStatusListTableView;
@property (weak, nonatomic) IBOutlet UILabel *selectTaskStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskListTitleLabel;
@end

@implementation taskCenterVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unLoading];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    IOS7;
    self.currentSelectedStatus = -1;//@"全部显示",@"未开始",@"正在做",@"已做完",@"正在阅卷",@"阅卷完成"    1,2,3,4,5     -1 全部显示
    self.taskStatusArr = @[@"全部显示",@"未开始",@"正在做",@"已做完",@"正在阅卷",@"阅卷完成"];
    self.taskListMulArr = [NSMutableArray array];
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    self.studentNameLabel.text = self.userModel.currentStudent.name;
    self.taskListTitleLabel.text = [NSString stringWithFormat:@"%@的作业",self.userModel.currentStudent.name];
    self.taskListTableView.delegate = self;
    self.taskListTableView.dataSource = self;
    self.taskListTableView.pullDelegate = self;
    self.studentListTableView.delegate = self;
    self.studentListTableView.dataSource = self;
    self.taskStatusListTableView.delegate = self;
    self.taskStatusListTableView.dataSource = self;
    [self refreshTableViewWithIndex:1];
    self.taskListTableView.pullTableIsRefreshing = YES;
    [self setButtonRect];
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
    
    UIButton* backBtn;
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}

- (void)setButtonRect
{
    [self.studentDropListButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -30, -5, -5)];
    [self.statusDropListButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -30, -5, -5)];
}

- (void)rotate:(UIButton*)sender
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    CGAffineTransform t = CGAffineTransformRotate(sender.transform, -M_PI);
    sender.transform = t;
    [UIView commitAnimations];
}

- (void)refreshTableViewWithIndex:(int)index
{
    if (self.userModel.currentStudent != nil) {
        [self.network sendRequest:[self.network createGetStudentTaskListRequestWithStudentAccount:self.userModel.currentStudent.account Status:self.currentSelectedStatus PageIndex:index PageISize:PAGE_SIZE] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    if (index == 1) {
                        [self.taskListMulArr removeAllObjects];
                    }
                    NSArray *arr = [data objectForKey:@"data"];
                    for (NSDictionary *key in arr) {
                        task *t = [[task alloc]init];
                        [t updateWithDic:key];
                        BOOL exsits = NO;
                        for (task *key in self.taskListMulArr) {
                            if ([key.test_task_id isEqualToString:t.test_task_id]) {
                                key.status = t.status;
                                exsits = YES;
                            }
                        }
                        if (!exsits) {
                            [self.taskListMulArr addObject:t];
                        }
                    }
                    [self.taskListTableView reloadData];
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                }
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
            }
            [self unLoading];
        }];
    }else{
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(unLoading) userInfo:nil repeats:NO];
    }
}
- (void)unLoading
{
    self.taskListTableView.pullTableIsRefreshing = NO;
    self.taskListTableView.pullTableIsLoadingMore = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 0) {
        return self.taskListMulArr.count;
    }else if (tableView.tag == 1){
        return self.userModel.bindStudentMulArr.count;
    }else if (tableView.tag == 2){
        return self.userModel.currentStudent.courseListMulArr.count;
    }else{
        return self.taskStatusArr.count;
    }
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 0){
        static NSString *taskCell_identifier = @"taskCell";
        taskCell *cell = [tableView dequeueReusableCellWithIdentifier:taskCell_identifier];
        if(!cell) {
            NSArray* taskNib = [[NSBundle mainBundle]loadNibNamed:taskCell_identifier owner:self options:nil];
            if ([taskNib count]>0) {
                cell = [taskNib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.delegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        task *tempTask = [self.taskListMulArr objectAtIndex:indexPath.row];
        if (indexPath.row % 2 == 1) {
            cell.contentView.backgroundColor = [UIColor colorWithRed:246/255.f green:246/255.f blue:246/255.f alpha:0.6];
        }else{
            cell.contentView.backgroundColor = [UIColor colorWithRed:208/255.f green:208/255.f blue:208/255.f alpha:0.6];
        }
        cell.currentTask  =tempTask;
        NSArray *dateArr = [tempTask.create_time componentsSeparatedByString:@"-"];
        cell.createDateLabel.text =  [NSString stringWithFormat:@"%@/%@/%@",[dateArr objectAtIndex:0],[dateArr objectAtIndex:1],[[[dateArr objectAtIndex:2] componentsSeparatedByString:@" "] objectAtIndex:0]];;
        cell.paperNameLabel.text = tempTask.task_name;
        cell.status = tempTask.status;
        [cell configure];
        return cell;
    }else if(tableView.tag == 1){
        static NSString *cell_identifier = @"studentCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
            cell.backgroundColor = [UIColor clearColor];
            cell.textLabel.font = [UIFont systemFontOfSize:10.f];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [[self.userModel.bindStudentMulArr objectAtIndex:indexPath.row] name];
        return cell;
    }else if(tableView.tag == 2){
        static NSString *cell_identifier = @"studentCourseCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
            cell.textLabel.font = [UIFont systemFontOfSize:10.f];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [[self.userModel.currentStudent.courseListMulArr objectAtIndex:indexPath.row] course_name];
        return cell;
    }else{
        static NSString *cell_identifier = @"taskStatusCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.font = [UIFont systemFontOfSize:10.f];
        }
        cell.textLabel.text = [self.taskStatusArr objectAtIndex:indexPath.row];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView.tag == 1) {
        [self rotate:self.studentDropListButton];
        self.currentSelectedStatus = -1;
        self.studentListTableView.hidden = YES;
        student *tempStudent = [self.userModel.bindStudentMulArr objectAtIndex:indexPath.row];
        self.studentNameLabel.text = [tempStudent name];
        self.userModel.currentStudent = tempStudent;
        self.taskListTitleLabel.text = [NSString stringWithFormat:@"%@的作业",self.userModel.currentStudent.name];
        if (tempStudent.courseListMulArr.count > 0) {
            self.userModel.currentStudent.currentCourse = [tempStudent.courseListMulArr objectAtIndex:0];
        }
        [self.taskListMulArr removeAllObjects];
        [self refreshTableViewWithIndex:1];
        self.taskListTableView.pullTableIsRefreshing = YES;
    }else if (tableView.tag == 3){
        //选择不同的任务状态
        [self rotate:self.statusDropListButton];
        self.taskStatusListTableView.hidden = YES;
        if (indexPath.row == 0) {
            self.currentSelectedStatus = -1;
        }else{
            self.currentSelectedStatus = indexPath.row;
        }
        self.selectTaskStatusLabel.text = [self.taskStatusArr objectAtIndex:indexPath.row];
        [self.taskListMulArr removeAllObjects];
        [self refreshTableViewWithIndex:1];
        self.taskListTableView.pullTableIsRefreshing = YES;
    }
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self refreshTableViewWithIndex:1];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self refreshTableViewWithIndex:(int)self.taskListMulArr.count /PAGE_SIZE +1];
}
- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //绑定学生列表
            if (self.taskListTableView.pullTableIsRefreshing == YES || self.taskListTableView.pullTableIsLoadingMore == YES) {
                [Util showAlertViewWithTitle:@"提示" Message:@"等待任务列表刷新完成"];
            }else{
                [self.studentListTableView reloadData];
                if (!self.taskStatusListTableView.hidden) {
                    self.taskStatusListTableView.hidden = YES;
                    [self rotate:self.statusDropListButton];
                }
                [self rotate:sender];
                if (self.studentListTableView.hidden) {
                    self.studentListTableView.hidden = NO;
                }else{
                    self.studentListTableView.hidden = YES;
                }
            }
        }
            break;
        case 2:
        {
            [self rotate:sender];
            //任务状态列表
            [self.taskStatusListTableView reloadData];
            if (!self.studentListTableView.hidden) {
                self.studentListTableView.hidden = YES;
                [self rotate:self.studentDropListButton];
            }
            if (self.taskStatusListTableView.hidden) {
                self.taskStatusListTableView.hidden = NO;
            }else{
                self.taskStatusListTableView.hidden = YES;
            }
        }
            break;
        case 3:
        {
            if (self.userModel.currentStudent.courseListMulArr.count == 0) {
                
            }else{
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                repoListVC *repo = [board instantiateViewControllerWithIdentifier:@"repoListVC"];
                [self.navigationController pushViewController:repo animated:YES];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark taskCellDelegate
- (void)checkReportByTaskId:(task *)task
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    reportVC *next = [board instantiateViewControllerWithIdentifier:@"reportVC"];
    next.currentTask = task;
    [self.navigationController pushViewController:next animated:YES];
}

#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
