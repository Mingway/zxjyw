//
//  forgetPasswordFirstVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-6.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "forgetPasswordFirstVC.h"
#import "forgetPasswordSecondVC.h"
#import "NetworkManager.h"
#import "Util.h"
#import "waitView.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface forgetPasswordFirstVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (nonatomic, strong) NetworkManager *network;
- (IBAction)pressBtn:(UIButton *)sender;

@end

@implementation forgetPasswordFirstVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	self.network = [[NetworkManager alloc]init];
    self.phoneNumberTextField.delegate = self;
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.frame];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
}

- (void)resign
{
    [self.phoneNumberTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            if ([Util checkTel:self.phoneNumberTextField.text]) {
                [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createSendcaptchaRequestWithMobile:self.phoneNumberTextField.text] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView]stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            forgetPasswordSecondVC *next = [board instantiateViewControllerWithIdentifier:@"forgetPasswordSecondVC"];
                            next.phoneNumber = self.phoneNumberTextField.text;
                            [self.navigationController pushViewController:next animated:YES];
                        }else{
                            [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                        }
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                    }
                }];
            }
        }
            break;
        case 1:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        default:
            break;
    }
}
#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
