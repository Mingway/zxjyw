//
//  notify.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-20.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "notify.h"

@interface notify ()
@property (nonatomic, strong) NSString *notifyId;
@property (nonatomic, assign) int type;
@property (nonatomic, strong) NSString *task_id;
@property (nonatomic, strong) NSString *ntime;
@property (nonatomic, strong) NSString *rtime;
@property (nonatomic, assign) int status;
@property (nonatomic, strong) NSString *student_account;
@property (nonatomic, strong) NSString *student_name;
@end

@implementation notify
- (void)updateWithDic:(NSDictionary*)dic
{
    self.notifyId = [dic objectForKey:@"id"];
    self.type = [[dic objectForKey:@"type"]intValue];
    self.task_id = [dic objectForKey:@"task_id"];
    self.ntime = [dic objectForKey:@"ntime"];
    self.rtime = [dic objectForKey:@"rtime"];
    self.status = [[dic objectForKey:@"status"]intValue];
    self.student_account = [dic objectForKey:@"student_account"];
    self.student_name = [dic objectForKey:@"student_name"];
}
@end
