//
//  answer.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "answer.h"

@interface answer ()
@property (nonatomic, strong)NSString *testitem_id;
@property (nonatomic, strong)NSString *testitem_type;
@property (nonatomic, strong)NSString *answer;
@property (nonatomic, assign)int correct;
@property (nonatomic, strong)NSString *score;
@end

@implementation answer
- (void)updateWithDic:(NSDictionary*)dic;
{
    self.testitem_id = [dic objectForKey:@"testitem_id"];
    self.testitem_type = [dic objectForKey:@"testitem_type"];
    self.answer = [dic objectForKey:@"answer"];
    self.correct = [[dic objectForKey:@"correct"]intValue];
    self.score = [dic objectForKey:@"score"];
}
@end
