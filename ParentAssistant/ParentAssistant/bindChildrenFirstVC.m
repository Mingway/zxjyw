//
//  bindChildrenFirstVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-14.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "bindChildrenFirstVC.h"
#import "NetworkManager.h"
#import "Util.h"
#import "user.h"
#import "waitView.h"
#import "student.h"
#import "bindChildrenSecondVC.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface bindChildrenFirstVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *studentPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *studentAccountTextField;
@property (nonatomic,strong) NetworkManager *network;
@property (nonatomic, strong) user *userModel;
- (IBAction)pressBtn:(UIButton *)sender;
@end

@implementation bindChildrenFirstVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    self.nameTextField.delegate = self;
    self.studentAccountTextField.delegate = self;
    self.studentPasswordTextField.delegate = self;
	// Do any additional setup after loading the view.
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.frame];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.nameTextField.text = @"";
    self.studentPasswordTextField.text  = @"";
    self.studentAccountTextField.text = @"";
}

- (void)resign
{
    [self.nameTextField resignFirstResponder];
    [self.studentPasswordTextField resignFirstResponder];
    [self.studentAccountTextField resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            if ([self.nameTextField.text isEqualToString:@""]) {
                [Util showAlertViewWithTitle:@"提示" Message:@"请输入您的真实姓名"];
            }else if([self.studentPasswordTextField.text isEqualToString:@""]){
                [Util showAlertViewWithTitle:@"提示" Message:@"请输入您需要绑定的子女的姓名"];
            }else if([self.studentAccountTextField.text isEqualToString:@""]){
                [Util showAlertViewWithTitle:@"提示" Message:@"请输入您需要绑定的子女的账号"];
            }else{
                [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createBindingRequestWithName:self.nameTextField.text StudentPassword:self.studentPasswordTextField.text StudentAccount:self.studentAccountTextField.text] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView]stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if([[data objectForKey:@"error_code"]intValue] == 0 ){
                            student *s = [[student alloc]init];
                            [s updateWithDic:[data objectForKey:@"student"]];
                            [self.userModel.bindStudentMulArr addObject:s];
                            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            bindChildrenSecondVC*next = [board instantiateViewControllerWithIdentifier:@"bindChildrenSecondVC"];
                            next.currentStudent = s;
                            [self.navigationController pushViewController:next animated:YES];
                        }else{
                             [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                        }
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                    }
                }];
            }
        }
            break;
        case 1:
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"1"];
        }
            break;
        default:
            break;
    }
}
#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
