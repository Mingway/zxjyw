//
//  type_stat.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "type_stat.h"

@interface type_stat ()
@property (nonatomic, assign) int itemtype;
@property (nonatomic, strong) NSString *itemtypestr;
@property (nonatomic, assign) int count;
@property (nonatomic, assign) int correct;
@property (nonatomic, assign) int error;
@end

@implementation type_stat
- (void)updateWithDic:(NSDictionary*)dic
{
    self.itemtype = [[dic objectForKey:@"itemtype"]intValue];
    self.itemtypestr = [dic objectForKey:@"itemtypestr"];
    self.count = [[dic objectForKey:@"count"]intValue];
    self.correct = [[dic objectForKey:@"correct"]intValue];
    self.error = [[dic objectForKey:@"error"]intValue];
}
@end
