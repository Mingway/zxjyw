//
//  articleQuestion.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "articleQuestion.h"

@interface articleQuestion ()
@property (nonatomic, strong) NSString *testitem_id;
@property (nonatomic, assign) int idx;
@property (nonatomic, strong) NSString *question;
@property (nonatomic, assign) int complex;
@property (nonatomic, assign) float score;
@property (nonatomic, assign) int repo_err;
@property (nonatomic, assign) int repo_dif;
@property (nonatomic, assign) int repo_good;
@property (nonatomic, strong) NSMutableArray *options;
@end

@implementation articleQuestion
- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)updateWithDic:(NSDictionary*)dic
{
    self.testitem_id = [dic objectForKey:@"testitem_id"];
    self.idx = [[dic objectForKey:@"idx"]intValue];
    self.complex = [[dic objectForKey:@"complex"]intValue];
    self.score = [[dic objectForKey:@"score"]floatValue];
    self.repo_err = [[dic objectForKey:@"repo_err"]intValue];
    self.repo_dif = [[dic objectForKey:@"repo_dif"]intValue];
    self.repo_good = [[dic objectForKey:@"repo_good"]intValue];
    NSArray *arr = [[dic objectForKey:@"question"] componentsSeparatedByString:@"\a"];
    self.question = [arr objectAtIndex:0];
    self.options = [NSMutableArray array];
    for (int i = 1; i < arr.count; i++) {
        [self.options addObject:[arr objectAtIndex:i]];
    }
}
@end
