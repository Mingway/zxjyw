//
//  stat_kpoint.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "stat_kpoint.h"

@interface stat_kpoint ()
@property (nonatomic, strong)NSString *kpoint_id;
@property (nonatomic, strong)NSString *kpoint_str;
@property (nonatomic, assign) int weight;
@property (nonatomic, assign) int tesitem_count;
@property (nonatomic, assign) int tesitem_error;
@end

@implementation stat_kpoint
- (void)updateWithDic:(NSDictionary*)dic
{
    self.kpoint_id = [dic objectForKey:@"kpoint_id"];
    self.kpoint_str = [dic objectForKey:@"kpoint_str"];
    self.weight = [[dic objectForKey:@"weight"]intValue];
    self.tesitem_count = [[dic objectForKey:@"tesitem_count"]intValue];
    self.tesitem_error = [[dic objectForKey:@"tesitem_error"]intValue];
}
@end
