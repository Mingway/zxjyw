//
//  psychagogyVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "psychagogyVC.h"
#import "psychagogyCell.h"
#import "article.h"
#import "psychagogyDetailVC.h"
#import "psychagogyPaperAssignmentVC.h"
#import "reportVC.h"
#import "task.h"
#import "PullTableView.h"
#import "NetworkManager.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface psychagogyVC ()<UITableViewDataSource,UITableViewDelegate,PullTableViewDelegate>
@property (weak, nonatomic) IBOutlet PullTableView *articleListTableView;
@property (nonatomic, strong) NSMutableArray *articleListMulArr;
@property (nonatomic, strong) NetworkManager *network;

@end

@implementation psychagogyVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	// Do any additional setup after loading the view.
    self.network= [[NetworkManager alloc]init];
    self.articleListMulArr = [NSMutableArray array];
    self.articleListTableView.delegate = self;
    self.articleListTableView.dataSource = self;
    self.articleListTableView.pullDelegate = self;
    self.articleListTableView.pullTableIsRefreshing  =YES;
    [self getTestPaperListWithIndex:1];
}
-(void)getTestPaperListWithIndex:(int)index
{
    [self.network sendRequest:[self.network createGetTestPaperListRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                NSArray *arr = [data objectForKey:@"testpapers"];
                for (NSDictionary *dic in arr) {
                    BOOL exits = NO;
                    article *a = [[article alloc]init];
                    [a updateListWithDic:dic];
                    for (article *tempArticle in self.articleListMulArr) {
                        if ([tempArticle.testpaper_id isEqualToString:a.testpaper_id]) {
                            exits = YES;
                        }
                    }
                    if (!exits) {
                        [self.articleListMulArr addObject:a];
                    }
                }
                [self.articleListTableView reloadData];
            }
        }
        [self unLoading];
    }];
}

-(void)unLoading
{
    self.articleListTableView.pullTableIsRefreshing = NO;
    self.articleListTableView.pullTableIsLoadingMore = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.articleListMulArr.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *psychagogyCell_identifier = @"psychagogyCell";
    psychagogyCell* cell = [tableView dequeueReusableCellWithIdentifier:psychagogyCell_identifier];
    if (!cell) {
        NSArray* psychagogy = [[NSBundle mainBundle]loadNibNamed:psychagogyCell_identifier owner:self options:nil];
        if ([psychagogy count]>0) {
            cell = [psychagogy objectAtIndex:0];
        }
    }
    article *tempArticle = (article*)[self.articleListMulArr objectAtIndex:indexPath.row];
    cell.currentArticle = tempArticle;
    cell.articleTitleLabel.text = tempArticle.title;
    cell.articleSourceLabel.text = tempArticle.source;
    cell.articleDateLabel.text = tempArticle.createtime;
    cell.articleBriefLabel.text = tempArticle.brief;
    cell.isSuitParent = tempArticle.forwho;
    [cell configure];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ( [(article*)[self.articleListMulArr objectAtIndex:indexPath.row] forwho]) {
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        psychagogyDetailVC *next = [board instantiateViewControllerWithIdentifier:@"psychagogyDetailVC"];
        next.currentArticle = [self.articleListMulArr objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:next animated:YES];
    }else{
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        psychagogyPaperAssignmentVC *next = [board instantiateViewControllerWithIdentifier:@"psychagogyPaperAssignmentVC"];
        next.currentArticle = [self.articleListMulArr objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:next animated:YES];
    }
}
#pragma mark PullingDelegate
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self getTestPaperListWithIndex:1];
}
- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self getTestPaperListWithIndex:(int)self.articleListMulArr.count /10 +1];
}
@end
