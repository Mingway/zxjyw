//
//  report.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "answer.h"
#import "type_stat.h"
#import "kpoint_stat.h"
#import "difficulty.h"
#import "weight.h"
#import "notify.h"
#import "course.h"
#import "stat_kpoint.h"

@interface report : NSObject
@property (nonatomic, readonly) NSString *task_id;
@property (nonatomic, readonly) NSString *do_time;
@property (nonatomic, readonly) NSString *finish_time;
@property (nonatomic, readonly) NSString *take_time;
@property (nonatomic, readonly) int score;
@property (nonatomic, readonly) int status;
@property (nonatomic, readonly) NSString *status_str;
@property (nonatomic, readonly) NSDictionary *task_brief;
@property (nonatomic, readonly) NSMutableArray *answers;
@property (nonatomic, readonly) int answers_count;
@property (nonatomic, readonly) NSMutableArray *type_stat;
@property (nonatomic, readonly) NSMutableArray *kpoint_stat;
@property (nonatomic, readonly) NSMutableArray *difficulty;
@property (nonatomic, readonly) NSMutableArray *weight;
@property (nonatomic, readonly) course *currentCourse;
@property (nonatomic, strong) kpoint_stat *currentKpoint_stat;
@property (nonatomic, strong) stat_kpoint *currentStat_kpoint;
@property (nonatomic, strong) notify *currentNotify;

- (void)updateWithDic:(NSDictionary*)dic;

@end
