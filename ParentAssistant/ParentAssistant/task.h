//
//  task.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//


//任务
#import <Foundation/Foundation.h>

@interface task : NSObject
@property (nonatomic, strong)NSString *test_task_id;
@property (nonatomic, readonly)NSString *test_paper_name;
@property (nonatomic, readonly)NSString *create_time;
@property (nonatomic, readonly)NSString *end_time;
@property (nonatomic, readonly)int time_limit;
@property (nonatomic, readonly)NSString *test_paper_id;
@property (nonatomic, readonly)NSString *creator_id;
@property (nonatomic, readonly)NSString *creator_type;
@property (nonatomic, readonly)NSString *creator_name;
@property (nonatomic, readonly)NSString *task_name;
@property (nonatomic, assign)int status;

- (void)updateWithDic:(NSDictionary*)dic;

 @end
