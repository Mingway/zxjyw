//
//  user.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//


//用户信息，也包含当前绑定学生、选定课程以及选定知识点信息
#import <Foundation/Foundation.h>

@class student;
@class course;
@interface user : NSObject
@property (nonatomic,strong)NSString *account;
@property (nonatomic,strong)NSString *password;
@property (nonatomic,readonly)NSString *name;
@property (nonatomic,strong)NSString *access_token;
@property (nonatomic,strong)NSMutableArray *bindStudentMulArr;
@property (nonatomic,strong)student *currentStudent;
//@property (nonatomic,strong)NSArray *currentKPointArr;

+ (user*)shareUser;

- (void)updateWithDic:(NSDictionary *)dic;

@end
