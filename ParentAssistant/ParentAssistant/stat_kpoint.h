//
//  stat_kpoint.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface stat_kpoint : NSObject
@property (nonatomic, readonly)NSString *kpoint_id;
@property (nonatomic, readonly)NSString *kpoint_str;
@property (nonatomic, readonly) int weight;
@property (nonatomic, readonly) int tesitem_count;
@property (nonatomic, readonly) int tesitem_error;

- (void)updateWithDic:(NSDictionary*)dic;

@end
