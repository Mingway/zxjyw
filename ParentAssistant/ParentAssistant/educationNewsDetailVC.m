//
//  educationNewsDetailVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "educationNewsDetailVC.h"
#import "news.h"
#import "task.h"
#import "reportVC.h"
#import "NetworkManager.h"
#import "Util.h"
#import "waitView.h"
#import "define.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface educationNewsDetailVC ()<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *newsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsSourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsDateTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *newsPreviewImageView;
@property (weak, nonatomic) IBOutlet UIWebView *newsContentWebView;
@property (weak, nonatomic) IBOutlet UIImageView *greenLine;
@property (nonatomic, strong) NetworkManager *network;
@end

@implementation educationNewsDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	// Do any additional setup after loading the view.
    self.newsContentWebView.scrollView.bounces = NO;
    self.network = [[NetworkManager alloc]init];
    [self getNews];
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
    
    UIButton* backBtn;
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}
- (void)getNews
{
   [self.network sendRequest:[self.network createGetNewsRequestWithNewsId:self.currentNews.news_id] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
       if (isSuccess) {
           if ([[data objectForKey:@"error_code"]intValue]  == 0) {
               [self.currentNews updateWithDic:data];
               [self loadData];
           }
       }
   }];
}
- (void)loadData
{
    self.newsTitleLabel.text = self.currentNews.news_title;
    self.newsSourceLabel.text = self.currentNews.news_source;
    self.newsDateLabel.text = self.currentNews.news_updatetime;
    [self.newsContentWebView loadHTMLString:self.currentNews.news_content baseURL:nil];
    [self resetFrame];
}

- (void)resetFrame
{
    CGRect titleLabelFrame = self.newsTitleLabel.frame;
    CGSize size = {titleLabelFrame.size.width,1000};
    CGSize newTitleSize = [self.newsTitleLabel.text sizeWithFont:self.newsTitleLabel.font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    [self.newsTitleLabel setFrame:CGRectMake(titleLabelFrame.origin.x, titleLabelFrame.origin.y, newTitleSize.width, newTitleSize.height)];
    float distance = newTitleSize.height - titleLabelFrame.size.height;
    [self.newsSourceLabel setFrame:CGRectOffset(self.newsSourceLabel.frame, 0, distance)];
    [self.newsDateTitleLabel setFrame:CGRectOffset(self.newsDateTitleLabel.frame, 0,  distance)];
    [self.newsDateLabel setFrame:CGRectOffset(self.newsDateLabel.frame, 0,  distance)];
    [self.newsPreviewImageView setFrame:CGRectOffset(self.newsPreviewImageView.frame, 0,  distance)];
    CGRect contentWebViewFrame = self.newsContentWebView.frame;
    [self.greenLine setFrame:CGRectOffset(self.greenLine.frame, 0, distance)];
    [self.newsContentWebView setFrame:CGRectMake(contentWebViewFrame.origin.x, contentWebViewFrame.origin.y + distance, contentWebViewFrame.size.width, contentWebViewFrame.size.height - distance)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
