//
//  answer.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface answer : NSObject
@property (nonatomic, readonly)NSString *testitem_id;
@property (nonatomic, readonly)NSString *testitem_type;
@property (nonatomic, readonly)NSString *answer;
@property (nonatomic, readonly)int correct;
@property (nonatomic, readonly)NSString *score;

- (void)updateWithDic:(NSDictionary*)dic;

@end
