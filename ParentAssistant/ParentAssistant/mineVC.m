//
//  mineVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-14.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "mineVC.h"
#import "user.h"
#import "studentCell.h"
#import "student.h"
#import "NetworkManager.h"
#import "Util.h"
#import "task.h"
#import "reportVC.h"
#import "define.h"
#import "waitView.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
    self.modalPresentationCapturesStatusBarAppearance =NO;\
    self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface mineVC ()<UITableViewDataSource,UITableViewDelegate,studentCellDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) user *userModel;
@property (weak, nonatomic) IBOutlet UITableView *studentListTableView;
@property (nonatomic, strong) NetworkManager *network;
@property (weak, nonatomic) IBOutlet UIImageView *notifyImageView;
@property (weak, nonatomic) IBOutlet UILabel *notifyNumLabel;
@property (weak, nonatomic) IBOutlet UIButton *notifyButton;
@end

@implementation mineVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.navigationController setWantsFullScreenLayout:NO];
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	// Do any additional setup after loading the view.
    self.userModel = [user shareUser];
    self.network = [[NetworkManager alloc]init];
    self.studentListTableView.delegate = self;
    self.studentListTableView.dataSource = self;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getNotifyNumRequest) name:@"getNotifyNumRequest" object:nil];
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}
- (void)getNotifyNumRequest
{
    [self.network sendRequest:[self.network createGetNotifyNumRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                self.notifyNumLabel.text =  [NSString stringWithFormat:@"%@",[data objectForKey:@"notify"]];
                self.notifyImageView.image = [UIImage imageNamed:@"haveNotify.png"];
                self.notifyNumLabel.hidden = NO;
                if ([[data objectForKey:@"notify"] intValue] == 0 ) {
                    self.navigationController.tabBarItem.badgeValue = nil;
                    self.notifyImageView.image = [UIImage imageNamed:@"noNotify.png"];
                    self.notifyNumLabel.hidden = YES;
                }else{
                    self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%@",[data objectForKey:@"notify"]];
                }
            }
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getNotifyNumRequest];
    [self.studentListTableView reloadData];
    self.accountLabel.text = self.userModel.account;
    self.nameLabel.text = self.userModel.name;
}

#pragma mark UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return  self.userModel.bindStudentMulArr.count;
    }else{
        return 1;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *studentCell_identifier = @"studentCell";
        studentCell* cell = [tableView dequeueReusableCellWithIdentifier:studentCell_identifier];
        if (!cell) {
            NSArray* studentNib = [[NSBundle mainBundle]loadNibNamed:studentCell_identifier owner:self options:nil];
            if ([studentNib count]>0) {
                cell = [studentNib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                student *tempStudent = [self.userModel.bindStudentMulArr objectAtIndex:indexPath.row];
                cell.delegate = self;
                cell.currentStudent = tempStudent;
                cell.studentNameLabel.text = tempStudent.name;
                cell.studentGenderLabel.text = tempStudent.gender;
            }
        }
        return cell;
    }else{
        UITableViewCell *bindingCell = [[UITableViewCell alloc]init];
        bindingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIButton *bindingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [bindingButton setFrame:CGRectMake(88, 6, 143, 30)];
        [bindingButton setImage:[UIImage imageNamed:@"bindChildrenVC_btn_application.png"] forState:UIControlStateNormal];
        [bindingButton addTarget:self action:@selector(bindApplication) forControlEvents:UIControlEventTouchUpInside];
        [bindingCell addSubview:bindingButton];
        return bindingCell;
    }
}
- (void)bindApplication
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"bindChildrenVC"];
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark studentCellDelegate
- (void)pressUnBindingBtnWithStudent:(student *)currentStudent
{
    [self.network sendRequest:[self.network createUnBindingRequestWithStudentAccount:currentStudent.account] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                if ([self.userModel.currentStudent.account isEqualToString: currentStudent.account]) {
                    self.userModel.currentStudent = nil;
                }
                [self.userModel.bindStudentMulArr removeObject:currentStudent];
                for (student *temp in self.userModel.bindStudentMulArr) {
                    if ([temp.account isEqualToString:currentStudent.account]) {
                        [self.userModel.bindStudentMulArr removeObject:temp];
                        NSLog(@"%@",self.userModel.bindStudentMulArr);
                        break;
                    }
                }
                [self.studentListTableView reloadData];
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
            }
        }else{
            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
        }
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
