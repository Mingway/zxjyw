//
//  repoCell.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-16.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol repoCellDelegate <NSObject>
- (void)StrengthTestItemWithTestItemId:(NSString*)testitem_id;

@end

@class repo;
@interface repoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIWebView *stemWebView;
@property (weak, nonatomic) IBOutlet UILabel *itemTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusDateLabel;
@property (nonatomic, strong) repo *currentRepo;
@property (nonatomic)id<repoCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UILabel *errorTimeLabel;
- (IBAction)pressBtn:(UIButton *)sender;

@end
