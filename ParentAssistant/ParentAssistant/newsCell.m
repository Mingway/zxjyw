//
//  newsCell.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "newsCell.h"
#import "news.h"

@interface newsCell ()
@property (weak, nonatomic) IBOutlet UILabel *dateTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *previewButton;

@property (weak, nonatomic) IBOutlet UIImageView *cellBackgroundImageView;

@end

@implementation newsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)configure
{
    if (self.currentNews.previewd) {
        [self.previewButton setImage:[UIImage imageNamed:@"educationNewsVC_previewd.png"] forState:UIControlStateNormal];
    }
    if (self.currentNews.isSingleLine) {
        CGRect titleFrame = self.newsTitleLabel.frame;
        [self.newsTitleLabel setFrame:CGRectMake(titleFrame.origin.x, titleFrame.origin.y, titleFrame.size.width, 22)];
        float offset = -18;
        [self.newsSourceLabel setFrame:CGRectOffset(self.newsSourceLabel.frame, 0, offset)];
        [self.dateTitleLabel setFrame:CGRectOffset(self.dateTitleLabel.frame, 0, offset)];
        [self.newsDateLabel setFrame:CGRectOffset(self.newsDateLabel.frame, 0, offset)];
        [self.previewButton setFrame:CGRectOffset(self.previewButton.frame, 0, offset)];
        CGRect backFrame = self.cellBackgroundImageView.frame;
        [self.cellBackgroundImageView setFrame:CGRectMake(backFrame.origin.x, backFrame.origin.y, backFrame.size.width, 62)];
        [self.cellBackgroundImageView setImage:[UIImage imageNamed:@"educationNewsVC_newsCell_background2.png"]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
