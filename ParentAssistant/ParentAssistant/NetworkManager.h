//
//  NetworkManager.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NetworkRequest;
@interface NetworkManager : NSObject

- (BOOL)logonAccount:(NSString*)account password:(NSString*)password comletionHandler:(void(^)(BOOL isSuccess,NSDictionary *data,NSString *errorMessage))completionHandler;
- (NetworkRequest*)createReloginRequest;
- (NetworkRequest*)createLogoutRequest;
- (NetworkRequest*)createSendcaptchaRequestWithMobile:(NSString*)mobile;
- (NetworkRequest*)createRegisrerRequestWithMobile:(NSString*)mobile Password:(NSString*)password Captcha:(NSString*)captcha;
- (NetworkRequest*)createBindingRequestWithName:(NSString*)name StudentPassword:(NSString*)student_password StudentAccount:(NSString*)student_account;
- (NetworkRequest*)createUnBindingRequestWithStudentAccount:(NSString*)student_account;
- (NetworkRequest*)createGetBindingStudentListRequest;
- (NetworkRequest*)createGetStudentTaskListRequestWithStudentAccount:(NSString*)student_account Status:(int)status PageIndex:(int)page_index PageISize:(int)page_size;
- (NetworkRequest*)createGetProgressRequestWithStudentAccount:(NSString*)student_account;
- (NetworkRequest*)createGetKpointListRequestWithCourseClassId:(NSString*)courseclass_id PageIndex:(int)page_index PageISize:(int)page_size;
- (NetworkRequest*)createGetRepoListRequestWithCourseClassId:(NSString*)courseclass_id StudentAccount:(NSString*)student_account PageIndex:(int)page_index PageISize:(int)page_size;
- (NetworkRequest*)createGetTermPassRequest;
- (NetworkRequest*)createTestTestPaperRequestWithType:(int)type Title:(NSString*)title kPointIds:(NSArray*)kPoint_idArr ChapterIn:(NSString*)chapter_in ChapterTo:(NSString*)chapter_to TestItemId:(NSString*)testitem_id CourseClassId:(NSString*)courseclass_id StudentAccount:(NSString*)student_account;
- (NetworkRequest*)createTaskRequestWithTaskType:(int)task_type  Title:(NSString*)title TestPaperId:(NSString*)testpaper_id Students:(NSArray*)students DeadLine:(NSString*)deadline;
- (NetworkRequest*)createGetReportRequestWithTaskId:(NSString*)task_id StudentAccount:(NSString*)student_account;
- (NetworkRequest*)createChangePassRequestWithAccount:(NSString*)account OldPass:(NSString*)oldpass NewPass:(NSString*)newpass Captcha:(NSString*)captcha;
- (NetworkRequest*)createGetNotifyNumRequest;
- (NetworkRequest*)createGetNotifyListRequestWithStatus:(int)status PageIndex:(int)page_index PageSize:(int)page_size;
- (NetworkRequest*)createSetNotifyReadRequestWithNotifyId:(NSString*)notify_id;
- (NetworkRequest*)createGetStatStudentRequestWithCourseId:(NSString*)course_id StudentAccount:(NSString*)student_account;
- (NetworkRequest*)createGetNewsListRequestWithPageIndex:(int)page_index PageSize:(int)page_size;
- (NetworkRequest*)createGetNewsRequestWithNewsId:(NSString*)news_id;

//心理测试接口
- (NetworkRequest*)createGetTestPaperListRequest;
- (NetworkRequest*)createGetTestPaperRequestWithTestPaperId:(NSString*)testpaper_id;
- (NetworkRequest*)createSubmitAnswerRequestWithTaskId:(NSString*)task_id Answers:(NSDictionary*)answers;
- (NetworkRequest*)createTestStartRequestWithTaskId:(NSString*)task_id;

- (BOOL)sendRequest:(NetworkRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSDictionary *data, NSString *errorMessage))completionHandler;


@end
