//
//  registerVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-6.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "registerVC.h"
#import "NetworkManager.h"
#import "Util.h"
#import "user.h"
#import "bindChildrenFirstVC.h"
#import "waitView.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

//判断是否为4寸屏
#define inch4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

@interface registerVC ()<UITextFieldDelegate>
{
    NSDate* startDate;
    NSTimer* timer;
}
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *captchaTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (nonatomic, strong) user *userModel;
@property (nonatomic, strong) NetworkManager *network;
- (IBAction)pressBtn:(UIButton *)sender;
@property (nonatomic, assign) BOOL isShowKeyboard;
@end

@implementation registerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isShowKeyboard = NO;
    if (!inch4) {
        //注册通知,监听键盘出现
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(handleKeyboardDidShow:)
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
        //注册通知，监听键盘消失事件
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(handleKeyboardDidHidden:)
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    self.phoneNumberTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
    self.captchaTextField.delegate = self;
	// Do any additional setup after loading the view.
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.frame];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
}
//监听事件
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification
{
    //    //获取键盘高度
    //    NSValue *keyboardRectAsObject=[[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    //    CGRect keyboardRect;
    //    [keyboardRectAsObject getValue:&keyboardRect];
    if (!self.isShowKeyboard) {
        [UIView animateWithDuration:0.3f animations:^{
            [[UIApplication sharedApplication].keyWindow setFrame:CGRectOffset([UIApplication sharedApplication].keyWindow.frame, 0, -100)];
        }];
        self.isShowKeyboard = YES;
    }
}

- (void)handleKeyboardDidHidden:(NSNotification*)paramNotification
{
    //    //获取键盘高度
    //    NSValue *keyboardRectAsObject=[[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    //    CGRect keyboardRect;
    //    [keyboardRectAsObject getValue:&keyboardRect];
    if (self.isShowKeyboard) {
        [UIView animateWithDuration:0.3f animations:^{
            [[UIApplication sharedApplication].keyWindow setFrame:CGRectOffset([UIApplication sharedApplication].keyWindow.frame, 0, 100)];
        }];
        self.isShowKeyboard = NO;
    }
}
- (void)resign
{
    [self.phoneNumberTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.captchaTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [timer invalidate];
//    timer = nil;
}
-(void)updateCount_down//更新倒计时
{
    self.phoneNumberTextField.enabled = NO;
    static int total_time = 60;
    NSDate* now = [Util getCurrentDate];
    int count_down = [now timeIntervalSinceDate:startDate];
    if (count_down < total_time) {
        count_down = total_time - count_down;
        self.timerLabel.text = [NSString stringWithFormat:@"%i秒未收到验证码可点击重发",count_down];
    }
    else
    {
        self.timerLabel.text = @"一分钟未收到验证码可点击重发";
        self.phoneNumberTextField.enabled = YES;
        [timer invalidate];
        timer = nil;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //获取验证码
            if (self.phoneNumberTextField.enabled == NO) {
                [Util showAlertViewWithTitle:@"提示" Message:@"已获取验证码，请稍等。"];
            }else if (![self.phoneNumberTextField.text isEqualToString:@""]) {
                [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createSendcaptchaRequestWithMobile:self.phoneNumberTextField.text] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView]stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            startDate = [Util getCurrentDate];
                            timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
                        }else{
                            [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                        }
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                    }
                }];
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:@"请填写手机号"];
            }
        }
            break;
        case 1:
        {
            //注册
            if ([Util checkTel:self.phoneNumberTextField.text]) {
                if (self.passwordTextField.text.length <6 || self.passwordTextField.text.length >32) {
                    [Util showAlertViewWithTitle:@"提示" Message:@"密码应由6-32个字符组成"];
                }else if([self.captchaTextField.text isEqualToString:@""]){
                    [Util showAlertViewWithTitle:@"提示" Message:@"请填写验证码"];
                }else{
                    [timer invalidate];
                    timer = nil;
                    self.timerLabel.text = @"一分钟未收到验证码可点击重发";
                    self.phoneNumberTextField.enabled = YES;
                    [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                    [[waitView getWaitView]startAnimation];
                    [self.network sendRequest:[self.network createRegisrerRequestWithMobile:self.phoneNumberTextField.text Password:self.passwordTextField.text Captcha:self.captchaTextField.text] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                        [[waitView getWaitView]stopAnimation];
                        [[waitView getWaitView]removeFromSuperview];
                        if (isSuccess) {
                            if([[data objectForKey:@"error_code"]intValue] == 0) {
                                self.userModel.access_token = [data objectForKey:@"access_token"];
                                self.userModel.account = self.phoneNumberTextField.text;
                                self.userModel.password = self.passwordTextField.text;
                                [Util saveToken:self.userModel.access_token];
                                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];bindChildrenFirstVC *next = [board instantiateViewControllerWithIdentifier:@"bindChildrenFirstVC"];
                                [self.navigationController pushViewController:next animated:YES];
                                
                            }else{
                                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                            }
                        }else{
                            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                        }
                    }];
                }
            }
        }
            break;
            
        default:
            break;
    }
}
#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
