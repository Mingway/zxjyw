//
//  report.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "report.h"

@interface report ()
@property (nonatomic, strong) NSString *task_id;
@property (nonatomic, strong) NSString *do_time;
@property (nonatomic, strong) NSString *finish_time;
@property (nonatomic, strong) NSString *take_time;
@property (nonatomic, assign) int score;
@property (nonatomic, assign) int status;
@property (nonatomic, strong) NSString *status_str;
@property (nonatomic, strong) NSDictionary *task_brief;
@property (nonatomic, strong) NSMutableArray *answers;
@property (nonatomic, assign) int answers_count;
@property (nonatomic, strong) NSMutableArray *type_stat;
@property (nonatomic, strong) NSMutableArray *kpoint_stat;
@property (nonatomic, strong) NSMutableArray *difficulty;
@property (nonatomic, strong) NSMutableArray *weight;
@property (nonatomic, strong) course *currentCourse;
@end

@implementation report

- (id)init
{
    self = [super init];
    if (self) {
        self.answers = [NSMutableArray array];
        self.type_stat = [NSMutableArray array];
        self.kpoint_stat = [NSMutableArray array];
        self.difficulty = [NSMutableArray array];
        self.weight = [NSMutableArray array];
    }
    return self;
}

- (void)updateWithDic:(NSDictionary*)dic
{
    self.task_id = [dic objectForKey:@"task_id"];
    self.do_time = [dic objectForKey:@"do_time"];
    self.finish_time = [dic objectForKey:@"finish_time"];
    self.take_time = [dic objectForKey:@"take_time"];
    self.score = [[dic objectForKey:@"score"]intValue];
    self.status = [[dic objectForKey:@"status"]intValue];
    self.status_str = [dic objectForKey:@"status_str"];
    self.task_brief = [dic objectForKey:@"task_brief"];
    self.currentCourse = [[course alloc]init];
    [self.currentCourse updateWithDic:dic];
    for (NSDictionary *key in [dic objectForKey:@"answers"]) {
        answer *a = [[answer alloc]init];
        [a updateWithDic:key];
        [self.answers addObject:a];
    }
    self.answers_count = [[dic objectForKey:@"answers_count"]intValue];
    for (NSDictionary *key in [dic objectForKey:@"type_stat"]) {
        type_stat *t = [[type_stat alloc]init];
        [t updateWithDic:key];
        [self.type_stat addObject:t];
    }
    for (NSDictionary *key in [dic objectForKey:@"kpoint_stat"]) {
        kpoint_stat *k = [[kpoint_stat alloc]init];
        [k updateWithDic:key];
        [self.kpoint_stat addObject:k];
    }
    for (NSDictionary *key in [dic objectForKey:@"difficulty"]) {
        difficulty *d = [[difficulty alloc]init];
        [d updateWithDic:key];
        [self.difficulty addObject:d];
    }
    for (NSDictionary *key in [dic objectForKey:@"weight"]) {
        weight *w = [[weight alloc]init];
        [w updateWithDic:key];
        [self.weight addObject:w];
    }
}

@end
