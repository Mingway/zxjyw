//
//  difficulty.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface difficulty : NSObject
@property (nonatomic, readonly) int difficulty;
@property (nonatomic, readonly) int testitem_count;
@property (nonatomic, readonly) int testitem_error;

- (void)updateWithDic:(NSDictionary*)dic;

@end
