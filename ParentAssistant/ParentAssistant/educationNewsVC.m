//
//  educationNewsVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "educationNewsVC.h"
#import "news.h"
#import "newsCell.h"
#import "educationNewsDetailVC.h"
#import "task.h"
#import "reportVC.h"
#import "PullTableView.h"
#import "NetworkManager.h"
#import "Util.h"
#import "waitView.h"
#import "define.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}
#define Page_Size  10

@interface educationNewsVC ()<UITableViewDataSource,UITableViewDelegate,PullTableViewDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet PullTableView *newsListTableView;
@property (nonatomic, strong) NSMutableArray *newsListMulArr;
@property (nonatomic, strong) NetworkManager *network;

@end

@implementation educationNewsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
    self.network  = [[NetworkManager alloc]init];
	// Do any additional setup after loading the view.
    self.newsListTableView.delegate = self;
    self.newsListTableView.dataSource = self;
    self.newsListTableView.pullDelegate = self;
    self.newsListMulArr = [NSMutableArray array];
    self.newsListTableView.pullTableIsRefreshing = YES;
    [self getNewsListWithIndex:1];
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}

- (void)getNewsListWithIndex:(int)index
{
    [self.network sendRequest:[self.network createGetNewsListRequestWithPageIndex:index PageSize:PAGE_SIZE] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if([[data objectForKey:@"error_code"]intValue] == 0){
                NSArray *arr = [data objectForKey:@"news"];
                for (NSDictionary *dic in arr) {
                    news *n = [[news alloc]init];
                    [n updateWithDic:dic];
                    BOOL exits = NO;
                    for (news *tempNews in self.newsListMulArr) {
                        if ([tempNews.news_id isEqualToString:n.news_id]) {
                            exits = YES;
                        }
                    }
                    if (!exits) {
                        [self.newsListMulArr addObject:n];
                    }
                }
                [self.newsListTableView reloadData];
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
            }
        }else{
            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
        }
        [self unLoading];
    }];
}
- (void)unLoading
{
    self.newsListTableView.pullTableIsLoadingMore = NO;
    self.newsListTableView.pullTableIsRefreshing = NO;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.newsListTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([(news*)[self.newsListMulArr objectAtIndex:indexPath.row] isSingleLine]) {
        return 72;
    }else{
        return 90;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.newsListMulArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *newsCell_identifier = @"newsCell";
    newsCell* cell = [tableView dequeueReusableCellWithIdentifier:newsCell_identifier];
    if (!cell) {
        NSArray* record = [[NSBundle mainBundle]loadNibNamed:newsCell_identifier owner:self options:nil];
        if ([record count]>0) {
            cell = [record objectAtIndex:0];
        }
        news *tempNews = (news*)[self.newsListMulArr objectAtIndex:indexPath.row];
        cell.currentNews = tempNews;
        cell.newsTitleLabel.text = tempNews.news_title;
        cell.newsSourceLabel.text = tempNews.news_source;
        cell.newsDateLabel.text = tempNews.news_updatetime;
        [cell configure];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    educationNewsDetailVC *next = [board instantiateViewControllerWithIdentifier:@"educationNewsDetailVC"];
    news *currentNews = [self.newsListMulArr objectAtIndex:indexPath.row];
    currentNews.previewd = YES;
    next.currentNews = currentNews;
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark - PullTableViewDelegate
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self getNewsListWithIndex:1];
}
- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self getNewsListWithIndex:(int)self.newsListMulArr.count /PAGE_SIZE +1];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
