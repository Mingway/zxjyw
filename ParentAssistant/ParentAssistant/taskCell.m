//
//  taskCell.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "taskCell.h"
#import "task.h"

@interface taskCell ()
@property (weak, nonatomic) IBOutlet UIButton *statusButton;

@end

@implementation taskCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (IBAction)pressBtn:(UIButton *)sender {
    //查看报告
    [self.delegate checkReportByTaskId:self.currentTask];
}

- (void)configure
{
    //@"全部显示",@"未开始",@"正在做",@"已做完",@"正在阅卷",@"阅卷完成"    1,2,3,4,5     -1 全部显示
    switch (self.status) {
        case -1:
        {
            [self.statusButton setTitle:@"全部显示" forState:UIControlStateNormal];
        }
            break;
        case 1:
        {
            [self.statusButton setTitle:@"未开始" forState:UIControlStateNormal];
//            self.statusButton.enabled  = YES;
        }
            break;
        case 2:
        {
            [self.statusButton setTitle:@"正在做" forState:UIControlStateNormal];
        }
            break;
        case 3:
        {
            [self.statusButton setTitle:@"已做完" forState:UIControlStateNormal];
        }
            break;
        case 4:
        {
            [self.statusButton setTitle:@"正在阅卷" forState:UIControlStateNormal];
        }
            break;
        case 5:
        {
            [self.statusButton setTitle:@"查看报告" forState:UIControlStateNormal];
            [self.statusButton setTitleColor:[UIColor colorWithRed:30/255.f green:230/255.f blue:150/255.f alpha:1] forState:UIControlStateNormal];
            self.statusButton.enabled  = YES;
        }
            break;
        default:
            break;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
