//
//  studentCell.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class student;
@protocol studentCellDelegate <NSObject>

- (void)pressUnBindingBtnWithStudent:(student*)currentStudent;

@end

@interface studentCell : UITableViewCell
@property (nonatomic, strong)student *currentStudent;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentGenderLabel;
@property (nonatomic)id<studentCellDelegate> delegate;
- (IBAction)pressBtn:(UIButton *)sender;


@end
