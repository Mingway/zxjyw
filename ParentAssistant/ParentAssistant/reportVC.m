//
//  reportVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "reportVC.h"
#import "NetworkManager.h"
#import "waitView.h"
#import "Util.h"
#import "report.h"
#import "user.h"
#import "student.h"
#import "task.h"
#import "type_statCell.h"
#import "kpoint_statCell.h"
#import "paperAssignmentVC.h"
#import "notify.h"
#import "define.h"


#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface reportVC ()<UITableViewDataSource,UITableViewDelegate,kpoint_statCellDelegate,UIAlertViewDelegate>
@property (nonatomic,strong) NetworkManager *network;
@property (nonatomic, strong) report *currentReport;
@property (nonatomic, strong) user *userModel;
@property (weak, nonatomic) IBOutlet UILabel *taskNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *takeTimeLabel;
@property (weak, nonatomic) IBOutlet UITableView *typeStatTableView;
@property (weak, nonatomic) IBOutlet UITableView *kpointTableView;

@end

@implementation reportVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    [self getReport];
    self.typeStatTableView.delegate = self;
    self.typeStatTableView.dataSource = self;
    self.kpointTableView.delegate = self;
    self.kpointTableView.dataSource = self;
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
    
    UIButton* backBtn;
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}
- (void)getReport
{
    NSString *tempStudentAccount;
    if (self.currentNotify != nil) {
        tempStudentAccount = self.currentNotify.student_account;
    }else{
        tempStudentAccount = self.userModel.currentStudent.account;
    }
    [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
    [[waitView getWaitView]startAnimation];
    [self.network sendRequest:[self.network createGetReportRequestWithTaskId:self.currentTask.test_task_id StudentAccount:tempStudentAccount] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [[waitView getWaitView]stopAnimation];
        [[waitView getWaitView]removeFromSuperview];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                self.currentReport = [[report alloc]init];
                [self.currentReport updateWithDic:data];
                [self updateInterface];
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
            }
        }else{
            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
        }
    }];
}

- (void)updateInterface
{
    self.taskNameLabel.text = self.currentTask.task_name;
    self.scoreLabel.text = [NSString stringWithFormat:@"%i分",self.currentReport.score];
    self.answerDateLabel.text = self.currentReport.do_time;
    self.takeTimeLabel.text = self.currentReport.take_time;
    [self.kpointTableView reloadData];
    [self.typeStatTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 0) {
        return self.currentReport.type_stat.count;
    }else{
        return self.currentReport.kpoint_stat.count;
    }
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0) {
        static NSString *type_identifier = @"type_statCell";
        type_statCell *cell = [tableView dequeueReusableCellWithIdentifier:type_identifier];
        if (!cell) {
            NSArray *typeNib = [[NSBundle mainBundle]loadNibNamed:type_identifier owner:Nil options:nil];
            if ([typeNib count] > 0) {
                cell = [typeNib objectAtIndex:0];
            }
        }
        type_stat *tempType = [self.currentReport.type_stat objectAtIndex:indexPath.row];
        cell.currentType_stat = tempType;
        cell.typeStatLabel.text = [NSString stringWithFormat:@"%@(%i对/%i总题数)",tempType.itemtypestr,tempType.correct,tempType.count];
        return cell;
    }else{
        static NSString *kpoint_identifier = @"kpoint_statCell";
        kpoint_statCell *cell = [tableView dequeueReusableCellWithIdentifier:kpoint_identifier];
        if (!cell) {
            NSArray *kpointNib = [[NSBundle mainBundle]loadNibNamed:kpoint_identifier owner:Nil options:nil];
            if ([kpointNib count] > 0) {
                cell = [kpointNib objectAtIndex:0];
            }
        }
        kpoint_stat *tempKpoint = [self.currentReport.kpoint_stat objectAtIndex:indexPath.row];
        cell.currentKpoint_stat = tempKpoint;
        cell.currentNotify = self.currentNotify;
        cell.delegate = self;
        cell.indexLabel.text = [NSString stringWithFormat:@"%i.",(int)indexPath.row+1];
        cell.kpointStatLabel.text =tempKpoint.kpoint_str;
        return cell;
    }
}
#pragma mark kpoint_statDelegate
- (void)pressStrengthButtonWithKpoint:(kpoint_stat *)currentKpoint_stat andNotify:(notify *)n
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    paperAssignmentVC *next = [board instantiateViewControllerWithIdentifier:@"paperAssignmentVC"];
    next.currentKpoint_stat = currentKpoint_stat;
    next.currentNotify = n;
    next.currentCourse = self.currentReport.currentCourse;
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
