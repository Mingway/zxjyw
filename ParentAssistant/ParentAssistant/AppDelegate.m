//
//  AppDelegate.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-6.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "AppDelegate.h"
#import "Util.h"
#import "RSLogger.h"

static NSString * const KEY_TOKEN = @"com.zxjyw.parentassistant.access_token";

@implementation AppDelegate
static void globalExceptionHandler(NSException *exception)
{
    RSLogError(@"MobileBI_CRASH: %@", exception);
    RSLogError(@"MobileBI_CallStack: %@", [exception callStackSymbols]);
    // Internal error reporting
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSSetUncaughtExceptionHandler(&globalExceptionHandler);
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if ([Util read]!= nil) {
        //relogin
        self.userModel.access_token = [[Util read] objectForKey:KEY_TOKEN];
        [self.network sendRequest:[self.network createReloginRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [self.userModel updateWithDic:data];
                    [Util saveToken:self.userModel.access_token];
                    self.window.rootViewController = [board instantiateViewControllerWithIdentifier:@"mainTabBarController"];
                    [self.network sendRequest:[self.network createGetNotifyNumRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                        if (isSuccess) {
                            if ([[data objectForKey:@"notify"] intValue] != 0) {
                                if ([self.window.rootViewController isKindOfClass:[UITabBarController class]]) {
                                    UITabBarController *tabBarController = (UITabBarController*)self.window.rootViewController;
                                    UIViewController *viewController = [tabBarController.viewControllers objectAtIndex:3];
                                    viewController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i",[[data objectForKey:@"notify"] intValue]];
                                }
                            }
                        }
                    }];
                }
            }else{
                self.window.rootViewController = [board instantiateViewControllerWithIdentifier:@"mainNavController"];
            }
        }];
    }else{
        self.window.rootViewController = [board instantiateViewControllerWithIdentifier:@"mainNavController"];
    }
    [[UIApplication sharedApplication]registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound)];
    [[UITabBar appearance]setBackgroundImage:[UIImage imageNamed:@"tab_background.png"]];
    [[UINavigationBar appearance]setBackgroundImage:[UIImage imageNamed:@"nav_background.png"] forBarMetrics:UIBarMetricsDefault];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeRootViewController:) name:@"changeRootViewController" object:nil];
    
    //当程序未启动，用户接收到消息。需要在AppDelegate中的didFinishLaunchingWithOptions得到消息内容
    NSDictionary* payload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (payload)
    {
        NSString* alertStr = nil;
        NSDictionary *apsInfo = [payload objectForKey:@"aps"];
        NSObject *alert = [apsInfo objectForKey:@"alert"];
        if ([alert isKindOfClass:[NSString class]])
        {
            alertStr = (NSString*)alert;
        }
        else if ([alert isKindOfClass:[NSDictionary class]])
        {
            NSDictionary* alertDict = (NSDictionary*)alert;
            alertStr = [alertDict objectForKey:@"body"];
        }
         [[NSNotificationCenter defaultCenter]postNotificationName:@"getNotifyNumRequest" object:nil];
    }
    return YES;
}

- (void)changeRootViewController:(NSNotification*)notification
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if ([notification.object isEqualToString:@"0"]) {
        self.window.rootViewController = [board instantiateViewControllerWithIdentifier:@"mainNavController"];
    }else{
        self.window.rootViewController = [board instantiateViewControllerWithIdentifier:@"mainTabBarController"];
    }
}

- (void)updateNotifyNum
{
    [self.network sendRequest:[self.network createGetNotifyNumRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if ([[data objectForKey:@"notify"] intValue] != 0) {
                if ([self.window.rootViewController isKindOfClass:[UITabBarController class]]) {
                    UITabBarController *tabBarController = (UITabBarController*)self.window.rootViewController;
                    UIViewController *viewController = [tabBarController.viewControllers objectAtIndex:3];
                    viewController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i",[[data objectForKey:@"notify"] intValue]];
                }
            }
        }
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self updateNotifyNum];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
//iPhone 从APNs服务器获取deviceToken后回调此方法
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString* dt = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"deviceToken:%@", dt);
    [[NSUserDefaults standardUserDefaults]setObject:dt forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

//注册push功能失败 后 返回错误信息，执行相应的处理
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSLog(@"Push Register Error:%@", err.description);
}
//当程序在前台运行，接收到消息不会有消息提示（提示框或横幅）。当程序运行在后台，接收到消息会有消息提示，点击消息后进入程序，AppDelegate的didReceiveRemoteNotification函数会被调用（需要自己重写），消息做为此函数的参数传入
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)payload
{
    NSLog(@"remote notification: %@",[payload description]);
    NSString* alertStr = nil;
    NSDictionary *apsInfo = [payload objectForKey:@"aps"];
    NSObject *alert = [apsInfo objectForKey:@"alert"];
    if ([alert isKindOfClass:[NSString class]])
    {
        alertStr = (NSString*)alert;
    }
    else if ([alert isKindOfClass:[NSDictionary class]])
    {
        NSDictionary* alertDict = (NSDictionary*)alert;
        alertStr = [alertDict objectForKey:@"body"];
    }
    application.applicationIconBadgeNumber = 0;
    if ([application applicationState] == UIApplicationStateActive && alertStr != nil)
    {
       //处理
        [self updateNotifyNum];
    }
}

@end
