//
//  NetworkManager.m
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "NetworkManager.h"
#import "Reachability.h"
#import "user.h"
#import "NetworkRequest.h"
#import "UrlDefine.h"
#import "Util.h"

@interface NetworkManager ()

@property (nonatomic, strong)user *userModel;
@property (nonatomic, assign)BOOL isRelogon;//是否已经重新登录

- (BOOL)isNetworkAvailable;

@end

@implementation NetworkManager

-(user *)userModel
{
    return [user shareUser];
}

- (BOOL)isNetworkAvailable
{
    //链接需要更改
    Reachability *r = [Reachability reachabilityWithHostname:@"demo703.tongyouxuetang.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
            return NO;
            break;
        case ReachableViaWWAN:
            // 使用3G网络
            return YES;
            break;
        case ReachableViaWiFi:
            // 使用WiFi网络
            return YES;
            break;
    }
}

- (BOOL)logonAccount:(NSString *)account password:(NSString *)password comletionHandler:(void (^)(BOOL, NSDictionary *, NSString *))completionHandler
{
    if (!self.isNetworkAvailable) {
        completionHandler(NO,nil,@"请检查网络");
        return NO;
    }
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_LOGIN;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObject:account forKey:@"account"];
    [parameters setObject:[Util md5:[NSString stringWithFormat:@"%@+zxjywusermd5",password]] forKey:@"password"];
    [parameters setObject:@"2" forKey:@"termtype"];
    [parameters setObject:@"1" forKey:@"usertype"];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]  != nil && ! [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
    {
         [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] forKey:@"devtoken"];
    }
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return [request sendAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        completionHandler(isSuccess,result,errorMessage);
    }];
}
- (NetworkRequest*)createReloginRequest
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_RELOGIN;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    request.parameters =  [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createLogoutRequest
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_LOGOUT;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    request.parameters =  [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createSendcaptchaRequestWithMobile:(NSString*)mobile
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_SENDCAPTCHA;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObject:mobile forKey:@"mobile"];
    request.parameters =  [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createRegisrerRequestWithMobile:(NSString*)mobile Password:(NSString*)password Captcha:(NSString*)captcha
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_REGISTER;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObject:mobile forKey:@"account"];
    [parameters setObject:[Util md5:[NSString stringWithFormat:@"%@+zxjywusermd5",password]]  forKey:@"password"];
    [parameters setObject:@"1" forKey:@"account_type"];
    [parameters setObject:captcha forKey:@"captcha"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createBindingRequestWithName:(NSString*)name StudentPassword:(NSString*)student_password StudentAccount:(NSString*)student_account
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_BINDING;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:name forKey:@"name"];
    [parameters setObject:student_account  forKey:@"student_account"];
    [parameters setObject:[Util md5:[NSString stringWithFormat:@"%@+zxjywusermd5",student_password]]   forKey:@"student_password"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createGetBindingStudentListRequest
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_STUDENTLIST;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createUnBindingRequestWithStudentAccount:(NSString*)student_account
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_UNBINDING;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    if (student_account != nil) {
        [parameters setObject:student_account forKey:@"student_account"];
    }
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createGetStudentTaskListRequestWithStudentAccount:(NSString*)student_account Status:(int)status PageIndex:(int)page_index PageISize:(int)page_size
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_STUDENTTASKLIST;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    if (student_account != nil) {
        [parameters setObject:student_account forKey:@"student_account"];
    }
    if (status == -1) {
         [parameters setObject:@"1,2,3,4,5" forKey:@"status"];
    }else{
         [parameters setObject:[NSString stringWithFormat:@"%i",status] forKey:@"status"];
    }
    [parameters setObject:[NSString stringWithFormat:@"%i",page_index] forKey:@"page_index"];
    [parameters setObject:[NSString stringWithFormat:@"%i",page_size] forKey:@"page_size"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createGetProgressRequestWithStudentAccount:(NSString*)student_account
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_STUDENTPROGRESS;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    if (student_account != nil) {
        [parameters setObject:student_account forKey:@"student_account"];
    }
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}

- (NetworkRequest*)createGetKpointListRequestWithCourseClassId:(NSString*)courseclass_id PageIndex:(int)page_index PageISize:(int)page_size
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETKPOINTLIST;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    if (courseclass_id != nil) {
        [parameters setObject:courseclass_id forKey:@"courseclass_id"];
    }
    [parameters setObject:[NSString stringWithFormat:@"%i",page_index] forKey:@"page_index"];
    [parameters setObject:[NSString stringWithFormat:@"%i",page_size] forKey:@"page_size"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createGetRepoListRequestWithCourseClassId:(NSString*)courseclass_id StudentAccount:(NSString*)student_account PageIndex:(int)page_index PageISize:(int)page_size
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETREPOLIST;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    if (student_account != nil) {
        [parameters setObject:student_account forKey:@"student_account"];
    }
    if (courseclass_id != nil) {
         [parameters setObject:courseclass_id forKey:@"courseclass_id"];
    }
    [parameters setObject:[NSString stringWithFormat:@"%i",page_index] forKey:@"page_index"];
    [parameters setObject:[NSString stringWithFormat:@"%i",page_size] forKey:@"page_size"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}

- (NetworkRequest*)createGetTermPassRequest
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETTERMPASS;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}

- (NetworkRequest*)createTestTestPaperRequestWithType:(int)type Title:(NSString*)title kPointIds:(NSArray*)kPoint_idArr ChapterIn:(NSString*)chapter_in ChapterTo:(NSString*)chapter_to TestItemId:(NSString*)testitem_id CourseClassId:(NSString*)courseclass_id StudentAccount:(NSString*)student_account
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_CREATETESTPAPER;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    if (title != nil) {
        [parameters setObject:title forKey:@"title"];
    }
    [parameters setObject:student_account forKey:@"student_account"];
    [parameters setObject:[NSString stringWithFormat:@"%i",type] forKey:@"type"];
    if (chapter_in != nil) {
        [parameters setObject:chapter_in forKey:@"chapter_in"];
    }
    if (chapter_to != nil) {
        [parameters setObject:chapter_to forKey:@"chapter_to"];
    }
    if (kPoint_idArr != nil) {
        NSMutableString *kPointStr = [NSMutableString string];
        if (kPoint_idArr.count > 0) {
            for (int i = 0; i < kPoint_idArr.count ; i ++) {
                [kPointStr appendString:[kPoint_idArr objectAtIndex:i]];
                if (i != kPoint_idArr.count - 1) {
                    [kPointStr appendString:@"\x1c"];
                }
            }
        }
        [parameters setObject:kPointStr forKey:@"kpoints"];
    }
    if (testitem_id != nil) {
        [parameters setObject:testitem_id forKey:@"testitem_id"];
    }
    if (courseclass_id != nil) {
        [parameters setObject:courseclass_id forKey:@"courseclass_id"];
    }
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createTaskRequestWithTaskType:(int)task_type  Title:(NSString*)title TestPaperId:(NSString*)testpaper_id Students:(NSArray*)students DeadLine:(NSString*)deadline
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_CREATETASK;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    if (title != nil) {
        [parameters setObject:title forKey:@"title"];
    }
    [parameters setObject:[NSString stringWithFormat:@"%i",task_type] forKey:@"task_type"];
    [parameters setObject:testpaper_id forKey:@"testpaper_id"];
    NSMutableString *studentStr = [NSMutableString string];
    for (int i = 0; i < students.count ; i ++) {
       [studentStr appendString:[students objectAtIndex:i]];
        if (i != students.count - 1) {
            [studentStr appendString:@"\x1c"];
        }
    }
    [parameters setObject:studentStr forKey:@"students"];
    [parameters setObject:deadline forKey:@"deadline"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}

- (NetworkRequest*)createGetReportRequestWithTaskId:(NSString*)task_id StudentAccount:(NSString*)student_account
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETREPORT;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:task_id forKey:@"task_id"];
    [parameters setObject:student_account forKey:@"student_account"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}

- (NetworkRequest*)createChangePassRequestWithAccount:(NSString*)account OldPass:(NSString*)oldpass NewPass:(NSString*)newpass Captcha:(NSString*)captcha
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_CHANGEPASS;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (account != nil) {
        [parameters setObject:account forKey:@"account"];
    }
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    if (oldpass != nil) {
        [parameters setObject:[Util md5:[NSString stringWithFormat:@"%@+zxjywusermd5",oldpass]]  forKey:@"oldpass"];
    }
    [parameters setObject:[Util md5:[NSString stringWithFormat:@"%@+zxjywusermd5",newpass]]   forKey:@"newpass"];
    if (captcha != nil) {
        [parameters setObject:captcha forKey:@"captcha"];
    }
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    NSLog(@"parameters = %@",request.parameters);
    parameters = nil;
    return request;
}
- (NetworkRequest*)createGetNotifyNumRequest
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETNOTIFYNUM;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createGetNotifyListRequestWithStatus:(int)status PageIndex:(int)page_index PageSize:(int)page_size
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETNOTITYLIST;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:[NSString stringWithFormat:@"%i",status] forKey:@"status"];
    [parameters setObject:[NSString stringWithFormat:@"%i",page_index] forKey:@"page_index"];
    [parameters setObject:[NSString stringWithFormat:@"%i",page_size] forKey:@"page_size"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createSetNotifyReadRequestWithNotifyId:(NSString*)notify_id
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_SETNOTIFYREAD;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:notify_id forKey:@"notify_id"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
-(NetworkRequest*)createGetStatStudentRequestWithCourseId:(NSString*)course_id StudentAccount:(NSString*)student_account
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETSTATSTUDENT;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:course_id forKey:@"course_id"];
    [parameters setObject:student_account forKey:@"student_account"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createGetNewsListRequestWithPageIndex:(int)page_index PageSize:(int)page_size
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETNEWSLIST;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:[NSString stringWithFormat:@"%i",page_index] forKey:@"page_index"];
    [parameters setObject:[NSString stringWithFormat:@"%i",page_size] forKey:@"page_size"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createGetNewsRequestWithNewsId:(NSString*)news_id
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETNEWS;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:news_id forKey:@"news_id"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
//心理测试
- (NetworkRequest*)createGetTestPaperListRequest
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETTESTPAPERLIST;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:@"4" forKey:@"type"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createGetTestPaperRequestWithTestPaperId:(NSString*)testpaper_id
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GETTESTPAPER;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:testpaper_id forKey:@"testpaper_id"];
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createSubmitAnswerRequestWithTaskId:(NSString*)task_id Answers:(NSDictionary *)answers
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_SUBMITANSWER;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:task_id forKey:@"task_id"];
    for (NSString *key in answers) {
        [parameters setObject:[NSString stringWithFormat:@"answer_%@",key] forKey:[answers objectForKey:key]];
    }
    request.parameters = [[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (NetworkRequest*)createTestStartRequestWithTaskId:(NSString*)task_id
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_SUBMITANSWER;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:task_id forKey:@"task_id"];
    request.parameters =[[NSMutableDictionary alloc ]initWithDictionary:parameters copyItems:YES];
    parameters = nil;
    return request;
}
- (BOOL)sendRequest:(NetworkRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSDictionary *data, NSString *errorMessage))completionHandler
{
    if (!self.isNetworkAvailable) {
        completionHandler(NO,nil,@"请检查网络");
        return NO;
    }
    return [request sendAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        if (isSuccess) {
            if ([[result objectForKey:@"error_code"]intValue] == 1) {
                [Util deleteUser];
               [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
            }else{
                NSLog(@"normal request:%@",result);
                completionHandler(isSuccess,result,errorMessage);
            }
        }else{
            NSLog(@"error message:%@",errorMessage);
            completionHandler(isSuccess,result,errorMessage);
        }
    }];
}
@end
