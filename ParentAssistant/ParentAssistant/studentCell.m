//
//  studentCell.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "studentCell.h"
#import "student.h"

@interface studentCell ()<UIAlertViewDelegate>


@end

@implementation studentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            UIAlertView *alert =[ [UIAlertView alloc]initWithTitle:@"提示" message:[NSString stringWithFormat:@"确定解除对%@的绑定吗?",self.currentStudent.name] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
            alert = nil;
        }
            break;
            
        default:
            break;
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [self.delegate pressUnBindingBtnWithStudent:self.currentStudent];
        }
            break;
        default:
            break;
    }
}
@end
