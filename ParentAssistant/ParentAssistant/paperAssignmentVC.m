//
//  paperAssignmentVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-13.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "paperAssignmentVC.h"
#import "course.h"
#import "student.h"
#import "Util.h"
#import "UIButton+Extensions.h"
#import "datePickerVC.h"
#import "NetworkManager.h"
#import "user.h"
#import "waitView.h"
#import "kpoint_stat.h"
#import "type_stat.h"
#import "reportVC.h"
#import "task.h"
#import "stat_kpoint.h"
#import "notify.h"
#import "define.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface paperAssignmentVC ()<datePickerDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *currentDayLabel;
- (IBAction)pressBtn:(UIButton *)sender;
@property (nonatomic, strong) datePickerVC *datePicker;//日期选择视图控制器
@property (weak, nonatomic) IBOutlet UIButton *selectDateButton;
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong)user *userModel;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseNameLabel;
@property (nonatomic, strong)NSString *selectDateStr;
@end

@implementation paperAssignmentVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(datePickerVC *)datePicker
{
    if (_datePicker == Nil) {
        _datePicker = [[datePickerVC alloc]init];
        _datePicker.delegate = self;
    }
    return _datePicker;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	// Do any additional setup after loading the view.
    self.userModel = [user shareUser];
    self.network = [[NetworkManager alloc]init];
    self.currentDayLabel.text = [Util getCurrentDay];
    [self.selectDateButton setTitle:[Util convertDateToString:[[NSDate alloc]initWithTimeInterval:86400 sinceDate:[Util getCurrentDate]]] forState:UIControlStateNormal];
    self.selectDateStr = [[Util getCurrentDate] description];
    [self setButtonRect];
    if (self.currentNotify == nil) {
        self.studentNameLabel.text = self.userModel.currentStudent.name;
    }else{
        self.studentNameLabel.text = self.currentNotify.student_name;
    }
    if (self.currentCourse == nil) {
        self.courseNameLabel.text = self.userModel.currentStudent.currentCourse.course_name;
    }else{
        self.courseNameLabel.text = self.currentCourse.course_name;   
    }
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
    
    UIButton* backBtn;
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.tag = 3;
    [alert show];
    alert = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)setButtonRect
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 3:
        {
            //选择日期
            [[UIApplication sharedApplication].keyWindow addSubview:self.datePicker.view];
        }
            break;
        case 4:
        {
            //确认分配
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"确认分配该任务吗？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = 1;
            [alert show];
            alert = nil;
        }
            break;
        default:
            break;
    }
}

#pragma mark datePickerDelegate

- (void)cancelSelectDate
{
    [self.datePicker.view removeFromSuperview];
    self.datePicker = nil;
}
- (void)selectDoneWithDate:(NSDate *)date
{
    [self.selectDateButton setTitle:[Util convertDateToString:date] forState:UIControlStateNormal];
    self.selectDateStr = [date description];
    [self.datePicker.view removeFromSuperview];
    self.datePicker = nil;
}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        switch (buttonIndex) {
            case 0:
            {
                
            }
                break;
            case 1:
            {
                NSString *paperName;
                //综合测试  type = 1
                int type  = 1;
                NSArray *tempKpoints = nil;
                if (self.currentKpoint_stat != nil) {
                    tempKpoints = [NSArray arrayWithObjects:self.currentKpoint_stat.kpoint_id,nil];
                    //强化知识点
                    type = 3;
                    paperName = [NSString stringWithFormat:@"%@的强化练习",self.currentType_stat.itemtypestr];
                }
                if (self.currentStat_kpoint != nil) {
                    tempKpoints = [NSArray arrayWithObjects:self.currentStat_kpoint.kpoint_id,nil];
                    //强化知识点
                    type = 3;
                    paperName = [NSString stringWithFormat:@"%@的强化练习",self.currentStat_kpoint.kpoint_str];
                }
                NSString *tempChapterIn = nil;
                if (self.currentCourse.courseclass_id != nil) {
                    tempChapterIn = self.currentCourse.courseclass_id;
                    //一课一练
                    type = 2;
                    paperName = [NSString stringWithFormat:@"%@的进度练习",self.currentCourse.course_name];
                }
                NSString *testItemId = nil;
                if (self.testitem_id != nil) {
                    testItemId = self.testitem_id;
                    //强化题目
                    type = 4;
                    paperName = [NSString stringWithFormat:@"错题强化"];
                }
                
                NSString *studentAccount = nil;
                NSString *studentName = nil;
                if (self.currentNotify == nil) {
                    studentAccount = self.userModel.currentStudent.account;
                    studentName = self.userModel.currentStudent.name;
                }else{
                    studentAccount = self.currentNotify.student_account;
                    studentName = self.currentNotify.student_name;
                }
                
                NSString *courseClassId = nil;
                for (student *tempStudent in self.userModel.bindStudentMulArr) {
                    if ([studentAccount isEqualToString:tempStudent.account]) {
                        for (course *tempCourse in tempStudent.courseListMulArr) {
                            if ([self.currentCourse.course_id intValue] == [tempCourse.course_id intValue]) {
                                courseClassId = tempCourse.courseclass_id;
                            }
                        }
                    }
                }
                [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createTestTestPaperRequestWithType:type Title:paperName kPointIds:tempKpoints ChapterIn:self.currentCourse.current_chapter_id ChapterTo:nil TestItemId:testItemId CourseClassId:courseClassId StudentAccount:studentAccount] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue]  == 0) {
                            NSString *testpaper_id = [data objectForKey:@"testpaper_id"];
                            [self.network sendRequest:[self.network createTaskRequestWithTaskType:0 Title:[NSString stringWithFormat:@"分配给%@的任务",studentName]  TestPaperId:testpaper_id Students:[NSArray arrayWithObjects:studentAccount, nil] DeadLine:self.selectDateStr] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                                [[waitView getWaitView]stopAnimation];
                                [[waitView getWaitView]removeFromSuperview];
                                if (isSuccess) {
                                    if ([[data objectForKey:@"error_code"]intValue]  == 0) {
                                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"分配任务成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                                        alert.tag = 2;
                                        [alert show];
                                        alert = nil;
                                    }else{
                                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                                    }
                                }else{
                                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                                }
                            }];
                        }else{
                            [[waitView getWaitView]stopAnimation];
                            [[waitView getWaitView]removeFromSuperview];
                            [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                        }
                    }else{
                        [[waitView getWaitView]stopAnimation];
                        [[waitView getWaitView]removeFromSuperview];
                        [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }else if(alertView.tag == 2){
        [self.navigationController popViewControllerAnimated:YES];
    }else if (alertView.tag == 3){
        switch (buttonIndex) {
            case 0:
            {
                
            }
                break;
            case 1:
            {
                [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView]stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [Util deleteUser];
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                        }else{
                            [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                        }
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}

@end
