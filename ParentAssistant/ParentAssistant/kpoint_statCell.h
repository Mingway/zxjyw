//
//  kpoint_statCell.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class stat_kpoint;//学期失分点
@class kpoint_stat;//某次任务失分点
@class notify;
@protocol kpoint_statCellDelegate <NSObject>
@optional
- (void)pressStrengthButtonWithKpoint:(kpoint_stat*)currentKpoint_stat andNotify:(notify*)n;
- (void)pressStrengthButtonWithStat_kpoint:(stat_kpoint*)currentStat_kpoint;
@end

@interface kpoint_statCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *indexLabel;
@property (weak, nonatomic) IBOutlet UILabel *kpointStatLabel;
@property (nonatomic)id <kpoint_statCellDelegate>delegate;
@property (nonatomic, strong) kpoint_stat *currentKpoint_stat;
@property (nonatomic, strong) stat_kpoint *currentStat_kpoint;
@property (nonatomic, strong) notify *currentNotify;
- (IBAction)pressBtn:(UIButton *)sender;

@end
