//
//  task.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-15.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "task.h"

@interface task ()
//@property (nonatomic, strong)NSString *test_task_id;
@property (nonatomic, strong)NSString *test_paper_name;
@property (nonatomic, strong)NSString *create_time;
@property (nonatomic, strong)NSString *end_time;
@property (nonatomic, assign)int time_limit;
@property (nonatomic, strong)NSString *test_paper_id;
@property (nonatomic, strong)NSString *creator_id;
@property (nonatomic, strong)NSString *creator_type;
@property (nonatomic, strong)NSString *creator_name;
@property (nonatomic, strong)NSString *task_name;
@end

@implementation task
- (void)updateWithDic:(NSDictionary*)dic
{
    self.time_limit = [[dic objectForKey:@"time_limit"]intValue];
    self.creator_type = [dic objectForKey:@"creator_type"];
    self.creator_name = [dic objectForKey:@"creator_name"];
    self.test_task_id = [dic objectForKey:@"test_task_id"];
    self.test_paper_name = [dic objectForKey:@"test_paper_name"];
    self.create_time = [dic objectForKey:@"create_time"];
    self.end_time = [dic objectForKey:@"end_time"];
    self.test_paper_id = [dic objectForKey:@"test_paper_id"];
    self.creator_id = [dic objectForKey:@"creator_id"];
    self.status = [[dic objectForKey:@"status"]intValue];
    self.task_name = [dic objectForKey:@"task_name"];
}
@end
