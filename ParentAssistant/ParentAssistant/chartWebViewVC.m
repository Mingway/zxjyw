//
//  chartWebViewVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-12-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "chartWebViewVC.h"
#import "user.h"
#import "student.h"
#import "course.h"

@interface chartWebViewVC ()
@property (weak, nonatomic) IBOutlet UIWebView *chartWebView;
@property (nonatomic, strong) user *userModel;
- (IBAction)pressBtn:(UIButton *)sender;



@end

@implementation chartWebViewVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userModel = [user shareUser];
    
    //设置应用程序的状态栏到指定的方向
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
    //view旋转
    [self.view setTransform:CGAffineTransformMakeRotation(M_PI/2)];
    [self loadChartWebViewHtml];
}
-(void)loadChartWebViewHtml
{
     [self.chartWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://demo703.tongyouxuetang.com/student-report.html#/?token=%@&course_id=%@&saccount=%@&weekno=%@",self.userModel.access_token,self.userModel.currentStudent.currentCourse.course_id,self.userModel.currentStudent.account,self.weekno]]]];
}
-(void)viewWillAppear:(BOOL)animated
{
    //隐藏navigationController
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    //显示navigationController
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}
@end
