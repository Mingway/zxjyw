//
//  article.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//


//心理测试文章，包含多条题目
#import <Foundation/Foundation.h>
@class articleQuestion;
@interface article : NSObject
@property (nonatomic, readonly) NSString *testpaper_id;
@property (nonatomic, readonly) int type;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *brief;
@property  (nonatomic, readonly) BOOL forwho;
@property (nonatomic, readonly) NSString *createtime;
@property (nonatomic, readonly) NSString *creator;
@property (nonatomic, readonly) NSString *source;
@property (nonatomic, readonly) NSString *course_id;
@property (nonatomic, readonly) NSMutableArray *items;
@property (nonatomic, assign) BOOL previewd;

- (void)updateListWithDic:(NSDictionary*)dic;
- (void)updateQuestionWithArr:(NSArray*)arr;

@end
