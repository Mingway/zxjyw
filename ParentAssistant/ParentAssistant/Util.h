//
//  Util.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-12.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Util : NSObject

+(id)read;//读取token
+(void)deleteUser;//删除token
+(void)saveToken:(NSString*)access_token;//保存token

+(NSString *) md5: (NSString *) inPutText ;//MD5加密
+(void)showAlertViewWithTitle:(NSString*)title Message:(NSString*)messgae;//显示AlertView
+ (NSDate*)getCurrentDate;
+ (NSString*)getCurrentDay;
+(NSString*)convertDateToString:(NSDate*)date;
+(BOOL)checkTel:(NSString *)str;//检查电话号码
@end
