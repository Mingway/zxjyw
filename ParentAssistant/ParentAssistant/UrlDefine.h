//
//  UrlDefine.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#ifndef DrawingBoard_UrlDefine_h
#define DrawingBoard_UrlDefine_h

#define BASE_URL @"http://demo703.tongyouxuetang.com/dataapi/base/"
#define PARENT_URL @"http://demo703.tongyouxuetang.com/dataapi/parent/"
#define STUDENT_URL @"http://demo703.tongyouxuetang.com/dataapi/student/"
#define REPORT_URL  @"http://demo703.tongyouxuetang.com/dataapi/report/"

#define URL_LOGIN                  [NSString stringWithFormat:@"%@login.do",BASE_URL]
#define URL_RELOGIN              [NSString stringWithFormat:@"%@relogin.do",BASE_URL]
#define URL_LOGOUT              [NSString stringWithFormat:@"%@logout.do",BASE_URL]
#define URL_SENDCAPTCHA   [NSString stringWithFormat:@"%@sendcaptcha.do",BASE_URL]
#define URL_REGISTER             [NSString stringWithFormat:@"%@register.do",BASE_URL]
#define URL_BINDING              [NSString stringWithFormat:@"%@binding.do",PARENT_URL]
#define URL_UNBINDING         [NSString stringWithFormat:@"%@unbinding.do",PARENT_URL]
#define URL_STUDENTLIST      [NSString stringWithFormat:@"%@studentlist.do",PARENT_URL]
#define URL_STUDENTTASKLIST [NSString stringWithFormat:@"%@get_student_task_list.do",PARENT_URL]
#define URL_STUDENTPROGRESS [NSString stringWithFormat:@"%@get_progress.do",PARENT_URL]
#define URL_GETKPOINTLIST  [NSString stringWithFormat:@"%@get_kpoint_list.do",STUDENT_URL]
#define URL_GETREPOLIST      [NSString stringWithFormat:@"%@get_repolist.do",BASE_URL]
#define URL_GETTERMPASS     [NSString stringWithFormat:@"%@get_termpass.do",BASE_URL]
#define URL_CREATETESTPAPER  [NSString stringWithFormat:@"%@create_testpaper.do",BASE_URL]
#define URL_CREATETASK        [NSString stringWithFormat:@"%@create_task.do",BASE_URL]
#define URL_GETREPORT          [NSString stringWithFormat:@"%@get_report.do",REPORT_URL]
#define URL_CHANGEPASS       [NSString stringWithFormat:@"%@changepass.do",BASE_URL]
#define URL_GETNOTIFYNUM   [NSString stringWithFormat:@"%@getnotifynum.do",BASE_URL]
#define URL_GETNOTITYLIST    [NSString stringWithFormat:@"%@getnotifylist.do",BASE_URL]
#define URL_SETNOTIFYREAD  [NSString stringWithFormat:@"%@setnotifyread.do",BASE_URL]
#define URL_GETSTATSTUDENT  [NSString stringWithFormat:@"%@get_stat_student.do",REPORT_URL]
#define URL_GETNEWSLIST [NSString stringWithFormat:@"%@get_newslist.do",BASE_URL]
#define URL_GETNEWS      [NSString stringWithFormat:@"%@get_news.do",BASE_URL]
//心理测试接口
#define URL_GETTESTPAPERLIST [NSString stringWithFormat:@"%@get_testpaperlist.do",BASE_URL]
#define URL_GETTESTPAPER      [NSString stringWithFormat:@"%@get_testpaper.do",BASE_URL]
#define URL_SUBMITANSWER   [NSString stringWithFormat:@"%@submit_answer.do",BASE_URL]
#define URL_TEST_START          [NSString stringWithFormat:@"%@test_start.do",STUDENT_URL]


#endif