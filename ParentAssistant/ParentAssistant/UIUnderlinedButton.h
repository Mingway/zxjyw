//
//  UIUnderlinedButton.h
//  ParentAssistant
//
//  Created by Mingway on 13-12-26.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIUnderlinedButton : UIButton
{
    
}
+ (UIUnderlinedButton *) underlinedButton; 
@end
