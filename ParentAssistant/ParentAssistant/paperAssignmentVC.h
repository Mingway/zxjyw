//
//  paperAssignmentVC.h
//  ParentAssistant
//
//  Created by HelyData on 13-11-13.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class stat_kpoint;
@class type_stat;
@class kpoint_stat;
@class course;
@class notify;
@interface paperAssignmentVC : UIViewController
@property (nonatomic, assign) int paperType;//强化试卷的类型，0表示强化当前进度章节,1表示强化知识点,2表示强化难题错题
@property (nonatomic, strong) type_stat *currentType_stat;
@property (nonatomic, strong) stat_kpoint *currentStat_kpoint;//科目状态的知识点
@property (nonatomic, strong) kpoint_stat *currentKpoint_stat;//报告里面的知识点
@property (nonatomic, strong) course *currentCourse;
@property (nonatomic, strong) NSString *testitem_id;
@property (nonatomic, strong) notify *currentNotify;
@end
