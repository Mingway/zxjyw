//
//  psychagogyDetailVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-11.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "psychagogyDetailVC.h"
#import "article.h"
#import "psychagogyQuestionVC.h"
#import "NetworkManager.h"
#import "waitView.h"
#import "user.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface psychagogyDetailVC ()
@property (weak, nonatomic) IBOutlet UILabel *articleTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionCountLabel;
@property (weak, nonatomic) IBOutlet UITextView *articleBriefTextView;
- (IBAction)pressBtn:(UIButton *)sender;
- (IBAction)pressBarButton:(UIBarButtonItem *)sender;
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong) user *userModel;
@end

@implementation psychagogyDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	// Do any additional setup after loading the view.
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    [self getTestPaper];
}
- (void)getTestPaper
{
    [self.network sendRequest:[self.network createGetTestPaperRequestWithTestPaperId:self.currentArticle.testpaper_id] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                [self.currentArticle updateQuestionWithArr:[[[data objectForKey:@"testitems"]objectAtIndex:0] objectForKey:@"items"]];
                [self loadData];
            }
        }
    }];
}
- (void)loadData
{
    self.articleTitleLabel.text = self.currentArticle.title;
    self.questionCountLabel.text = [NSString stringWithFormat:@"%i",self.currentArticle.items.count];
    self.articleBriefTextView.text = self.currentArticle.brief;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //调用创建任务，调用开始考试接口
//            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
//            [[waitView getWaitView]startAnimation];
//            self.network sendRequest:[self.network createTaskRequestWithTaskType:4 Title:@"心理测试" TestPaperId:self.currentArticle.testpaper_id Students:[NSArray arrayWithObjects:self.us,nil] DeadLine:<#(NSString *)#>] withCompletionHandler:<#^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage)completionHandler#>
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            psychagogyQuestionVC *next = [board instantiateViewControllerWithIdentifier:@"psychagogyQuestionVC"];
            next.currentArticle = self.currentArticle;
            [self.navigationController pushViewController:next animated:YES];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)pressBarButton:(UIBarButtonItem *)sender {
    switch (sender.tag) {
        case 0:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}
@end
