//
//  notifyListVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-20.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "notifyListVC.h"
#import "NetworkManager.h"
#import "user.h"
#import "Util.h"
#import "PullTableView.h"
#import "notify.h"
#import "reportVC.h"
#import "task.h"
#import "waitView.h"
#import "student.h"
#import "define.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface notifyListVC ()<UITableViewDataSource,UITableViewDelegate,PullTableViewDelegate,UIAlertViewDelegate>
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong) user *userModel;
@property (nonatomic, strong) NSMutableArray *notifyListMulArr;
@property (weak, nonatomic) IBOutlet PullTableView *notifyListTableView;
@end

@implementation notifyListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
	// Do any additional setup after loading the view.
    self.notifyListMulArr = [NSMutableArray array];
    self.userModel = [user shareUser];
    self.network = [[NetworkManager alloc]init];
    self.notifyListTableView.delegate = self;
    self.notifyListTableView.dataSource = self;
    self.notifyListTableView.pullDelegate = self;
    self.notifyListTableView.pullTableIsRefreshing = YES;
    [self getNotifyListRequestWithIndex:1];
    [self addBarButton];
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
    
    UIButton* backBtn;
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}

- (void)getNotifyListRequestWithIndex:(int)index
{
    [self.network sendRequest:[self.network createGetNotifyListRequestWithStatus:-1 PageIndex:index PageSize:20] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                NSArray *tempArr = [data objectForKey:@"data"];
                for (NSDictionary *dic in tempArr) {
                    notify *n = [[notify alloc]init];
                    [n updateWithDic:dic];
                    BOOL exits = NO;
                    for (notify *tempNotifiy in self.notifyListMulArr) {
                        if ([tempNotifiy.notifyId isEqualToString:n.notifyId]) {
                            exits = YES;
                        }
                    }
                    if (!exits) {
                        [self.notifyListMulArr addObject:n];
                    }
                }
                [self.notifyListTableView reloadData];
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
            }
        }else{
            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
        }
        [self unLoading];
    }];
}
- (void)unLoading
{
    self.notifyListTableView.pullTableIsRefreshing = NO;
    self.notifyListTableView.pullTableIsLoadingMore = NO;
}
#pragma mark UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notifyListMulArr.count;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cell_identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
    }
    notify *n = [self.notifyListMulArr objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@   分配给%@的作业完成了，点击查看报告。",n.ntime,n.student_name];
    if (![n.rtime isEqualToString:@""]) {
        cell.textLabel.textColor = [UIColor grayColor];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:10.0f];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    notify *n = [self.notifyListMulArr objectAtIndex:indexPath.row];
    //置为已读
    BOOL exits = NO;
    for (student *tempStudent in self.userModel.bindStudentMulArr) {
        if ([tempStudent.account isEqualToString:n.student_account]) {
            exits = YES;
        }
    }
    if (exits) {
        if ([n.rtime isEqualToString:@""]) {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createSetNotifyReadRequestWithNotifyId:n.notifyId] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"getNotifyNumRequest" object:nil];
                        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                        cell.textLabel.textColor = [UIColor grayColor];
                        [self pushReportVC:n];
                    }
                }
            }];
        }else{
            [self pushReportVC:n];
        }
    }else{
        [Util showAlertViewWithTitle:@"提示" Message:@"已解除绑定！"];
    }
}
- (void)pushReportVC:(notify*)n
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    reportVC *next = [board instantiateViewControllerWithIdentifier:@"reportVC"];
    NSString *task_id = n.task_id;
    task *t = [[task alloc]init];
    t.test_task_id = task_id;
    next.currentTask = t;
    next.currentNotify = n;
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark - PullTableViewDelegate
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self getNotifyListRequestWithIndex:1];
}
- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self getNotifyListRequestWithIndex:(int)self.notifyListMulArr.count /20 +1];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
