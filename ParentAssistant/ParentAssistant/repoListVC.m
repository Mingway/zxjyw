//
//  repoListVC.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-16.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "repoListVC.h"
#import "student.h"
#import "course.h"
#import "user.h"
#import "PullTableView.h"
#import "NetworkManager.h"
#import "Util.h"
#import "repo.h"
#import "repoCell.h"
#import "reportVC.h"
#import "task.h"
#import "paperAssignmentVC.h"
#import "UIButton+Extensions.h"
#import "define.h"
#import "waitView.h"

#define IOS7 if([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=7)\
{self.extendedLayoutIncludesOpaqueBars = NO;\
self.modalPresentationCapturesStatusBarAppearance =NO;\
self.edgesForExtendedLayout = UIRectEdgeNone;}

@interface repoListVC ()<UITableViewDataSource,UITableViewDelegate,PullTableViewDelegate,repoCellDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseNameLabel;
@property (nonatomic, strong) user *userModel;
@property (weak, nonatomic) IBOutlet PullTableView *repoListTableView;
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong) NSMutableArray *repoListMulArr;
@property (weak, nonatomic) IBOutlet UITableView *studentListTableView;
@property (weak, nonatomic) IBOutlet UITableView *courseListTableView;
- (IBAction)pressBtn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *studentDropListButton;
@property (weak, nonatomic) IBOutlet UIButton *courseDropListButton;
@property (weak, nonatomic) IBOutlet UIWebView *questionWebView;
@property (weak, nonatomic) IBOutlet UIView *backView;


@end

@implementation repoListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unLoading];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    IOS7;
    self.userModel = [user shareUser];
	self.studentNameLabel.text = self.userModel.currentStudent.name;
    self.courseNameLabel.text = self.userModel.currentStudent.currentCourse.course_name;
    self.network = [[NetworkManager alloc]init];
    self.repoListMulArr = [NSMutableArray array];
    self.studentListTableView.delegate = self;
    self.studentListTableView.dataSource = self;
    self.courseListTableView.delegate = self;
    self.courseListTableView.dataSource = self;
    self.repoListTableView.pullDelegate = self;
    self.repoListTableView.dataSource  = self;
    self.repoListTableView.delegate = self;
    [self refreshTableWithIndex:1];
    [self setButtonRect];
    self.repoListTableView.pullTableIsRefreshing = YES;
    [self addBarButton];
    UIControl *remove = [[UIControl alloc]initWithFrame:self.backView.bounds];
    [remove addTarget:self action:@selector(remove) forControlEvents:UIControlEventTouchDown];
    [self.backView addSubview:remove];
    [self.backView sendSubviewToBack:remove];
}
-(void)remove
{
    self.backView.hidden = YES;
}
-(void)addBarButton
{
    UIButton* logoffBtn;
    logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame = CGRectMake(0, 0, 30, 30);
    [logoffBtn addTarget:self action:@selector(logoff) forControlEvents:UIControlEventTouchUpInside];
    [logoffBtn setImage:[UIImage imageNamed:@"logoff"] forState:UIControlStateNormal];
    UIBarButtonItem* logoffItem = [[UIBarButtonItem alloc]initWithCustomView:logoffBtn];
    self.navigationItem.rightBarButtonItem = logoffItem;
    UIButton* backBtn;
    backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 30, 30);
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = backItem;
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)logoff
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:LOGOUT_TIPS delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    alert = nil;
}
-(void)setButtonRect
{
    [self.studentDropListButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -30, -5, -5)];
    [self.courseDropListButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -30, -5, -5)];
}
-(void)unLoading
{
    self.repoListTableView.pullTableIsRefreshing = NO;
    self.repoListTableView.pullTableIsLoadingMore = NO;
}
- (void)refreshTableWithIndex:(int)index
{
    [self.network sendRequest:[self.network createGetRepoListRequestWithCourseClassId:self.userModel.currentStudent.currentCourse.courseclass_id StudentAccount:self.userModel.currentStudent.account PageIndex:index PageISize:10] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [self unLoading];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                if (index == 1) {
                    [self.repoListMulArr removeAllObjects];
                }
                NSArray *arr = [data objectForKey:@"testitems"];
                for (NSDictionary *key in arr) {
                    repo *r = [[repo alloc]init];
                    [r updateWithDic:key];
                    BOOL exsits = NO;
                    for (repo *key in self.repoListMulArr) {
                        if ([key.testitem_id isEqualToString:r.testitem_id]) {
                            exsits = YES;
                        }
                    }
                    if (!exsits) {
                        [self.repoListMulArr addObject:r];
                    }
                }
                [self.repoListTableView reloadData];
                
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
            }
        }else{
            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
        }
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)rotate:(UIButton*)sender
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    CGAffineTransform t = CGAffineTransformRotate(sender.transform, -M_PI);
    sender.transform = t;
    [UIView commitAnimations];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 0) {
        return self.repoListMulArr.count;
    }else if (tableView.tag == 1){
        return self.userModel.bindStudentMulArr.count;
    }else{
        return self.userModel.currentStudent.courseListMulArr.count;
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0) {
        static NSString *repoCell_identifier = @"repoCell";
        repoCell *cell = [tableView dequeueReusableCellWithIdentifier:repoCell_identifier];
        if (!cell) {
            NSArray *repoNib = [[NSBundle mainBundle]loadNibNamed:repoCell_identifier owner:Nil options:nil];
            if ([repoNib count] > 0) {
                cell = [repoNib objectAtIndex:0];
            }
            cell.delegate = self;
        }
        repo *tempRepo = [self.repoListMulArr objectAtIndex:indexPath.row];
        [cell.stemWebView loadHTMLString:tempRepo.stem baseURL:nil];
        if (tempRepo.error_time < 10) {
            cell.errorTimeLabel.text = [NSString stringWithFormat:@"%i",tempRepo.error_time];
        }else{
            cell.errorTimeLabel.text = @"n";
        }
        cell.itemTypeLabel.text = tempRepo.itemtype_str;
        cell.currentRepo = tempRepo;
        return  cell;
    }else if (tableView.tag == 1){
        static NSString *cell_identifier = @"studentCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
            cell.textLabel.font = [UIFont systemFontOfSize:10.f];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [[self.userModel.bindStudentMulArr objectAtIndex:indexPath.row] name];
        
        return cell;
    }else{
        static NSString *cell_identifier = @"studentCourseCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
            cell.textLabel.font = [UIFont systemFontOfSize:10.f];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.textLabel.text = [[self.userModel.currentStudent.courseListMulArr objectAtIndex:indexPath.row] course_name];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0) {
        self.backView.hidden = NO;
        repo *tempRepo = [self.repoListMulArr objectAtIndex:indexPath.row];
        [self.questionWebView loadHTMLString:tempRepo.stem baseURL:nil];
    }else if (tableView.tag == 1){
        [self rotate:self.studentDropListButton];
        self.studentListTableView.hidden = YES;
        student *tempStudent = [self.userModel.bindStudentMulArr objectAtIndex:indexPath.row];
        self.studentNameLabel.text = [tempStudent name];
        self.userModel.currentStudent = tempStudent;
        self.courseNameLabel.text = self.userModel.currentStudent.currentCourse.course_name;
        if (tempStudent.courseListMulArr.count > 0) {
            self.userModel.currentStudent.currentCourse = [tempStudent.courseListMulArr objectAtIndex:0];
        }
        [self.courseListTableView reloadData];
        [self.repoListMulArr removeAllObjects];
        if (self.userModel.currentStudent.courseListMulArr.count >0) {
            [self refreshTableWithIndex:1];
        }else{
            [self.repoListTableView reloadData];
        }
    }else{
        [self rotate:self.courseDropListButton];
        self.courseListTableView.hidden = YES;
        course *tempCourse = [self.userModel.currentStudent.courseListMulArr objectAtIndex:indexPath.row];
        self.userModel.currentStudent.currentCourse = tempCourse;
        self.courseNameLabel.text = [tempCourse course_name];
        [self.repoListMulArr removeAllObjects];
        [self refreshTableWithIndex:1];
    }
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self refreshTableWithIndex:1];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self refreshTableWithIndex:(int)self.repoListMulArr.count /20 +1];
}
#pragma mark repoCellDelegate
- (void)StrengthTestItemWithTestItemId:(NSString *)testitem_id
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    paperAssignmentVC *next = [board instantiateViewControllerWithIdentifier:@"paperAssignmentVC"];
    next.testitem_id = testitem_id;
    next.currentCourse = self.userModel.currentStudent.currentCourse;
    [self.navigationController pushViewController:next animated:YES];
}
- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            [self rotate:sender];
            if (!self.courseListTableView.hidden) {
                self.courseListTableView.hidden = YES;
                [self rotate:self.courseDropListButton];
            }
            if (self.studentListTableView.hidden) {
                self.studentListTableView.hidden = NO;
            }else{
                self.studentListTableView.hidden = YES;
            }
        }
            break;
        case 1:
        {
            [self rotate:sender];
            if (!self.studentListTableView.hidden) {
                self.studentListTableView.hidden = YES;
                [self rotate:self.studentDropListButton];
            }
            if (self.courseListTableView.hidden) {
                self.courseListTableView.hidden = NO;
            }else{
                self.courseListTableView.hidden = YES;
            }
        }
            break;
        default:
            break;
    }
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createLogoutRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView]stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util deleteUser];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeRootViewController" object:@"0"];
                    }else{
                        [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                    }
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
