//
//  user.m
//  ParentAssistant
//
//  Created by HelyData on 13-11-8.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "user.h"
#import "student.h"

static user *defalutUser = nil;

@interface user()
@property (nonatomic,strong)NSString *name;

@end

@implementation user

- (id)init
{
    self = [super init];
    if (self) {
        self.account = @"15801886505";
        self.password = @"111111";
        self.name = @"石茗伟";
        self.bindStudentMulArr = [NSMutableArray array];
//        self.currentKPointArr = [NSArray array];
    }
    return self;
}

+ (user*)shareUser
{
    if (defalutUser == nil) {
        defalutUser = [[user alloc]init];
    }
    return defalutUser;
}
- (void)updateWithDic:(NSDictionary *)dic
{
    NSDictionary *dataDic = [dic objectForKey:@"data"];
    self.access_token = [dataDic objectForKey:@"access_token"];
    self.name = [dataDic objectForKey:@"name"];
     [self.bindStudentMulArr removeAllObjects];
    for (NSDictionary *key in [dic objectForKey:@"students"]) {
        student *s = [[student alloc]init];
        [s updateWithDic:key];
        [self.bindStudentMulArr addObject:s];
    }
}
@end
