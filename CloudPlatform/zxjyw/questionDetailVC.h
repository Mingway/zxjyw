//
//  questionDetailVC.h
//  zxjyw
//
//  Created by HelyData on 13-9-23.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  问题详情类

#import <UIKit/UIKit.h>
#import "question.h"

@interface questionDetailVC : UIViewController<UIAlertViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIView* picker_background;
    UIPickerView* picker;
}
@property(nonatomic,strong)question* currentQuestion;
@end
