//
//  coinCell.m
//  zxjyw
//
//  Created by HelyData on 13-10-11.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "coinCell.h"

@interface coinCell ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *coinLabel;
@end

@implementation coinCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)resolvingWithRecord:(coinRecord*)record
{
    self.questionTypeLabel.text = record.reason;
    self.coinLabel.text = [NSString stringWithFormat:@"%i",record.coin];
    self.dateLabel.text = record.date;
}
@end
