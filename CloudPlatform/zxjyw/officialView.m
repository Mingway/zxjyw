//
//  officialView.m
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "officialView.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "ResourseDefine.h"
#import "waitView.h"

@interface officialView()
{
    NSTimer* timer;
}
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;//显示题目ID 科目 类型 以及总分值的Label
@property (weak, nonatomic) IBOutlet UIImageView *havePictureImageView;//显示该问题是否包含图片
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;//问题内容webView
@property (weak, nonatomic) IBOutlet UILabel *coinLabel;//本题悬赏积分
@property (weak, nonatomic) IBOutlet UILabel *timeLabel1;//倒计时间第一位
@property (weak, nonatomic) IBOutlet UILabel *timeLabel2;//倒计时间第二位
@property (weak, nonatomic) IBOutlet UILabel *timeLabel3;//倒计时间第三位
@property (weak, nonatomic) IBOutlet UILabel *timeLabel4;//倒计时间第四位
@property (weak, nonatomic) IBOutlet UIButton *solutionButton1;//按钮原题解答1
@property (weak, nonatomic) IBOutlet UIButton *solutionButton2;//按钮原题解答2
@property (weak, nonatomic) IBOutlet UIButton *solutionButton3;//按钮原题解答3
@property (weak, nonatomic) IBOutlet UIWebView *solutionWebView;//显示正确答案的webView
@property (nonatomic, retain)userInfo* user;//单例的userInfo
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;//底部用户信息显示框的用户手机号Label
@property (weak, nonatomic) IBOutlet UILabel *myCoinLabel;//用户积分显示Label
@property (weak, nonatomic) IBOutlet UIButton *notNowButton;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UIButton *getQuestionButton;
@property (weak, nonatomic) IBOutlet UIButton *notTodayButton;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (nonatomic, strong) NetworkManager *network;
@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *cellBackGroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *notTodayTitleLabel;
@end


@implementation officialView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
            //返回
            [self.delegate officialPop];
            break;
        case 1:
            //logoff
            [self.delegate officialLogoff];
        case 2:
        {
            //抢分
            [self addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
             [timer invalidate];
            timer = nil;
            [self.network sendRequest:[self.network createRecieveQuestionRequestWithAnswerId:self.currentQuestion.AnswerId andType:1] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView] stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [self.delegate officialGetQuestion];
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                    }
                }
                else
                {
                    [tools showAlertView:@"提示" andMessage:errorMessage];
                }
            }];
        }
            break;
        case 3:
        {
            //现在没空
            [self addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
             [timer invalidate];
            timer = nil;
            if (self.notTodayButton.isSelected) {
                [self.network sendRequest:[self.network createRefuseQuestionRequestWithAnswerId:self.currentQuestion.AnswerId andRefuse_type:1] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [self.delegate officialPop];
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"removePushQuestion" object:self.currentQuestion.AnswerId];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
            else
            {
                 [timer invalidate];
                timer = nil;
                [self.network sendRequest:[self.network createRefuseQuestionRequestWithAnswerId:self.currentQuestion.AnswerId andRefuse_type:0] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [self.delegate officialPop];
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"removePushQuestion" object:self.currentQuestion.AnswerId];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
        }
            break;
        case 4:
            //选中是否今天都不再接受推送
            [sender setSelected:!sender.isSelected];
            break;
        case 5:
            //进入进入用户中心
            [self.delegate officialUserCenter];
            break;
        case 6:
            //进入兑换界面
            [self.delegate officialExchange];
            break;
        case 7:
        {
            //点击原题解答1
            [sender setSelected:YES];
            [self.solutionButton2 setSelected:NO];
            [self.solutionButton3 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:0] baseURL:nil];
        }
            break;
        case 8:
        {
            //点击原题解答2
            [sender setSelected:YES];
            [self.solutionButton1 setSelected:NO];
            [self.solutionButton3 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:1]  baseURL:nil];
        }
            break;
        case 9:
        {
            //点击原题解答3
            [sender setSelected:YES];
            [self.solutionButton2 setSelected:NO];
            [self.solutionButton1 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:2]  baseURL:nil];
        }
            break;
        default:
            break;
    }
}
//根绝question解析显示
-(void)resolvingWithQuestion:(question*)question
{
    if (!inch4) {
        self.cellBackGroundImageView.image = [UIImage imageNamed:@"questionUnfoldView_cell_background_960.png"];
        self.backGroundImageView.image = [UIImage imageNamed:@"questionUnfoldVC_background_960.png"];
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.userNameLabel setFrame:CGRectOffset(self.userNameLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.myCoinLabel setFrame:CGRectOffset(self.myCoinLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];
        [self.solutionButton1 setFrame:CGRectOffset(self.solutionButton1.frame, 0, -73)];
        [self.solutionButton2 setFrame:CGRectOffset(self.solutionButton2.frame, 0, -73)];
        [self.solutionButton3 setFrame:CGRectOffset(self.solutionButton3.frame, 0, -73)];
        [self.getQuestionButton setFrame:CGRectOffset(self.getQuestionButton.frame, 0, -73)];
        [self.notNowButton setFrame:CGRectOffset(self.notNowButton.frame, 0, -73)];
        [self.notTodayButton setFrame:CGRectOffset(self.notTodayButton.frame, 0, -73)];
        [self.notTodayTitleLabel setFrame:CGRectOffset(self.notTodayTitleLabel.frame, 0, -73)];
        [self.questionContentWebView setFrame:CGRectMake(13, 93, 228, 90)];
        [self.cellBackGroundImageView setFrame:CGRectMake(8, 66, 305, 360)];
        [self.solutionWebView setFrame:CGRectMake(14, 212, 292, 158)];
    }
    self.network = [[NetworkManager alloc]init];
    self.user = [userInfo shareUserInfo];
    self.titleImageView.image = [UIImage imageNamed:[tools judgeTitleImageWithCourse:self.user.course]];
    self.userNameLabel.text = self.user.loginName;
    self.myCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    self.currentQuestion = question;
    self.coinLabel.text = [NSString stringWithFormat:@"%i",question.QuestionCoin];
    self.titleLabel.text = [NSString stringWithFormat:@"%@ %@ %i 总分值",question.QuestionCourse,question.QuestionStyle,question.QuestionScore];
    if (question.QuestionSolutions.count == 2) {
        self.solutionButton3.hidden = YES;
    }
    else if(question.QuestionSolutions.count == 1)
    {
        self.solutionButton2.hidden = YES;
        self.solutionButton3.hidden = YES;
    }
    [self.questionContentWebView loadHTMLString:question.QuestionContent baseURL:nil];
        if([tools htmlIsIncludeImg:[self.currentQuestion.QuestionSolutions objectAtIndex:0]])
        {
            _havePictureImageView.hidden = NO;
        }
        else
        {
            _havePictureImageView.hidden = YES;
        }
    [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:0]  baseURL:nil];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
    [self setButtonRect];
}
-(void)stopTimer
{
    [timer invalidate];
    timer = nil;
}
-(void)continueTimer
{
    if (timer == nil) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
    }
}
-(void)updateCount_down//更新倒计时
{
    static int total_time = 5*60;
    NSDate* now = [tools localDate];
    int count_down = [now timeIntervalSinceDate:self.currentQuestion.startDate];
    if (count_down < total_time) {
        count_down = total_time - count_down;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.timeLabel1.text = @"0";
            self.timeLabel2.text = [NSString stringWithFormat:@"%i",count_down/60];
            int second = count_down%60;
            self.timeLabel3.text = [NSString stringWithFormat:@"%i",second/10];
            self.timeLabel4.text = [NSString stringWithFormat:@"%i",second%10];
        });
    }
    else
    {
        [timer invalidate];
        timer = nil;
        [tools showAlertView:@"提示" andMessage:@"本题抢分时间已过"];
        self.timeLabel1.text  = @"0";
        self.timeLabel2.text  = @"0";
        self.timeLabel3.text  = @"0";
        self.timeLabel4.text  = @"0";
        //移除该题
        [[NSNotificationCenter defaultCenter]postNotificationName:@"removePushQuestion" object:self.currentQuestion.AnswerId];
    }
}
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.solutionButton1 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, 0)];
    [self.solutionButton2 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, 0)];
    [self.solutionButton3 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, -10)];
    [self.getQuestionButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -5)];
    [self.notNowButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -5, -10, -5)];
    [self.notTodayButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -5, -10, -50)];
}
@end
