//
//  updatePasswordVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-30.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "updatePasswordVC.h"
#import "userInfo.h"
#import "tools.h"
#import "NetworkManager.h"
#import "UIButton+Extensions.h"
#import "ResourseDefine.h"
#import "waitView.h"


@interface updatePasswordVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UITextField *oldPsswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *reNewPasswordTextField;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userCoinLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property (nonatomic, strong) NetworkManager *network;
@property (retain, nonatomic)userInfo* user;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;

@end

@implementation updatePasswordVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.userNameLabel setFrame:CGRectOffset(self.userNameLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.userCoinLabel setFrame:CGRectOffset(self.userCoinLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];
        self.backGroundImageView.image = [UIImage imageNamed:@"updatePasswordVC_background_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    self.user = [userInfo shareUserInfo];
    self.userNameLabel.text = self.user.loginName;
    self.userCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    UIControl* resign = [[UIControl alloc]initWithFrame:self.view.bounds];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    self.oldPsswordTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.reNewPasswordTextField.delegate  = self;
    [self setButtonRect];
}
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.submitButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.exchangeButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
-(void)resign
{
    [self.oldPsswordTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.reNewPasswordTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 2:
            if ([self.passwordTextField.text isEqualToString:@""]||[self.reNewPasswordTextField.text isEqualToString:@""]||[self.oldPsswordTextField.text isEqualToString:@""]) {
                [tools showAlertView:@"提示" andMessage:@"请输入密码"];
            }
            else if (![self.passwordTextField.text isEqualToString:self.reNewPasswordTextField.text]) {
                [tools showAlertView:@"提示" andMessage:@"两次密码不一致"];
            }
            else if(self.passwordTextField.text.length < 6 || self.passwordTextField.text.length >32)
            {
                 [tools showAlertView:@"提示" andMessage:@"两次密码不一致"];
            }
            else
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createUpdatePasswordRequestWithPassword:self.oldPsswordTextField.text andNewPassword:self.passwordTextField.text] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            self.user.access_token = [data objectForKey:@"access_token"];
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"error" andMessage:errorMessage];
                    }
                }];
            }
            break;
        case 3:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        default:
            break;
    }
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
@end
