//
//  practiseQuestionDetailView.h
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "question.h"
#import "userInfo.h"
#import "EGOImageView.h"

@protocol practiseQuestionDetailDelegate <NSObject>
-(void)practisePop;
-(void)practiseLogoff;
-(void)practiseUserCenter;
-(void)practiseExchange;
-(void)practiseRecordwithScore:(int)score;//跳转录音点评界面，包含参数用户给予该题的评分
-(void)practisePushQuestionList;//转到问题列表界面
@end

@interface practiseQuestionDetailView : UIView<UIAlertViewDelegate>
@property (nonatomic)id<practiseQuestionDetailDelegate>delegate;
@property (nonatomic,retain)question* currentQuestion;
-(void)resolvingWithQuestion:(question*)question;
@end
