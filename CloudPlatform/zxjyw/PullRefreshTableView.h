//
//  pullRefreshTableView.h
//  testRefreshTableView
//
//  Created by HelyData on 13-10-20.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    StatusDragging = 0 ,
    StatusRelease ,
    StatusLoading
}currenrtStatus;

@interface StatusView : UIView
{
    UILabel *statusLabel;
    UIImageView *arrowView;
    UIActivityIndicatorView *activityView;
    CALayer *arrow;
}
@property (nonatomic) currenrtStatus status;
@end
@protocol PullRefreshTableViewDelegate;

@interface PullRefreshTableView : UITableView<UITableViewDelegate,UIScrollViewDelegate>
{
    StatusView *headerView;
}
@property (nonatomic)id <PullRefreshTableViewDelegate> pullDelegate;
-(void)addHeaderView;
-(id)initWithFrame:(CGRect)frame pullDelegate:(id<PullRefreshTableViewDelegate>)delegate;
-(void)launchRefreshing;
-(void)loadingFinished;
@end

@protocol PullRefreshTableViewDelegate <NSObject>
@required
-(void)loadData;
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
@optional

@end