//
//  questionListDetailVC.m
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "questionListDetailVC.h"
#import "officialQuestionDetailView.h"
#import "practiseQuestionDetailView.h"
#import "questionListVC.h"
#import "questionRecordVC.h"
#import "tools.h"
#import "NetworkManager.h"
#import "waitView.h"
#import "ResourseDefine.h"

@interface questionListDetailVC ()<officiaQuestionDetailViewDelegate,practiseQuestionDetailDelegate>


@end

@implementation questionListDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (![self.currentQuestion.QuestionType isEqualToString:@"训练"]) {
        officialQuestionDetailView* tempView = (officialQuestionDetailView*)self.view;
        [tempView stopTimer];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (![self.currentQuestion.QuestionType isEqualToString:@"训练"]) {
        officialQuestionDetailView* tempView = (officialQuestionDetailView*)self.view;
        [tempView continueTimer];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.network = [[NetworkManager alloc]init];
    static NSString* practiseQuestionView_identifier = @"practiseQuestionDetailView";
    static NSString* officialQuestionView_identifier = @"officialQuestionDetailView";
    if ([self.currentQuestion.QuestionType isEqualToString:@"训练"]) {
        NSArray* practise = [[NSBundle mainBundle]loadNibNamed:practiseQuestionView_identifier owner:self options:nil];
        if ([practise count]>0) {
            practiseQuestionDetailView* tempView = [practise objectAtIndex:0];
            tempView.delegate = self;
            self.view = tempView;
            [tempView resolvingWithQuestion:self.currentQuestion];
        }
    }
    else
    {
        NSArray* official = [[NSBundle mainBundle]loadNibNamed:officialQuestionView_identifier owner:self options:nil];
        if ([official count]>0) {
            officialQuestionDetailView* tempView = [official objectAtIndex:0];
            tempView.delegate = self;
            self.view = tempView;
            [tempView resolvingWithQuestion:self.currentQuestion];
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark officialDelegate
-(void)officialPop
{
    [self.view addSubview:[waitView getWaitView]];
    [[waitView getWaitView]startAnimation];
    [self.network sendRequest:[self.network createRefuseQuestionRequestWithAnswerId:self.currentQuestion.AnswerId andRefuse_type:0] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [[waitView getWaitView] stopAnimation];
        [[waitView getWaitView]removeFromSuperview];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                if (![self.currentQuestion.QuestionType isEqualToString:@"训练"]) {
                    officialQuestionDetailView* tempView = (officialQuestionDetailView*)self.view;
                    [tempView stopTimer];
                }
                for (NSInteger i = self.navigationController.viewControllers.count-1; i >= 0; i--) {
                    UIViewController* subViewController = [self.navigationController.viewControllers objectAtIndex:i];
                    if ([subViewController isKindOfClass:[questionListVC class]]) {
                        [self.navigationController popToViewController:subViewController animated:YES];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"removePushQuestion" object:self.currentQuestion.AnswerId];
                    }
                }
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
            }
        }
        else
        {
            [tools showAlertView:@"提示" andMessage:errorMessage];
        }
    }];
}
-(void)officialRecordwithScore:(int)score andStartDate:(NSDate *)startDate
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    questionRecordVC *next = [board instantiateViewControllerWithIdentifier:@"questionRecordVC"];
    next.currentQuestion = self.currentQuestion;
    next.startDate = startDate;
    next.getScore = score;
    [self.navigationController pushViewController:next animated:YES];
}
-(void)officialLogoff
{
    //logoff
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.tag = LOGOFF_TAG;
    [alert show];
    alert = nil;
}
-(void)officialUserCenter
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"userCenterVC"];
    [self.navigationController pushViewController:next animated:YES];
}
-(void)officialExchange
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"exchangeVC"];
    [self.navigationController pushViewController:next animated:YES];
}
-(void)officialPushQuestionList
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"questionListVC"];
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark practiseQuestionDetailDelegate
-(void)practiseUserCenter
{
    [self officialUserCenter];
}
-(void)practiseExchange
{
    [self officialExchange];
}
-(void)practiseLogoff
{
    [self officialLogoff];
}
-(void)practisePop
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)practisePushQuestionList
{
    [self officialPushQuestionList];
}
-(void)practiseRecordwithScore:(int)score
{
    [self officialRecordwithScore:score andStartDate:nil];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
@end
