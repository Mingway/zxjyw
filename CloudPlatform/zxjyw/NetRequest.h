//
//  NetRequest.h
//  zxjyw
//
//  Created by HelyData on 13-10-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetRequest : NSObject <NSCopying>

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSMutableDictionary *parameters;
@property (nonatomic, strong) NSString *tokenID;

- (BOOL)sendAsyncRequestWithCompletionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler;

- (BOOL)downloadAsyncRequestWithCompletionHandler:(void(^)(BOOL isSuccess, NSData *result, NSString *errorMessage))completionHandler;

@end
