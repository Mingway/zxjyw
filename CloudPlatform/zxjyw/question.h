//
//  question.h
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  问题的封装类

#import <Foundation/Foundation.h>

@interface question : NSObject
@property(nonatomic,strong)NSString* QuestionType;//问题分类，分为训练和正式
@property(nonatomic,strong)NSString* QuestionStyle;//问题类型，分为证明题，解答题等
@property(nonatomic,strong)NSString* QuestionCourse;//问题所属科目
@property(nonatomic,strong)NSString* QuestionGrade;//问题所属年级
@property(nonatomic,assign)int QuestionScore;//问题总分
@property(nonatomic,assign)int QuestionScored;//该问题已获评分
@property(nonatomic,strong)NSString* QuestionContent;//问题内容
@property(nonatomic,strong)NSArray* QuestionSolutions;//问题的正确方法
@property(nonatomic,strong)NSString* QuestionAnswer;//学生给出的问题解答
@property(nonatomic,assign)int QuestionCoin;//悬赏积分
@property(nonatomic,strong)NSString* AnswerId;//问题ID
@property(nonatomic,strong)NSDate* startDate;//起始时间
@property(nonatomic,strong)NSMutableArray* answerComments;//问题点评
@property(nonatomic,strong)NSMutableArray* answerCommentsTime;//点评时间
-(void)updateWithDictionary:(NSDictionary*)dic;

@end
