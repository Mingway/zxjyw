//
//  greenCell.m
//  zxjyw
//
//  Created by HelyData on 13-10-9.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "practiseCell.h"
#import "tools.h"
#import "ResourseDefine.h"

@interface practiseCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *havePictureImageView;
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;
@end

@implementation practiseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)resolvingWithQuestion:(question*)question
{
    self.currentQuestion = question;
    self.titleLabel.text = [NSString stringWithFormat:@"%@ %@ %i 总分值",question.QuestionCourse,question.QuestionStyle,question.QuestionScore];
    [self.questionContentWebView loadHTMLString:question.QuestionContent baseURL:nil];
    if([tools htmlIsIncludeImg:[self.currentQuestion.QuestionSolutions objectAtIndex:0]])
     {
         _havePictureImageView.hidden = NO;
     }
     else
    {
        _havePictureImageView.hidden = YES;
    }
}
@end
