//
//  registerVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "registerVC.h"
#import "UIHyperlinksButton.h"
#import "tools.h"
#import "userInfo.h"
#import "UIButton+Extensions.h"
#import "NetworkManager.h"
#import "ResourseDefine.h"
#import "waitView.h"

@interface registerVC ()<UITextFieldDelegate>
{
    NSDate* startDate;
    NSTimer* timer;
}
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *captcha;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *getCaptchaBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *getUserRegisterProtocolBtn;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *submitRegisterBtn;
@property (nonatomic, strong) NetworkManager* network;
@property (nonatomic, strong) userInfo* user;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;

@end

@implementation registerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        self.backGroundImageView.image = [UIImage imageNamed:@"registerVC_background_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    
	UIControl* resign = [[UIControl alloc]initWithFrame:self.view.bounds];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    self.username.delegate = self;
    self.password.delegate = self;
    self.captcha.delegate = self;
    [self.getCaptchaBtn setIsHaveUnderLine:YES];
    [self.getUserRegisterProtocolBtn setIsHaveUnderLine:YES];
    [self.submitRegisterBtn setIsHaveUnderLine:NO];
    [self setButtonRect];
}
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.getCaptchaBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [_checkBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.getUserRegisterProtocolBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.submitRegisterBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
    timer = nil;
}
-(void)resign
{
    [self.username resignFirstResponder];
    [self.password resignFirstResponder];
    [self.captcha resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateCount_down//更新倒计时
{
    self.username.enabled = NO;
    static int total_time = 60;
    NSDate* now = [tools localDate];
    int count_down = [now timeIntervalSinceDate:startDate];
    if (count_down < total_time) {
        count_down = total_time - count_down;
        self.timerLabel.text = [NSString stringWithFormat:@"%i秒后重新获取验证码",count_down];
    }
    else
    {
        self.timerLabel.text = @"";
        self.username.enabled = YES;
    }
}
- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            //获取验证码
            if ([tools checkTel:self.username.text]) {
                if (self.username.enabled == YES ) {
                    [self.view addSubview:[waitView getWaitView]];
                    [[waitView getWaitView]startAnimation];
                    [self.network sendRequest:[self.network createGetCaptchaCodeRequestWithMobieNumber:self.username.text andType:@"0"] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                        [[waitView getWaitView] stopAnimation];
                        [[waitView getWaitView]removeFromSuperview];
                        if (isSuccess) {
                            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                                startDate = [tools localDate];
                                timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
                            }
                            else
                            {
                                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                            }
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:errorMessage];
                        }
                    }];
                }
                else
                {
                    [tools showAlertView:@"提示" andMessage:@"请一分钟后重新获取"];
                }
            }
        }
            break;
        case 1:
        {
            //选中我已经阅读协议
            [sender setSelected:!sender.isSelected];
        }
            break;
        case 2:
        {
            //跳转用户注册协议页面
        }
            break;
        case 3:
        {
            if ([tools checkTel:self.username.text])
            {
                if([self.password.text isEqualToString:@""])
                {
                    [tools showAlertView:@"提示" andMessage:@"请填写您的账号密码！"];
                }
                else if (self.password.text.length < 6||self.password.text.length >32)
                {
                     [tools showAlertView:@"提示" andMessage:@"两次密码不一致"];
                }
                else if ([self.captcha.text isEqualToString:@""])
                {
                    [tools showAlertView:@"提示" andMessage:@"请填写短信验证码！"];
                }
                else if (_checkBtn.isSelected == NO)
                {
                    [tools showAlertView:@"提示" andMessage:@"请在阅读并同意用户注册协议之后再提交注册信息！"];
                }
                else
                {
                    [self.view addSubview:[waitView getWaitView]];
                    [[waitView getWaitView]startAnimation];
                    [self.network sendRequest:[self.network createRegisterRequestWithUserName:self.username.text andPassword:self.password.text andCaptchaCode:self.captcha.text] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                        [[waitView getWaitView] stopAnimation];
                        [[waitView getWaitView]removeFromSuperview];
                        if (isSuccess) {
                            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                                self.user = [userInfo shareUserInfo];
                                self.user.loginName = self.username.text;
                                [self.user updateWithDic:data];
                                [tools saveToken:self.user.access_token LoginName:self.user.loginName];
                                self.user.coins = 0;
                                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                UIViewController *next = [board instantiateViewControllerWithIdentifier:@"addUserInfoVC"];
                                [self.navigationController pushViewController:next animated:YES];
                            }
                            else
                            {
                                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                            }
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:errorMessage];
                        }
                    }];
                }
            }
        }
            break;
        case 4:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        default:
            break;
    }
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
