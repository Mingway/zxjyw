//
//  exchangeVC.m
//  zxjyw
//
//  Created by HelyData on 13-10-5.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "exchangeVC.h"
#import "tools.h"
#import "NetworkManager.h"
#import "userInfo.h"
#import "exchangePlatformCell.h"
#import "exchangeDetailVC.h"
#import "UIButton+Extensions.h"
#import "ResourseDefine.h"
#import "waitView.h"

@interface exchangeVC ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic, strong)NetworkManager *network;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *exchageLabel;
@property (nonatomic, strong) userInfo *user;
@property (weak, nonatomic) IBOutlet UILabel *myCoinLabel;
@property (weak, nonatomic) IBOutlet UITextField *coinTextField;
@property (weak, nonatomic) IBOutlet UITableView *selectPlatformTableView;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;

@end

@implementation exchangeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.usernameLabel setFrame:CGRectOffset(self.usernameLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.myCoinLabel setFrame:CGRectOffset(self.myCoinLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];

        [self.coinTextField setFrame:CGRectOffset(self.coinTextField.frame, 0, -20)];
        [self.exchageLabel setFrame:CGRectOffset(self.exchageLabel.frame, 0, -20)];
        [self.selectPlatformTableView setFrame:CGRectMake(17, 192, 291, 200)];
        self.backGroundImageView.image = [UIImage imageNamed:@"exchangeVC_background_960.png"];
    }
	// Do any additional setup after loading the view.
    self.network = [[NetworkManager alloc]init];
    self.user = [userInfo shareUserInfo];
    self.exchageLabel.text = [NSString stringWithFormat:@"您还有%i个积分[10积分=1元]",self.user.coins];
    self.usernameLabel.text = self.user.loginName;
    self.myCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    UIControl* resign = [[UIControl alloc]initWithFrame:self.view.bounds];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    self.coinTextField.delegate = self;
    self.selectPlatformTableView.delegate = self;
    self.selectPlatformTableView.dataSource = self;
    [self setButtonRect];
}
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
}
-(void)resign
{
    [self.coinTextField resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            //back
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 1:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        case 2:
        {
            //userCenter
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *next = [board instantiateViewControllerWithIdentifier:@"userCenterVC"];
            [self.navigationController pushViewController:next animated:YES];
            
        }
            break;
        default:
            break;
    }
}
#pragma mark UITextField
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *exchangePlatformIdentifier = @"exchangePlatformCell";
    exchangePlatformCell* cell = [tableView dequeueReusableCellWithIdentifier:exchangePlatformIdentifier];
    if (!cell) {
        NSArray* platform = [[NSBundle mainBundle]loadNibNamed:exchangePlatformIdentifier owner:self options:nil];
        cell = [platform objectAtIndex:0];
        if (indexPath.row == 1) {
            [cell setPlatformImage:[UIImage imageNamed:@"cft.png"] andFrame:CGRectMake(90, 12, 111, 45)];
        }
        cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"exchangeDetailVC"];
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
@end
