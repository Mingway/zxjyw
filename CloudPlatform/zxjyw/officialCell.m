//
//  orangeCell.m
//  zxjyw
//
//  Created by HelyData on 13-10-9.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "officialCell.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "userInfo.h"
#import "ResourseDefine.h"
#import "waitView.h"

@interface officialCell ()
@property (weak, nonatomic) IBOutlet UILabel *coinLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *havePictureImageView;
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel1;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel2;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel3;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel4;
@property (weak, nonatomic) IBOutlet UIButton *getQuestionButton;
@property (nonatomic, strong) NetworkManager *network;
@end

@implementation officialCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)resolvingWithQuestion:(question*)question
{
    self.network = [[NetworkManager alloc]init];
    self.timeLabel1.text = @"0";
    self.timeLabel2.text = @"0";
    self.timeLabel3.text = @"0";
    self.timeLabel4.text = @"0";
    self.currentQuestion = question;
    self.coinLabel.text = [NSString stringWithFormat:@"%i",question.QuestionCoin];
    self.titleLabel.text = [NSString stringWithFormat:@"%@ %@ %i 总分值",question.QuestionCourse,question.QuestionStyle,question.QuestionScore];
    [self.questionContentWebView loadHTMLString:question.QuestionContent baseURL:nil];
     if([tools htmlIsIncludeImg:[self.currentQuestion.QuestionSolutions objectAtIndex:0]])
      {
        _havePictureImageView.hidden = NO;
       }
      else
      {
        _havePictureImageView.hidden = YES;
       }
    [self setButtonRect];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateCount_down) name:@"updateCountDown" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removeNotification) name:@"removeNotification" object:nil];;
}
- (void)removeNotification
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"updateCountDown" object:nil];
}
-(void)setButtonRect
{
    [self.getQuestionButton setHitTestEdgeInsets:UIEdgeInsetsMake(-40, -10, 0, 0)];
}
-(void)updateCount_down//更新倒计时
{
    static int total_time = 5*60;
    NSDate* now = [tools localDate];
    int count_down = [now timeIntervalSinceDate:self.currentQuestion.startDate];
    if (count_down < total_time) {
        count_down = total_time - count_down;
        self.timeLabel1.text = @"0";
        self.timeLabel2.text = [NSString stringWithFormat:@"%i",count_down/60];
        int second = count_down%60;
        self.timeLabel3.text = [NSString stringWithFormat:@"%i",second/10];
        self.timeLabel4.text = [NSString stringWithFormat:@"%i",second%10];
    }
    else
    {
        self.timeLabel1.text  = @"0";
        self.timeLabel2.text  = @"0";
        self.timeLabel3.text  = @"0";
        self.timeLabel4.text  = @"0";
        //移除该题
        [[NSNotificationCenter defaultCenter]postNotificationName:@"removePushQuestion" object:self.currentQuestion.AnswerId];
    }
}
- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            [self.window.rootViewController.view addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createRecieveQuestionRequestWithAnswerId:self.currentQuestion.AnswerId andType:1] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getWaitView] stopAnimation];
                [[waitView getWaitView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [self.delegate officialGetQuestion:self.currentQuestion];
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                    }
                }
                else
                {
                    [tools showAlertView:@"提示" andMessage:errorMessage];
                }
            }];
        }
            break;
        default:
            break;
    }
}
@end
