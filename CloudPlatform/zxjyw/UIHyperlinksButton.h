//
//  UIUnderlinedButton.h
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
// 下划线按钮

#import <UIKit/UIKit.h>

@interface UIHyperlinksButton : UIButton
{
    UIColor *lineColor;//下划线颜色
    BOOL isHighlight;//按钮是否高光
    BOOL isHaveUnderLine;//按钮是否含有下划线
}
-(void)setColor:(UIColor*)color;
-(void)setIsHaveUnderLine:(BOOL)flag;
+ (UIHyperlinksButton *) hyperlinksButton;
@end
