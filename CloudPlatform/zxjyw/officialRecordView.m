//
//  officialRecordView.m
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "officialRecordView.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "ResourseDefine.h"
#import "waitView.h"

#define SUBMIT_ALERTVIEW_TAG 1000
@interface officialRecordView()<UIGestureRecognizerDelegate>
{
    NSURL *recordedFile;//录音文件路径
    AVAudioPlayer *audioPlayer;//播放录音
    AVAudioRecorder *recorder;//录音
    NSTimer* timer;//记录时长和改变视图状态的Timer
    NSTimer* countTimer;
}
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;//问题内容webView
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;//显示得分的Label
@property (weak, nonatomic) IBOutlet UITableView *recordListTableView;
@property (weak, nonatomic) IBOutlet UIImageView *showVoiceImageView;//显示后面语音大小改变的图
@property (retain, nonatomic)userInfo* user;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *myCoinLabel;
@property (weak, nonatomic) recordCell* currentPlayCell;//当前播放cell
@property (strong, nonatomic) NSMutableArray* recordMutableArr;//记录录音文件路径的MutableArray
@property (strong, nonatomic) NSMutableArray* recordTimeMutableArr;//记录录音文件时长的MutableArray;
@property (nonatomic,assign)float time;//录音时长记录
@property (nonatomic) BOOL isRecording;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;//录音按钮，大的话筒的那个
@property (weak, nonatomic) IBOutlet UIButton *submitButton;//提交点评按钮
@property (weak, nonatomic) IBOutlet UIImageView *webViewBackImageView;//webView后面的绿色背景图
@property (weak, nonatomic) IBOutlet UIButton *webViewDragButton;//webView下面的半透明长条
@property (weak, nonatomic) IBOutlet UIButton *updateScoreButton;//修改得分按钮
@property (nonatomic, strong) NetworkManager *network;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;//背景图
@property (weak, nonatomic) IBOutlet UILabel *total_pointLabel;//总分Label
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;//个人中心按钮
@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;//个人中心上面的积分：字Label,主要用于适配调位置
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;//个人中心上面头像ImageView
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;//个人中心上的兑换按钮
@property (weak, nonatomic) IBOutlet UIImageView *scoreBackImageVuew;//得分显示背景图
@property (weak, nonatomic) IBOutlet UILabel *scoreTitleLabel;//显示得分的文字分的Label，主要用于适配调位置
@property (strong, nonatomic)NSMutableArray *answerConmmetsArr;//存储上传的录音id，拼成字符串

@end
@implementation officialRecordView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
//解析question
-(void)resolvingWithQuestion:(question*)question andScore:(int)score
{
    if (!inch4) {
        [self.recordButton setFrame:CGRectOffset(self.recordButton.frame, 0, -10)];
        [self.showVoiceImageView setFrame:CGRectMake(17, 153, 290, 127)];
        [self.submitButton setFrame:CGRectOffset(self.submitButton.frame, 0, -70)];
        [self.updateScoreButton setFrame:CGRectOffset(self.updateScoreButton.frame, 0, -70)];
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.userNameLabel setFrame:CGRectOffset(self.userNameLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.myCoinLabel setFrame:CGRectOffset(self.myCoinLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];
        [self.recordListTableView setFrame:CGRectMake(17, 285, 290, 100)];
        [self.scoreLabel setFrame:CGRectOffset(self.scoreLabel.frame, 0, -70)];
        [self.scoreTitleLabel setFrame:CGRectOffset(self.scoreTitleLabel.frame, 0, -70)];
        [self.scoreBackImageVuew setFrame:CGRectOffset(self.scoreBackImageVuew.frame, 0, -70)];
        self.backGroundImageView.image = [UIImage imageNamed:@"questionListRecordVC_background_960.png"];
    }
    self.submitButton.enabled = NO;
    [self bringSubviewToFront:self.webViewBackImageView];
    [self bringSubviewToFront:self.webViewDragButton];
    [self bringSubviewToFront:self.questionContentWebView];
    self.network = [[NetworkManager alloc]init];
    self.user = [userInfo shareUserInfo];
    self.userNameLabel.text = self.user.loginName;
    self.myCoinLabel.text = [NSString stringWithFormat:@"%i",_user.coins];
    self.currentQuestion = question;
    self.scoreLabel.text = [NSString stringWithFormat:@"%i",question.QuestionScore];
    [self.questionContentWebView loadHTMLString:question.QuestionContent baseURL:nil];
    self.scoreLabel.text = [NSString stringWithFormat:@"%i",score];
    self.total_pointLabel.text = [NSString stringWithFormat:@"%i",self.currentQuestion.QuestionScore];
    UITapGestureRecognizer* QuestionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeWebViewFrame:)];
    [QuestionTap setNumberOfTapsRequired:1];
    QuestionTap.delegate = self;
    [self.questionContentWebView addGestureRecognizer:QuestionTap];
    [self audio];
    [self setButtonRect];
    self.answerConmmetsArr = [NSMutableArray array];
    countTimer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
}
//页面小时停止timer
-(void)stopTimer
{
    [countTimer invalidate];
    countTimer = nil;
}
//从别的页面返回继续timer
-(void)continueTimer
{
    if (countTimer == nil) {
        countTimer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
    }
}
//更新倒计时
-(void)updateCount_down
{
    static int total_time = 5*60;
    NSDate* now = [tools localDate];
    int count_down = [now timeIntervalSinceDate:self.currentQuestion.startDate];
    if (count_down < total_time) {
        count_down = total_time - count_down;
        self.submitButton.enabled = YES;
    }
    else
    {
        [countTimer invalidate];
        countTimer = nil;
        [tools showAlertView:@"提示" andMessage:@"评分时间已过"];
        self.submitButton.enabled = NO;
    }
}
//改变webView的frame
-(void)changeWebViewFrame:(UITapGestureRecognizer*)tap
{
     [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
    if (self.questionContentWebView.frame.size.height > 300) {
        [UIView animateWithDuration:0.3f animations:^{
            [self.questionContentWebView setFrame:CGRectMake(15, 70, 240,49)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 70, 240,49)];
            [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, -(302-49))];
        } completion:nil];
    }
    else
    {
         [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
            [self.questionContentWebView setFrame:CGRectMake(15, 70, 240,302)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 70, 240,302)];
            [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, 302-49)];
        } completion:nil];
    }
    if (self.webViewDragButton.imageView.image == [UIImage imageNamed:@"orangeLine_down"]) {
        [self.webViewDragButton setImage:[UIImage imageNamed:@"orangeLine_up"] forState:UIControlStateNormal];
    }else{
        [self.webViewDragButton setImage:[UIImage imageNamed:@"orangeLine_down"] forState:UIControlStateNormal];
    }
}
//改变按钮响应区域大小
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.recordButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -40, -10, -40)];
    [self.updateScoreButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, 0)];
    [self.submitButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, -10)];
}
//录音设置
- (void)audio
{
    self.recordMutableArr = [NSMutableArray array];
    self.recordTimeMutableArr = [NSMutableArray array];
    self.time = 0.00f;
    self.recordListTableView.delegate = self;
    self.recordListTableView.dataSource = self;
}
//终止录音
-(void)stopPlayer
{
    if ([audioPlayer isPlaying]) {
        self.showVoiceImageView.image = nil;
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
        [audioPlayer stop];
        [timer invalidate];
        timer = nil;
    }
}
//按钮方法汇总
- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            //修改评分返回上一级界面
            [self.delegate officialPop];
        }
            break;
        case 1:
        {
            //改变界面背景
            //提交点评
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"确认提交该题的评分？" message:[NSString stringWithFormat:@"评分：%@分\n语音点评：%i段",_scoreLabel.text,(int)self.recordMutableArr.count] delegate:self cancelButtonTitle:@"确认提交" otherButtonTitles:@"修改评分", nil];
            alert.tag = SUBMIT_ALERTVIEW_TAG;
            [alert show];
        }
            break;
        case 2:
        {
            [self.delegate officialUserCenter];
        }
            break;
        case 3:
        {
            [self.delegate officialExchange];
        }
            break;
            case 4:
        {
            [self.delegate officialLogoff];
        }
            break;
        case 5:
        {
            [self.delegate officialPushQuestionList];
        }
            break;
        default:
            break;
    }
}
//开始录音
- (IBAction)startRecord:(UIButton *)sender {
    if ([audioPlayer isPlaying]) {
        [audioPlayer stop];
        audioPlayer = nil;
        [timer invalidate];
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
    }
    self.time = 0.00f;
    if (timer == nil) {
        timer = [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(detectionVoice) userInfo:nil repeats:YES];
    }
    recordedFile = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"%@.aac",[tools getCurrentDateTOSting]]]];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *sessionError;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    if(session == nil)
        NSLog(@"Error creating session: %@", [sessionError description]);
    else
        [session setActive:YES error:nil];
    UInt32 doChangeDefault = 1;
    AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefault), &doChangeDefault);
    self.isRecording = YES;
    //录音设置
     NSMutableDictionary *recordSetting = [NSMutableDictionary dictionary];
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
    [recordSetting setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    [recordSetting setValue:[NSNumber numberWithBool:YES] forKey:AVLinearPCMIsBigEndianKey];
    [recordSetting setValue:[NSNumber numberWithBool:YES] forKey:AVLinearPCMIsFloatKey];
    recorder = [[AVAudioRecorder alloc] initWithURL:recordedFile settings:recordSetting error:nil];
    [recorder prepareToRecord];
    recorder.meteringEnabled=YES;
    [recorder record];
}
//取消录音
- (IBAction)cancelRecord:(UIButton *)sender {
    self.isRecording = NO;
    [recorder stop];
    [recorder deleteRecording];
    [timer invalidate];
    recorder = nil;
    timer = nil;
    self.showVoiceImageView.image = nil;
}
//完成录音
- (IBAction)endRecord:(UIButton *)sender {
    self.isRecording = NO;
    [recorder stop];
    [timer invalidate];
    timer = nil;
    recorder = nil;
    if (self.time > 2.0)
    {
        NSURL* tempURL = recordedFile;
        [self.recordMutableArr insertObject:tempURL atIndex:0];
        [self.recordTimeMutableArr insertObject:[NSString stringWithFormat:@"%f",_time] atIndex:0];
        [self.recordListTableView reloadData];
        [self.recordListTableView scrollsToTop];
    }
    else
    {
        [recorder deleteRecording];
        [tools showAlertView:@"提示" andMessage:@"录音时间太短，需要超过2秒！"];
    }
    self.showVoiceImageView.image = nil;
}
//此处方法进行针对声音的变化改变视图显示
-(void)detectionVoice
{
    self.time += 0.01f;
    [recorder updateMeters];
    const double alpha=0.5;
    double lowPassResults=pow(10, (0.05)*[recorder peakPowerForChannel:0]);
    lowPassResults=alpha*lowPassResults+(1.0-alpha)*lowPassResults;
    if (lowPassResults<0.1) {
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_1.png"];
    }
    else if(lowPassResults <0.2){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_2.png"];
    }
    else if(lowPassResults <0.3){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_3.png"];
    }
    else if(lowPassResults <0.4){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_4.png"];
    }
    else if(lowPassResults <0.5){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_5.png"];
    }
    else if(lowPassResults <0.6){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_6.png"];
    }
    else if(lowPassResults <0.7){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_7.png"];
    }
    else if(lowPassResults <0.8){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_8.png"];
    }
    else if(lowPassResults <0.9){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_9.png"];
    }
}
//播放时按钮背景的变化
-(void)changeBackgroundForPlay
{
    audioPlayer.meteringEnabled = YES;
    [audioPlayer updateMeters];
    const double alpha=0.5;
    double lowPassResults=pow(10, (0.05)*[audioPlayer peakPowerForChannel:0]);
    lowPassResults=alpha*lowPassResults+(1.0-alpha)*lowPassResults;
    if (lowPassResults<0.1) {
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_1.png"];
    }
    else if(lowPassResults <0.2){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_2.png"];
    }
    else if(lowPassResults <0.3){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_3.png"];
    }
    else if(lowPassResults <0.4){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_4.png"];
    }
    else if(lowPassResults <0.5){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_5.png"];
    }
    else if(lowPassResults <0.6){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_6.png"];
    }
    else if(lowPassResults <0.7){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_7.png"];
    }
    else if(lowPassResults <0.8){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_8.png"];
    }
    else if(lowPassResults <0.9){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_9.png"];
    }
    
    if (self.currentPlayCell.imageForVoice.image == [UIImage imageNamed:@"play_1.png"])
    {
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_2.png"];
    }
    else if(self.currentPlayCell.imageForVoice.image == [UIImage imageNamed:@"play_2.png"])
    {
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
    }
    else if(self.currentPlayCell.imageForVoice.image == [UIImage imageNamed:@"play_3.png"])
    {
        self.currentPlayCell.imageForVoice.image =[UIImage imageNamed:@"play_1.png"];
    }
}
#pragma mark UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordMutableArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* record_identifier = @"recordCell";
    recordCell* cell = [tableView dequeueReusableCellWithIdentifier:record_identifier];
    if (!cell) {
        NSArray* record = [[NSBundle mainBundle]loadNibNamed:record_identifier owner:self options:nil];
        if ([record count]>0) {
            cell = [record objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    cell.delegate = self;
    cell.time = [self.recordTimeMutableArr objectAtIndex:indexPath.row];
    [cell showTime];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark RecordCellDelegate
//主要处理录音的播放或停止和删除
-(void)play_or_pause:(recordCell *)cell
{
    if([audioPlayer isPlaying])
    {
        self.showVoiceImageView.image = nil;
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
        [audioPlayer stop];
        [timer invalidate];
        timer = nil;
    }
    else
    {
        NSIndexPath* indexPath = [self.recordListTableView indexPathForCell:cell];
        recordedFile = [self.recordMutableArr objectAtIndex:indexPath.row];
        NSError *playerError;
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:recordedFile error:&playerError];
        audioPlayer.delegate = self;
        if (audioPlayer == nil)
        {
            NSLog(@"ERror creating player: %@", [playerError description]);
        }
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *err = nil;
        [audioSession setCategory :AVAudioSessionCategoryPlayback error:&err];
        [audioPlayer play];
        self.currentPlayCell = cell;
        if (timer == nil)
        {
            timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(changeBackgroundForPlay) userInfo:nil repeats:YES];
        }
    }
    
}
//删除这条录音
-(void)deleteThisRecord:(recordCell *)cell
{
    if ([audioPlayer isPlaying]&&self.currentPlayCell == cell)
    {
        self.showVoiceImageView.image = nil;
        [audioPlayer stop];
        audioPlayer = nil;
        [timer invalidate];
        timer = nil;
    }
    NSIndexPath* indexPath = [self.recordListTableView indexPathForCell:cell];
    [self deleteOneFile: [[self.recordMutableArr objectAtIndex:indexPath.row] absoluteString]];
    [self.recordMutableArr removeObjectAtIndex:indexPath.row];
    [self.recordTimeMutableArr removeObjectAtIndex:indexPath.row];
    [self.recordListTableView reloadData];
}
//上传录音
-(void)uploadRecord
{
    if (self.recordMutableArr.count >0) {
        NSData *recordData = [NSData dataWithContentsOfURL: [self.recordMutableArr objectAtIndex:0]];
        NSString *dataString = [recordData base64Encoding];
        [self.network sendRequest:[self.network createUploadResourseRequestWithResData:dataString andTime:[(NSString*)[self.recordTimeMutableArr objectAtIndex:0]intValue]] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [self.recordMutableArr removeObjectAtIndex:0];
                    [self.recordTimeMutableArr removeObjectAtIndex:0];
                    [self.answerConmmetsArr addObject:[data objectForKey:@"res_id"]];
                    [self uploadRecord];
                }
                else
                {
                    [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error_str"]];
                }
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:errorMessage];
            }
        }];
    }
    else
    {
        //0,1,2,3 推送，激活，解冻， 练习
        [self addSubview:[waitView getWaitView]];
        [[waitView getWaitView]startAnimation];
        [self.network sendRequest:[self.network createSubmitResultRequestWithAnswerId:self.currentQuestion.AnswerId andType:0 andAnswerScore:[_scoreLabel.text intValue] andAnswerComments:self.answerConmmetsArr] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            [[waitView getWaitView] stopAnimation];
            [[waitView getWaitView]removeFromSuperview];
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [tools showAlertView:@"提示" andMessage:@"本次评分已提交到后台审核"];
                    [_delegate officialPushQuestionList];
                }
                else
                {
                    [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                }
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:errorMessage];
            }
        }];
    }
}
//根据路径删除某个文件
-(void)deleteOneFile:(NSString*)filePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:filePath error:nil];
}
#pragma mark AVAudioPlayerDelegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    self.showVoiceImageView.image = nil;
    audioPlayer = nil;
    [timer invalidate];
    timer = nil;
    self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags
{
    self.showVoiceImageView.image = nil;
    [audioPlayer stop];
    audioPlayer = nil;
    [timer invalidate];
    timer = nil;
    self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == SUBMIT_ALERTVIEW_TAG) {
        switch (buttonIndex) {
            case 1:
            {
                //修改评分
                //            [self.delegate officialPop];
                
            }
                break;
            case 0:
            {
                //上传录音
                [self uploadRecord];
            }
                break;
            default:
                break;
        }

    }
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return  YES;
    }
    else
    {
        return NO;
    }
}
@end
