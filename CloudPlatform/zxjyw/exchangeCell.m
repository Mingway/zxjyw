//
//  exchangeCell.m
//  zxjyw
//
//  Created by HelyData on 13-10-11.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "exchangeCell.h"

@interface exchangeCell ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *coinLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@end

@implementation exchangeCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)resolvingWithRecord:(exchangeRecord*)record
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.dateLabel.text = record.date;
        self.coinLabel.text = [NSString stringWithFormat:@"%i",record.coin];
        self.statusLabel.text = record.status;
        if ([record.status isEqualToString:@"兑换成功"]) {
            self.statusLabel.textColor = [UIColor colorWithRed:75/255.0 green:154/255.0 blue:133/255.0 alpha:1.0];
        }
        else if([record.status isEqualToString:@"兑换失败"])
        {
            self.statusLabel.textColor = [UIColor orangeColor];
        }
        else
        {
            self.statusLabel.textColor = [UIColor blackColor];
        }
    });
}

@end
