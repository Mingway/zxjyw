//
//  NetworkManager.m
//  zxjyw
//
//  Created by HelyData on 13-10-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "NetworkManager.h"
#import "NetRequest.h"
#import "UrlDefine.h"
#import "Reachability.h"
#import "tools.h"
#import "userInfo.h"

@interface NetworkManager ()
@property (nonatomic, strong) userInfo *user;
@end

@implementation NetworkManager
- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
-(userInfo *)user
{
    return [userInfo shareUserInfo];
}
-(BOOL)isNetworkAvailable
{
    //链接需要更改
    Reachability *r = [Reachability reachabilityWithHostname:@"demo703.tongyouxuetang.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
            return NO;
            break;
        case ReachableViaWWAN:
            // 使用3G网络
            return YES;
            break;
        case ReachableViaWiFi:
            // 使用WiFi网络
            return YES;
            break;
    }
}
- (BOOL)logonUserName:(NSString *)userName password:(NSString *)password completionHandler:(void (^)(BOOL isSuccess, NSDictionary *data, NSString *errorMessage))completionHandler
{
    if (!self.isNetworkAvailable) {
        completionHandler(NO,nil,@"请检查网络");
        return NO;
    }
    NetRequest *request = [[NetRequest alloc] init];
    request.url = URL_LOGIN;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObject:userName forKey:@"loginName"];
    [parameters setObject:[tools md5:[NSString stringWithFormat:@"%@zxjywusermd5",password]] forKey:@"password"];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]  != nil && ! [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
    {
        [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] forKey:@"deviceInfo"];
    }else{
        [parameters setObject:@"111111" forKey:@"deviceInfo"];
    }
    request.parameters = parameters;
    return [request sendAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        completionHandler(isSuccess,result,errorMessage);
        if (isSuccess) {
            if ([[result objectForKey:@"error_code"]intValue] == 0) {
                self.user.loginName = userName;
                [self.user updateWithDic:result];
                [tools saveToken:self.user.access_token LoginName:self.user.loginName];
            }
        }
    }];
}
- (NetRequest *)createLogoffRequest
{
    NetRequest *request = [[NetRequest alloc] init];
    request.tokenID = self.user.access_token;
    request.url = URL_LOGOFF;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    request.parameters = parameters;
    return request;
}
- (NetRequest *)createReloginRequest
{
    NetRequest *request = [[NetRequest alloc] init];
    request.tokenID = self.user.access_token;
    request.url = URL_RELOGIN;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    request.parameters = parameters;
    return request;
}
- (NetRequest *)createGetCourseListRequest
{
    NetRequest *request = [[NetRequest alloc] init];
    request.url = URL_GET_COURSELIST;
    return request;
}
- (NetRequest *)createRegisterRequestWithUserName:(NSString*)userName andPassword:(NSString*)password andCaptchaCode:(NSString*)captchaCode
{
    NetRequest *request = [[NetRequest alloc] init];
    request.url = URL_REGISTER;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObject:userName forKey:@"loginName"];
    [parameters setObject:[tools md5:[NSString stringWithFormat:@"%@zxjywusermd5",password]] forKey:@"password"];
    [parameters setObject:captchaCode forKey:@"captchaCode"];
    [parameters setObject:[tools localDate] forKey:@"timeStamp"];
    [parameters setObject:[tools md5:[tools dateString]] forKey:@"timeStamp_MD5"];
    [parameters setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"deviceToken"] forKey:@"deviceInfo"];
    [parameters setObject:[NSString stringWithFormat:@"%@ %@",[UIDevice currentDevice].systemName,[UIDevice currentDevice].systemVersion] forKey:@"osVersion"];
    request.parameters = parameters;
    return request;
}
- (NetRequest *)createGetCaptchaCodeRequestWithMobieNumber:(NSString*)mobileNumber andType:(NSString*)type
{
    NetRequest* request = [[NetRequest alloc]init];
    request.url = URL_GET_CAPTCHA;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObject:mobileNumber forKey:@"loginName"];
    [parameters setObject:type forKey:@"type"];
    request.parameters = parameters;
    return  request;
}
- (NetRequest *)createUpdateUserInfoRequestWithAvailableTime:(NSString*)availableTime andCourseId:(NSString*)courseId
{
    NetRequest* request = [[NetRequest alloc]init];
    request.tokenID = self.user.access_token;
    request.url = URL_UPDATE_USERINFO;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:availableTime forKey:@"availableTime"];
    [parameters setObject:courseId forKey:@"courseId"];
    request.parameters = parameters;
    return  request;
}
- (NetRequest *)createSubmitNewPasswordRequestWithUserName:(NSString*)userName andNewPassword:(NSString*)newPassword andCaptcha:(NSString*)captcha
{
    NetRequest* request = [[NetRequest alloc]init];
    request.url = URL_SUBMIT_PASSWORD;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObject:userName forKey:@"loginName"];
    [parameters setObject:[tools md5:[NSString stringWithFormat:@"%@zxjywusermd5",newPassword]] forKey:@"newPassword"];
    [parameters setObject:captcha forKey:@"captcha"];
    [parameters setObject:[tools localDate] forKey:@"timeStamp"];
    [parameters setObject:[tools md5:[tools dateString]] forKey:@"timeStamp_MD5"];
    request.parameters = parameters;
    return  request;
}
- (NetRequest *)createGetQuestionListRequestWithCount:(int)count
{
    NetRequest* request = [[NetRequest alloc]init];
    request.tokenID = self.user.access_token;
    request.url = URL_GET_QUESTION;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:[NSString stringWithFormat:@"%i",count] forKey:@"count"];
//    [parameters setObject:[NSString stringWithFormat:@"%i",self.user.courseId] forKey:@"courseId"];
    request.parameters = parameters;
    return  request;
}
- (NetRequest *)createSubmitResultRequestWithAnswerId:(NSString*)answerId andType:(int)type andAnswerScore:(int)answerScore andAnswerComments:(NSMutableArray*)answerComments
{
    NetRequest* request = [[NetRequest alloc]init];
    request.tokenID = self.user.access_token;
    request.url = URL_SUBMIT_RESULT;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:answerId forKey:@"answerId"];
    [parameters setObject:[NSString stringWithFormat:@"%i",type] forKey:@"type"];
    [parameters setObject:[NSString stringWithFormat:@"%i",answerScore] forKey:@"answerScore"];
    NSMutableString* comments = [NSMutableString string];
    for (NSInteger i = 0; i < answerComments.count; i ++) {
        [comments appendString:[answerComments objectAtIndex:i]];
        if (i != answerComments.count - 1) {
            [comments appendString:@","];
        }
    }
    [parameters setObject:comments forKey:@"answerComments"];
    request.parameters = parameters;
    return  request;
}
- (NetRequest *)createGetQuestionByAnswerIdRequestWithAnswerId:(NSString*)answerId
{
    NetRequest* request = [[NetRequest alloc]init];
    request.tokenID = self.user.access_token;
    request.url = URL_GET_QUESTION_BY_ANSWERID;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:answerId forKey:@"answerId"];
    request.parameters = parameters;
    return  request;
}
- (NetRequest *)createRecieveQuestionRequestWithAnswerId:(NSString*)answerId andType:(int)type
{
    NetRequest* request = [[NetRequest alloc]init];
    request.tokenID = self.user.access_token;
    request.url = URL_RECIEVE_QUESTION;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:answerId forKey:@"answerId"];
    [parameters setObject:[NSString stringWithFormat:@"%i",type] forKey:@"type"];
    request.parameters = parameters;
    return  request;
}
- (NetRequest *)createRefuseQuestionRequestWithAnswerId:(NSString*)answerId andRefuse_type:(int)refuse_type
{
    NetRequest* request = [[NetRequest alloc]init];
    request.tokenID = self.user.access_token;
    request.url = URL_REFUSE_QUESTION;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:answerId forKey:@"answerId"];
    [parameters setObject:[NSString stringWithFormat:@"%i",refuse_type] forKey:@"refuse_type"];
    request.parameters = parameters;
    return  request;
}
- (NetRequest *)createUpdatePasswordRequestWithPassword:(NSString*)password andNewPassword:(NSString*)newPassword
{
    NetRequest *request = [[NetRequest alloc] init];
    request.tokenID = self.user.access_token;
    request.url = URL_UPDATE_PASSWORD;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:[tools md5:[NSString stringWithFormat:@"%@zxjywusermd5",password]] forKey:@"password"];
    [parameters setObject:[tools md5:[NSString stringWithFormat:@"%@zxjywusermd5",newPassword]] forKey:@"newPassword"];
    [parameters setObject:[tools localDate] forKey:@"timeStamp"];
    [parameters setObject:[tools md5:[tools dateString]] forKey:@"timeStamp_MD5"];
    request.parameters = parameters;
    return request;
}
- (NetRequest*)createGetResourseRequestWithResID:(NSString*)res_id
{
    NetRequest *request = [[NetRequest alloc]init];
    request.url = URL_GET_RESOURCE;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:res_id forKey:@"res_id"];
    request.parameters = parameters;
    return request;
}
- (NetRequest *)createGetCoinRecordRequestWithPageIndex:(int)pageIndex andPageSize:(int)pageSize
{
    NetRequest *request = [[NetRequest alloc] init];
    request.tokenID = self.user.access_token;
    request.url = URL_GET_COIN_RECORD;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:[NSString stringWithFormat:@"%i",pageIndex] forKey:@"pageIndex"];
    [parameters setObject:[NSString stringWithFormat:@"%i",pageSize] forKey:@"pageSize"];
    request.parameters = parameters;
    return request;
}
- (NetRequest *)createUploadResourseRequestWithResData:(NSString*)resdata andTime:(int)time
{
    NetRequest *request = [[NetRequest alloc] init];
    request.tokenID = self.user.access_token;
    request.url = URL_ADD_RES;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    [parameters setObject:resdata forKey:@"resdata"];
    [parameters setObject:@"aac" forKey:@"restype"];
    [parameters setObject:@"1" forKey:@"prop_count"];
    [parameters setObject:[NSString stringWithFormat:@"time%i",time] forKey:@"prop_1"];
    request.parameters = parameters;
    return request;
}
- (NetRequest*)createGetPushQuestionRequest
{
    NetRequest *request = [[NetRequest alloc] init];
    request.tokenID = self.user.access_token;
    request.url = URL_GET_PUSH_QUESTION;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.user.access_token != nil) {
        [parameters setObject:self.user.access_token forKey:@"access_token"];
    }
    request.parameters = parameters;
    return request;
}
- (NetRequest*)createDownloadResourceRequestWithUrl:(NSString*)url
{
    NetRequest *request = [[NetRequest alloc] init];
    request.tokenID = self.user.access_token;
    request.url = url;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    request.parameters = parameters;
    return request;
}
- (BOOL)downloadReourceRequest:(NetRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSData *data, NSString *errorMessage))completionHandler
{
    if (!self.isNetworkAvailable) {
        completionHandler(NO,nil,@"请检查网络");
        return NO;
    }
    return [request downloadAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSData *result, NSString *errorMessage) {
        if (isSuccess) {
            NSLog(@"normal request:%@",result);
            completionHandler(isSuccess,result,errorMessage);
        }else{
            NSLog(@"error message:%@",errorMessage);
            completionHandler(isSuccess,result,errorMessage);
        }
    }];
}
- (BOOL)sendRequest:(NetRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSDictionary *data, NSString *errorMessage))completionHandler
{
    if (!self.isNetworkAvailable) {
        completionHandler(NO,nil,@"请检查网络");
        return NO;
    }
    return [request sendAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        if (isSuccess) {
            //error_code == 1 token失效
            if ([[result valueForKey:@"error_code"] intValue] == 1) {
                [tools deleteUser];
                if ([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:[UINavigationController class]]) {
                    UINavigationController *nav = (UINavigationController*)[UIApplication sharedApplication].keyWindow.rootViewController;
                    [nav popToRootViewControllerAnimated:YES];
                }
            }
            else
            {
                NSLog(@"normal request:%@",result);
                completionHandler(isSuccess,result,errorMessage);
            }
        }
        else
        {
            NSLog(@"error message:%@",errorMessage);
            completionHandler(isSuccess,result,errorMessage);
        }
    }];
}
@end
