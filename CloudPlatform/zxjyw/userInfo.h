//
//  UserInfo_data.h
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  用户信息类

#import <Foundation/Foundation.h>

@interface userInfo : NSObject
@property(nonatomic,strong)NSString* loginName;//手机号码
@property(nonatomic,strong)NSString* access_token;//token
@property(nonatomic,strong)NSString* AvailableTime;//可工作时间,0和1表示对应时间是否为可工作时间
@property(nonatomic,assign)int coins;//用户现有积分
@property(nonatomic,strong)NSString* course;//擅长科目
@property(nonatomic,assign)int status;//用户当前的状态,0表示已激活，1表示未激活，2表示冻结状态
@property(nonatomic,strong)NSString* deviceToken;
@property(nonatomic,strong)NSString* level;//用户等级
@property(nonatomic,assign)int courseId;//擅长科目Id
+(userInfo*)shareUserInfo;//单例
-(void)updateWithDic:(NSDictionary*)dic;
@end
