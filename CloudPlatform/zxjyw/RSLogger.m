//
//  BTLogger.m
//  Bumblebee Taxi
//
//  Created by Tim Huang on 13-1-30.
//  Copyright (c) 2013年 Beaver BI. All rights reserved.
//

#import "RSLogger.h"

#ifdef DEBUG
    static NSInteger __RSLogLevel = LOG_DEBUG;
#else
    static NSInteger __RSLogLevel = LOG_ERROR;
#endif

static FILE *logFile;

// matching display strings for the log levels.
#define __LOG_FATAL_STR     @"FATAL"
#define __LOG_ERROR_STR     @"ERROR"
#define __LOG_WARN_STR      @"WARN"
#define __LOG_INFO_STR      @"INFO"
#define __LOG_DEBUG_STR     @"DEBUG"

#define BTLOGGING_KEY       @"loggingKey"

@implementation RSLogger

+ (void)load
{
	[self setLogLeveLFromSettings];
    
#ifdef LOG_TO_FILE
    [self redirectLogToFile];
#endif

}

+ (void) redirectLogToFile
{
    logFile = freopen([[self createLogPath] cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
}

+(NSString *) createLogPath {
    NSArray *dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docDir = [dirs objectAtIndex:0];
	
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyyMMddHHmmss"];
	NSDate *curDate = [[NSDate alloc] init];
	NSString *curDateStr = [dateFormat stringFromDate:curDate];

	NSString *logFileName = [NSString stringWithFormat:@"BT_%@.log",curDateStr];
	NSString *logPath = [docDir stringByAppendingPathComponent:logFileName];
    
    [dateFormat release];
	[curDate release];
	return logPath;
}

+(void)withLevel:(NSInteger)level sourceFile:(const char*)sourceFile lineNumber:(int)lineNumber
          format:(NSString*)format, ...;
{
    if (__RSLogLevel == LOG_OFF)
        return;
    if (level > __RSLogLevel)
        return;
    
    va_list ap;
    NSString        *print,*file;
    
    va_start(ap,format);
    file=[[NSString alloc] initWithBytes:sourceFile
                                  length:strlen(sourceFile)
                                encoding:NSUTF8StringEncoding];
    print=[[NSString alloc] initWithFormat:format arguments:ap];
    va_end(ap);

    NSLog(@"[%@] [%s:%d] %@", [self levelString:level], [[file lastPathComponent] UTF8String],
          lineNumber, print);
    [file release];
    [print release];
    return;
}

+(BOOL)supportLevel:(NSInteger)level
{
    return __RSLogLevel != LOG_OFF && __RSLogLevel >= level;
}

+(void)setLogLevel:(NSInteger)level
{
    __RSLogLevel = level;
}

+(NSInteger)level
{
    return __RSLogLevel;
}

+(void)setLogLeveLFromSettings
{
	NSString *testValue = [[NSUserDefaults standardUserDefaults] stringForKey:BTLOGGING_KEY];
	if (testValue != nil)
	{
		[self setLogLevel: [[NSUserDefaults standardUserDefaults] integerForKey:BTLOGGING_KEY]];
	}
}

+(const NSString*)levelString:(NSInteger)level
{
    switch( level )
    {
        case LOG_FATAL: return __LOG_FATAL_STR;
        case LOG_ERROR:  return __LOG_ERROR_STR;
        case LOG_WARN:  return __LOG_WARN_STR;
        case LOG_INFO: return __LOG_INFO_STR;
        case LOG_DEBUG: return __LOG_DEBUG_STR;
            //case LOG_OFF:   return nil;
    }
    return nil;
}
@end
