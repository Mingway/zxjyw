//
//  startVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-23.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "startVC.h"
#import "userInfo.h"
#import "questionDetailVC.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "NetworkManager.h"
#import "ResourseDefine.h"
#import "waitView.h"

@interface startVC ()

@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgrounImageView;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (nonatomic, strong) userInfo *user;
@property (nonatomic, strong) NetworkManager *network;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
- (IBAction)pressBtn:(UIButton *)sender;
@end

@implementation startVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.network = [[NetworkManager alloc]init];
    self.user = [userInfo shareUserInfo];
    //适配
    if (!inch4) {
        [self.startBtn setFrame:CGRectOffset(self.startBtn.frame, 0, -45)];
        [self.userNameLabel setFrame:CGRectOffset(self.userNameLabel.frame, 0, -75)];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -75)];
        [self.updateButton setFrame:CGRectOffset(self.updateButton.frame, 0, -75)];
    }
    if (self.user.status == 1 && !inch4) {
        self.backgrounImageView.image = [UIImage imageNamed:@"startVC_active_background_960.png"];
    }
    if (self.user.status == 2) {
        if (!inch4) {
            self.backgrounImageView.image = [UIImage imageNamed:@"startVC_unfreeze_background_960.png"];
        }
        else
        {
            self.backgrounImageView.image = [UIImage imageNamed:@"startVC_unfreeze_background.png"];
        }
        [self.startBtn setImage:[UIImage imageNamed:@"startVC_btn_startUnfreeze.png"] forState:UIControlStateNormal];
    }
    self.userNameLabel.text = _user.loginName;
    [self setButtonRect];
}
-(void)setButtonRect
{
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.startBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            [self.view addSubview:[waitView getWaitView]];
            [[waitView getWaitView]startAnimation];
            [self.network sendRequest:[self.network createGetQuestionListRequestWithCount:1] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            [[waitView getWaitView] stopAnimation];
            [[waitView getWaitView]removeFromSuperview];
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    question* tempQUestion = [[question alloc]init];
                    [tempQUestion updateWithDictionary:[[data objectForKey:@"questionList"]objectAtIndex:0]];
                    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    questionDetailVC *next = [board instantiateViewControllerWithIdentifier:@"questionDetailVC"];
                    next.currentQuestion = tempQUestion;
                    [self.navigationController pushViewController:next animated:YES];
                }
                else
                {
                    [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                }
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:errorMessage];
            }
        }];
        }
            break;
        case 1:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        default:
            break;
    }
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
@end
