//
//  activeSuccessfulVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-26.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "successfulVC.h"
#import "UIHyperlinksButton.h"
#import "userInfo.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "NetworkManager.h"
#import "ResourseDefine.h"
#import "waitView.h"

@interface successfulVC ()
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *viewBtn;
@property (strong, nonatomic) userInfo* user;
@property (strong, nonatomic) NetworkManager *network;
- (IBAction)pressBtn:(UIButton *)sender;

@end

@implementation successfulVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.network = [[NetworkManager alloc]init];
    self.user = [userInfo shareUserInfo];
    
    if (!inch4) {
        [self.viewBtn setFrame:CGRectOffset(self.viewBtn.frame, 0, -22)];
        [self.startBtn setFrame:CGRectOffset(self.startBtn.frame, 0, -75)];
    }
    
    if (self.user.status == 1 &&self.coin == 20 &&!inch4) {
        self.backgroundImageView.image = [UIImage imageNamed:@"successfulVC_active_background_960.png"];
    }
    if (self.user.status == 1 && self.coin == 5) {
        if (!inch4) {
            self.backgroundImageView.image = [UIImage imageNamed:@"successfulVC_reActive_background_960.png"];
        }
        else{
            self.backgroundImageView.image = [UIImage imageNamed:@"successfulVC_reActive_background.png"];
        }
        self.user.coins+=5;
    }
    if (self.user.status == 1 && self.coin == 20) {
        self.user.coins += 20;
    }
    if (self.user.status == 2) {
        if (!inch4) {
            self.backgroundImageView.image = [UIImage imageNamed:@"successfulVC_unfreeze_background_960.png"];
        }
        else{
            self.backgroundImageView.image = [UIImage imageNamed:@"successfulVC_unfreeze_background.png"];
        }
        self.viewBtn.hidden = YES;
        [self.startBtn setImage:[UIImage imageNamed:@"successfulVC_btn_reStartJurney.png"] forState:UIControlStateNormal];
    }
    self.user.status = 0;
    [self setButtonRect];
}
-(void)setButtonRect
{
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.startBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.viewBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        default:
            break;
    }
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}

@end
