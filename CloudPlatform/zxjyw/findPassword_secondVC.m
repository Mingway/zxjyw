//
//  findPassword_secondVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-26.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "findPassword_secondVC.h"
#import "UIHyperlinksButton.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "NetworkManager.h"
#import "ResourseDefine.h"
#import "waitView.h"
#import "userInfo.h"

@interface findPassword_secondVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *confirmNewPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *captchaTextField;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *resendCaptchaBtn;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *checkMyMobileBtn;
@property (nonatomic,strong) NetworkManager *network;
@property (nonatomic, strong) userInfo *user;
- (IBAction)pressBtn:(UIHyperlinksButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *loginNameLabel;

@end

@implementation findPassword_secondVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.user = [userInfo shareUserInfo];
    if (!inch4) {
        self.backGroundImageView.image = [UIImage imageNamed:@"findPassword_secondVC_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    self.captchaTextField.delegate =self;
    self.loginNameLabel.text = self.mobile;
	[self.resendCaptchaBtn setIsHaveUnderLine:YES];
    [self.checkMyMobileBtn setIsHaveUnderLine:NO];
    UIControl* resign = [[UIControl alloc]initWithFrame:self.view.bounds];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    [self setButtonRect];
}
-(void)setButtonRect
{
    [self.resendCaptchaBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.checkMyMobileBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
-(void)resign
{
    [self.captchaTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.confirmNewPasswordTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIHyperlinksButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            //重发验证码
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 1:
        {
            //此处需要进行判断填写的验证码是否正确
            if ([self.captchaTextField.text isEqualToString:@""]) {
                [tools showAlertView:@"提示" andMessage:@"请输入获取的验证码进行验证！"];
            }else if([self.passwordTextField.text isEqualToString:@""])
            {
                [tools showAlertView:@"提示" andMessage:@"请输入新密码！"];
            }else if ([self.confirmNewPasswordTextField.text isEqualToString:@""]){
                 [tools showAlertView:@"提示" andMessage:@"请输入确认新密码！"];
            }else
            {
                if (![self.passwordTextField.text isEqualToString:self.confirmNewPasswordTextField.text]) {
                    [tools showAlertView:@"提示" andMessage:@"两次密码不一致"];
                }
                else if (self.passwordTextField.text.length < 6||self.passwordTextField.text.length >32)
                {
                    [tools showAlertView:@"提示" andMessage:@"密码长度应该在6-32之间"];
                }
                else{
                    [self.view addSubview:[waitView getWaitView]];
                    [[waitView getWaitView]startAnimation];
                    [self.network sendRequest:[self.network createSubmitNewPasswordRequestWithUserName:self.mobile andNewPassword:self.passwordTextField.text andCaptcha:self.captchaTextField.text] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                        [[waitView getWaitView] stopAnimation];
                        [[waitView getWaitView]removeFromSuperview];
                        if (isSuccess) {
                            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                                //提交新的密码成功后，转入问题列表界面
                                self.user.loginName = self.mobile;
                                [self.user updateWithDic:data];
                                [tools saveToken:self.user.access_token LoginName:self.user.loginName];
                                [self pushController];
                            }
                            else
                            {
                                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                            }
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:errorMessage];
                        }
                    }];
                }
            }
        }
            break;
        default:
            break;
    }
}
-(void)pushController
{
    if (self.user.AvailableTime == nil) {
        //没有添加用户信息,进入添加用户信息页面
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *next = [board instantiateViewControllerWithIdentifier:@"addUserInfoVC"];
        [self.navigationController pushViewController:next animated:YES];
    }
    else
    {
        if (self.user.status == 1 || self.user.status == 2) {
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *next = [board instantiateViewControllerWithIdentifier:@"startVC"];
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *next = [board instantiateViewControllerWithIdentifier:@"questionListVC"];
            [self.navigationController pushViewController:next animated:YES];
        }
    }
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
