//
//  orangeCell.h
//  zxjyw
//
//  Created by HelyData on 13-10-9.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "question.h"
#import "NetworkManager.h"

@protocol officialCellDelegate <NSObject>
-(void)officialGetQuestion:(question*)question;

@end

@interface officialCell : UITableViewCell
@property (nonatomic)id<officialCellDelegate>delegate;
@property (nonatomic,strong)question* currentQuestion;
- (IBAction)pressBtn:(UIButton *)sender;
-(void)resolvingWithQuestion:(question*)question;

@end
