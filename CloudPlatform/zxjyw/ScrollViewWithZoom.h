//
//  ScrollViewWithZoom.h
//  zxjyw
//
//  Created by HelyData on 13-9-24.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  图片缩放类

#import <UIKit/UIKit.h>
@interface ScrollViewWithZoom : UIScrollView
<UIScrollViewDelegate>
{
    UIImageView *imageView;
}

@property (nonatomic, weak) UIImageView *imageView;

-(void)setImage:(UIImage*)image;

@end
