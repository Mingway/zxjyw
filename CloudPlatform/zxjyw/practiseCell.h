//
//  greenCell.h
//  zxjyw
//
//  Created by HelyData on 13-10-9.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "question.h"


@interface practiseCell : UITableViewCell
@property (strong, nonatomic)question* currentQuestion;

-(void)resolvingWithQuestion:(question*)question;

@end
