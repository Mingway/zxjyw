//
//  tools.m
//  zxjyw
//
//  Created by HelyData on 13-9-23.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "tools.h"
#import "keychainUtils.h"
#import "CommonCrypto/CommonDigest.h"
#import "waitView.h"
#import "NetworkManager.h"

static NSString * const KEY_IN_KEYCHAIN = @"com.zxjyw.app.allinfo";
static NSString * const KEY_TOKEN = @"com.zxjyw.app.access_token";
static NSString * const KEY_LOGINNAME = @"com.zxjyw.app.loginName";

@implementation tools

+(void)showAlertView:(NSString*)title andMessage:(NSString*)message
{
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
}
+(NSString*)getCurrentDateTOSting
{
    NSString* date;
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYYMMddhhmmss"];
    date = [formatter stringFromDate:[NSDate date]];
    return date;
}
+(void)saveToken:(NSString*)access_token LoginName:(NSString*)loginName
{
    NSMutableDictionary *usernamepasswordKVPairs = [NSMutableDictionary dictionary];
    [usernamepasswordKVPairs setObject:access_token forKey:KEY_TOKEN];
    [usernamepasswordKVPairs setObject:loginName forKey:KEY_LOGINNAME];
    [keychainUtils save:KEY_IN_KEYCHAIN data:usernamepasswordKVPairs];
}
+(id)read
{
    NSMutableDictionary *usernamepasswordKVPair = (NSMutableDictionary *)[keychainUtils load:KEY_IN_KEYCHAIN];
    return usernamepasswordKVPair;
}

+(void)deleteUser
{
    [keychainUtils delete:KEY_IN_KEYCHAIN];
}

+(NSString *) md5: (NSString *) inPutText
{
    const char *cStr = [inPutText UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, strlen(cStr), result);
    return [[NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
             result[0], result[1], result[2], result[3],
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]
             ] lowercaseString];
}
+(CGSize)caculateCGSizeForText:(NSString*)text withFont:(UIFont*)font
{
    CGSize size = {320,2000};
    CGSize textSize = [text sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping];
    return textSize;
}

+(NSString*)dateString
{
    NSString* date;
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    date = [formatter stringFromDate:[self localDate]];
    return date;
}
+(NSDate*)localDate
{
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date];
    NSDate *locale = [date  dateByAddingTimeInterval: interval];
    return locale;
}
+(BOOL)htmlIsIncludeImg:(NSString*)html
{
    NSRange range=[html rangeOfString:@"<img"];
    if(range.length > 0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
+ (BOOL)checkTel:(NSString *)str
{
    if ([str length] == 0) {
        [tools showAlertView:@"提示" andMessage:@"请输入手机号码！"];
        return NO;
    }
    //1[0-9]{10}
    //^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$
    //    NSString *regex = @"[0-9]{11}";
    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:str];
    if (!isMatch) {
        [tools showAlertView:@"提示" andMessage:@"请输入正确的手机号"];
        return NO;
    }
    return YES;
}
+(NSString*)judgeTitleImageWithCourse:(NSString*)course
{
    NSRange range1 = [course rangeOfString:@"语文"];
    NSRange range2 = [course rangeOfString:@"数学"];
    NSRange range3 = [course rangeOfString:@"英语"];
    NSRange range4 = [course rangeOfString:@"物理"];
    //NSRange range5 = [course rangeOfString:@"化学"];
    if (range1.length > 0) {
        return @"course_chinese.png";
    }
    else if(range2.length >0)
    {
        return @"course_math.png";
    }
    else if(range3.length >0)
    {
        return @"course_english.png";
    }
    else if(range4.length >0)
    {
        return @"course_physics.png";
    }
    else
    {
        return @"course_ chemistry.png";
    }
}
@end
