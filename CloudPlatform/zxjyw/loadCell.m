//
//  loadCell.m
//  zxjyw
//
//  Created by HelyData on 13-10-21.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "loadCell.h"

@interface loadCell()

@end

@implementation loadCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.isLoading = NO;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
