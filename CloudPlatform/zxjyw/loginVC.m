//
//  ViewController.m
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "loginVC.h"
#import "tools.h"
#import "userInfo.h"
#import "UIHyperlinksButton.h"
#import "UIButton+Extensions.h"
#import "NetworkManager.h"
#import "ResourseDefine.h"
#import "waitView.h"

static NSString * const KEY_TOKEN = @"com.zxjyw.app.access_token";
static NSString * const KEY_LOGINNAME = @"com.zxjyw.app.loginName";

@interface loginVC ()<UITextFieldDelegate>
@property (strong,nonatomic) userInfo* user;
@property (weak, nonatomic) IBOutlet UITextField *username;//用户名，即手机号码
@property (weak, nonatomic) IBOutlet UITextField *password;//密码
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *forgetPasswordBtn;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *registerBtn;
@property (nonatomic, strong) NetworkManager* network;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;

- (IBAction)pressBtn:(UIButton *)sender;
@end

@implementation loginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        self.backGroundImageView.image = [UIImage imageNamed:@"loginVC_background_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    UIControl* resign = [[UIControl alloc]initWithFrame:self.view.bounds];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    self.username.delegate = self;
    self.password.delegate = self;
    [self.loginBtn setIsHaveUnderLine:NO];
    [self.forgetPasswordBtn setIsHaveUnderLine:YES];
    [self.registerBtn setIsHaveUnderLine:YES];
    [self setButtonRect];
    self.user = [userInfo shareUserInfo];
    NSDictionary *userInfo = [tools read];
    if (userInfo != nil) {
        self.user.access_token = [userInfo objectForKey:KEY_TOKEN];
        self.user.loginName = [userInfo objectForKey:KEY_LOGINNAME];
        [[UIApplication sharedApplication].keyWindow addSubview:[waitView getWaitView]];
        [[waitView getWaitView]startAnimation];
        [self.network sendRequest:[self.network createReloginRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            [[waitView getWaitView]stopAnimation];
            [[waitView getWaitView]removeFromSuperview];
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [self.user updateWithDic:data];
                    [tools saveToken:self.user.access_token LoginName:self.user.loginName];
                    [self pushController:NO];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"getPushQuestion" object:nil];
                }
            }
        }];
    }
}
-(void)pushController:(BOOL)animation
{
    if (self.user.AvailableTime  == nil) {
        //没有添加用户信息,进入添加用户信息页面
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *next = [board instantiateViewControllerWithIdentifier:@"addUserInfoVC"];
        [self.navigationController pushViewController:next animated:animation];
    }
    else
    {
        if (self.user.status == 1 || self.user.status == 2) {
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *next = [board instantiateViewControllerWithIdentifier:@"startVC"];
            [self.navigationController pushViewController:next animated:animation];
        }
        else
        {
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UIViewController *next = [board instantiateViewControllerWithIdentifier:@"questionListVC"];
            [self.navigationController pushViewController:next animated:animation];
        }
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.username.text = @"";
    self.password.text = @"";
}

- (void)login
{
    //转动等待提示
    [self.view addSubview:[waitView getWaitView]];
    [[waitView getWaitView]startAnimation];
    [self.network logonUserName:self.username.text password:self.password.text completionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [[waitView getWaitView] stopAnimation];
        [[waitView getWaitView]removeFromSuperview];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                if (![self.username.text isEqualToString:@""]) {
                    self.user.loginName = self.username.text;
                }
                [self.user updateWithDic:data];
                [tools saveToken:self.user.access_token LoginName:self.user.loginName];
                [self pushController:YES];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"getPushQuestion" object:nil];
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
            }
            
        }
        else
        {
            [tools showAlertView:@"提示" andMessage:errorMessage];
        }
    }];
}
-(void)setButtonRect
{
    [self.forgetPasswordBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.loginBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.registerBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
-(void)resign
{
    [self.username resignFirstResponder];
    [self.password resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            [sender setSelected:!sender.isSelected];
        }
            break;
        case 3:
        {
            if ([tools checkTel:self.username.text]) {
                if (![self.password.text isEqualToString:@""]) {
                    [self login];
                }
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
