//
//  exchangeDetailVC.m
//  zxjyw
//
//  Created by HelyData on 13-10-20.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "exchangeDetailVC.h"
#import "NetworkManager.h"
#import "userInfo.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "ResourseDefine.h"
#import "waitView.h"

@interface exchangeDetailVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *exchangeCoinLabel;
@property (weak, nonatomic) IBOutlet UILabel *myCoinLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UITextField *zfbAccountTextField;
@property (weak, nonatomic) IBOutlet UITextField *zfbAccountUserTextField;
@property (weak, nonatomic) IBOutlet UITextField *yunPasswordTextField;
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong) userInfo *user;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@end

@implementation exchangeDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.usernameLabel setFrame:CGRectOffset(self.usernameLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.myCoinLabel setFrame:CGRectOffset(self.myCoinLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];
        [self.cancelButton setFrame:CGRectOffset(self.cancelButton.frame, 0, -20)];
        [self.confirmButton setFrame:CGRectOffset(self.confirmButton.frame, 0, -20)];
        self.backGroundImageView.image = [UIImage imageNamed:@"exchangeDetailVC_background_960.png"];
    }
	// Do any additional setup after loading the view.
    self.network = [[NetworkManager alloc]init];
    self.user = [userInfo shareUserInfo];
    self.usernameLabel.text = self.user.loginName;
    self.myCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    UIControl* resign = [[UIControl alloc]initWithFrame:self.view.bounds];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    self.zfbAccountTextField.delegate = self;
    self.zfbAccountUserTextField.delegate = self;
    self.yunPasswordTextField.delegate = self;
    [self setButtonRect];
}
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
}
-(void)resign
{
    [self.zfbAccountTextField resignFirstResponder];
    [self.zfbAccountUserTextField resignFirstResponder];
    [self.yunPasswordTextField resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 1:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        default:
            break;
    }
}
#pragma  mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}

@end
