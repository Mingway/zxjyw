//
//  questionRecordVC.m
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "questionRecordVC.h"
#import "practiseRecordView.h"
#import "officialRecordView.h"
#import "tools.h"
#import "questionListVC.h"
#import "waitView.h"
#import "ResourseDefine.h"

@interface questionRecordVC ()<officialRecordViewDelegate,practiseRecordViewDelegate>

@end

@implementation questionRecordVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
//清空缓存
-(void)deleteFile {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *fileList ;
    fileList =[fileManager contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in fileList) {
        NSString *path =[NSTemporaryDirectory() stringByAppendingPathComponent:file];
        [fileManager removeItemAtPath:path error:nil];
    }
}
-(void)stopPlayer
{
    if ([self.currentQuestion.QuestionType isEqualToString:@"训练"]) {
        practiseRecordView* tempView  = (practiseRecordView*)self.view;
        [tempView stopPlayer];
    }
    else
    {
        officialRecordView* tempView = (officialRecordView*)self.view;
        [tempView stopPlayer];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (![self.currentQuestion.QuestionType isEqualToString:@"训练"]) {
         officialRecordView* tempView = (officialRecordView*)self.view;
        [tempView stopTimer];
    }
    [self stopPlayer];
    [self deleteFile];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (![self.currentQuestion.QuestionType isEqualToString:@"训练"]) {
        officialRecordView* tempView = (officialRecordView*)self.view;
        [tempView continueTimer];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.network = [[NetworkManager alloc]init];
	static NSString* practiseRecordView_identifier = @"practiseRecordView";
    static NSString* officialRecordView_identifier = @"officialRecordView";
    if ([self.currentQuestion.QuestionType isEqualToString:@"训练"]) {
        NSArray* practise = [[NSBundle mainBundle]loadNibNamed:practiseRecordView_identifier owner:self options:nil];
        if ([practise count]>0) {
            practiseRecordView* tempView = [practise objectAtIndex:0];
            tempView.delegate = self;
            self.view = tempView;
            [tempView resolvingWithQuestion:self.currentQuestion andScore:self.getScore];
        }
    }
    else
    {
        NSArray* official = [[NSBundle mainBundle]loadNibNamed:officialRecordView_identifier owner:self options:nil];
        if ([official count]>0) {
            officialRecordView* tempView = [official objectAtIndex:0];
            tempView.startDate = self.startDate;
            tempView.delegate = self;
            self.view = tempView;
            [tempView resolvingWithQuestion:self.currentQuestion andScore:self.getScore];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark officialRecordDelegate
-(void)officialExchange
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"exchangeVC"];
    [self.navigationController pushViewController:next animated:YES];
}
-(void)officialPop
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)officialPushQuestionList
{
    [self.view addSubview:[waitView getWaitView]];
    [[waitView getWaitView]startAnimation];
    [self.network sendRequest:[self.network createRefuseQuestionRequestWithAnswerId:self.currentQuestion.AnswerId andRefuse_type:0] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [[waitView getWaitView] stopAnimation];
        [[waitView getWaitView]removeFromSuperview];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                if (![self.currentQuestion.QuestionType isEqualToString:@"训练"]) {
                    officialRecordView* tempView = (officialRecordView*)self.view;
                    [tempView stopTimer];
                }
                for (NSInteger i = self.navigationController.viewControllers.count-1; i >= 0; i--) {
                    UIViewController* subViewController = [self.navigationController.viewControllers objectAtIndex:i];
                    if ([subViewController isKindOfClass:[questionListVC class]]) {
                        [self.navigationController popToViewController:subViewController animated:YES];
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"removePushQuestion" object:self.currentQuestion.AnswerId];
                    }
                }
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
            }
        }
        else
        {
            [tools showAlertView:@"提示" andMessage:errorMessage];
        }
    }];
//    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"questionListVC"];
//    [self.navigationController pushViewController:next animated:YES];
}
-(void)officialUserCenter
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"userCenterVC"];
    [self.navigationController pushViewController:next animated:YES];
}
-(void)officialLogoff
{
    //logoff
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.tag = LOGOFF_TAG;
    [alert show];
    alert = nil;
}
#pragma mark practiseRecordDelegate
-(void)practiseExchange
{
    [self officialExchange];
}
-(void)practiseLogoff
{
    [self officialLogoff];
}
-(void)practisePop
{
    [self stopPlayer];
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)practisePopToQuestionList
{
    [self stopPlayer];
    for (NSInteger i = self.navigationController.viewControllers.count-1; i >= 0; i--) {
        UIViewController* subViewController = [self.navigationController.viewControllers objectAtIndex:i];
        if ([subViewController isKindOfClass:[questionListVC class]]) {
            [self.navigationController popToViewController:subViewController animated:YES];
        }
    }
}
-(void)practiseUserCenter
{
    [self officialUserCenter];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self stopPlayer];
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
@end
