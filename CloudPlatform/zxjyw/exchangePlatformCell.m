//
//  exchangePlatformCell.m
//  zxjyw
//
//  Created by HelyData on 13-10-20.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "exchangePlatformCell.h"

@implementation exchangePlatformCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setPlatformImage:(UIImage*)image andFrame:(CGRect)frame
{
    [self.platformImageView setFrame:frame];
    self.platformImageView.image = image;
}
@end
