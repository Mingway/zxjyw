//
//  practiseRecordView.m
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "practiseRecordView.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "ResourseDefine.h"
#import "NetworkManager.h"
#import "waitView.h"
@interface practiseRecordView()<UIGestureRecognizerDelegate>
{
    NSURL *recordedFile;//录音文件路径
    AVAudioPlayer *audioPlayer;//播放录音
    AVAudioRecorder *recorder;//录音
    NSTimer* timer;//记录时长和改变视图状态的Timer
}
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;
@property (weak, nonatomic) IBOutlet UIImageView *showVoiceImageView;
@property (weak, nonatomic) IBOutlet UITableView *recordListTableView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *myCoinLabel;
@property (retain, nonatomic)userInfo* user;
@property (weak, nonatomic) recordCell* currentPlayCell;//当前播放cell
@property (strong, nonatomic) NSMutableArray* recordMutableArr;//记录录音文件路径的MutableArray
@property (strong, nonatomic) NSMutableArray* recordTimeMutableArr;//记录录音文件时长的MutableArray;
@property (weak, nonatomic) IBOutlet UIButton *webViewDragButton;
@property (weak, nonatomic) IBOutlet UIImageView *webViewBackImageView;
@property (weak, nonatomic) IBOutlet UIButton *viewScoreButton;//查看评分按钮
@property (weak, nonatomic) IBOutlet UIButton *viewQuestionButton;//查看答题按钮
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property (nonatomic,assign)float time;//录音时长记录
@property (nonatomic) BOOL isRecording;
@property (weak, nonatomic) IBOutlet UILabel *total_pointLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (nonatomic, strong) NetworkManager *network;

@end
@implementation practiseRecordView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
//解析question
-(void)resolvingWithQuestion:(question*)question andScore:(int)score
{
    self.network = [[NetworkManager alloc]init];
    if (!inch4) {
        [self.showVoiceImageView setFrame:CGRectMake(17, 153, 290, 127)];
        [self.viewQuestionButton setFrame:CGRectOffset(self.viewQuestionButton.frame, 0, -70)];
        [self.viewScoreButton setFrame:CGRectOffset(self.viewScoreButton.frame, 0, -70)];
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.userNameLabel setFrame:CGRectOffset(self.userNameLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.myCoinLabel setFrame:CGRectOffset(self.myCoinLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];
        [self.recordListTableView setFrame:CGRectMake(17, 285, 290, 100)];
        self.backGroundImageView.image = [UIImage imageNamed:@"questionListRecordVC_background_960.png"];
    }
    [self bringSubviewToFront:self.webViewBackImageView];
    [self bringSubviewToFront:self.webViewDragButton];
    [self bringSubviewToFront:self.questionContentWebView];
    self.currentQuestion = question;
    self.user = [userInfo shareUserInfo];
    self.userNameLabel.text = self.user.loginName;
    self.myCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    [self.questionContentWebView loadHTMLString:question.QuestionContent baseURL:nil];
    self.total_pointLabel.text = [NSString stringWithFormat:@"%i",self.currentQuestion.QuestionScore];
    UITapGestureRecognizer* QuestionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeWebViewFrame:)];
    QuestionTap.delegate = self;
    [QuestionTap setNumberOfTapsRequired:1];
    [self.questionContentWebView addGestureRecognizer:QuestionTap];
    [self audio];
    [self setButtonRect];
    //加载点评
    [self loadRecord];
}
-(void)loadRecord
{
    self.recordMutableArr = [NSMutableArray arrayWithArray:self.currentQuestion.answerComments];
    self.recordTimeMutableArr = [NSMutableArray arrayWithArray:self.currentQuestion.answerCommentsTime];
    [self.recordListTableView reloadData];
}
//改变webView的frame
-(void)changeWebViewFrame:(UITapGestureRecognizer*)tap
{
     [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
    if (self.questionContentWebView.frame.size.height > 300) {
        [UIView animateWithDuration:0.3f animations:^{
            [self.questionContentWebView setFrame:CGRectMake(15, 70, 240,49)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 70, 240,49)];
            [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, -(302-49))];
        } completion:nil];
    }
    else
    {
         [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
            [self.questionContentWebView setFrame:CGRectMake(15, 70, 240,302)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 70, 240,302)];
            [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, 302-49)];
        } completion:nil];
    }
    if (self.webViewDragButton.imageView.image == [UIImage imageNamed:@"greenLine_down"]) {
        [self.webViewDragButton setImage:[UIImage imageNamed:@"greenLine_up"] forState:UIControlStateNormal];
    }else{
        [self.webViewDragButton setImage:[UIImage imageNamed:@"greenLine_down"] forState:UIControlStateNormal];
    }
}
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.viewScoreButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, 0)];
    [self.viewQuestionButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, 0)];
}
//录音设置
- (void)audio
{
    self.recordMutableArr = [NSMutableArray array];
    self.recordTimeMutableArr = [NSMutableArray array];
    self.time = 0.00f;
    self.recordListTableView.delegate = self;
    self.recordListTableView.dataSource = self;
}
-(void)stopPlayer
{
    if ([audioPlayer isPlaying]) {
        self.showVoiceImageView.image = nil;
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
        [audioPlayer stop];
        [timer invalidate];
        timer = nil;
    }
}
- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            //查看评分
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"本题已评分数" message:[NSString stringWithFormat:@"评分：%i分",self.currentQuestion.QuestionScored] delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
            [alert show];
            
        }
            break;
        case 1:
        {
            //查看答题
            [self.delegate practisePop];
        }
            break;
        case 2:
        {
            [self.delegate practiseUserCenter];
        }
            break;
        case 3:
        {
            [self.delegate practiseExchange];
        }
            break;
        case 4:
        {
            [self.delegate practiseLogoff];
        }
            break;
        case 5:
        {
            [self.delegate practisePopToQuestionList];
        }
            break;
        default:
            break;
    }
}
//此处方法进行针对声音的变化改变视图显示
-(void)detectionVoice
{
    self.time += 0.01f;
    [recorder updateMeters];
    const double alpha=0.5;
    double lowPassResults=pow(10, (0.05)*[recorder peakPowerForChannel:0]);
    lowPassResults=alpha*lowPassResults+(1.0-alpha)*lowPassResults;
    if (lowPassResults<0.1) {
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_1.png"];
    }
    else if(lowPassResults <0.2){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_2.png"];
    }
    else if(lowPassResults <0.3){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_3.png"];
    }
    else if(lowPassResults <0.4){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_4.png"];
    }
    else if(lowPassResults <0.5){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_5.png"];
    }
    else if(lowPassResults <0.6){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_6.png"];
    }
    else if(lowPassResults <0.7){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_7.png"];
    }
    else if(lowPassResults <0.8){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_8.png"];
    }
    else if(lowPassResults <0.9){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_9.png"];
    }
}
//播放时按钮背景的变化
-(void)changeBackgroundForPlay
{
    audioPlayer.meteringEnabled = YES;
    [audioPlayer updateMeters];
    const double alpha=0.5;
    double lowPassResults=pow(10, (0.05)*[audioPlayer peakPowerForChannel:0]);
    lowPassResults=alpha*lowPassResults+(1.0-alpha)*lowPassResults;
    if (lowPassResults<0.1) {
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_1.png"];
    }
    else if(lowPassResults <0.2){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_2.png"];
    }
    else if(lowPassResults <0.3){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_3.png"];
    }
    else if(lowPassResults <0.4){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_4.png"];
    }
    else if(lowPassResults <0.5){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_5.png"];
    }
    else if(lowPassResults <0.6){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_6.png"];
    }
    else if(lowPassResults <0.7){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_7.png"];
    }
    else if(lowPassResults <0.8){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_8.png"];
    }
    else if(lowPassResults <0.9){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_9.png"];
    }
    
    if (self.currentPlayCell.imageForVoice.image == [UIImage imageNamed:@"play_1.png"])
    {
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_2.png"];
    }
    else if(self.currentPlayCell.imageForVoice.image == [UIImage imageNamed:@"play_2.png"])
    {
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
    }
    else if(self.currentPlayCell.imageForVoice.image == [UIImage imageNamed:@"play_3.png"])
    {
        self.currentPlayCell.imageForVoice.image =[UIImage imageNamed:@"play_1.png"];
    }
}
#pragma mark UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordMutableArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* record_identifier = @"recordCell";
    recordCell* cell = [tableView dequeueReusableCellWithIdentifier:record_identifier];
    if (!cell) {
        NSArray* record = [[NSBundle mainBundle]loadNibNamed:record_identifier owner:self options:nil];
        if ([record count]>0) {
            cell = [record objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    cell.deleteBtn.hidden = YES;
    cell.delegate = self;
    cell.time = [self.recordTimeMutableArr objectAtIndex:indexPath.row];
    [cell showTime];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark RecordCellDelegate
//主要处理录音的播放或停止和删除
-(void)play_or_pause:(recordCell *)cell
{
    if([audioPlayer isPlaying])
    {
        self.showVoiceImageView.image = nil;
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
        [audioPlayer stop];
        [timer invalidate];
        timer = nil;
    }
    else
    {
        NSIndexPath* indexPath = [self.recordListTableView indexPathForCell:cell];
        recordedFile = [self.recordMutableArr objectAtIndex:indexPath.row];
        [self addSubview:[waitView getWaitView]];
        [[waitView getWaitView]startAnimation];
        [self.network downloadReourceRequest:[self.network createDownloadResourceRequestWithUrl:[recordedFile absoluteString]] withCompletionHandler:^(BOOL isSuccess, NSData *data, NSString *errorMessage) {
            [[waitView getWaitView]stopAnimation];
            [[waitView getWaitView]removeFromSuperview];
            if (isSuccess) {
                NSError *playerError;
                audioPlayer = [[AVAudioPlayer alloc] initWithData:data error:&playerError];
                audioPlayer.delegate = self;
                if (audioPlayer == nil)
                {
                    NSLog(@"ERror creating player: %@", [playerError description]);
                }
                AVAudioSession *audioSession = [AVAudioSession sharedInstance];
                NSError *err = nil;
                [audioSession setCategory :AVAudioSessionCategoryPlayback error:&err];
                [audioPlayer play];
                self.currentPlayCell = cell;
                if (timer == nil)
                {
                    timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(changeBackgroundForPlay) userInfo:nil repeats:YES];
                }
            }
        }];
    }
    
}
-(void)deleteThisRecord:(recordCell *)cell
{
    if ([audioPlayer isPlaying]&&self.currentPlayCell == cell)
    {
        self.showVoiceImageView.image = nil;
        [audioPlayer stop];
        audioPlayer = nil;
        [timer invalidate];
        timer = nil;
    }
    NSIndexPath* indexPath = [self.recordListTableView indexPathForCell:cell];
    [self deleteOneFile: [[self.recordMutableArr objectAtIndex:indexPath.row] absoluteString]];
    [self.recordMutableArr removeObjectAtIndex:indexPath.row];
    [self.recordTimeMutableArr removeObjectAtIndex:indexPath.row];
    [self.recordListTableView reloadData];
}
//根据路径删除某个文件
-(void)deleteOneFile:(NSString*)filePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:filePath error:nil];
}
#pragma mark AVAudioPlayerDelegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    self.showVoiceImageView.image = nil;
    audioPlayer = nil;
    [timer invalidate];
    timer = nil;
    self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags
{
    self.showVoiceImageView.image = nil;
    [audioPlayer stop];
    audioPlayer = nil;
    [timer invalidate];
    timer = nil;
    self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
}
#pragma mark UIGestureDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return  YES;
    }
    else
    {
        return NO;
    }
}
@end
