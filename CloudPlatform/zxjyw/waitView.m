//
//  waitView.m
//  zxjyw
//
//  Created by HelyData on 13-11-13.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "waitView.h"
#import <QuartzCore/QuartzCore.h>
#import "ResourseDefine.h"

static waitView * defaulView = nil;

@interface waitView()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UIView *showView;
@property (weak, nonatomic) IBOutlet UIView *statusLabel;

@end

@implementation waitView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
+ (waitView*)getWaitView
{
    if (defaulView == nil) {
        NSArray *waitViewArr = [[NSBundle mainBundle]loadNibNamed:@"waitView" owner:self options:nil];
        defaulView = [waitViewArr  objectAtIndex:0];
        defaulView.showView.layer.cornerRadius = 5;
        if (!inch4) {
            [defaulView.statusLabel setFrame:CGRectOffset(defaulView.statusLabel.frame, 0, -50)];
        }
    }
    return defaulView;
}
- (void)startAnimation
{
    [self.activityIndicatorView startAnimating];
}
- (void)stopAnimation
{
    [self.activityIndicatorView stopAnimating];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
