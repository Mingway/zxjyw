//
//  questionListVC.h
//  zxjyw
//
//  Created by HelyData on 13-9-27.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  问题列表类，即正常用户登录之后看到的主视图

#import <UIKit/UIKit.h>

@interface questionListVC : UIViewController
-(void)addPushQuestion:(NSString*)answerId;
@end
