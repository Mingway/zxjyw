//
//  ResourseDefine.h
//  zxjyw
//
//  Created by HelyData on 13-10-21.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#ifndef zxjyw_ResourseDefine_h
#define zxjyw_ResourseDefine_h

#define MathJax nil

#define SACN_QUESTION_TIME_LIMIT  5*60.0f
#define SCORE_QUESTION_TIME_LIMIT  5*60.0f

//判断系统版本是否是iOS7
#define IOS7_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending )

//判断是否为4寸屏
#define inch4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define LOGOFF_TAG  100000

#endif
