//
//  findPassword_firstVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-26.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "findPassword_firstVC.h"
#import "UIHyperlinksButton.h"
#import "tools.h"
#import "findPassword_secondVC.h"
#import "UIButton+Extensions.h"
#import "NetworkManager.h"
#import "ResourseDefine.h"
#import "waitView.h"

@interface findPassword_firstVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *sendCaptchaBtn;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *registerBtn;
@property (nonatomic, strong) NetworkManager *network;
- (IBAction)pressBtn:(UIHyperlinksButton *)sender;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;

@end

@implementation findPassword_firstVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        self.backGroundImageView.image = [UIImage imageNamed:@"findPassword_firstVC_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    self.mobileTextField.delegate = self;
	[self.sendCaptchaBtn setIsHaveUnderLine:NO];
    [self.registerBtn setIsHaveUnderLine:YES];
    UIControl* resign = [[UIControl alloc]initWithFrame:self.view.bounds];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    [self setButtonRect];
    [self.backButton setIsHaveUnderLine:YES];
}
-(void)setButtonRect
{
    [self.sendCaptchaBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.registerBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
-(void)resign
{
    [self.mobileTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIHyperlinksButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            if ([tools checkTel:self.mobileTextField.text])
            {
                [self.view  addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createGetCaptchaCodeRequestWithMobieNumber:self.mobileTextField.text andType:@"1"] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            findPassword_secondVC *next = [board instantiateViewControllerWithIdentifier:@"findPassword_secondVC"];
                            next.mobile = self.mobileTextField.text;
                            [self.navigationController pushViewController:next animated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
        }
            break;
        case 1:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        default:
            break;
    }
}
-(void)failGetResponseFromInterface
{
    
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
