//
//  recordVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-24.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "recordVC.h"
#import "tools.h"
#import "recordCell.h"
#import "userInfo.h"
#import <AVFoundation/AVFoundation.h>
#import "NetworkManager.h"
#import "UIButton+Extensions.h"
#import "successfulVC.h"
#import "startVC.h"
#import "ResourseDefine.h"
#import "successfulVC.h"
#import "waitView.h"

#define SCORE_ALERT_TAG 1000   //提交评分alertTag
#define ERROR_ALERT_TAG 2000   //评分错误AlertTag



@interface recordVC ()<UITableViewDataSource,UITableViewDelegate,RecordCellDelegate,AVAudioPlayerDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    NSURL *recordedFile;//录音文件路径
    AVAudioPlayer *audioPlayer;//播放录音
    AVAudioRecorder *recorder;//录音
    NSTimer* timer;//记录时长和改变视图状态的Timer
}
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (nonatomic) BOOL isRecording;
@property (strong, nonatomic) userInfo* user;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *webViewBackImageView;//问题内容webView背景图
@property (weak, nonatomic) IBOutlet UIButton *webViewDragButton;//问题webView下方条
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;//问题内容webView
@property (weak, nonatomic) IBOutlet UILabel *totalPointLable;//显示问题总分数的Lable
@property (weak, nonatomic) IBOutlet UILabel *scoreLable;//显示已评评分的Lable
@property (weak, nonatomic) IBOutlet UITableView *recordListTableView;//录音列表的TableView
@property (weak, nonatomic) IBOutlet UIImageView *showVoiceImageView;
@property (weak, nonatomic) recordCell* currentPlayCell;//当前播放cell
@property (strong, nonatomic) NSMutableArray* recordMutableArr;//记录录音文件路径的MutableArray
@property (strong, nonatomic) NSMutableArray* recordTimeMutableArr;//记录录音文件时长的MutableArray;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UIButton *updateScoreButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (nonatomic,assign)float time;//录音时长记录
@property (nonatomic, strong) NetworkManager* network;
- (IBAction)pressBtn:(UIButton *)sender;
- (IBAction)startRecord:(UIButton *)sender;
- (IBAction)endRecord:(UIButton *)sender;
- (IBAction)cancelRecord:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *scoreTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *scoreBackLabel;

@end

@implementation recordVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.network = [[NetworkManager alloc]init];
    //从单例中获取当前用户信息
    self.user = [userInfo shareUserInfo];
    if (!inch4) {
        [self.recordButton setFrame:CGRectOffset(self.recordButton.frame, 0, -10)];
        [self.showVoiceImageView setFrame:CGRectMake(16, 173, 291, 120)];
        [self.scoreLable setFrame:CGRectOffset(self.scoreLable.frame, 0, -85)];
        [self.scoreTitleLabel setFrame:CGRectOffset(self.scoreTitleLabel.frame, 0, -85)];
        [self.scoreBackLabel setFrame:CGRectOffset(self.scoreBackLabel.frame, 0, -85)];
        [self.updateScoreButton setFrame:CGRectOffset(self.updateScoreButton.frame, 0, -85)];
        [self.submitButton setFrame:CGRectOffset(self.submitButton.frame, 0, -85)];
    }
    
    //根据用户的状态换背景图片
    if (self.user.status == 1) {
        if (!inch4) {
            self.backgroundImageView.image = [UIImage imageNamed:@"recordVC_active_background_960.png"];
        }
        else{
            self.backgroundImageView.image = [UIImage imageNamed:@"recordVC_active_background.png"];
        }
    }
    else if (self.user.status == 2) {
        if (!inch4) {
             self.backgroundImageView.image = [UIImage imageNamed:@"recordVC_unfreeze_background_960.png"];
        }
        else{
             self.backgroundImageView.image = [UIImage imageNamed:@"recordVC_unfreeze_background.png"];
        }
    }
    //初始化录音设置
    [self audio];
    //改变问题总分数
    self.totalPointLable.text = [NSString stringWithFormat:@"%i",self.currentQuestion.QuestionScore];
    //改变用户已评分数
    self.scoreLable.text = self.comment_point;
    //利用webView加载html文本
    [self loadHtml:self.currentQuestion.QuestionContent];
    self.recordListTableView.delegate = self;
    self.recordListTableView.dataSource = self;
    UITapGestureRecognizer* QuestionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeWebViewFrame:)];
    [QuestionTap setNumberOfTapsRequired:1];
    QuestionTap.delegate = self;
    [self.questionContentWebView addGestureRecognizer:QuestionTap];
    [self setButtonRect];
    [self.view  bringSubviewToFront:self.webViewBackImageView];
    [self.view bringSubviewToFront:self.questionContentWebView];
    [self.view bringSubviewToFront:self.webViewDragButton];
}
-(void)setButtonRect
{
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.recordButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -40, -10, -40)];
    [self.updateScoreButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, 0)];
    [self.submitButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, -10)];
}
//改变webView的frame
-(void)changeWebViewFrame:(UITapGestureRecognizer*)tap
{
    if (self.questionContentWebView.frame.size.height > 300) {
         [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
            [self.questionContentWebView setFrame:CGRectMake(15, 83, 240,49)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 83, 240,49)];
            [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, -(302-49))];
        } completion:nil];
    }
    else
    {
         [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
            [self.questionContentWebView setFrame:CGRectMake(15, 83, 240,302)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 83, 240,302)];
            [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, 302-49)];
        } completion:nil];
    }
    if (self.webViewDragButton.imageView.image == [UIImage imageNamed:@"greenLine_down"]) {
        [self.webViewDragButton setImage:[UIImage imageNamed:@"greenLine_up"] forState:UIControlStateNormal];
    }else{
        [self.webViewDragButton setImage:[UIImage imageNamed:@"greenLine_down"] forState:UIControlStateNormal];
    }
}
//webView加载html文本
-(void)loadHtml:(NSString *)html
{
    [self.questionContentWebView loadHTMLString:html baseURL:MathJax];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//录音设置
- (void)audio
{
    self.recordMutableArr = [NSMutableArray array];
    self.recordTimeMutableArr = [NSMutableArray array];
    self.time = 0.00f;
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 2:
        {
            if (self.user.status == 1) {
                if (!inch4) {
                    self.backgroundImageView.image = [UIImage imageNamed:@"submitActive_background_960.png"];
                }
                else{
                    self.backgroundImageView.image = [UIImage imageNamed:@"submitActive_background.png"];
                }
            }
            else if(self.user.status == 2)
            {
                if (!inch4) {
                    self.backgroundImageView.image = [UIImage imageNamed:@"submitUnfreeze_background_960.png"];
                }
                else{
                    self.backgroundImageView.image = [UIImage imageNamed:@"submitUnfreeze_background.png"];
                }
            }
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"确认提交该题的评分？" message:[NSString stringWithFormat:@"评分：%@分\n语音点评：%i段",self.comment_point,(int)self.recordMutableArr.count] delegate:self cancelButtonTitle:@"确认提交" otherButtonTitles:@"修改评分", nil];
            alert.tag = SCORE_ALERT_TAG;
            [alert show];
        }
            break;
        case 3:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        default:
            break;
    }
}
//开始录音
- (IBAction)startRecord:(UIButton *)sender {
    if ([audioPlayer isPlaying]) {
        [audioPlayer stop];
        audioPlayer = nil;
        [timer invalidate];
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
    }
    self.time = 0.00f;
    if (timer == nil) {
        timer = [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(detectionVoice) userInfo:nil repeats:YES];
    }
    recordedFile = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingString:[NSString stringWithFormat:@"%@",[tools getCurrentDateTOSting]]]];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSError *sessionError;
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    if(session == nil)
        NSLog(@"Error creating session: %@", [sessionError description]);
    else
        [session setActive:YES error:nil];
    UInt32 doChangeDefault = 1;
    AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefault), &doChangeDefault);
    self.isRecording = YES;
    recorder = [[AVAudioRecorder alloc] initWithURL:recordedFile settings:nil error:nil];
    [recorder prepareToRecord];
    recorder.meteringEnabled=YES;
    [recorder record];
}
//完成录音
- (IBAction)endRecord:(UIButton *)sender {
    self.isRecording = NO;
    [recorder stop];
    [timer invalidate];
    timer = nil;
    recorder = nil;
    if (self.time > 2.0)
    {
        NSURL* tempURL = recordedFile;
        [self.recordMutableArr insertObject:tempURL atIndex:0];
        [self.recordTimeMutableArr insertObject:[NSString stringWithFormat:@"%f",self.time] atIndex:0];
        [self.recordListTableView reloadData];
        [self.recordListTableView scrollsToTop];
    }
    else
    {
        [recorder deleteRecording];
        [tools showAlertView:@"提示" andMessage:@"录音时间太短，需要超过2秒！"];
    }
    self.showVoiceImageView.image = nil;
}
//取消录音
- (IBAction)cancelRecord:(UIButton *)sender {
    self.showVoiceImageView.image = nil;
    self.isRecording = NO;
    [recorder stop];
    [recorder deleteRecording];
    [timer invalidate];
    recorder = nil;
    timer = nil;
}
//此处方法进行针对声音的变化改变视图显示
-(void)detectionVoice
{
    self.time += 0.01f;
    [recorder updateMeters];
    const double alpha=0.5;
    double lowPassResults=pow(10, (0.05)*[recorder peakPowerForChannel:0]);
    lowPassResults=alpha*lowPassResults+(1.0-alpha)*lowPassResults;
    if (lowPassResults<0.1) {
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_1.png"];
    }
    else if(lowPassResults <0.2){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_2.png"];
    }
    else if(lowPassResults <0.3){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_3.png"];
    }
    else if(lowPassResults <0.4){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_4.png"];
    }
    else if(lowPassResults <0.5){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_5.png"];
    }
    else if(lowPassResults <0.6){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_6.png"];
    }
    else if(lowPassResults <0.7){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_7.png"];
    }
    else if(lowPassResults <0.8){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_8.png"];
    }
    else if(lowPassResults <0.9){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_9.png"];
    }
}
//播放时按钮背景的变化
-(void)changeBackgroundForPlay
{
    audioPlayer.meteringEnabled = YES;
    [audioPlayer updateMeters];
    const double alpha=0.5;
    double lowPassResults=pow(10, (0.05)*[audioPlayer peakPowerForChannel:0]);
    lowPassResults=alpha*lowPassResults+(1.0-alpha)*lowPassResults;
    if (lowPassResults<0.1) {
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_1.png"];
    }
    else if(lowPassResults <0.2){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_2.png"];
    }
    else if(lowPassResults <0.3){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_3.png"];
    }
    else if(lowPassResults <0.4){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_4.png"];
    }
    else if(lowPassResults <0.5){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_5.png"];
    }
    else if(lowPassResults <0.6){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_6.png"];
    }
    else if(lowPassResults <0.7){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_7.png"];
    }
    else if(lowPassResults <0.8){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_8.png"];
    }
    else if(lowPassResults <0.9){
        self.showVoiceImageView.image = [UIImage imageNamed:@"record_9.png"];
    }

    if (self.currentPlayCell.imageForVoice.image == [UIImage imageNamed:@"play_1.png"])
    {
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_2.png"];
    }
    else if(self.currentPlayCell.imageForVoice.image == [UIImage imageNamed:@"play_2.png"])
    {
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
    }
    else if(self.currentPlayCell.imageForVoice.image == [UIImage imageNamed:@"play_3.png"])
    {
        self.currentPlayCell.imageForVoice.image =[UIImage imageNamed:@"play_1.png"];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self deleteFile];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //此处上传录音文件
    //停止播放的音频
    if ([audioPlayer isPlaying]) {
        self.showVoiceImageView.image = nil;
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
        [audioPlayer stop];
        [timer invalidate];
        timer = nil;
    }
    //清空缓存录音文件
    [self deleteFile];
}
//清空缓存
-(void)deleteFile {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *fileList ;
    fileList =[fileManager contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in fileList) {
        NSString *path =[NSTemporaryDirectory() stringByAppendingPathComponent:file];
        [fileManager removeItemAtPath:path error:nil];
    }
}

//根据路径删除某个文件
-(void)deleteOneFile:(NSString*)filePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:filePath error:nil];
}

#pragma mark UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recordMutableArr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* record_identifier = @"recordCell";
    recordCell* cell = [tableView dequeueReusableCellWithIdentifier:record_identifier];
    if (!cell) {
        NSArray* record = [[NSBundle mainBundle]loadNibNamed:record_identifier owner:self options:nil];
        if ([record count]>0) {
            cell = [record objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    cell.delegate = self;
    cell.time = [self.recordTimeMutableArr objectAtIndex:indexPath.row];
    [cell showTime];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark RecordCellDelegate
//主要处理录音的播放或停止和删除
-(void)play_or_pause:(recordCell *)cell
{
    if([audioPlayer isPlaying])
    {
        self.showVoiceImageView.image = nil;
        self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
        [audioPlayer stop];
        [timer invalidate];
        timer = nil;
    }
    else
    {
        NSIndexPath* indexPath = [self.recordListTableView indexPathForCell:cell];
        recordedFile = [self.recordMutableArr objectAtIndex:indexPath.row];
        NSError *playerError;
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:recordedFile error:&playerError];
        audioPlayer.delegate = self;
        if (audioPlayer == nil)
        {
            NSLog(@"ERror creating player: %@", [playerError description]);
        }
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *err = nil;
        [audioSession setCategory :AVAudioSessionCategoryPlayback error:&err];
        [audioPlayer play];
        self.currentPlayCell = cell;
        if (timer == nil)
        {
            timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(changeBackgroundForPlay) userInfo:nil repeats:YES];
        }
    }

}
-(void)deleteThisRecord:(recordCell *)cell
{
    if ([audioPlayer isPlaying]&&self.currentPlayCell == cell)
    {
        self.showVoiceImageView.image = nil;
        [audioPlayer stop];
        audioPlayer = nil;
        [timer invalidate];
        timer = nil;
    }
    NSIndexPath* indexPath = [self.recordListTableView indexPathForCell:cell];
    [self deleteOneFile: [[self.recordMutableArr objectAtIndex:indexPath.row] absoluteString]];
    [self.recordMutableArr removeObjectAtIndex:indexPath.row];
    [self.recordTimeMutableArr removeObjectAtIndex:indexPath.row];
    [self.recordListTableView reloadData];
}
#pragma mark AVAudioPlayerDelegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    self.showVoiceImageView.image = nil;
    audioPlayer = nil;
    [timer invalidate];
    timer = nil;
    self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags
{
    self.showVoiceImageView.image = nil;
    [audioPlayer stop];
    audioPlayer = nil;
    [timer invalidate];
    timer = nil;
    self.currentPlayCell.imageForVoice.image = [UIImage imageNamed:@"play_3.png"];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == SCORE_ALERT_TAG) {
        switch (buttonIndex) {
            case 0:
            {
                //0,1,2,3 推送，激活，解冻， 练习
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                int type;
                if (self.user.status == 1) {
                    type = 1;
                }else{
                    type = 2;
                }
                [self.network sendRequest:[self.network createSubmitResultRequestWithAnswerId:self.currentQuestion.AnswerId  andType:type andAnswerScore:[self.scoreLable.text intValue] andAnswerComments:Nil] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            if (self.user.status == 1 || self.user.status == 2) {
                                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                successfulVC *next = [board instantiateViewControllerWithIdentifier:@"successfulVC"];
                                next.coin = [[data objectForKey:@"coin"]intValue];
                                [self.navigationController pushViewController:next animated:YES];
                            }
                        }else if ([[data objectForKey:@"error_code"]intValue] == 2){
                            //激活失败
                            if (self.user.status == 1) {
                                UIAlertView* alert =[[UIAlertView alloc]initWithTitle:@"提示" message:[data objectForKey:@"error_str"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                                alert.tag = ERROR_ALERT_TAG;
                                [alert show];
                            }
                            else
                            {
                                UIAlertView* alert =[[UIAlertView alloc]initWithTitle:@"提示" message:[data objectForKey:@"error_str"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                                alert.tag = ERROR_ALERT_TAG;
                                [alert show];
                            }
                            
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            case 1:
            {
                //修改评分，跳回本界面
                //根据用户的状态换背景图片
                if (self.user.status == 1) {
                    if (!inch4) {
                        self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_active_background_960.png"];
                    }
                    else{
                        self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_active_background.png"];
                    }
                }
                else if (_user.status == 2) {
                    if (!inch4) {
                        self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_unfreeze_background_960.png"];
                    }
                    else{
                        self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_unfreeze_background.png"];
                    }
                }
            }
                break;
            default:
                break;
        }    }
    else if (alertView.tag == ERROR_ALERT_TAG)
    {
        for (NSInteger i = self.navigationController.viewControllers.count-1; i >= 0; i--) {
            UIViewController* subViewController = [self.navigationController.viewControllers objectAtIndex:i];
            if ([subViewController isKindOfClass:[startVC class]]) {
                [self.navigationController popToViewController:subViewController animated:NO];
            }
        }
    }else if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                if ([audioPlayer isPlaying]) {
                    [audioPlayer stop];
                    audioPlayer = nil;
                }
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return  YES;
    }
    else
    {
        return NO;
    }
}
@end
