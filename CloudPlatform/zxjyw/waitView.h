//
//  waitView.h
//  zxjyw
//
//  Created by HelyData on 13-11-13.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface waitView : UIView

- (void)startAnimation;
- (void)stopAnimation;

+ (waitView*)getWaitView;

@end
