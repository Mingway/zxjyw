//
//  pullRefreshTableView.m
//  testRefreshTableView
//
//  Created by HelyData on 13-10-20.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "pullRefreshTableView.h"

#define LOADING_HEIGHT 60.0f

#define STATUSVIEW_HEIGHT  80.0f

@interface StatusView()
- (void)layouts;
@end
@implementation StatusView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        statusLabel = [[UILabel alloc]init];
        statusLabel.textColor = [UIColor whiteColor];
        statusLabel.backgroundColor = [UIColor clearColor];
        statusLabel.textAlignment = NSTextAlignmentCenter;
        statusLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        statusLabel.text = @"下拉刷新";
        [self addSubview:statusLabel];
        arrowView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        arrow = [CALayer layer];
        arrow.frame = CGRectMake(0, 0, 20, 20);
        arrow.contentsGravity = kCAGravityResizeAspect;
        arrow.contents = (id)[UIImage imageWithCGImage:[UIImage imageNamed:@"whiteArrow.png"].CGImage scale:1 orientation:UIImageOrientationDown].CGImage;
        [self.layer addSublayer:arrow];
        activityView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self addSubview:activityView];
        [self layouts];
    }
    return self;
}
- (void)layouts
{
    [statusLabel setFrame:CGRectMake(50, 0, 320 - 100, STATUSVIEW_HEIGHT)];
    UIImage *arrowImage = [UIImage imageNamed:@"whiteArrow.png"];
    [arrowView setFrame:CGRectMake(20, (STATUSVIEW_HEIGHT - 38)/2, 38, 38)];
    [arrow setFrame:CGRectMake(20, (STATUSVIEW_HEIGHT - 38)/2, 38, 38)];
    arrow.contents = (id)arrowImage.CGImage;
    activityView.center = arrowView.center;
    arrow.transform = CATransform3DIdentity;
}
- (void)setStatus:(currenrtStatus)status
{
    [self setStatus:status animated:YES];
}
- (void)setStatus:(currenrtStatus)status animated:(BOOL)animated
{
    float duration = 0.3f;
    if (_status != status) {
        _status = status;
        if (_status == StatusLoading) {    //Loading
            arrow.hidden = YES;
            activityView.hidden = NO;
            [activityView startAnimating];
            statusLabel.text = @"正在刷新";
        } else if (_status == StatusRelease ) {    //Release
            arrow.hidden = NO;
            activityView.hidden = YES;
            [activityView stopAnimating];
            [CATransaction begin];
            [CATransaction setAnimationDuration:duration];
            arrow.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            [CATransaction commit];
            statusLabel.text = @"释放刷新";
        } else if (_status == StatusDragging ){    //Dragging
            arrow.hidden = NO;
            activityView.hidden = YES;
            [activityView stopAnimating];
            [CATransaction begin];
            [CATransaction setAnimationDuration:duration];
            arrow.transform = CATransform3DIdentity;
            [CATransaction commit];
            statusLabel.text = @"下拉刷新";
        }
    }
    
}
@end
@interface PullRefreshTableView ()
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isDragging;
@end
@implementation PullRefreshTableView
-(void)addHeaderView
{
    self.delegate = self;
    CGRect rect = CGRectMake(0, -STATUSVIEW_HEIGHT, 320, STATUSVIEW_HEIGHT);
    headerView = [[StatusView alloc] initWithFrame:rect];
    [self addSubview:headerView];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        CGRect rect = CGRectMake(0, -STATUSVIEW_HEIGHT, 320, STATUSVIEW_HEIGHT);
        headerView = [[StatusView alloc] initWithFrame:rect];
        [self addSubview:headerView];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame pullDelegate:(id<PullRefreshTableViewDelegate>)delegate
{
    self = [self initWithFrame:frame];
    if (self) {
        self.pullDelegate = delegate;
    }
    return self;
}
#pragma mark UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    if (self.isLoading) {
        return;
    }
    self.isDragging = YES;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.isLoading) {
        if (scrollView.contentOffset.y > 0) {
            self.contentInset = UIEdgeInsetsZero;
        }else if (scrollView.contentOffset.y >= -LOADING_HEIGHT) {
            self.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
    }else if (self.isDragging && scrollView.contentOffset.y < 0 && scrollView.contentOffset.y >= -LOADING_HEIGHT) {
        headerView.status = StatusDragging;
    }
    else if (self.isDragging && scrollView.contentOffset.y <= -LOADING_HEIGHT)
    {
        headerView.status = StatusRelease;
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (self.isLoading) {
        return;
    }
    self.isDragging = NO;
    if (scrollView.contentOffset.y <= -LOADING_HEIGHT) {
        [self launchRefreshing];
    }
}
-(void)launchRefreshing
{
    self.isLoading = YES;
    headerView.status = StatusLoading;
    [self.pullDelegate loadData];
    [UIView animateWithDuration:0.3f animations:^{
        self.contentInset = UIEdgeInsetsMake(60.0f, 0, 0, 0);
    } completion:^(BOOL finished) {
        
    }];
}
-(void)loadingFinished
{
    [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    } completion:^(BOOL finished){
        headerView.status = StatusDragging;
        self.isLoading = NO;
    }];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.pullDelegate tableView:tableView heightForRowAtIndexPath:indexPath];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.pullDelegate tableView:tableView didSelectRowAtIndexPath:indexPath];
}
@end
