//
//  loadCell.h
//  zxjyw
//
//  Created by HelyData on 13-10-21.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loadCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (nonatomic, assign) BOOL isLoading;
@end
