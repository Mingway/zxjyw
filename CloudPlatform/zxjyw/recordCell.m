//
//  recordCell.m
//  zxjyw
//
//  Created by HelyData on 13-9-24.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "recordCell.h"
#import "UIButton+Extensions.h"

@interface recordCell ()
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end

@implementation recordCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)play_or_pause:(UIButton*)button
{
    [self.delegate play_or_pause:self];
}

-(void)deleteThisRecord:(UIButton*)button
{
    [self.delegate deleteThisRecord:self];
}
-(void)showTime
{
    [self.recordBtn addTarget:self action:@selector(play_or_pause:) forControlEvents:UIControlEventTouchUpInside];
    [self.deleteBtn addTarget:self action:@selector(deleteThisRecord:) forControlEvents:UIControlEventTouchUpInside];
    NSString* timeStr = [NSString stringWithFormat:@"%@ \"",[self notRounding:[_time floatValue] afterPoint:2]];
    self.timeLabel.text = timeStr;
    [self.recordBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -40, -5, -10)];
    [self.deleteBtn setHitTestEdgeInsets:UIEdgeInsetsMake(-20, -5, -20, -40)];
}
//浮点型数保留几位小数，第一个参数是需要转化的浮点数，第二个参数是保留小数点后的位数
-(NSString *)notRounding:(float)price afterPoint:(int)position{
    NSDecimalNumberHandler* roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:position raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSDecimalNumber *ouncesDecimal;
    NSDecimalNumber *roundedOunces;
    ouncesDecimal = [[NSDecimalNumber alloc] initWithFloat:price];
    roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
    ouncesDecimal = nil;
    return [NSString stringWithFormat:@"%@",roundedOunces];
}
@end
