//
//  practiseQuestionDetailView.m
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "practiseQuestionDetailView.h"
#import "ScrollViewWithZoom.h"
#import "UIButton+Extensions.h"
#import "ResourseDefine.h"

//判断系统版本是否是iOS7
#define IOS7_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending )

@interface practiseQuestionDetailView ()<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *solutionButton1;
@property (weak, nonatomic) IBOutlet UIButton *solutionButton2;

@property (weak, nonatomic) IBOutlet UIButton *solutionButton3;
@property (weak, nonatomic) IBOutlet UIWebView *solutionWebView;
@property (weak, nonatomic) IBOutlet EGOImageView *answerImageView;
@property (weak, nonatomic) IBOutlet UIImageView *webViewBackImageView;

@property (weak, nonatomic) IBOutlet UIButton *webViewDragButton;

- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *myCoinLabel;
@property (nonatomic, retain)userInfo* user;
@property (nonatomic,assign)int decade;//评分十位数字
@property (nonatomic,assign)int theUnit;//个位数字
@property (nonatomic,assign)int row_num;//个位数字行数
@property (weak, nonatomic) IBOutlet UIButton *submitButton;//查看评分按钮
@property (weak, nonatomic) IBOutlet UIButton *commentButton;//查看点评
@property (nonatomic,assign)int selectDecade;//选中的十位数字
@property (nonatomic,assign)int selectTheUnit;//选中的个位数字
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;

@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

@end

@implementation practiseQuestionDetailView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)resolvingWithQuestion:(question*)question
{
    if (!inch4) {
        [self.submitButton setFrame:CGRectOffset(self.submitButton.frame, 0, 7)];
        [self.commentButton setFrame:CGRectOffset(self.commentButton.frame, 0, 7)];
        [self.solutionWebView setFrame:CGRectMake(23, 153, 280, 125)];
        [self.answerImageView setFrame:CGRectMake(23, 305, 280, 80)];
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.userNameLabel setFrame:CGRectOffset(self.userNameLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.myCoinLabel setFrame:CGRectOffset(self.myCoinLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];
        self.backGroundImageView.image = [UIImage imageNamed:@"questionListDetailVC_background_960.png"];
    }
    self.user = [userInfo shareUserInfo];
    self.userNameLabel.text= self.user.loginName;
    self.myCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    self.currentQuestion = question;
    self.scoreLabel.text = [NSString stringWithFormat:@"%i",question.QuestionScore];
    if (question.QuestionSolutions.count == 2) {
        self.solutionButton3.hidden = YES;
    }
    else if(question.QuestionSolutions.count == 1)
    {
        self.solutionButton2.hidden = YES;
        self.solutionButton3.hidden = YES;
    }
    [self.questionContentWebView loadHTMLString:question.QuestionContent baseURL:nil];
    [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:0]  baseURL:nil];
    self.answerImageView.imageURL = [NSURL URLWithString:question.QuestionAnswer];
    UITapGestureRecognizer* showBigImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showBigImage)];
    [self.answerImageView addGestureRecognizer:showBigImage];
    UITapGestureRecognizer* QuestionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeWebViewFrame:)];
    QuestionTap.delegate = self;
    [QuestionTap setNumberOfTapsRequired:1];
    [self.questionContentWebView addGestureRecognizer:QuestionTap];
    [self setButtonRect];
    UITapGestureRecognizer* SolutionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showBigSolutionWebView:)];
    [SolutionTap setNumberOfTapsRequired:1];
    SolutionTap.delegate = self;
    [self.solutionWebView addGestureRecognizer:SolutionTap];
    [self  bringSubviewToFront:self.webViewBackImageView];
    [self bringSubviewToFront:self.webViewDragButton];
    [self bringSubviewToFront:self.questionContentWebView];
}
//改变webView的frame
-(void)changeWebViewFrame:(UITapGestureRecognizer*)tap
{
    if (self.questionContentWebView.frame.size.height > 300) {
         [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
            [self.questionContentWebView setFrame:CGRectMake(15, 70, 240,49)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 70, 240,49)];
            [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, -(302-49))];
        } completion:nil];
    }
    else
    {
        [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
                [self.questionContentWebView setFrame:CGRectMake(15, 70, 240,302)];
                [self.webViewBackImageView setFrame:CGRectMake(15, 70, 240,302)];
                [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, 302-49)];
            } completion:nil];
    }
    if (self.webViewDragButton.imageView.image == [UIImage imageNamed:@"greenLine_down"]) {
        [self.webViewDragButton setImage:[UIImage imageNamed:@"greenLine_up"] forState:UIControlStateNormal];
    }else{
        [self.webViewDragButton setImage:[UIImage imageNamed:@"greenLine_down"] forState:UIControlStateNormal];
    }
}
-(void)showBigSolutionWebView:(UITapGestureRecognizer*)tap
{
    if (self.solutionWebView.frame.origin.x == 23) {
         [self.solutionWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.0f animations:^{[self bringSubviewToFront:self.solutionWebView];} completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3f animations:^{self.solutionWebView.frame = self.frame;}];
        }];
    }
    else
    {
         [self.solutionWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
            if (inch4) {
                self.solutionWebView.frame = CGRectMake(23, 153, 280, 145);
            }
            else
            {
                 [self.solutionWebView setFrame:CGRectMake(23, 153, 280, 125)];
            }
            } completion:^(BOOL finished) {
            [self insertSubview:self.solutionWebView belowSubview:self.webViewBackImageView];
        }];
    }
}
//改变按钮响应区域
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.solutionButton1 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, 0)];
    [self.solutionButton2 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, 0)];
    [self.solutionButton3 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, -10)];
    [self.submitButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, -10)];
    [self.commentButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
-(void)reSetRow_Num
{
    if (self.decade != 0 &&self.theUnit == 0) {
        self.row_num = 10;
    }
    else
    {
        self.row_num = self.theUnit + 1;
    }
}
-(void)showBigImage
{
    ScrollViewWithZoom* bigImage = [[ScrollViewWithZoom alloc]initWithFrame:self.bounds];
    [bigImage  setImage:self.answerImageView.image];
    [self addSubview:bigImage];
}
- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
            [self.delegate practisePop];
            break;
        case 1:
            [self.delegate practiseLogoff];
            break;
        case 3:
        {
            //查看评分
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"本题已评分数" message:[NSString stringWithFormat:@"评分：%i分",self.currentQuestion.QuestionScored] delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
            [alert show];
            
        }
            break;
        case 4:
        {
            //跳转点评界面点评
            [self.delegate practiseRecordwithScore:self.currentQuestion.QuestionScored];
        }
            break;
        case 5:
        {
            //跳转用户中心
            [self.delegate practiseUserCenter];
        }
            break;
        case 6:
        {
            //跳转兑换中心
            [self.delegate practiseExchange];
        }
        case 7:
        {
            [sender setSelected:YES];
            [self.solutionButton2 setSelected:NO];
            [self.solutionButton3 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:0]  baseURL:nil];
        }
            break;
        case 8:
        {
            [sender setSelected:YES];
            [self.solutionButton1 setSelected:NO];
            [self.solutionButton3 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:1]  baseURL:nil];
        }
            break;
        case 9:
        {
            [sender setSelected:YES];
            [self.solutionButton2 setSelected:NO];
            [self.solutionButton1 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:2]  baseURL:nil];
        }
            break;
        default:
            break;
    }
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return  YES;
    }
    else
    {
        return NO;
    }
}
@end
