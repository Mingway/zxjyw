//
//  UIUnderlinedButton.m
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "UIHyperlinksButton.h"

@implementation UIHyperlinksButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (UIHyperlinksButton*) hyperlinksButton {
    UIHyperlinksButton* button = [[UIHyperlinksButton alloc] init];
    return button;
}

-(void)setColor:(UIColor *)color{
    lineColor = color;
    [self setNeedsDisplay];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    isHighlight = YES;
    [self setColor:self.titleLabel.textColor];
}
-(void)setIsHaveUnderLine:(BOOL)flag
{
    isHaveUnderLine = flag;
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesMoved:touches withEvent:event];
    
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    isHighlight = NO;
    [self setColor:self.titleLabel.highlightedTextColor];
    
}
-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesCancelled:touches withEvent:event];
    [self setColor:self.titleLabel.highlightedTextColor];
}

- (void) drawRect:(CGRect)rect {
    CGRect textRect = self.titleLabel.frame;
    CGContextRef contextRef = UIGraphicsGetCurrentContext();

    if (isHighlight)
    {
        [self setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        lineColor = [UIColor grayColor];
    }
    else
    {
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        lineColor = [UIColor whiteColor];
    }
    if (isHaveUnderLine == YES)
    {
        CGFloat descender = self.titleLabel.font.descender;
        if([lineColor isKindOfClass:[UIColor class]]){
            CGContextSetStrokeColorWithColor(contextRef, lineColor.CGColor);
        }
        
        CGContextMoveToPoint(contextRef, textRect.origin.x+0.5, textRect.origin.y + textRect.size.height + descender+1 +0.5);
        CGContextAddLineToPoint(contextRef, textRect.origin.x + textRect.size.width+0.5, textRect.origin.y + textRect.size.height + descender+1+0.5);
        CGContextClosePath(contextRef);
        CGContextDrawPath(contextRef, kCGPathStroke);
    }
}
@end

