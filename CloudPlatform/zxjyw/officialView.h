//
//  officialView.h
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "question.h"
#import "userInfo.h"
#import "NetworkManager.h"

@protocol officialViewDelegate <NSObject>

-(void)officialPop;
-(void)officialLogoff;
-(void)officialUserCenter;
-(void)officialExchange;
-(void)officialGetQuestion;
@end

@interface officialView : UIView
@property (nonatomic)id<officialViewDelegate>delegate;
@property (retain,nonatomic) question* currentQuestion;//存储当前问题
- (IBAction)pressBtn:(UIButton *)sender;
-(void)resolvingWithQuestion:(question*)question;
-(void)stopTimer;
-(void)continueTimer;
@end
