//
//  questionUnfoldVC.m
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "questionUnfoldVC.h"
#import "questionListDetailVC.h"
#import "officialView.h"
#import "tools.h"
#import "waitView.h"
#import "ResourseDefine.h"

@interface questionUnfoldVC ()<officialViewDelegate>

@end

@implementation questionUnfoldVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    officialView* tempView = (officialView*)self.view;
    [tempView continueTimer];
}
-(void)viewWillDisappear:(BOOL)animated
{
    officialView* tempView = (officialView*)self.view;
    [tempView stopTimer];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.network = [[NetworkManager alloc]init];
    static NSString* officialView_identifier = @"officialView";
    NSArray* official = [[NSBundle mainBundle]loadNibNamed:officialView_identifier owner:self options:nil];
    if ([official count]>0) {
            officialView* tempView = [official objectAtIndex:0];
            tempView.delegate = self;
            self.view = tempView;
            [tempView resolvingWithQuestion:self.currentQuestion];
        }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark officialDelegate
-(void)officialLogoff
{
    //logoff
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.tag = LOGOFF_TAG;
    [alert show];
    alert = nil;
}
-(void)officialPop
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)officialUserCenter
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"userCenterVC"];
    [self.navigationController pushViewController:next animated:YES];
}
-(void)officialExchange
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *next = [board instantiateViewControllerWithIdentifier:@"exchangeVC"];
    [self.navigationController pushViewController:next animated:YES];
}
-(void)officialGetQuestion
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    questionListDetailVC *next = [board instantiateViewControllerWithIdentifier:@"questionListDetailVC"];
    next.currentQuestion = self.currentQuestion;
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
@end
