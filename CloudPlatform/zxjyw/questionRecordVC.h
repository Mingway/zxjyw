//
//  questionRecordVC.h
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "question.h"
#import "NetworkManager.h"

@interface questionRecordVC : UIViewController
@property(nonatomic,retain)question* currentQuestion;
@property (nonatomic, strong) NetworkManager *network;
@property(nonatomic,assign)int getScore;
@property(nonatomic,strong)NSDate* startDate;
@end
