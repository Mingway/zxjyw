//
//  exchangeCell.h
//  zxjyw
//
//  Created by HelyData on 13-10-11.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "exchangeRecord.h"

@interface exchangeCell : UITableViewCell
-(void)resolvingWithRecord:(exchangeRecord*)record;

@end
