//
//  UserInfo_data.m
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "userInfo.h"

static userInfo* user = nil;
@implementation userInfo

- (id)init
{
    self = [super init];
    if (self) {
        self.level = @"普通用户";
    }
    return self;
}
+(userInfo*)shareUserInfo
{
    if (user == nil) {
        user = [[userInfo alloc]init];
    }
    return user;
}
-(void)updateWithDic:(NSDictionary*)dic
{
    self.access_token = [dic objectForKey:@"access_token"];
    if ([dic objectForKey:@"course"] != [NSNull null]) {
        self.course = [dic objectForKey:@"course"];
    }
    if([dic objectForKey:@"status"] != [NSNull null]){
        self.status = [[dic objectForKey:@"status"] intValue];
    }
    if ([dic objectForKey:@"access_token"] != [NSNull null]) {
        self.access_token = [dic objectForKey:@"access_token"];
    }
    if ([dic objectForKey:@"coin"] != [NSNull null]) {
        self.coins = [[dic objectForKey:@"coin"]intValue];
    }
    if (![[dic objectForKey:@"availableTime"] isEqualToString:@""] && [dic objectForKey:@"availableTime"] != [NSNull null]) {
        self.course = [dic objectForKey:@"course"];
        self.AvailableTime = [dic objectForKey:@"availableTime"];
        self.courseId = [[dic objectForKey:@"courseId"]intValue];
    }
}
@end
