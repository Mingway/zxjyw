//
//  UrlDefine.h
//  zxjyw
//
//  Created by HelyData on 13-10-19.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#ifndef zxjyw_UrlDefine_h
#define zxjyw_UrlDefine_h

#define BASE_URL            @"http://demo703.tongyouxuetang.com/dataapi/cloud/"
#define RES_URL             @"http://demo703.tongyouxuetang.com/dataapi/res/"

#define URL_GET_CAPTCHA     [NSString stringWithFormat:@"%@getCaptcha.do",BASE_URL]
#define URL_REGISTER        [NSString stringWithFormat:@"%@register.do",BASE_URL]
#define URL_GET_COURSELIST  [NSString stringWithFormat:@"%@getCourseList.do",BASE_URL]
#define URL_LOGIN           [NSString stringWithFormat:@"%@login.do",BASE_URL]
#define URL_RELOGIN       [NSString stringWithFormat:@"%@relogin.do",BASE_URL]
#define URL_UPDATE_USERINFO [NSString stringWithFormat:@"%@updateUserInfo.do",BASE_URL]
#define URL_GET_QUESTION    [NSString stringWithFormat:@"%@getQuestion.do",BASE_URL]
#define URL_CHECK_CAPTCHA   [NSString stringWithFormat:@"%@checkCaptcha.do",BASE_URL]
#define URL_SUBMIT_PASSWORD [NSString stringWithFormat:@"%@submitNewPassword.do",BASE_URL]
#define URL_UPDATE_PASSWORD [NSString stringWithFormat:@"%@updatePassword.do",BASE_URL]
#define URL_SUBMIT_RESULT   [NSString stringWithFormat:@"%@submitResult.do",BASE_URL]
#define URL_GET_QUESTION_BY_ANSWERID  [NSString stringWithFormat:@"%@getQuestionByAnswerID.do",BASE_URL]
#define URL_GET_COIN_RECORD [NSString stringWithFormat:@"%@getCoinRecord.do",BASE_URL]
#define URL_REFUSE_QUESTION [NSString stringWithFormat:@"%@refuseQuestion.do",BASE_URL]
#define URL_RECIEVE_QUESTION [NSString stringWithFormat:@"%@recieveQuestion.do",BASE_URL]
#define URL_LOGOFF          [NSString stringWithFormat:@"%@logoff.do",BASE_URL]

#define URL_ADD_RES         [NSString stringWithFormat:@"%@add_res.do",RES_URL]
#define URL_GET_RESOURCE [NSString stringWithFormat:@"%@get_res.do",RES_URL]
#define URL_GET_PUSH_QUESTION  [NSString stringWithFormat:@"%@getPushQuestion.do",BASE_URL]
#endif
