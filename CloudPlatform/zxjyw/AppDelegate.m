//
//  AppDelegate.m
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "AppDelegate.h"
#import "tools.h"
#import "questionListVC.h"
#import "waitView.h"
#import "RSLogger.h"

@implementation AppDelegate
static void globalExceptionHandler(NSException *exception)
{
    RSLogError(@"MobileBI_CRASH: %@", exception);
    RSLogError(@"MobileBI_CallStack: %@", [exception callStackSymbols]);
    // Internal error reporting
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSSetUncaughtExceptionHandler(&globalExceptionHandler);
   // [self getEnabledRemoteNotificationTypes];
    self.isShowAlertView = NO;
    self.user = [userInfo shareUserInfo];
    self.answerIdArr = [NSMutableArray array];
    self.network = [[NetworkManager alloc]init];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getPushQuestion) name:@"getPushQuestion" object:nil];
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    //当程序未启动，用户接收到消息。需要在AppDelegate中的didFinishLaunchingWithOptions得到消息内容
    NSDictionary* payload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (payload)
    {
        NSString* alertStr = nil;
        NSDictionary *apsInfo = [payload objectForKey:@"aps"];
        NSObject *alert = [apsInfo objectForKey:@"alert"];
        if ([alert isKindOfClass:[NSString class]])
        {
            alertStr = (NSString*)alert;
        }
        else if ([alert isKindOfClass:[NSDictionary class]])
        {
            NSDictionary* alertDict = (NSDictionary*)alert;
            alertStr = [alertDict objectForKey:@"body"];
        }
        [self.answerIdArr addObject:[payload objectForKey:@"answerId"]];
    }
    return YES;
}
//判断是否开启接受推送
-(BOOL)getEnabledRemoteNotificationTypes
{
    if([[UIApplication sharedApplication] enabledRemoteNotificationTypes] == UIRemoteNotificationTypeNone)
    {
        [tools showAlertView:@"提示" andMessage:@"推送被关闭，不能接收题目"];
        return NO;
    }
    return YES;
}
-(void)pushToListVC:(NSString*)answerId
{
    UINavigationController *nav = (UINavigationController*)self.window.rootViewController;
    BOOL isHaveQuestionListVC = NO;
    for (NSInteger i = nav.viewControllers.count-1; i >= 0; i--) {
        UIViewController* subViewController = [nav.viewControllers objectAtIndex:i];
        if ([subViewController isKindOfClass:[questionListVC class]]) {
            [nav popToViewController:subViewController animated:NO];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"addPushQuestion" object:answerId];
            isHaveQuestionListVC = YES;
        }
    }
    if (!isHaveQuestionListVC) {
          UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        questionListVC * list = [board instantiateViewControllerWithIdentifier:@"questionListVC"];
          [nav popToRootViewControllerAnimated:NO];
          [nav pushViewController:list animated:NO];
          [list addPushQuestion:answerId];
    }
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)getPushQuestion
{
    NSLog(@"anserIdArr = %@",self.answerIdArr);
    [self.network sendRequest:[self.network createGetPushQuestionRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                if (![[data objectForKey:@"answerId"] isEqualToString:@""] && [data objectForKey:@"answerId"] != [NSNull null]) {
                    if (!self.isShowAlertView) {
                        [self.answerIdArr addObject:[data objectForKey:@"answerId"]];
                        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您有一道题需要评分" delegate:self cancelButtonTitle:@"接受" otherButtonTitles:@"不接受",nil];
                        [alertView show];
                        alertView = nil;
                        self.isShowAlertView = YES;
                    }
                }
            }
        }
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self getPushQuestion];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if (self.answerIdArr.count>0 &&!self.isShowAlertView) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您有一道题需要评分" delegate:self cancelButtonTitle:@"接受" otherButtonTitles:@"不接受",nil];
        [alertView show];
        alertView = nil;
        self.isShowAlertView = YES;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//iPhone 从APNs服务器获取deviceToken后回调此方法
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString* dt = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"deviceToken:%@", dt);
    [[NSUserDefaults standardUserDefaults]setObject:dt forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

//注册push功能失败 后 返回错误信息，执行相应的处理
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSLog(@"Push Register Error:%@", err.description);
}
//当程序在前台运行，接收到消息不会有消息提示（提示框或横幅）。当程序运行在后台，接收到消息会有消息提示，点击消息后进入程序，AppDelegate的didReceiveRemoteNotification函数会被调用（需要自己重写），消息做为此函数的参数传入
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)payload
{
    NSLog(@"remote notification: %@",[payload description]);
    NSString* alertStr = nil;
    NSDictionary *apsInfo = [payload objectForKey:@"aps"];
    NSObject *alert = [apsInfo objectForKey:@"alert"];
    if ([alert isKindOfClass:[NSString class]])
    {
        alertStr = (NSString*)alert;
    }
    else if ([alert isKindOfClass:[NSDictionary class]])
    {
        NSDictionary* alertDict = (NSDictionary*)alert;
        alertStr = [alertDict objectForKey:@"body"];
    }
    application.applicationIconBadgeNumber = 0;
    if ([application applicationState] == UIApplicationStateActive && alertStr != nil)
    {
        if (!self.isShowAlertView) {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:alertStr delegate:self cancelButtonTitle:@"接受" otherButtonTitles:@"不接受",nil];
            [alertView show];
            alert = nil;
            self.isShowAlertView = YES;
        }
    }
    [self.answerIdArr addObject:[payload objectForKey:@"answerId"]];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.isShowAlertView = NO;
    switch (buttonIndex) {
        case 0:
        {
            //接受，加载题目
            if (self.answerIdArr.count > 0) {
                [self.window.rootViewController.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createRecieveQuestionRequestWithAnswerId:[self.answerIdArr lastObject] andType:0] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [self pushToListVC:[self.answerIdArr lastObject]];
                            [self.answerIdArr removeLastObject];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
        }
            break;
        case 1:
        {
            //不接受
            if(self.answerIdArr.count > 0)
            {
                [self.network sendRequest:[self.network createRefuseQuestionRequestWithAnswerId:[self.answerIdArr  lastObject] andRefuse_type:0] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [self.answerIdArr removeLastObject];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
        }
            break;
        default:
            break;
    }
}

@end
