//
//  coinRecord.h
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  积分记录封装类

#import <Foundation/Foundation.h>

@interface coinRecord : NSObject
@property(nonatomic,assign)int coin;//积分数
@property(nonatomic,strong)NSString* reason;//获取该积分的原因
@property(nonatomic,strong)NSString* date;//获取该积分的时间
//@property(nonatomic,strong)NSString* questionType;//题目类型

-(void)updateWithDictionary:(NSDictionary*)dic;

@end
