//
//  coinRecord.m
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "coinRecord.h"

@implementation coinRecord

- (id)init
{
    self = [super init];
    if (self) {
        self.coin = 80;
        self.reason = @"评分奖励";
        self.date = @"2013-08-20 12:30:30";
    }
    return self;
}
-(void)updateWithDictionary:(NSDictionary*)dic
{
    self.coin = [[dic objectForKey:@"coin"]intValue];
    self.reason = [dic objectForKey:@"reason"];
    self.date = [dic objectForKey:@"date"];
}

@end
