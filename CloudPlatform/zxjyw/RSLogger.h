//
//  BTLogger.h
//  Bumblebee Taxi
//
//  Created by Tim Huang on 13-1-31.
//  Copyright (c) 2013年 Beaver BI. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LOG_OFF		0
#define LOG_FATAL	1
#define LOG_ERROR	2
#define LOG_WARN	3
#define LOG_INFO	4
#define LOG_DEBUG	5

#if defined __cplusplus
extern "C" {
#endif
    
#define RSLogFatal(s,...) \
            [RSLogger withLevel:LOG_FATAL sourceFile:__FILE__ lineNumber:__LINE__ \
            format:(s),##__VA_ARGS__]

#define RSLogError(s,...) \
            [RSLogger withLevel:LOG_ERROR sourceFile:__FILE__ lineNumber:__LINE__ \
            format:(s),##__VA_ARGS__]

#define RSLogWarn(s,...) \
            [RSLogger withLevel:LOG_WARN sourceFile:__FILE__ lineNumber:__LINE__ \
            format:(s),##__VA_ARGS__]

#define RSLogInfo(s,...) \
            [RSLogger withLevel:LOG_INFO sourceFile:__FILE__ lineNumber:__LINE__ \
            format:(s),##__VA_ARGS__]

#define RSLogDebug(s,...) \
            [RSLogger withLevel:LOG_DEBUG sourceFile:__FILE__ lineNumber:__LINE__ \
            format:(s),##__VA_ARGS__]
    
#define RSLogSupport(n) \
            [RSLogger supportLevel:(n)]
    
#if defined __cplusplus
};
#endif

@interface RSLogger : NSObject

+(void)withLevel:(NSInteger)level sourceFile:(const char*)sourceFile lineNumber:(int)lineNumber format:(NSString*)format, ...;
+(BOOL)supportLevel:(NSInteger)level;
+(NSInteger)level;
+(void)setLogLevel:(NSInteger)level;
+(void)setLogLeveLFromSettings;
+(const NSString*)levelString:(NSInteger)level;

@end
