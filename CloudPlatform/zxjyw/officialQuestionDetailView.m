//
//  officialQuestionDetailView.m
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "officialQuestionDetailView.h"
#import "ScrollViewWithZoom.h"
#import "UIButton+Extensions.h"
#import "tools.h"
#import "ResourseDefine.h"
#import "waitView.h"

#define TIP_ALERTVIEW_TAG 1000

//判断系统版本是否是iOS7
#define IOS7_OR_LATER   ( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending )
@interface officialQuestionDetailView()<UIGestureRecognizerDelegate>
{
    UIView* picker_background;
    UIPickerView* picker;
    NSTimer* timer;
    NSDate* startDate;
}
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *solutionButton1;
@property (weak, nonatomic) IBOutlet UIButton *solutionButton2;
@property (weak, nonatomic) IBOutlet UIButton *solutionButton3;
@property (weak, nonatomic) IBOutlet UIWebView *solutionWebView;
@property (weak, nonatomic) IBOutlet UIButton *setScoreButton;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *myCoinLabel;
@property (weak, nonatomic) IBOutlet EGOImageView *answerImageView;
@property (nonatomic,retain)question* currentQuestion;
@property (weak, nonatomic) IBOutlet UILabel *getScoreLabel;
@property (nonatomic, retain)userInfo* user;
@property (nonatomic,assign)int decade;//评分十位数字
@property (nonatomic,assign)int theUnit;//个位数字
@property (nonatomic,assign)int row_num;//个位数字行数
@property (nonatomic,assign)int selectDecade;//选中的十位数字
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIImageView *webViewBackImageView;
@property (weak, nonatomic) IBOutlet UIButton *webViewDragButton;
@property (nonatomic,assign)int selectTheUnit;//选中的个位数字
@property (nonatomic, strong) NetworkManager *network;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;

@end
@implementation officialQuestionDetailView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)resolvingWithQuestion:(question*)question
{
    if (!inch4) {
        [self.setScoreButton setFrame:CGRectOffset(self.setScoreButton.frame, 0, -70)];
        [self.getScoreLabel setFrame:CGRectOffset(self.getScoreLabel.frame, 0, -70)];
        [self.submitButton setFrame:CGRectOffset(self.submitButton.frame, 0, -70)];
        [self.commentButton setFrame:CGRectOffset(self.commentButton.frame, 0, -70)];
        [self.solutionWebView setFrame:CGRectMake(23, 153, 280, 125)];
        [self.answerImageView setFrame:CGRectMake(23, 305, 280, 80)];
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.userNameLabel setFrame:CGRectOffset(self.userNameLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.myCoinLabel setFrame:CGRectOffset(self.myCoinLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];
        self.backGroundImageView.image = [UIImage imageNamed:@"questionListDetailVC_background_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    [tools showAlertView:@"提示" andMessage:@"本题评分时间为5分钟"];
    self.user = [userInfo shareUserInfo];
    self.userNameLabel.text = self.user.loginName;
    self.myCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    self.currentQuestion = question;
    self.scoreLabel.text = [NSString stringWithFormat:@"%i",question.QuestionScore];
    if (question.QuestionSolutions.count == 2) {
        self.solutionButton3.hidden = YES;
    }
    else if(question.QuestionSolutions.count == 1)
    {
            self.solutionButton2.hidden = YES;
            self.solutionButton3.hidden = YES;
    }
    [self.questionContentWebView loadHTMLString:question.QuestionContent baseURL:nil];
    [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:0]  baseURL:nil];
    self.getScoreLabel.text = [NSString stringWithFormat:@"%i",question.QuestionScore];
    self.answerImageView.imageURL = [NSURL URLWithString:question.QuestionAnswer];
    UITapGestureRecognizer* showBigImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showBigImage)];
            [self.answerImageView addGestureRecognizer:showBigImage];
    //点击放大问题手势
    UITapGestureRecognizer* QuestionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeWebViewFrame:)];
    [QuestionTap setNumberOfTapsRequired:1];
    QuestionTap.delegate = self;
    [self.questionContentWebView addGestureRecognizer:QuestionTap];
    [self setButtonRect];
    //双击放大原题解答
    UITapGestureRecognizer* SolutionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showBigSolutionWebView:)];
    SolutionTap.delegate = self;
    [SolutionTap setNumberOfTapsRequired:1];
    [self.solutionWebView addGestureRecognizer:SolutionTap];
    //滑动原题解答手势
//    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(handleSwipeGesture:)];
//    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
//    swipeRight.delegate = self;
//    [self.solutionWebView addGestureRecognizer:swipeRight];
//    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
//    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
//    swipeLeft.delegate = self;
//    [self.solutionWebView addGestureRecognizer:swipeLeft];
    [self bringSubviewToFront:self.webViewBackImageView];
    [self bringSubviewToFront:self.webViewDragButton];
    [self bringSubviewToFront:self.questionContentWebView];
    startDate = [tools localDate];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
}
-(void)continueTimer
{
    if (timer == nil) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(updateCount_down) userInfo:nil repeats:YES];
    }
}
-(void)stopTimer
{
    [timer invalidate];
    timer = nil;
}
-(void)updateCount_down//更新倒计时
{
    static int total_time = 5*60;
    NSDate* now = [tools localDate];
    int count_down = [now timeIntervalSinceDate:self.currentQuestion.startDate];
    if (count_down < total_time) {
        count_down = total_time - count_down;
    }
    else
    {
        [timer invalidate];
        timer = nil;
        [tools showAlertView:@"提示" andMessage:@"评分时间已过"];
        self.submitButton.enabled = NO;
    }
}
//手势控制
-(void)handleSwipeGesture:(UISwipeGestureRecognizer*)swip
{
    switch (swip.direction) {
        case UISwipeGestureRecognizerDirectionLeft:
        {
            
        }
            break;
        case UISwipeGestureRecognizerDirectionRight:
        {
            
        }
            break;
        default:
            break;
    }
}
//改变webView的frame
-(void)changeWebViewFrame:(UITapGestureRecognizer*)tap
{
    if (self.questionContentWebView.frame.size.height > 300) {
         [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
            [self.questionContentWebView setFrame:CGRectMake(15, 70, 240,49)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 70, 240,49)];
            [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, -(302-49))];
        } completion:nil];
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
             [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
            [self.questionContentWebView setFrame:CGRectMake(15, 70, 240,302)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 70, 240,302)];
            [self.webViewDragButton setFrame:CGRectOffset(self.webViewDragButton.frame, 0, 302-49)];
        } completion:nil];
    }
    if (self.webViewDragButton.imageView.image == [UIImage imageNamed:@"orangeLine_down"]) {
        [self.webViewDragButton setImage:[UIImage imageNamed:@"orangeLine_up"] forState:UIControlStateNormal];
    }else{
        [self.webViewDragButton setImage:[UIImage imageNamed:@"orangeLine_down"] forState:UIControlStateNormal];
    }
}
-(void)showBigSolutionWebView:(UITapGestureRecognizer*)tap
{
    if (self.solutionWebView.frame.origin.x == 23) {
         [self.solutionWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.0f animations:^{[self bringSubviewToFront:self.solutionWebView];} completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3f animations:^{self.solutionWebView.frame = self.frame;}];
        }];
    }
    else
    {
         [self.solutionWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
            if (inch4) {
                self.solutionWebView.frame = CGRectMake(23, 153, 280, 145);
            }
            else
            {
                [self.solutionWebView setFrame:CGRectMake(23, 153, 280, 125)];
            }
        } completion:^(BOOL finished) {
            [self insertSubview:self.solutionWebView belowSubview:self.webViewBackImageView];
        }];
    }
}
//改变按钮响应区域
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.solutionButton1 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, 0)];
    [self.solutionButton2 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, 0)];
    [self.solutionButton3 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, -10)];
    [self.setScoreButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, 0)];
    [self.submitButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, -10)];
    [self.commentButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -30, -10)];
}
-(void)reSetRow_Num
{
    if (self.decade != 0 &&self.theUnit == 0) {
        self.row_num = 10;
    }
    else
    {
        self.row_num = self.theUnit + 1;
    }
}
-(void)showBigImage
{
    ScrollViewWithZoom* bigImage = [[ScrollViewWithZoom alloc]initWithFrame:self.bounds];
    [bigImage  setImage:self.answerImageView.image];
    [self addSubview:bigImage];
}
- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
            [self.delegate officialPop];
            break;
        case 1:
            [self.delegate officialLogoff];
            break;
        case 2:
        {
            if (picker_background == nil) {
                picker_background=[[UIView alloc] initWithFrame:self.bounds];
                picker_background.backgroundColor = [UIColor clearColor];
                //显示选择分数的picker
                self.decade = self.currentQuestion.QuestionScore/10;
                self.theUnit = self.currentQuestion.QuestionScore%10;
                [self reSetRow_Num];
                self.selectDecade = self.decade;
                self.selectTheUnit = self.theUnit;
                if (!inch4) {
                    picker=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 265, 320,100036)];
                }
                else
                {
                    picker=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 352, 320,100036)];
                }
                picker.backgroundColor = [UIColor clearColor];
                picker.delegate=self;
                picker.dataSource = self;
                picker.showsSelectionIndicator=YES;
                [picker_background addSubview:picker];
                UIImageView* backImageView = [[UIImageView alloc]initWithFrame:picker.frame];
                backImageView.backgroundColor = [UIColor whiteColor];
                backImageView.image = [UIImage imageNamed:@"pickerBackground.png"];
                [picker_background addSubview:backImageView];
                [picker_background sendSubviewToBack:backImageView];
                
                UIButton* confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
                if (!inch4) {
                    [confirmButton setFrame:CGRectMake(220, 265, 40, 20)];
                }
                else
                {
                    [confirmButton setFrame:CGRectMake(220, 357, 40, 20)];
                }
                [confirmButton addTarget:self action:@selector(confirmSelected:) forControlEvents:UIControlEventTouchUpInside];
                [picker_background addSubview:confirmButton];
                UIButton* cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
                if (!inch4) {
                    [cancelButton setFrame:CGRectMake(60, 265, 40, 20)];
                }
                else
                {
                    [cancelButton setFrame:CGRectMake(60, 357, 40, 20)];
                }
                [cancelButton addTarget:self action:@selector(cancelSelected:) forControlEvents:UIControlEventTouchUpInside];
                [picker_background addSubview:cancelButton];
            }
            [picker selectRow:self.decade inComponent:0 animated:NO];
            [picker selectRow:self.theUnit inComponent:1 animated:NO];
            [self addSubview:picker_background];
        }
            break;
        case 3:
        {
            //评分
            //换背景
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"确认提交该题的评分？" message:[NSString stringWithFormat:@"评分：%@分\n语音点评：0段",_getScoreLabel.text] delegate:self cancelButtonTitle:@"确认提交" otherButtonTitles:@"修改评分", nil];
            alert.tag = TIP_ALERTVIEW_TAG;
            [alert show];
        }
            break;
        case 4:
        {
            //跳转点评界面点评
            [self.delegate officialRecordwithScore:[self.getScoreLabel.text intValue]andStartDate:startDate];
        }
            break;
        case 5:
        {
            //跳转用户中心
            [self.delegate officialUserCenter];
        }
            break;
        case 6:
        {
            //跳转兑换中心
            [self.delegate officialExchange];
        }
        case 7:
        {
            [sender setSelected:YES];
            [self.solutionButton2 setSelected:NO];
            [self.solutionButton3 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:0]  baseURL:nil];
        }
            break;
        case 8:
        {
            [sender setSelected:YES];
            [self.solutionButton1 setSelected:NO];
            [self.solutionButton3 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:1]  baseURL:nil];
        }
            break;
        case 9:
        {
            [sender setSelected:YES];
            [self.solutionButton2 setSelected:NO];
            [self.solutionButton1 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:2]  baseURL:nil];
        }
            break;
        default:
            break;
    }
}
-(void)confirmSelected:(UIButton*)button
{
    if (self.selectDecade == 0) {
        self.getScoreLabel.text = [NSString stringWithFormat:@"%i",self.selectTheUnit];
    }
    else
    {
         self.getScoreLabel.text = [NSString stringWithFormat:@"%i%i",self.selectDecade,self.selectTheUnit];
    }
    [picker_background removeFromSuperview];
}
-(void)cancelSelected:(UIButton*)button
{
    [picker_background removeFromSuperview];
}
#pragma mark UIALerViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == TIP_ALERTVIEW_TAG) {
        switch (buttonIndex) {
            case 1:
            {
                //取消修改界面背景
                
            }
                break;
            case 0:
            {
                //0,1,2,3 推送，激活，解冻， 练习
                [self addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createSubmitResultRequestWithAnswerId:self.currentQuestion.AnswerId andType:0 andAnswerScore:[_getScoreLabel.text intValue] andAnswerComments:nil] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools showAlertView:@"提示" andMessage:@"本次评分已提交到后台审核"];
                            [_delegate officialPushQuestionList];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
#pragma mark UIPickerViewDelegate
// 返回显示的列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    return 2;
}
// 返回当前列显示的行数
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return self.decade + 1;
    }
    else
    {
        return self.row_num;
    }
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        if (self.decade != 0 &&self.theUnit == 0 && row == self.decade) {
            self.row_num = 1;
        }
        else if(self.decade > row)
        {
            self.row_num = 10;
        }
        else
        {
            if (self.theUnit == 0) {
                self.row_num = 10;
            }
            else
            {
                self.row_num = self.theUnit + 1;
            }
        }
        [pickerView reloadComponent:1];
    }
    return [NSString stringWithFormat:@"%li",(long)row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        if (self.decade != 0 &&self.theUnit == 0 && row == self.decade) {
            self.row_num = 1;
        }
        else if(self.decade > row)
        {
            self.row_num = 10;
        }
        else
        {
            if (self.theUnit == 0) {
                self.row_num = 10;
            }
            else
            {
                self.row_num = self.theUnit + 1;
            }
        }
        [pickerView reloadComponent:1];
    }
    if (component == 0) {
        self.selectDecade = row;
    }
    else
    {
        self.selectTheUnit = row;
    }
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return  YES;
    }
    else
    {
        return NO;
    }
}

@end
