//
//  AppDelegate.h
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"
#import "userInfo.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray* answerIdArr;
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, strong) userInfo *user;
@property (nonatomic, assign) BOOL isShowAlertView;//是否显示已AlertView
@end
