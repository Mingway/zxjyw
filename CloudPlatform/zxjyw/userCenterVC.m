//
//  userCenterVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-27.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "userCenterVC.h"
#import "UIHyperlinksButton.h"
#import "userInfo.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "NetworkManager.h"
#import "ResourseDefine.h"
#import "waitView.h"

@interface userCenterVC ()
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *updatePasswordButton;
@property (weak, nonatomic) IBOutlet UILabel *userLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *userCoinLabel;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *userCenterButton;
@property (weak, nonatomic) IBOutlet UILabel *userSkillLabel;
@property (retain, nonatomic)userInfo* user;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIButton *updateUesrInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property (nonatomic, strong) NetworkManager *network;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;

@end

@implementation userCenterVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, 10)];
        self.backGroundImageView.image = [UIImage imageNamed:@"userCenterVC_background_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    [self.updatePasswordButton setIsHaveUnderLine:YES];
    [self.userCenterButton setIsHaveUnderLine:YES];
    self.user = [userInfo shareUserInfo];
    self.userNameLabel.text = self.user.loginName;
    self.userCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    self.userSkillLabel.text = self.user.course;
    self.userLevelLabel.text = self.user.level;
    [self setButtonRect];
}
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.updatePasswordButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.userCenterButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.updateUesrInfoButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.exchangeButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.btn1 setSelected:NO];
    [self.btn2 setSelected:NO];
    [self.btn3 setSelected:NO];
    [self.btn4 setSelected:NO];
    [self.btn5 setSelected:NO];
    [self.btn6 setSelected:NO];
    [self initAvailableTimeButton];
}
-(void)initAvailableTimeButton
{
    NSArray *timeArr  = [self.user.AvailableTime componentsSeparatedByString:@","];
    if ([[timeArr objectAtIndex:0 ]isEqualToString:@"1"]) {
        [self.btn1 setSelected:!self.btn1.isSelected];
    }
    if ([[timeArr objectAtIndex:1 ]isEqualToString:@"1"])
    {
        [self.btn2 setSelected:!self.btn2.isSelected];
    }
    if ([[timeArr objectAtIndex:2 ]isEqualToString:@"1"])
    {
        [self.btn3 setSelected:!self.btn3.isSelected];
    }
    if ([[timeArr objectAtIndex:3 ]isEqualToString:@"1"])
    {
        [self.btn4 setSelected:!self.btn4.isSelected];
    }
    if ([[timeArr objectAtIndex:4 ]isEqualToString:@"1"])
    {
        [self.btn5 setSelected:!self.btn5.isSelected];
    }
    if ([[timeArr objectAtIndex:5 ]isEqualToString:@"1"])
    {
        [self.btn6 setSelected:!self.btn6.isSelected];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 1:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        default:
            break;
    }
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}

@end
