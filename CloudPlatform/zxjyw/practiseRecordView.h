//
//  practiseRecordView.h
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "question.h"
#import "userInfo.h"
#import "recordCell.h"
#import <AVFoundation/AVFoundation.h>

@protocol practiseRecordViewDelegate <NSObject>
-(void)practisePopToQuestionList;
-(void)practisePop;
-(void)practiseUserCenter;
-(void)practiseExchange;
-(void)practiseLogoff;
@end

@interface practiseRecordView : UIView<UITableViewDataSource,UITableViewDelegate,RecordCellDelegate,AVAudioPlayerDelegate,UIAlertViewDelegate>
@property(nonatomic)id<practiseRecordViewDelegate>delegate;
@property (retain, nonatomic)question* currentQuestion;
- (IBAction)pressBtn:(UIButton *)sender;
-(void)resolvingWithQuestion:(question*)question andScore:(int)score;
-(void)stopPlayer;//停止播放的音频
@end
