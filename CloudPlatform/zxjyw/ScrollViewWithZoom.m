//
//  ScrollViewWithZoom.m
//  zxjyw
//
//  Created by HelyData on 13-9-24.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "ScrollViewWithZoom.h"

@interface ScrollViewWithZoom (Utility)

@end

@implementation ScrollViewWithZoom
@synthesize imageView  =_imageView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.maximumZoomScale = 4.0;
        self.delegate = self;
        [self initImageView];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)initImageView
{
    imageView = [[UIImageView alloc]init];
    imageView.frame = self.bounds;
    imageView.userInteractionEnabled = YES;
    [self addSubview:imageView];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                       action:@selector(handleTap:)];
    [tapGesture setNumberOfTapsRequired:1];
    [imageView addGestureRecognizer:tapGesture];
}
-(void)setImage:(UIImage*)image
{
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = image;
}

#pragma mark - Zoom methods

- (void)handleTap:(UIGestureRecognizer *)gesture
{
    [self removeFromSuperview];
}


#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imageView;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    [scrollView setZoomScale:scale animated:NO];
}

@end
