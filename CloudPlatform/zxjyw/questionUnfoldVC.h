//
//  questionUnfoldVC.h
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "question.h"
#import "NetworkManager.h"

@interface questionUnfoldVC : UIViewController
@property(nonatomic,strong)question* currentQuestion;//当前的问题
@property (nonatomic, strong) NetworkManager *network;
@end
