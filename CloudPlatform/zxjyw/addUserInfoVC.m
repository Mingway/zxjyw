//
//  updateUserInfo.m
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "addUserInfoVC.h"
#import "tools.h"
#import "startVC.h"
#import "userInfo.h"
#import "UIHyperlinksButton.h"
#import "UIButton+Extensions.h"
#import "NetworkManager.h"
#import "ResourseDefine.h"
#import "waitView.h"
#import <QuartzCore/QuartzCore.h>

#define listTVTag 1000

@interface addUserInfoVC ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)NSArray* courseList;//科目列表数组
@property(nonatomic,assign)int course_id;//记录课程ID
@property (weak, nonatomic) IBOutlet UITextField *selectedCourseTextField;
@property (nonatomic,assign)int num1;//1-6分别代表工作日和非工作日的上午、下午和晚上,0表示不属于工作时间,1表示属于工作时间
@property (nonatomic,assign)int num2;
@property (nonatomic,assign)int num3;
@property (nonatomic,assign)int num4;
@property (nonatomic,assign)int num5;
@property (nonatomic,assign)int num6;
@property (weak, nonatomic) IBOutlet UIButton *showCourseListButton;//显示课程列表的按钮
@property (weak, nonatomic) IBOutlet UIButton *btn1;//分别表示对应时间的按钮
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *submitUserInfoButton;//提交注册信息按钮
@property (nonatomic, strong) userInfo *user;
@property (nonatomic, strong) NetworkManager *network;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;

- (IBAction)pressBtn:(UIButton *)sender;

@end

@implementation addUserInfoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        self.backGroundImageView.image = [UIImage imageNamed:@"addUserInfoVC_background_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    self.num1 = 0;
    self.num2 = 0;
    self.num3 = 0;
    self.num4 = 0;
    self.num5 = 0;
    self.num6 = 0;
    [self setButtonRect];
    [self.view addSubview:[waitView getWaitView]];
    [[waitView getWaitView]startAnimation];
    [self getCourseList];
}

- (void)getCourseList
{
    [self.network sendRequest:[self.network createGetCourseListRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            [[waitView getWaitView] stopAnimation];
            [[waitView getWaitView]removeFromSuperview];
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                self.courseList = [data objectForKey:@"courseList"];
            }
            else
            {
                [self getCourseList];
                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
            }
        }
        else
        {
             [self getCourseList];
            [tools showAlertView:@"提示" andMessage:errorMessage];
        }
    }];
}
-(void)setButtonRect
{
    [self.showCourseListButton setHitTestEdgeInsets:UIEdgeInsetsMake(-20, -20, -20, -20)];
    [self.btn1 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn2 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn3 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn4 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn5 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn6 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.submitUserInfoButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)rotate:(UIButton*)sender
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    CGAffineTransform t = CGAffineTransformRotate(sender.transform, -M_PI);
    sender.transform = t;
    [UIView commitAnimations];
}
- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 0:
        {
            [sender setSelected:!sender.isSelected];
            if (self.num1 == 0) {
                self.num1 = 1;
            }
            else
                self.num1 = 0;
        }
            break;
        case 1:
        {
            [sender setSelected:!sender.isSelected];
            if (self.num2 == 0) {
                self.num2 = 1;
            }
            else
                self.num2 = 0;
        }
            break;
        case 2:
        {
            [sender setSelected:!sender.isSelected];
            if (self.num3 == 0) {
                self.num3 = 1;
            }
            else
                self.num3 = 0;
        }
            break;
        case 3:
        {
            [sender setSelected:!sender.isSelected];
            if (self.num4 == 0) {
                self.num4 = 1;
            }
            else
                self.num4 = 0;
        }
            break;
        case 4:
        {
            [sender setSelected:!sender.isSelected];
            if (self.num5 == 0) {
                self.num5 = 1;
            }
            else
                self.num5 = 0;
        }
            break;
        case 5:
        {
            [sender setSelected:!sender.isSelected];
            if (self.num6 == 0) {
                self.num6 = 1;
            }
            else
                self.num6 = 0;
        }
            break;
        case 6:
        {
            //显示或隐藏科目列表
            [self rotate:sender];
            UITableView* listTV = (UITableView*)[self.view viewWithTag:listTVTag];
            if (listTV == nil)
            {
                listTV = [[UITableView alloc]initWithFrame:CGRectMake(28, 145, 219, 0)];
                listTV.delegate = self;
                [listTV setTag:listTVTag];
                listTV.separatorStyle = UITableViewCellSeparatorStyleNone;
                listTV.dataSource = self;
                [self.view addSubview:listTV];
            }
            if (CGRectGetHeight(listTV.frame)>=0&&CGRectGetHeight(listTV.frame)<=0)
            {
                [UIView animateWithDuration:0.3f animations:^{[listTV setFrame:CGRectMake(28, 145, 219, 200)];} completion:nil];
            }
            else
            {
                [UIView animateWithDuration:0.3f animations:^{[listTV setFrame:CGRectMake(28, 145, 219, 0)];} completion:nil];
            }
        }
            break;
        case 7:
        {
            //判断是否进行了规定的选择，如没有，则提示；如已选择，则push到下一级视图
            if ([self.selectedCourseTextField.text isEqualToString:@""])
            {
                [tools showAlertView:nil andMessage:@"请选择擅长的学科!"];
            }
            else if(self.num1 == 0 && self.num2 == 0 && self.num3 == 0 && self.num4 == 0 && self.num5 == 0 && self.num6 == 0)
            {
                [tools showAlertView:nil andMessage:@"请至少选择一个可工作时间!"];
            }
            else
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createUpdateUserInfoRequestWithAvailableTime:[NSString stringWithFormat:@"%i,%i,%i,%i,%i,%i",self.num1,self.num2,self.num3,self.num4,self.num5,self.num6] andCourseId:[NSString stringWithFormat:@"%i",self.course_id]] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if(isSuccess)
                    {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            self.user = [userInfo shareUserInfo];
                            self.user.course = self.selectedCourseTextField.text;
                            self.user.courseId = self.course_id;
                            self.user.AvailableTime = [NSString stringWithFormat:@"%i%i%i%i%i%i",self.num1,self.num2,self.num3,self.num4,self.num5,self.num6];
                            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            UIViewController *next = [board instantiateViewControllerWithIdentifier:@"startVC"];
                            [self.navigationController pushViewController:next animated:YES];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
        }
            break;
        default:
            break;
    }
}
#pragma mark UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.courseList.count % 2 == 0) {
        return self.courseList.count/2;
    }
    else
    {
        return self.courseList.count/2+1;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* Identifier = @"cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    UIButton* btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, 110, 45);
    btn1.tag = indexPath.row*2;
    btn1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"addUserInfoVC_courseBackground.png"]];
    [btn1 setTitle:[[self.courseList objectAtIndex:indexPath.row *2] objectForKey:@"course_name"] forState:UIControlStateNormal] ;
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cell addSubview:btn1];
    [btn1 addTarget:self action:@selector(showSelectedCourse:) forControlEvents:UIControlEventTouchUpInside];
    if (indexPath.row *2<=self.courseList.count) {
        UIButton* btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn2.frame = CGRectMake(CGRectGetMaxX(btn1.frame)-1, 0, 110, 45);
        btn2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"addUserInfoVC_courseBackground.png"]];
        btn2.tag = indexPath.row*2+1;
        [btn2 setTitle:[[self.courseList objectAtIndex:indexPath.row * 2+1] objectForKey:@"course_name"] forState:UIControlStateNormal];
        [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cell addSubview:btn2];
        [btn2 addTarget:self action:@selector(showSelectedCourse:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}
-(void)showSelectedCourse:(UIButton*)button
{
    [self rotate:self.showCourseListButton];
    //改变显示的擅长学科
    self.selectedCourseTextField.text = [[self.courseList objectAtIndex:button.tag] objectForKey:@"course_name"];
    self.course_id = [[[self.courseList objectAtIndex:button.tag] objectForKey:@"course_id"]intValue];
    //隐藏学科类表
    UITableView* listTV = (UITableView*)[self.view viewWithTag:listTVTag];
    [UIView animateWithDuration:0.3f animations:^{[listTV setFrame:CGRectMake(28, 145, 219, 0)];} completion:nil];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
@end
