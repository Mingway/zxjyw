//
//  updateUserInfoVC.m
//  zxjyw
//
//  Created by HelyData on 13-10-11.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "updateUserInfoVC.h"
#import "userInfo.h"
#import "NetworkManager.h"
#import "tools.h"
#import "userCenterVC.h"
#import "UIButton+Extensions.h"
#import "ResourseDefine.h"
#import "waitView.h"
#import <QuartzCore/QuartzCore.h>
#define listTag 2000

@interface updateUserInfoVC ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UITextField *selectedCourseTextField;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property(nonatomic,strong)NSArray* courseList;//科目列表数组
@property(nonatomic,assign)int courseId;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIButton *showCourseListButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property (nonatomic,assign)int num1;//1-6分别代表工作日和非工作日的上午、下午和晚上,0表示不属于工作时间,1表示属于工作时间
@property (nonatomic,assign)int num2;
@property (nonatomic,assign)int num3;
@property (nonatomic,assign)int num4;
@property (nonatomic,assign)int num5;
@property (nonatomic,assign)int num6;
@property (nonatomic,retain)userInfo* user;
@property (nonatomic, strong) NetworkManager *network;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *myCoinLabel;

@end

@implementation updateUserInfoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        self.backGroundImageView.image = [UIImage imageNamed:@"updateUserInfoVC_background_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    [self.view addSubview:[waitView getWaitView]];
    [[waitView getWaitView]startAnimation];
    [self.network sendRequest:[self.network createGetCourseListRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [[waitView getWaitView] stopAnimation];
        [[waitView getWaitView]removeFromSuperview];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"] intValue] == 0) {
                self.courseList = [data objectForKey:@"courseList"];
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
            }
        }
        else
        {
            [tools showAlertView:@"提示" andMessage:errorMessage];
        }
    }];
    self.user = [userInfo shareUserInfo];
    self.userNameLabel.text = self.user.loginName;
    self.myCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    self.num1 = 0;
    self.num2 = 0;
    self.num3 = 0;
    self.num4 = 0;
    self.num5 = 0;
    self.num6 = 0;
    self.selectedCourseTextField.text  =self.user.course;
    self.courseId = self.user.courseId;
    NSArray *timeArr = [self.user.AvailableTime componentsSeparatedByString:@","];
    if ([[timeArr objectAtIndex:0 ]isEqualToString:@"1"]) {
        self.num1 = 1;
        [self.btn1 setSelected:!self.btn1.isSelected];
    }
    if ([[timeArr objectAtIndex:1 ]isEqualToString:@"1"])
    {
        self.num2 = 1;
        [self.btn2 setSelected:!self.btn2.isSelected];
    }
    if ([[timeArr objectAtIndex:2 ]isEqualToString:@"1"])
    {
        self.num3 = 1;
        [self.btn3 setSelected:!self.btn3.isSelected];
    }
    if ([[timeArr objectAtIndex:3 ]isEqualToString:@"1"])
    {
        self.num4 = 1;
        [self.btn4 setSelected:!self.btn4.isSelected];
    }
    if ([[timeArr objectAtIndex:4 ]isEqualToString:@"1"])
    {
        self.num5 = 1;
        [self.btn5 setSelected:!self.btn5.isSelected];
    }
    if ([[timeArr objectAtIndex:5 ]isEqualToString:@"1"])
    {
        self.num6 = 1;
        [self.btn6 setSelected:!self.btn6.isSelected];
    }
	[self setButtonRect];
}
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [_showCourseListButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn1 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn2 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn3 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn4 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn5 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.btn6 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.submitButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
    [self.exchangeButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)rotate:(UIButton*)sender
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    CGAffineTransform t = CGAffineTransformRotate(sender.transform, -M_PI);
    sender.transform = t;
    [UIView commitAnimations];
}
- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            self.submitButton.enabled  = YES;
            [sender setSelected:!sender.isSelected];
            if (self.num1 == 0) {
                self.num1 = 1;
            }
            else
                self.num1 = 0;
        }
            break;
        case 1:
        {
            self.submitButton.enabled  = YES;
            [sender setSelected:!sender.isSelected];
            if (self.num2 == 0) {
                self.num2 = 1;
            }
            else
                self.num2 = 0;
        }
            break;
        case 2:
        {
            self.submitButton.enabled  = YES;
            [sender setSelected:!sender.isSelected];
            if (self.num3 == 0) {
                self.num3 = 1;
            }
            else
                self.num3 = 0;
        }
            break;
        case 3:
        {
            self.submitButton.enabled  = YES;
            [sender setSelected:!sender.isSelected];
            if (self.num4 == 0) {
                self.num4 = 1;
            }
            else
                self.num4 = 0;
        }
            break;
        case 4:
        {
            self.submitButton.enabled  = YES;
            [sender setSelected:!sender.isSelected];
            if (self.num5 == 0) {
                self.num5 = 1;
            }
            else
                self.num5 = 0;
        }
            break;
        case 5:
        {
            self.submitButton.enabled  = YES;
            [sender setSelected:!sender.isSelected];
            if (self.num6 == 0) {
                self.num6 = 1;
            }
            else
                self.num6 = 0;
        }
            break;
        case 6:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 7:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        case 8:
        {
            [self rotate:sender];
            //显示或隐藏科目列表
            UITableView* listTV = (UITableView*)[self.view viewWithTag:listTag];
            if (listTV == nil)
            {
                listTV = [[UITableView alloc]initWithFrame:CGRectMake(27, 142, 219, 0)];
                listTV.delegate = self;
                [listTV setTag:listTag];
                listTV.separatorStyle = UITableViewCellSeparatorStyleNone;
                listTV.dataSource = self;
                [self.view addSubview:listTV];
            }
            if (CGRectGetHeight(listTV.frame)>=0&&CGRectGetHeight(listTV.frame)<=0)
            {
                [UIView animateWithDuration:0.3f animations:^{[listTV setFrame:CGRectMake(27, 142, 219, 200)];} completion:nil];
            }
            else
            {
                [UIView animateWithDuration:0.3f animations:^{[listTV setFrame:CGRectMake(27, 142, 219, 0)];} completion:nil];
            }
        }
            break;
        case 9:
        {
            if ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count -2]isKindOfClass:[userCenterVC class]]) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *next = [board instantiateViewControllerWithIdentifier:@"userCenterVC"];
                [self.navigationController pushViewController:next animated:YES];
            }
        }
            break;
        case 10:
        {
            //提交注册信息，修改学科成功跳转激活界面，后台更改用户状态；如果没修改学科就不用跳转激活
            if (![self.user.course isEqualToString:self.selectedCourseTextField.text] || ![[NSString stringWithFormat:@"%i,%i,%i,%i,%i,%i",self.num1,self.num2,self.num3,self.num4,self.num5,self.num6] isEqualToString:self.user.AvailableTime]) {
                //修改之后才提交，未修改不提交
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                int  tempId = -1;
                if (![self.user.course isEqualToString:self.selectedCourseTextField.text]) {
                    tempId = self.courseId;
                }
                [self.network sendRequest:[self.network createUpdateUserInfoRequestWithAvailableTime:[NSString stringWithFormat:@"%i,%i,%i,%i,%i,%i",self.num1,self.num2,self.num3,self.num4,self.num5,self.num6] andCourseId:[NSString stringWithFormat:@"%i",tempId]] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            self.user.AvailableTime = [NSString stringWithFormat:@"%i,%i,%i,%i,%i,%i",self.num1,self.num2,self.num3,self.num4,self.num5,self.num6];
                            if ([self.user.course isEqualToString:self.selectedCourseTextField.text]) {
                                //不用跳转激活页面
                                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"更改信息成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                                [alert show];
                                alert = nil;
                            }
                            else
                            {
                                [tools showAlertView:@"提示" andMessage:@"更改信息成功"];
                                self.user.course = self.selectedCourseTextField.text;
                                self.user.courseId = self.courseId;
                                self.user.status = 1;
                                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                UIViewController *next = [board instantiateViewControllerWithIdentifier:@"startVC"];
                                [self.navigationController pushViewController:next animated:YES];
                            }
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }else{
                [tools showAlertView:@"提示" andMessage:@"未修改"];
            }
        }
            break;
        default:
            break;
    }
}
#pragma mark UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.courseList.count % 2 == 0) {
        return self.courseList.count/2;
    }
    else
    {
        return self.courseList.count/2+1;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* Identifier = @"cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    UIButton* btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(0, 0, 110, 45);
    btn1.tag = indexPath.row*2;
    btn1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"addUserInfoVC_courseBackground.png"]];
    [btn1 setTitle:[[self.courseList objectAtIndex:indexPath.row *2] objectForKey:@"course_name"] forState:UIControlStateNormal] ;
    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cell addSubview:btn1];
    [btn1 addTarget:self action:@selector(showSelectedCourse:) forControlEvents:UIControlEventTouchUpInside];
    if (indexPath.row *2<=self.courseList.count) {
        UIButton* btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn2.frame = CGRectMake(CGRectGetMaxX(btn1.frame)-1, 0, 110, 45);
        btn2.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"addUserInfoVC_courseBackground.png"]];
        btn2.tag = indexPath.row*2+1;
        [btn2 setTitle:[[self.courseList objectAtIndex:indexPath.row * 2+1] objectForKey:@"course_name"] forState:UIControlStateNormal];
        [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cell addSubview:btn2];
        [btn2 addTarget:self action:@selector(showSelectedCourse:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}
-(void)showSelectedCourse:(UIButton*)button
{
    [self rotate:self.showCourseListButton];
    //改变显示的擅长学科
    self.selectedCourseTextField.text = [[self.courseList objectAtIndex:button.tag] objectForKey:@"course_name"];
    self.courseId = [[[self.courseList objectAtIndex:button.tag] objectForKey:@"course_id"]intValue];
    //隐藏学科类表
    UITableView* listTV = (UITableView*)[self.view viewWithTag:listTag];
    [UIView animateWithDuration:0.3f animations:^{[listTV setFrame:CGRectMake(27, 142, 219, 0)];} completion:nil];
    if (![self.selectedCourseTextField.text isEqualToString:self.user.course]) {
        self.submitButton.enabled = YES;
    }
}
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }else if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
@end
