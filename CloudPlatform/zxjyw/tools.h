//
//  tools.h
//  zxjyw
//
//  Created by HelyData on 13-9-23.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  工具类

#import <Foundation/Foundation.h>

@interface tools : NSObject<UIAlertViewDelegate>

+(void)showAlertView:(NSString*)title andMessage:(NSString*)message;//显示AlertView

+(NSString*)getCurrentDateTOSting;//获取当前日期的字符串，用以保存录音文件

+(NSString *) md5: (NSString *) inPutText ;//MD5加密

+(CGSize)caculateCGSizeForText:(NSString*)text withFont:(UIFont*)font;//计算文本所占Size

+(void)saveToken:(NSString*)access_token LoginName:(NSString*)loginName;//保存token
+(id)read;//读取token
+(void)deleteUser;//删除token

+(NSString*)dateString;//获取当前时间的字符串

+(BOOL)htmlIsIncludeImg:(NSString*)html;//判断html文本中是否含有图片

+(NSDate*)localDate;//本地时间

+(BOOL)checkTel:(NSString *)str;//检查电话号码
+(NSString*)judgeTitleImageWithCourse:(NSString*)course;//根据擅长学科判断学科图片名称


@end
