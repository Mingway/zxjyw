//
//  recordCell.h
//  zxjyw
//
//  Created by HelyData on 13-9-24.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  录音记录列表单条cell

#import <UIKit/UIKit.h>

@class recordCell;
@protocol RecordCellDelegate <NSObject>

@optional

-(void)play_or_pause:(recordCell*)cell;
-(void)deleteThisRecord:(recordCell*)cell;

@end

@interface recordCell : UITableViewCell
@property(nonatomic,assign)id<RecordCellDelegate> delegate;
@property(nonatomic,weak)NSString* time;
@property (weak, nonatomic) IBOutlet UIImageView *imageForVoice;
@property (weak, nonatomic) IBOutlet UIButton *recordBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
-(void)showTime;
@end
