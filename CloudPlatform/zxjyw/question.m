//
//  question.m
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "question.h"
#import "tools.h"

#define DEMO_URL  @"http://demo703.tongyouxuetang.com"

@implementation question

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSString*)convertImgUrlWithString:(NSString*)string
{
    NSScanner *scanner = [NSScanner scannerWithString:string];
    while ([scanner scanUpToString:@"<img" intoString:NULL]) {
        NSString *ImgTagContents;
        if ([scanner scanUpToString:@">" intoString:&ImgTagContents]) {
            // Do something with tag contents
            NSScanner *scanner1 = [NSScanner scannerWithString:ImgTagContents];
            while ([scanner1 scanUpToString:@"src=\"" intoString:NULL]) {
                NSString *srcTagContents;
                if ([scanner1 scanUpToString:@"\" /" intoString:&srcTagContents]) {
                    string  = [string stringByReplacingOccurrencesOfString:srcTagContents withString: [NSString stringWithFormat:@"src=\"%@%@",DEMO_URL,[srcTagContents substringFromIndex:5]]];
                }
            }
        }
        else {
            // Do nothing? I think this would be hit on the last time through the loop
        }
    }
    return string;
}

-(NSString*)updateOptionsWithStr1:(NSString*)str1 toStr2:(NSString*)str2 htmlString:(NSString*)html_str;
{
    NSRange range = [html_str rangeOfString:str1];
    html_str = [html_str stringByReplacingOccurrencesOfString:str1 withString:str2 options:NSBackwardsSearch range:NSMakeRange(range.location + range.length, html_str.length-range.location - range.length)];
    return html_str;
}

-(void)updateWithDictionary:(NSDictionary*)dic
{
    self.QuestionType = [dic objectForKey:@"questionType"];
    self.QuestionStyle = [dic objectForKey:@"questionStyle"];
    self.QuestionCourse = [dic objectForKey:@"questionCourse"];
    self.QuestionScore = [[dic objectForKey:@"questionScore"] intValue];
    self.QuestionScored = [[dic objectForKey:@"questionScored"]intValue];
    NSString* new_questionContent = [dic objectForKey:@"questionContent"] ;
    if([self.QuestionStyle isEqualToString:@"单选题"] || [self.QuestionStyle isEqualToString:@"多选题"]){
        new_questionContent = [new_questionContent stringByReplacingOccurrencesOfString:@"\\a" withString:@"<br/>A."];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>A." toStr2:@"<br/>B." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>B." toStr2:@"<br/>C." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>C." toStr2:@"<br/>D." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>D." toStr2:@"<br/>E." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>E." toStr2:@"<br/>F." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>F." toStr2:@"<br/>G." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>G." toStr2:@"<br/>H." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>H." toStr2:@"<br/>I." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>I." toStr2:@"<br/>J." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>J." toStr2:@"<br/>K." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>K." toStr2:@"<br/>L." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>L." toStr2:@"<br/>M." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>M." toStr2:@"<br/>N." htmlString:new_questionContent];
    }else{
        new_questionContent = [[dic objectForKey:@"questionContent"] stringByReplacingOccurrencesOfString:@"\\a" withString:@"_____"];
    }
   new_questionContent = [self convertImgUrlWithString:new_questionContent];
    NSString* content_html = [NSString stringWithFormat:@"<html> <head><title>问题内容</title><script type = \"text/javascript\"src = \"MathJax.js\"></script> </head><body>%@</body>  </html>",new_questionContent];
    self.QuestionContent = content_html;
    NSArray *solutionArr = [dic objectForKey:@"questionSolutions"];
    NSMutableArray *tempMulArr = [NSMutableArray array];
    for (int i = 0; i < solutionArr.count; i++) {
        NSString* solution_html = [NSString stringWithFormat:@"<html> <head><title>答案内容</title><script type = \"text/javascript\"src = \"MathJax.js\"></script> </head><body>%@</body>  </html>",[[solutionArr objectAtIndex:i]objectForKey:@"solution"]];
        solution_html = [self convertImgUrlWithString:solution_html];
        [tempMulArr addObject:solution_html];
    }
    self.QuestionSolutions = [NSArray arrayWithArray:tempMulArr];
    self.QuestionAnswer = [NSString stringWithFormat:@"%@/dataapi/res/get_res.do?res_id=%@", DEMO_URL,[dic objectForKey:@"questionAnswer"]];
    self.QuestionCoin = [[dic objectForKey:@"questionCoin"]intValue];
    self.AnswerId = [dic objectForKey:@"answerId"];
    NSArray* tempArr = [dic objectForKey:@"answerComments"];
    if (tempArr != nil) {
        for (int i = 0; i < tempArr.count; i++) {
            [self.answerComments addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@/dataapi/res/get_res.do?res_id=%@",DEMO_URL,[[tempArr objectAtIndex:i]objectForKey:@"comment"]]]] ;
            [self.answerCommentsTime addObject:[[tempArr objectAtIndex:i]objectForKey:@"time"]];
        }
    }
}

@end
