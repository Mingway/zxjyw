//
//  recordVC.h
//  zxjyw
//
//  Created by HelyData on 13-9-24.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  录音点评界面

#import <UIKit/UIKit.h>
#import "question.h"

@interface recordVC : UIViewController
@property(nonatomic,strong)question* currentQuestion;
@property(nonatomic,strong)NSString* comment_point;//点评分数
@end
