//
//  NetworkManager.h
//  zxjyw
//
//  Created by HelyData on 13-10-17.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NetRequest;

@interface NetworkManager : NSObject

@property (nonatomic, readonly) BOOL isNetworkAvailable;

- (BOOL)logonUserName:(NSString *)userName password:(NSString *)password completionHandler:(void (^)(BOOL isSuccess, NSDictionary *data, NSString *errorMessage))completionHandlercompletionHandler;
- (NetRequest *)createLogoffRequest;//创建登出请求
- (NetRequest *)createReloginRequest;//重登陆
- (NetRequest *)createGetCaptchaCodeRequestWithMobieNumber:(NSString*)mobileNumber andType:(NSString*)type;//创建获取验证码请求
- (NetRequest *)createRegisterRequestWithUserName:(NSString*)userName andPassword:(NSString*)password andCaptchaCode:(NSString*)captchaCode;//创建注册请求
- (NetRequest *)createGetCourseListRequest;//创建获取课程列表请求
- (NetRequest *)createUpdateUserInfoRequestWithAvailableTime:(NSString*)availableTime andCourseId:(NSString*)courseId;//创建修改用户信息请求
- (NetRequest *)createSubmitNewPasswordRequestWithUserName:(NSString*)userName andNewPassword:(NSString*)newPassword andCaptcha:(NSString*)captcha;//创建提交修改新密码请求
- (NetRequest *)createGetQuestionListRequestWithCount:(int)count;//创建获取题目列表请求
- (NetRequest *)createSubmitResultRequestWithAnswerId:(NSString*)answerId andType:(int)type andAnswerScore:(int)answerScore andAnswerComments:(NSMutableArray*)answerComments;//创建提交评分请求
- (NetRequest *)createGetQuestionByAnswerIdRequestWithAnswerId:(NSString*)answerId;//创建根据answerId获取题目请求
- (NetRequest *)createRecieveQuestionRequestWithAnswerId:(NSString*)answerId andType:(int)type;//创建接受题目请求
- (NetRequest *)createRefuseQuestionRequestWithAnswerId:(NSString*)answerId andRefuse_type:(int)refuse_type;//创建拒绝题目请求
- (NetRequest *)createUpdatePasswordRequestWithPassword:(NSString*)password andNewPassword:(NSString*)newPassword;//创建修改密码请求
- (NetRequest *)createGetCoinRecordRequestWithPageIndex:(int)pageIndex andPageSize:(int)pageSize;//创建获取积分记录请求
- (NetRequest *)createUploadResourseRequestWithResData:(NSString*)resdata andTime:(int)time;//创建上传录音请求
- (NetRequest*)createGetPushQuestionRequest;//创建获取推送题目请求
- (NetRequest*)createDownloadResourceRequestWithUrl:(NSString*)url;//创建下载资源请求
- (BOOL)sendRequest:(NetRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSDictionary *data, NSString *errorMessage))completionHandler;

- (BOOL)downloadReourceRequest:(NetRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSData *data, NSString *errorMessage))completionHandler;

@end
