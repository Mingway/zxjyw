//
//  exchangePlatformCell.h
//  zxjyw
//
//  Created by HelyData on 13-10-20.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface exchangePlatformCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *platformImageView;
- (void)setPlatformImage:(UIImage*)image andFrame:(CGRect)frame;
@end
