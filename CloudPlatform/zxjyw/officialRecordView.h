//
//  officialRecordView.h
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "question.h"
#import "userInfo.h"
#import "recordCell.h"
#import <AVFoundation/AVFoundation.h>
#import "NetworkManager.h"


@protocol officialRecordViewDelegate <NSObject>
-(void)officialPop;
-(void)officialPushQuestionList;
-(void)officialUserCenter;
-(void)officialExchange;
-(void)officialLogoff;
@end

@interface officialRecordView : UIView<UITableViewDataSource,UITableViewDelegate,RecordCellDelegate,AVAudioPlayerDelegate,UIAlertViewDelegate>

@property (retain, nonatomic)question* currentQuestion;
@property (nonatomic,strong)NSDate* startDate;
@property (nonatomic)id<officialRecordViewDelegate>delegate;
- (IBAction)pressBtn:(UIButton *)sender;
- (IBAction)startRecord:(UIButton *)sender;
- (IBAction)cancelRecord:(UIButton *)sender;
- (IBAction)endRecord:(UIButton *)sender;
-(void)resolvingWithQuestion:(question*)question andScore:(int)score;
-(void)stopPlayer;//停止播放中的音频
-(void)stopTimer;
-(void)continueTimer;
@end
