//
//  questionDetailVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-23.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "questionDetailVC.h"
#import "ScrollViewWithZoom.h"
#import "recordVC.h"
#import "userInfo.h"
#import "tools.h"
#import "UIButton+Extensions.h"
#import "startVC.h"
#import "NetworkManager.h"
#import "EGOImageView.h"
#import "ResourseDefine.h"
#import "successfulVC.h"
#import "waitView.h"

#define SCORE_ALERT_TAG 1000   //提交评分alertTag
#define ERROR_ALERT_TAG 2000   //评分错误AlertTag

@interface questionDetailVC ()<UIScrollViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) userInfo* user;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *scoreLable;//显示评分的Lable
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;//显示问题的webView
@property (weak, nonatomic) IBOutlet UIImageView *webViewBackImageView;//问题webView的背景图片
@property (weak, nonatomic) IBOutlet UIButton *webViewDragBtn;//webView的Button条
@property (weak, nonatomic) IBOutlet UIWebView *solutionWebView;//原题解答webView
@property (weak, nonatomic) IBOutlet EGOImageView *answerImageView;//学生答案ImageView
@property (weak, nonatomic) IBOutlet UIButton *solutionButton1;
@property (weak, nonatomic) IBOutlet UIButton *solutionButton2;
@property (weak, nonatomic) IBOutlet UIButton *solutionButton3;
@property (weak, nonatomic) IBOutlet UILabel *totalPointLable;//显示本题总分Lable
@property (nonatomic,assign)int total_point;//题目的总分
@property (nonatomic,assign)int solution_num;//正确答案个数
@property (nonatomic,assign)int decade;//评分十位数字
@property (nonatomic,assign)int theUnit;//个位数字
@property (nonatomic,assign)int row_num;//个位数字行数
@property (nonatomic,assign)int selectDecade;//选中的十位数字
@property (nonatomic,assign)int selectTheUnit;//选中的个位数字
@property (weak, nonatomic) IBOutlet UIButton *setScoreButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (nonatomic,strong)NSString* questionHtml;//问题的html文本
@property (nonatomic,strong) NetworkManager *network;
- (IBAction)pressBtn:(UIButton *)sender;

@end

@implementation questionDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
-(void)initData
{
    self.total_point = self.currentQuestion.QuestionScore;
    self.solution_num = (int)[self.currentQuestion.QuestionSolutions count];
    self.scoreLable.text = [NSString stringWithFormat:@"%i",self.total_point];
    self.questionHtml = self.currentQuestion.QuestionContent;
    //利用webView加载html文本
    [self loadHtml:_questionHtml];
    //显示原题解答个数按钮
    if (self.currentQuestion.QuestionSolutions.count == 2) {
        self.solutionButton3.hidden = YES;
    }
    else if(self.currentQuestion.QuestionSolutions.count == 1)
    {
        self.solutionButton2.hidden = YES;
        self.solutionButton3.hidden = YES;
    }
    //加载正确答案
    [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:0]  baseURL:nil];
    //加载学生回答
    self.answerImageView.imageURL = [NSURL URLWithString:self.currentQuestion.QuestionAnswer];
    UITapGestureRecognizer* showBigImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showBigImage)];
    [self.answerImageView addGestureRecognizer:showBigImage];
    //显示总分数
    self.totalPointLable.text = [NSString stringWithFormat:@"%i",self.total_point];
    //把webView和拖拽按钮放到最上面
    [self.view bringSubviewToFront:self.webViewBackImageView];
    [self.view bringSubviewToFront:self.webViewDragBtn];
    [self.view bringSubviewToFront:self.questionContentWebView];
}
-(void)showBigImage
{
    ScrollViewWithZoom* bigImage = [[ScrollViewWithZoom alloc]initWithFrame:self.view.bounds];
    [bigImage  setImage:self.answerImageView.image];
    [self.view addSubview:bigImage];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //从单例中获取当前用户信息
    self.user = [userInfo shareUserInfo];
    //根据用户的状态换背景图片
    if (self.user.status == 1) {
        if (!inch4) {
            self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_active_background_960.png"];
        }
        else
        {
            self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_active_background.png"];
        }
    }
    else if (self.user.status == 2) {
        if (!inch4) {
            self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_unfreeze_background_960.png"];
        }
        else
        {
            self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_unfreeze_background.png"];
        }
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.network = [[NetworkManager alloc]init];
    if (self.currentQuestion == nil) {
        self.currentQuestion = [[question alloc]init];
    }
    if (!inch4) {
        [self.solutionWebView setFrame:CGRectMake(23, 173, 280, 125)];
        [self.answerImageView setFrame:CGRectMake(23, 323, 280, 100)];
        [self.setScoreButton setFrame:CGRectOffset(self.setScoreButton.frame, 0, -85)];
        [self.scoreLable setFrame:CGRectOffset(self.scoreLable.frame, 0, -85)];
        [self.commentButton setFrame:CGRectOffset(self.commentButton.frame, 0, -85)];
        [self.submitButton setFrame:CGRectOffset(self.submitButton.frame, 0, -85)];
    }
    [self initData];
    UITapGestureRecognizer* QuestionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeWebViewFrame:)];
    [QuestionTap setNumberOfTapsRequired:1];
    QuestionTap.delegate = self;
    [self.questionContentWebView addGestureRecognizer:QuestionTap];
    [self setButtonRect];
    UITapGestureRecognizer* SolutionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showBigSolutionWebView:)];
    SolutionTap.delegate = self;
    [SolutionTap setNumberOfTapsRequired:1];
    [self.solutionWebView addGestureRecognizer:SolutionTap];
    [self.view bringSubviewToFront:self.webViewBackImageView];
    [self.view bringSubviewToFront:self.questionContentWebView];
    [self.view bringSubviewToFront:self.webViewDragBtn];
}
-(void)showBigSolutionWebView:(UITapGestureRecognizer*)tap
{
        if (self.solutionWebView.frame.origin.x == 23) {
             [self.solutionWebView.scrollView setContentOffset:CGPointZero animated:NO];
            [UIView animateWithDuration:0.0f animations:^{[self.view bringSubviewToFront:self.solutionWebView];} completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3f animations:^{self.solutionWebView.frame = self.view.frame;}];
            }];
        }
        else
        {
             [self.solutionWebView.scrollView setContentOffset:CGPointZero animated:NO];
            [UIView animateWithDuration:0.3f animations:^{
                if(!inch4){
                     [self.solutionWebView setFrame:CGRectMake(23, 173, 280, 125)];
                }
                else
                {
                    self.solutionWebView.frame = CGRectMake(23, 173, 280, 152);
                }
                } completion:^(BOOL finished) {
                [self.view insertSubview:self.solutionWebView belowSubview:self.webViewBackImageView];
            }];
        }
}
//改变按钮响应区域
-(void)setButtonRect
{
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.solutionButton1 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, 0)];
    [self.solutionButton2 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, 0)];
    [self.solutionButton3 setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, -10)];
    [self.setScoreButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, 0)];
    [self.submitButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, 0, -10, -10)];
    [self.commentButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
//改变webView的frame
-(void)changeWebViewFrame:(UITapGestureRecognizer*)tap
{
    if (self.questionContentWebView.frame.size.height > 300) {
         [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
        [UIView animateWithDuration:0.3f animations:^{
            [self.questionContentWebView setFrame:CGRectMake(15, 83, 240,49)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 83, 240,49)];
            [self.webViewDragBtn setFrame:CGRectOffset(_webViewDragBtn.frame, 0, -(302-49))];
        } completion:nil];
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
             [self.questionContentWebView.scrollView setContentOffset:CGPointZero animated:NO];
            [self.questionContentWebView setFrame:CGRectMake(15, 83, 240,302)];
            [self.webViewBackImageView setFrame:CGRectMake(15, 83, 240,302)];
            [self.webViewDragBtn setFrame:CGRectOffset(_webViewDragBtn.frame, 0, 302-49)];
        } completion:nil];
    }
    if (self.webViewDragBtn.imageView.image == [UIImage imageNamed:@"greenLine_down"]) {
        [self.webViewDragBtn setImage:[UIImage imageNamed:@"greenLine_up"] forState:UIControlStateNormal];
    }else{
        [self.webViewDragBtn setImage:[UIImage imageNamed:@"greenLine_down"] forState:UIControlStateNormal];
    }
}
//webView加载html文本
-(void)loadHtml:(NSString *)html
{
    [self.questionContentWebView loadHTMLString:html baseURL:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//所有按钮方法汇总
- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            if (self.user.status == 1) {
                if (!inch4) {
                     self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_active_background_done_960.png"];
                }
                else
                {
                    self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_active_background_done.png"];
                }
            }
            else if(self.user.status == 2)
            {
                if (!inch4) {
                    self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_unfreeze_background_done_960.png"];
                }
                else
                {
                    self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_unfreeze_background_done.png"];
                }
            }
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"确认提交该题的评分？" message:[NSString stringWithFormat:@"评分：%@分\n语音点评：0段",_scoreLable.text] delegate:self cancelButtonTitle:@"确认提交" otherButtonTitles:@"修改评分", nil];
            alert.tag = SCORE_ALERT_TAG;
            [alert show];
        }
            break;
        case 2:
        {
            
        }
            break;
        case 3:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        case 4:
        {
            //界面设计
            if (picker_background == nil) {
                picker_background=[[UIView alloc] initWithFrame:self.view.bounds];
                picker_background.backgroundColor = [UIColor clearColor];
                //显示选择分数的picker
                self.decade = self.currentQuestion.QuestionScore/10;
                self.theUnit = self.currentQuestion.QuestionScore%10;
                [self reSetRow_Num];
                self.selectDecade = self.decade;
                self.selectTheUnit = self.theUnit;
                if (!inch4) {
                    picker=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 265, 320,100036)];
                }
                else
                {
                    picker=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 352, 320,100036)];
                }
                picker.backgroundColor = [UIColor clearColor];
                picker.delegate=self;
                picker.dataSource = self;
                picker.showsSelectionIndicator=YES;
                [picker_background addSubview:picker];
                UIImageView* backImageView = [[UIImageView alloc]initWithFrame:picker.frame];
                backImageView.backgroundColor = [UIColor whiteColor];
                backImageView.image = [UIImage imageNamed:@"pickerBackground.png"];
                [picker_background addSubview:backImageView];
                [picker_background sendSubviewToBack:backImageView];
                UIButton* confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
                if (!inch4) {
                     [confirmButton setFrame:CGRectMake(220, 265, 40, 20)];
                }
               else
               {
                    [confirmButton setFrame:CGRectMake(220, 357, 40, 20)];
               }
                [confirmButton addTarget:self action:@selector(confirmSelected:) forControlEvents:UIControlEventTouchUpInside];
                [picker_background addSubview:confirmButton];
                UIButton* cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
                if (!inch4) {
                    [cancelButton setFrame:CGRectMake(60, 265, 40, 20)];
                }
                else
                {
                    [cancelButton setFrame:CGRectMake(60, 357, 40, 20)];
                }
                [cancelButton addTarget:self action:@selector(cancelSelected:) forControlEvents:UIControlEventTouchUpInside];
                [picker_background addSubview:cancelButton];
            }
            [picker selectRow:self.decade inComponent:0 animated:NO];
            [picker selectRow:self.theUnit inComponent:1 animated:NO];
            [self.view addSubview:picker_background];

        }
            break;
        case 5:
        {
            //减分按钮
            if ([self.scoreLable.text intValue] != 0) {
                self.scoreLable.text = [NSString stringWithFormat:@"%i",[self.scoreLable.text intValue]-1];
            }
        }
            break;
        case 6:
        {
            //加分按钮
            if ([self.scoreLable.text intValue] != self.total_point) {
                self.scoreLable.text = [NSString stringWithFormat:@"%i",[self.scoreLable.text intValue]+1];
            }
        }
            break;
        case 7:
        {
            [sender setSelected:YES];
            [self.solutionButton2 setSelected:NO];
            [self.solutionButton3 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:0]  baseURL:nil];
        }
            break;
        case 8:
        {
            [sender setSelected:YES];
            [self.solutionButton1 setSelected:NO];
            [self.solutionButton3 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:1]  baseURL:nil];
        }
            break;
        case 9:
        {
            [sender setSelected:YES];
            [self.solutionButton2 setSelected:NO];
            [self.solutionButton1 setSelected:NO];
            [self.solutionWebView loadHTMLString:[self.currentQuestion.QuestionSolutions objectAtIndex:2]  baseURL:nil];
        }
            break;
        default:
            break;
    }
}
//重置picker
-(void)reSetRow_Num
{
    if (self.decade != 0 &&self.theUnit == 0) {
        self.row_num = 10;
    }
    else
    {
        self.row_num = self.theUnit + 1;
    }
}
//跳转页面传参
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    recordVC* record = segue.destinationViewController;
    if ([record respondsToSelector:@selector(setCurrentQuestion:)]) {
        [record setValue:self.currentQuestion forKey:@"currentQuestion"];
    }
    if ([record respondsToSelector:@selector(setComment_point:)]) {
        [record setValue:self.scoreLable.text forKey:@"comment_point"];
    }
}
#pragma mark UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == SCORE_ALERT_TAG) {
        switch (buttonIndex) {
            case 0:
            {
                //0,1,2,3 推送，激活，解冻， 练习
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                int type;
                if (self.user.status == 1) {
                    type = 1;
                }else{
                    type = 2;
                }
                [self.network sendRequest:[self.network createSubmitResultRequestWithAnswerId:self.currentQuestion.AnswerId  andType:type andAnswerScore:[self.scoreLable.text intValue] andAnswerComments:Nil] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            if (self.user.status == 1 || self.user.status == 2) {
                                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                successfulVC *next = [board instantiateViewControllerWithIdentifier:@"successfulVC"];
                                next.coin = [[data objectForKey:@"coin"]intValue];
                                [self.navigationController pushViewController:next animated:YES];
                            }
                        }else if ([[data objectForKey:@"error_code"]intValue] == 2){
                            //激活失败
                            if (self.user.status == 1) {
                                UIAlertView* alert =[[UIAlertView alloc]initWithTitle:@"提示" message:[data objectForKey:@"error_str"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                                alert.tag = ERROR_ALERT_TAG;
                                [alert show];
                            }
                            else
                            {
                                UIAlertView* alert =[[UIAlertView alloc]initWithTitle:@"提示" message:[data objectForKey:@"error_str"]  delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                                alert.tag = ERROR_ALERT_TAG;
                                [alert show];
                            }
                            
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            case 1:
            {
                //修改评分，跳回本界面
                //根据用户的状态换背景图片
                if (self.user.status == 1) {
                    if (!inch4) {
                        self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_active_background_960.png"];
                    }
                    else{
                        self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_active_background.png"];
                    }
                }
                else if (self.user.status == 2) {
                    if (!inch4) {
                        self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_unfreeze_background_960.png"];
                    }
                    else{
                        self.backgroundImageView.image = [UIImage imageNamed:@"questionDetailVC_unfreeze_background.png"];
                    }
                }
            }
                break;
            default:
                break;
        }

    }
    else if (alertView.tag == ERROR_ALERT_TAG)
    {
        for (NSInteger i = self.navigationController.viewControllers.count-1; i >= 0; i--) {
            UIViewController* subViewController = [self.navigationController.viewControllers objectAtIndex:i];
            if ([subViewController isKindOfClass:[startVC class]]) {
                [self.navigationController popToViewController:subViewController animated:NO];
            }
        }
    }else if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
#pragma mark Pick
-(void)confirmSelected:(UIButton*)button
{
    if (self.selectDecade == 0) {
        self.scoreLable.text = [NSString stringWithFormat:@"%i",self.selectTheUnit];
    }
    else
    {
        self.scoreLable.text = [NSString stringWithFormat:@"%i%i",self.selectDecade,self.selectTheUnit];
    }
    [picker_background removeFromSuperview];
}
-(void)cancelSelected:(UIButton*)button
{
    [picker_background removeFromSuperview];
}
#pragma mark UIPickerViewDelegate
// 返回显示的列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView
{
    return 2;
}
// 返回当前列显示的行数
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return self.decade + 1;
    }
    else
    {
        return self.row_num;
    }
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        if (self.decade != 0 &&self.theUnit == 0 && row == self.decade) {
            self.row_num = 1;
        }
        else if(self.decade > row)
        {
            self.row_num = 10;
        }
        else
        {
            if (self.theUnit == 0) {
                self.row_num = 10;
            }
            else
            {
                self.row_num = self.theUnit + 1;
            }
        }
        [pickerView reloadComponent:1];
    }
    return [NSString stringWithFormat:@"%li",(long)row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        if (self.decade != 0 &&self.theUnit == 0 && row == self.decade) {
            self.row_num = 1;
        }
        else if(self.decade > row)
        {
            self.row_num = 10;
        }
        else
        {
            if (self.theUnit == 0) {
                self.row_num = 10;
            }
            else
            {
                self.row_num = self.theUnit + 1;
            }
        }
        [pickerView reloadComponent:1];
    }
    if (component == 0) {
        self.selectDecade = row;
    }
    else
    {
        self.selectTheUnit = row;
    }
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return  YES;
    }
    else
    {
        return NO;
    }
}
@end
