//
//  officialQuestionDetailView.h
//  zxjyw
//
//  Created by HelyData on 13-10-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "userInfo.h"
#import "question.h"
#import "NetworkManager.h"
#import "EGOImageView.h"

@protocol officiaQuestionDetailViewDelegate <NSObject>

-(void)officialPop;
-(void)officialLogoff;
-(void)officialUserCenter;
-(void)officialExchange;
-(void)officialRecordwithScore:(int)score andStartDate:(NSDate*)startDate;//跳转录音点评界面，包含参数用户给予该题的评分
-(void)officialPushQuestionList;//转到问题列表界面
@end

@interface officialQuestionDetailView : UIView<UIAlertViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate>- (IBAction)pressBtn:(UIButton *)sender;
@property (nonatomic)id<officiaQuestionDetailViewDelegate>delegate;
-(void)resolvingWithQuestion:(question*)question;
-(void)stopTimer;
-(void)continueTimer;
@end
