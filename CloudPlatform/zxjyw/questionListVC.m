//
//  questionListVC.m
//  zxjyw
//
//  Created by HelyData on 13-9-27.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "questionListVC.h"
#import "question.h"
#import "tools.h"
#import "questionListDetailVC.h"
#import "userInfo.h"
#import "officialCell.h"
#import "practiseCell.h"
#import "questionUnfoldVC.h"
#import "UIButton+Extensions.h"
#import "NetworkManager.h"
#import "PullRefreshTableView.h"
#import "ResourseDefine.h"
#import "waitView.h"

#define LOADING_HEIGHT  60.0f

@interface questionListVC ()<UITableViewDataSource,officialCellDelegate,PullRefreshTableViewDelegate>
{
    NSTimer *timer;
}
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet PullRefreshTableView *questionListTableView;
@property (nonatomic,strong)NSMutableArray* questionListMutableArr;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@property (weak, nonatomic) IBOutlet UILabel *userCoinLabel;
@property (nonatomic,retain)userInfo* user;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@property (nonatomic,retain)NSMutableDictionary *selectedIndexes;
@property (nonatomic,assign)BOOL isLoading;
@property (nonatomic,assign)BOOL isDragging;
@property (nonatomic, strong) NetworkManager* network;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *exchangButton;
@property (nonatomic, assign) BOOL showRemindCell;

@end

@implementation questionListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    if (timer == nil) {
        timer =  [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCountDown) userInfo:nil repeats:YES];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
    timer = nil;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.showRemindCell = YES;
    if (!inch4) {
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.userNameLabel setFrame:CGRectOffset(self.userNameLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.userCoinLabel setFrame:CGRectOffset(self.userCoinLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];
        self.backGroundImageView.image = [UIImage imageNamed:@"questionListVC_background_960.png"];
    }
    [self.questionListTableView addHeaderView];
    self.questionListTableView.pullDelegate = self;
    self.network = [[NetworkManager alloc]init];
    self.isLoading = NO;
    self.isDragging = NO;
    self.user = [userInfo shareUserInfo];
    self.titleImageView.image = [UIImage imageNamed:[tools judgeTitleImageWithCourse:self.user.course]];
	self.questionListMutableArr = [NSMutableArray array];
    self.questionListTableView.backgroundColor = [UIColor clearColor];
    self.questionListTableView.dataSource = self;
    self.questionListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.userNameLabel.text = self.user.loginName;
    self.userCoinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    self.selectedIndexes = [[NSMutableDictionary alloc] init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addPushQuestionWithNotification:) name:@"addPushQuestion" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removePushQuestion:) name:@"removePushQuestion" object:nil];
    [self setButtonRect];
    [self.questionListTableView launchRefreshing];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCountDown) userInfo:nil repeats:YES];
}
- (void)updateCountDown
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"updateCountDown" object:nil];;
}
-(void)loadData
{
    [self.network sendRequest:[self.network createGetQuestionListRequestWithCount:3] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [self.questionListTableView loadingFinished];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                for (NSInteger i = self.questionListMutableArr.count-1; i >= 0; i--)
                {
                    if ([[(question*)[self.questionListMutableArr objectAtIndex:i] QuestionType]isEqualToString:@"训练"]) {
                        [self.questionListMutableArr removeObjectAtIndex:i];
                    }
                }
                NSArray* tempArr = [data objectForKey:@"questionList"];
                for (NSDictionary* key in tempArr) {
                    question* q = [[question alloc]init];
                    [q updateWithDictionary:key];
                    BOOL exits = NO;
                    for (question *tempQuestion in self.questionListMutableArr) {
                        if ([tempQuestion.AnswerId isEqualToString:q.AnswerId]) {
                            exits = YES;
                            break;
                        }
                    }
                    if (!exits) {
                        [self.questionListMutableArr addObject:q];
                    }
                }
                [self.questionListTableView reloadData];
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
            }
        }
        else
        {
            [tools showAlertView:@"提示" andMessage:errorMessage];
        }
    }];
}
-(void)setButtonRect
{
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
    [self.exchangeButton setHitTestEdgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10)];
}
-(void)addPushQuestionWithNotification:(NSNotification*)notification
{
    [self addPushQuestion:notification.object];
}
-(void)addPushQuestion:(NSString*)answerId
{
    [self.view addSubview:[waitView getWaitView]];
    [[waitView getWaitView]startAnimation];
    [self.network sendRequest:[self.network createGetQuestionByAnswerIdRequestWithAnswerId:answerId] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [[waitView getWaitView] stopAnimation];
        [[waitView getWaitView]removeFromSuperview];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                NSArray* tempArr = [data objectForKey:@"questionList"];
                for (NSDictionary* key in tempArr) {
                    question* q = [[question alloc]init];
                    [q updateWithDictionary:key];
                    [self.questionListMutableArr insertObject:q atIndex:0];
                }
                self.showRemindCell = NO;
                [self.questionListTableView reloadData];
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
            }
        }
        else
        {
            [tools showAlertView:@"提示" andMessage:errorMessage];
        }
    }];
}
-(void)removePushQuestion:(NSNotification*)notification
{
    [self.view addSubview:[waitView getWaitView]];
    [[waitView getWaitView]startAnimation];
    [self.network sendRequest:[self.network createRefuseQuestionRequestWithAnswerId: notification.object andRefuse_type:0] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [[waitView getWaitView] stopAnimation];
        [[waitView getWaitView]removeFromSuperview];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"removeNotification" object:nil];
                for(int i = 0; i < self.questionListMutableArr.count;i++)
                {
                    question* key = [self.questionListMutableArr objectAtIndex:i];
                    if ([key.AnswerId isEqualToString:notification.object]) {
                        [self.questionListMutableArr removeObject:key];
                        break;
                    }
                }
                self.showRemindCell = YES;
                [self.questionListTableView reloadData];
            }
            else
            {
                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
            }
        }
        else
        {
            [tools showAlertView:@"提示" andMessage:errorMessage];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
            
        default:
            break;
    }
}

#pragma mark UITableViewDelegate
//判断cell是否被选中
//- (BOOL)cellIsSelected:(NSIndexPath *)indexPath {
//    for (NSIndexPath *currentIndex in _selectedIndexes.keyEnumerator) {
//        if (indexPath.row == currentIndex.row) {
//            NSNumber *isSelected = [_selectedIndexes objectForKey:currentIndex];
//            return isSelected ? isSelected.boolValue : NO;
//        }
//    }
//    return NO;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if([self cellIsSelected:indexPath]) {
//		return 360;
//	}
    if (self.showRemindCell) {
        if (indexPath.section == 0) {
            return 80;
        }
    }
    return 120;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.showRemindCell) {
        return 2;
    }else{
       return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.showRemindCell) {
        if (section == 0) {
            return 1;
        }else{
             return self.questionListMutableArr.count;
        }
    }else{
      return self.questionListMutableArr.count;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.showRemindCell) {
        if (indexPath.section == 0) {
            static NSString *remind_identifier = @"remindCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:remind_identifier];
            if (!cell) {
                NSArray *remindNib = [[NSBundle mainBundle]loadNibNamed:remind_identifier owner:Nil options:nil];
                if (remindNib.count > 0) {
                    cell = [remindNib objectAtIndex:0];
                    cell.backgroundColor = [UIColor clearColor];
                }
            }
            return cell;
        }
    }
    static NSString* practise_identifier = @"practiseCell";
    static NSString* official_identifier = @"officialCell";
    NSInteger row = indexPath.row;
    question* tempQuestion = (question*)[self.questionListMutableArr objectAtIndex:row];
    if ([tempQuestion.QuestionType isEqualToString:@"训练"]) {
        practiseCell* cell = [tableView dequeueReusableCellWithIdentifier:practise_identifier];
        if (!cell) {
            NSArray* practise = [[NSBundle mainBundle]loadNibNamed:practise_identifier owner:self options:nil];
            if ([practise count]>0) {
                cell = [practise objectAtIndex:0];
                cell.backgroundColor = [UIColor clearColor];
                [cell resolvingWithQuestion:tempQuestion];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        }
        return cell;
    }
    else
    {
        officialCell* cell = [tableView dequeueReusableCellWithIdentifier:official_identifier];
        if (!cell) {
            NSArray* official = [[NSBundle mainBundle]loadNibNamed:official_identifier owner:self options:nil];
            if ([official count]>0) {
                cell = [official objectAtIndex:0];
                cell.backgroundColor = [UIColor clearColor];
                [cell resolvingWithQuestion:tempQuestion];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.delegate = self;
            }
        }
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    //在tableView展开cell
//    BOOL isSelected = ![self cellIsSelected:indexPath];
//	NSNumber *selectedIndex = [NSNumber numberWithBool:isSelected];
//	[_selectedIndexes setObject:selectedIndex forKey:indexPath];
//	[tableView beginUpdates];
//    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
//	[tableView endUpdates];
//	[tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionNone animated:YES];
    if (self.showRemindCell) {
        if (indexPath.section == 0) {
            
        }else{
            if ([[(question*)[_questionListMutableArr objectAtIndex:indexPath.row] QuestionType]isEqualToString:@"训练"]) {
                //直接跳转问题详情
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                questionListDetailVC *next = [board instantiateViewControllerWithIdentifier:@"questionListDetailVC"];
                next.currentQuestion = [self.questionListMutableArr objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:next animated:YES];
            }
            else
            {
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                questionUnfoldVC *next = [board instantiateViewControllerWithIdentifier:@"questionUnfoldVC"];
                next.currentQuestion = [self.questionListMutableArr objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:next animated:YES];
            }
        }
    }else{
        if ([[(question*)[_questionListMutableArr objectAtIndex:indexPath.row] QuestionType]isEqualToString:@"训练"]) {
            //直接跳转问题详情
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            questionListDetailVC *next = [board instantiateViewControllerWithIdentifier:@"questionListDetailVC"];
            next.currentQuestion = [self.questionListMutableArr objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            questionUnfoldVC *next = [board instantiateViewControllerWithIdentifier:@"questionUnfoldVC"];
            next.currentQuestion = [self.questionListMutableArr objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:next animated:YES];
        }
    }
}

#pragma mark officialCellDelegate
-(void)officialGetQuestion:(question*)question
{
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    questionListDetailVC *next = [board instantiateViewControllerWithIdentifier:@"questionListDetailVC"];
    next.currentQuestion = question;
    [self.navigationController pushViewController:next animated:YES];
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
@end
