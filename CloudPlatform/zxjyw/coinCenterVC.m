//
//  coinCenterVC.m
//  zxjyw
//
//  Created by HelyData on 13-10-5.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//  积分中心

#import "coinCenterVC.h"
#import "UIHyperlinksButton.h"
#import "userInfo.h"
#import "coinCell.h"
#import "exchangeCell.h"
#import "coinRecord.h"
#import "exchangeRecord.h"
#import "tools.h"
#import "NetworkManager.h"
#import "loadCell.h"
#import "UIButton+Extensions.h"
#import "ResourseDefine.h"
#import "waitView.h"


@interface coinCenterVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *logoffButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *coinLabel;
@property (weak, nonatomic) IBOutlet UILabel *skillLabel;
@property (weak, nonatomic) IBOutlet UIHyperlinksButton *viewRuleButton;
@property (weak, nonatomic) IBOutlet UILabel *userName_bottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *coin_bottomLabel;
@property (retain, nonatomic) userInfo* user;
@property (weak, nonatomic) IBOutlet UIButton *btn1;//兑换记录按钮
@property (weak, nonatomic) IBOutlet UIButton *btn2;//积分记录按钮
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *recordListTableView;
@property (nonatomic,strong)NSMutableArray* coinArr;//积分记录数组
@property (nonatomic,strong)NSMutableArray* exchangeArr;//兑换记录数组
@property (nonatomic,assign)BOOL isLoading;
@property (nonatomic,assign)BOOL isDragging;
@property (nonatomic,assign)int pageIndex;
@property (nonatomic, strong) NetworkManager *network;
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *userTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *coinTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *userCenterButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *myCoinTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *exchangeButton;
@end

@implementation coinCenterVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!inch4) {
        [self.userTitleLabel setFrame:CGRectOffset(self.userTitleLabel.frame, 0, -10)];
        [self.userNameLabel setFrame:CGRectOffset(self.userNameLabel.frame, 0, -10)];
        [self.levelTitleLabel setFrame:CGRectOffset(self.levelTitleLabel.frame, 0, -10)];
        [self.levelLabel setFrame:CGRectOffset(self.levelLabel.frame, 0, -10)];
        [self.coinTitleLabel setFrame:CGRectOffset(self.coinTitleLabel.frame, 0, -10)];
        [self.coinLabel setFrame:CGRectOffset(self.coinLabel.frame, 0, -10)];
        [self.courseTitleLabel setFrame:CGRectOffset(self.courseTitleLabel.frame, 0, -10)];
        [self.skillLabel setFrame:CGRectOffset(self.skillLabel.frame, 0, -10)];
        [self.viewRuleButton setFrame:CGRectOffset(self.viewRuleButton.frame, 0, -10)];
        [self.btn1 setFrame:CGRectOffset(self.btn1.frame, 0, -30)];
        [self.btn2 setFrame:CGRectOffset(self.btn2.frame, 0, -30)];
        [self.titleLabel setFrame:CGRectOffset(self.titleLabel.frame, 0, -30)];
        [self.recordListTableView setFrame:CGRectMake(16, 245, 291, 170)];
        
        [self.userCenterButton setImage:[UIImage imageNamed:@"btn_userCenter_960.png"] forState:UIControlStateNormal];
        [self.userCenterButton setFrame:CGRectMake(0, 430, 320, 50)];
        [self.avatarImageView setFrame:CGRectOffset(self.avatarImageView.frame, 0, -85)];
        [self.userName_bottomLabel setFrame:CGRectOffset(self.userName_bottomLabel.frame, 0, -85)];
        [self.myCoinTitleLabel setFrame:CGRectOffset(self.myCoinTitleLabel.frame, 0, -85)];
        [self.coin_bottomLabel setFrame:CGRectOffset(self.coin_bottomLabel.frame, 0, -85)];
        [self.exchangeButton setFrame:CGRectOffset(self.exchangeButton.frame, 0, -85)];
        self.backGroundImageView.image = [UIImage imageNamed:@"coinCenterVC_background_960.png"];
    }
    self.network = [[NetworkManager alloc]init];
    //更新用户信息
    self.user = [userInfo shareUserInfo];
    self.userNameLabel.text = self.user.loginName;
    self.userName_bottomLabel.text = self.userNameLabel.text;
    self.levelLabel.text = self.user.level;
    self.coinLabel.text = [NSString stringWithFormat:@"%i",self.user.coins];
    self.coin_bottomLabel.text = self.coinLabel.text;
    self.skillLabel.text = self.user.course;
    self.pageIndex = 0;
    self.coinArr = [NSMutableArray array];
    self.exchangeArr = [NSMutableArray array];
    [self.viewRuleButton setIsHaveUnderLine:YES];
    self.recordListTableView.delegate = self;
    self.recordListTableView.dataSource = self;
    [self setButtonRect];
}
-(void)setButtonRect
{
    [self.backButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -5, -5, -20)];
    [self.logoffButton setHitTestEdgeInsets:UIEdgeInsetsMake(-5, -20, -5, -5)];
}
-(void)getRecord
{
    loadCell *cell = (loadCell*)[self.recordListTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    cell.activityIndicatorView.hidden = NO;
    cell.isLoading = YES;
    [cell.activityIndicatorView startAnimating];
    cell.statusLabel.text = @"正在加载...";
    self.pageIndex ++;
    [self.network sendRequest:[self.network createGetCoinRecordRequestWithPageIndex:self.pageIndex andPageSize:4] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        loadCell *cell = (loadCell*)[self.recordListTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        [cell.activityIndicatorView stopAnimating];
        cell.activityIndicatorView.hidden = YES;
        cell.isLoading = NO;
        cell.statusLabel.text = @"点击加载更多";
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                NSArray* arr = [data objectForKey:@"coinRecord"];
                if (arr.count == 0) {
                    [tools showAlertView:@"提示" andMessage:@"没有记录了"];
                }
                else
                {
                    for (NSDictionary* dic in arr) {
                        coinRecord* coin = [[coinRecord alloc]init];
                        [coin updateWithDictionary:dic];
                        [self.coinArr addObject:coin];
                    }
                }
                [self.recordListTableView reloadData];
            }
            else
            {
                self.pageIndex --;
                [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
            }
        }
        else
        {
            self.pageIndex --;
            [tools showAlertView:@"提示" andMessage:errorMessage];
        }
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressBtn:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 1:
        {
            //logoff
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"退出之后不再接收推送题目,是否确定退出？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = LOGOFF_TAG;
            [alert show];
            alert = nil;
        }
            break;
        case 2:
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        case 3:
        {
            //跳入兑换界面
            
        }
            break;
        case 4:
        {
            //点击兑换记录
            [self.btn1 setSelected:YES];
            [self.btn2 setSelected:NO];
            self.titleLabel.text = @"    兑换时间                      积分数                      兑换状态";
            [self.recordListTableView reloadData];
        }
            break;
        case 5:
        {
            //跳入积分记录
            [self.btn2 setSelected:YES];
            [self.btn1 setSelected:NO];
             [self.recordListTableView reloadData];
            if (self.coinArr.count == 0) {
                [self getRecord];
            }
            self.titleLabel.text = @"    评分时间                        获得原因               获得积分";
        }
            break;
        default:
            break;
    }
}
#pragma mark UITAbleViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.btn1.isSelected) {
        if (section == 0) {
            return  self.exchangeArr.count ;
        }else{
            return 1;
        }
    }else{
        if (section == 0) {
            return self.coinArr.count;
        }else{
            return 1;
        }
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *coin_identifier = @"coinCell";
    static NSString *exchange_identifier = @"exchangeCell";
    static NSString *load_identifier = @"loadCell";
    if (self.btn1.isSelected) {
        if (indexPath.section == 0) {
            exchangeCell* cell = [tableView dequeueReusableCellWithIdentifier:exchange_identifier];
            if (!cell) {
                NSArray* exchange = [[NSBundle mainBundle]loadNibNamed:exchange_identifier owner:self options:nil];
                if ([exchange count]>0) {
                    cell = [exchange objectAtIndex:0];
                    cell.backgroundColor = [UIColor clearColor];
                    [cell resolvingWithRecord:[self.exchangeArr objectAtIndex:indexPath.row]];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
            }
            return cell;
        }
        else
        {
            loadCell* cell = [tableView dequeueReusableCellWithIdentifier:load_identifier];
            if (!cell) {
                NSArray* load = [[NSBundle mainBundle]loadNibNamed:load_identifier owner:self options:nil];
                if ([load count]>0) {
                    cell = [load objectAtIndex:0];
                    cell.backgroundColor = [UIColor clearColor];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.activityIndicatorView.hidden = YES;
                    self.isLoading = NO;
                }
            }
            return cell;
        }
    }
    else
        {
            if (indexPath.section == 0) {
                coinCell* cell = [tableView dequeueReusableCellWithIdentifier:coin_identifier];
                if (!cell) {
                    NSArray* coin = [[NSBundle mainBundle]loadNibNamed:coin_identifier owner:self options:nil];
                    if ([coin count]>0) {
                        cell = [coin objectAtIndex:0];
                        cell.backgroundColor = [UIColor clearColor];
                        [cell resolvingWithRecord:[self.coinArr objectAtIndex:indexPath.row]];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    }
                }
                return cell;
            }
            else
            {
                loadCell* cell = [tableView dequeueReusableCellWithIdentifier:load_identifier];
                if (!cell) {
                    NSArray* load = [[NSBundle mainBundle]loadNibNamed:load_identifier owner:self options:nil];
                    if ([load count]>0) {
                        cell = [load objectAtIndex:0];
                        cell.backgroundColor = [UIColor clearColor];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        cell.activityIndicatorView.hidden = YES;
                        self.isLoading = NO;
                    }
                }
                 return cell;
            }
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.btn1.isSelected == YES && indexPath.section == 1) {
        //加载兑换记录
    }
    else if (self.btn2.isSelected == YES && indexPath.section == 1)
    {
        //加载积分记录
        loadCell *cell = (loadCell*)[tableView cellForRowAtIndexPath:indexPath];
        if (cell.isLoading == NO) {
            cell.activityIndicatorView.hidden = NO;
            [cell.activityIndicatorView startAnimating];
            cell.statusLabel.text = @"正在加载...";
            [self getRecord];
        }
    }
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == LOGOFF_TAG) {
        switch (buttonIndex) {
            case 0:
                
                break;
            case 1:
            {
                [self.view addSubview:[waitView getWaitView]];
                [[waitView getWaitView]startAnimation];
                [self.network sendRequest:[self.network createLogoffRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                    [[waitView getWaitView] stopAnimation];
                    [[waitView getWaitView]removeFromSuperview];
                    if (isSuccess) {
                        if ([[data objectForKey:@"error_code"]intValue] == 0) {
                            [tools deleteUser];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                        else
                        {
                            [tools showAlertView:@"提示" andMessage:[data objectForKey:@"error"]];
                        }
                    }
                    else
                    {
                        [tools showAlertView:@"提示" andMessage:errorMessage];
                    }
                }];
            }
                break;
            default:
                break;
        }
    }
}
@end
