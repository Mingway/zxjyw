//
//  exchangeRecord.h
//  zxjyw
//
//  Created by HelyData on 13-10-11.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface exchangeRecord : NSObject
@property(nonatomic,assign)int recordID;//兑换记录ID
@property(nonatomic,assign)int coin;//兑换积分数
@property(nonatomic,assign)int AnswerID;//该记录所指向的学生解答ID
@property(nonatomic,strong)NSString* date;//兑换积分的时间
@property(nonatomic,strong)NSString* status;//兑换状态

-(void)updateWithDictionary:(NSDictionary*)dic;
@end
