//
//  DrawingBoardViewController.m
//  DrawingBoard
//
//  Created by HelyData on 13-10-31.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "DrawingBoardViewController.h"
#import "ACEDrawingView.h"
#import <QuartzCore/QuartzCore.h>

#import "task.h"
#import "item.h"
#import "NetworkManager.h"
#import "Util.h"
#import "UIButton+UIButtonImageWithLable.h"
#import "user.h"
#import "EGOImageView.h"
#import  "waitView.h"
#import "WaitViewController.h"

#define DRAWING_VIEW_WIDTH 815.f
#define DRAWING_VIEW_HEIGHT 1000.f

#define LOADING_HEIGHT    60.0f

#define SHADOW_IMAGE_X  20.0f
#define SHADOW_IMAGE_WIDTH  170.0f

#define DRAWINGSCROLLVIEW_TAG  1001
#define THUMBNILSCROLLVIEW_TAG  1002

#define kActionSheetColor       100
#define kActionSheetTool        101
#define kActionSheetCarmer    102
#define kActionSheetAddRepo  103

#define degressToRadian(x) (M_PI * (x)/180.0)


@interface DrawingBoardViewController ()<UIActionSheetDelegate, ACEDrawingViewDelegate,UIScrollViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>
{
    NSTimer *timer;
    UIImagePickerController *imagePicker;
    UIPopoverController *popoverController;
}
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIScrollView *thumbNailScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *thumbNailImageView;//缩略图
@property (weak, nonatomic) IBOutlet UIView *thumbNailView;//显示缩略图区域
@property (weak, nonatomic) IBOutlet UIScrollView *drawingScrollView;//画板滚动区域
@property (weak, nonatomic) IBOutlet ACEDrawingView *drawingView;//画板
- (IBAction)pressBtn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *toolsView;
@property (weak, nonatomic) IBOutlet UIButton *undoButton;//撤销按钮
@property (weak, nonatomic) IBOutlet UIButton *redoButton;//恢复按钮
@property (weak, nonatomic) IBOutlet UIButton *clearButton;//清屏按钮
@property (weak, nonatomic) IBOutlet UIButton *eraserButton;//橡皮按钮
@property (weak, nonatomic) IBOutlet UIButton *colorButton;//颜色按钮
@property (weak, nonatomic) IBOutlet UIButton *toolButton;//画笔工具按钮
@property (weak, nonatomic) IBOutlet UIButton *addButton;//加一屏按钮
@property (weak, nonatomic) IBOutlet UIButton *lineWidthButton;
@property (weak, nonatomic) IBOutlet UISlider *lineWidthSlider;
- (IBAction)widthChanged:(UISlider *)sender;
@property (weak, nonatomic) IBOutlet UIButton *getLastQuestionButton;//上一题
@property (weak, nonatomic) IBOutlet UIButton *getNextQuestionButton;//下一题
@property (weak, nonatomic) IBOutlet UIWebView *questionContentWebView;//问题内容
@property (nonatomic, strong) NetworkManager *network;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isDragging;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel1;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel2;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel3;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel4;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel5;
@property (weak, nonatomic) IBOutlet EGOImageView *questionImageView;//题图
@property (weak, nonatomic) IBOutlet UIView *questionAreaView;//问题区域
@property (nonatomic, assign) float oldLineWidth;//橡皮前的线宽
@property (nonatomic, strong) UIColor *oldLineColor;//橡皮前的颜色
@property (nonatomic, assign) ACEDrawingToolType oldTool;//橡皮前的工具
@property (nonatomic, strong) UIImageView *tempImageView;//移动缩略图产生的中间ImageView
@property (weak, nonatomic) IBOutlet UILabel *courseNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (nonatomic,assign) BOOL isDragShadeImageView;
@property (nonatomic, strong) user *userModel;
@property (nonatomic, strong) item *currentItem;
@property (nonatomic, strong) UIView *noInputView;
@property (nonatomic, strong) UIButton *dragButton;
@property (nonatomic, assign) BOOL isFirstPoint;
@property (nonatomic, assign) CGPoint firstPoint;
@property (nonatomic, assign) BOOL isScrolled;
@end

@implementation DrawingBoardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [timer invalidate];
    timer = nil;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (timer == nil) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(count_down) userInfo:nil repeats:YES];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.thumbNailView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"thumbnail.png"]];
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    self.drawingView.delegate = self;
    [self initButtonImageAndTitle];
    self.lineWidthSlider.value = self.drawingView.lineWidth;
    [self updateGetQuestionButtonStatus];
    [self initData];
    self.drawingScrollView.delegate =self;
    self.drawingScrollView.tag = DRAWINGSCROLLVIEW_TAG;
    self.thumbNailScrollView.delegate = self;
    self.thumbNailScrollView.tag = THUMBNILSCROLLVIEW_TAG;
    for (UIGestureRecognizer *gestureRecognizer in self.drawingScrollView.gestureRecognizers)  {
        if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] == NO)  {continue;}
        UIPanGestureRecognizer *panGestureRecognizer = (UIPanGestureRecognizer *)gestureRecognizer;
        [panGestureRecognizer setMinimumNumberOfTouches:2];
    }
    [self loadData];
    self.toolsView.layer.cornerRadius = 5;
    //加载手掌禁写区
    [self addNoInputView];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.thumbNailScrollView addGestureRecognizer:singleTap];
    CGAffineTransform rotation = CGAffineTransformMakeRotation(degressToRadian(90));
    self.lineWidthSlider.transform = rotation;
     timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(count_down) userInfo:nil repeats:YES];
}
- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    CGPoint touchPoint=[gesture locationInView:self.thumbNailScrollView];
    float scale = self.drawingScrollView.frame.size.width / self.thumbNailScrollView.frame.size.width;
    CGRect visibleRect = CGRectMake(0, touchPoint.y*scale - self.drawingScrollView.frame.size.height/2, self.drawingScrollView.frame.size.width, self.drawingScrollView.frame.size.height);
    [self.drawingScrollView scrollRectToVisible:visibleRect animated:YES];
}
-(void)loadData
{
    self.studentNameLabel.text = self.userModel.name;
    self.courseNameLabel.text = self.currentTask.course_name;
    self.taskNameLabel.text = self.currentTask.task_name;
}
//初始化画板按钮标题和图片
-(void)initButtonImageAndTitle
{
    [self.undoButton setImage:[UIImage imageNamed:@"undo.png"] withTitle:@"撤销" forState:UIControlStateNormal];
    [self.redoButton setImage:[UIImage imageNamed:@"redo.png"] withTitle:@"恢复"forState:UIControlStateNormal];
    [self.clearButton setImage:[UIImage imageNamed:@"clear.png"] withTitle:@"清屏" forState:UIControlStateNormal];
    [self.eraserButton setImage:[UIImage imageNamed:@"eraser.png"] withTitle:@"橡皮" forState:UIControlStateNormal];
    [self.colorButton setImage:[UIImage imageNamed:@"color.png"] withTitle:@"黑色" forState:UIControlStateNormal];
    [self.toolButton setImage:[UIImage imageNamed:@"pen.png"] withTitle:@"铅笔" forState:UIControlStateNormal];
    [self.lineWidthButton setImage:[UIImage imageNamed:@"thickness.png"] withTitle:@"粗细"  forState:UIControlStateNormal];
    [self.addButton setImage:[UIImage imageNamed:@"add.png"] withTitle:@"加一屏" forState:UIControlStateNormal];
    [self.undoButton setImage:[UIImage imageNamed:@"undo_selected.png"] withTitle:@"撤销" forState:UIControlStateHighlighted];
    [self.redoButton setImage:[UIImage imageNamed:@"redo_selected.png"] withTitle:@"恢复" forState:UIControlStateHighlighted];
    [self.clearButton setImage:[UIImage imageNamed:@"clear_selected.png"] withTitle:@"清屏"  forState:UIControlStateHighlighted];
    [self.eraserButton setImage:[UIImage imageNamed:@"eraser_selected.png"] withTitle:@"橡皮" forState:UIControlStateHighlighted];
    [self.eraserButton setImage:[UIImage imageNamed:@"eraser_selected.png"] withTitle:@"橡皮" forState:UIControlStateSelected];
    [self.colorButton setImage:[UIImage imageNamed:@"color_selected.png"] withTitle:@"黑色" forState:UIControlStateHighlighted];
    [self.toolButton setImage:[UIImage imageNamed:@"pen_selected.png"] withTitle:@"铅笔" forState:UIControlStateHighlighted];
    [self.toolButton setImage:[UIImage imageNamed:@"pen_selected.png"] withTitle:@"铅笔" forState:UIControlStateSelected];
    [self.lineWidthButton setImage:[UIImage imageNamed:@"thickness_selected.png"] withTitle:@"粗细" forState:UIControlStateSelected];
     [self.addButton setImage:[UIImage imageNamed:@"add_selected.png"] withTitle:@"加一屏" forState:UIControlStateHighlighted];
}
-(void)resetSubmitButtonStatus
{
    if (self.currentItem.status == 0) {
        self.submitButton.enabled = NO;
    }else{
        self.submitButton.enabled = YES;
    }
}
#pragma mark initData
//重设画笔和颜色
- (void)resetSetting
{
    //reset
    self.photo = nil;
    [self.drawingView clear];
    [self updateBoardButtonStatus];
    [self.colorButton setTitle:@"黑色" forState:UIControlStateNormal];
    self.drawingView.lineColor = [UIColor blackColor];
    [self.toolButton setTitle:@"铅笔" forState:UIControlStateNormal];
    self.drawingView.drawTool = ACEDrawingToolTypePen;
    [self.toolButton setSelected:YES];
}
//初始化数据
- (void)initData
{
    self.questionImageView.image = nil;
    self.currentItem = [self.currentTask.testItemArr objectAtIndex:self.currentIndex];
    //重置保存的颜色和粗细数据
    self.oldLineColor = [UIColor blackColor];
    self.oldTool = ACEDrawingToolTypePen;
    self.lineWidthSlider.hidden = YES;
    self.oldLineWidth = 2.0f;
    self.lineWidthSlider.value = 2.0f;
    [self.toolButton setSelected:YES];
    [self resetSubmitButtonStatus];
    [self.lineWidthButton scaleImageWithFloat:2];
    //加载问题内容
    [self.questionContentWebView loadHTMLString:self.currentItem.question baseURL:nil];
    //加载图片
    BOOL drawQuestionImage = NO;
    if ([self.currentItem.itemType isEqualToString:@"填空题"]) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@.png",[self getPath:self.currentTask.taskId],self.currentItem.itemId];
        UIImage *tempImage = nil;
        if([fileManager fileExistsAtPath:filePath]) {
            tempImage = [UIImage imageWithContentsOfFile:filePath];
            [self setImageFrame:tempImage WithPoint:CGPointZero];
        }else{
            [self.drawingView setFrame:CGRectMake(0, 0, DRAWING_VIEW_WIDTH,DRAWING_VIEW_HEIGHT)];
            self.drawingScrollView.contentSize = CGSizeMake(DRAWING_VIEW_WIDTH, DRAWING_VIEW_HEIGHT);
            //加载填空题区域
            for (int i = 0; i < self.currentItem.blankNum;i++) {
                [self.drawingView drawText:[NSString stringWithFormat:@"第%i个空",i+1]atPoint:CGPointMake(50, 100+200*i)];
            }
        }
    }else{
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@.png",[self getPath:self.currentTask.taskId],self.currentItem.itemId];
        UIImage *tempImage = nil;
        if (self.photo != nil) {
            //有拍照的图片
            tempImage = self.photo;
            [self setImageFrame:tempImage WithPoint:CGPointZero];
        }else if([fileManager fileExistsAtPath:filePath]) {
            //有缓存的图片
            tempImage = [UIImage imageWithContentsOfFile:filePath];
            [self setImageFrame:tempImage WithPoint:CGPointZero];
        }else{
            if (self.currentItem.answer != nil) {
                //加载答案
                [self.view addSubview:[waitView getDeafaultView]];
                [[waitView getDeafaultView]startAnimation];
                [self.network downloadReourceRequest:[self.network createDownloadImageRequestWithResorseId:self.currentItem.answer] withCompletionHandler:^(BOOL isSuccess, NSData *data, NSString *errorMessage) {
                    [[waitView getDeafaultView]startAnimation];
                    [[waitView getDeafaultView]removeFromSuperview];
                    if (isSuccess) {
                        UIImage* tempImage = [UIImage imageWithData:data];
                        [self setImageFrame:tempImage WithPoint:CGPointZero];
                    }
                }];
            }else if(self.currentItem.imageUrl != nil){
                //有题图
                drawQuestionImage = YES;
            }else{
                [self.drawingView setFrame:CGRectMake(0, 0, DRAWING_VIEW_WIDTH,DRAWING_VIEW_HEIGHT)];
                self.drawingScrollView.contentSize = CGSizeMake(DRAWING_VIEW_WIDTH, DRAWING_VIEW_HEIGHT);
            }
        }
    }
    //加载题图
    if (self.currentItem.imageUrl != nil) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@_figureImage.png",[self getPath:self.currentTask.taskId],self.currentItem.itemId];
        if ([fileManager fileExistsAtPath:filePath]) {
            self.questionImageView.image = [UIImage imageWithContentsOfFile:filePath];
        }else{
            [self.view addSubview:[waitView getDeafaultView]];
            [[waitView getDeafaultView]startAnimation];
            [self.network downloadReourceRequest:[self.network  createDownloadImageRequestWithUrl:[self.currentItem.imageUrl absoluteString]] withCompletionHandler:^(BOOL isSuccess, NSData *data, NSString *errorMessage) {
                [[waitView getDeafaultView]startAnimation];
                [[waitView getDeafaultView]removeFromSuperview];
                if (isSuccess) {
                    self.questionImageView.image = [UIImage imageWithData:data];
                }
                if (drawQuestionImage) {
                    float x = self.questionImageView.image.size.width;
                    if (x > 815) {
                        x = 815;
                    }
                    [self setImageFrame:self.questionImageView.image WithPoint:CGPointMake(815-x, 50)];
                }
                [self saveQuestionFigureImage:self.questionImageView.image];
                [self saveImageReset:NO];
            }];
        }
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handelPan:)];
        [self.questionImageView addGestureRecognizer:pan];
    }else{
        [self saveImageReset:NO];
    }
    [self.drawingView refreshView];
}

//根据图片大小划定输入区大小
-(void)setImageFrame:(UIImage*)tempImage WithPoint:(CGPoint)point
{
    if (tempImage) {
        if (tempImage.size.width > DRAWING_VIEW_WIDTH) {
            tempImage = [DrawingBoardViewController fitSmallImage:tempImage];
            if (tempImage.size.height > DRAWING_VIEW_HEIGHT) {
                [self.drawingView setFrame:CGRectMake(0, 0, tempImage.size.width,tempImage.size.height)];
            }else{
                [self.drawingView setFrame:CGRectMake(0, 0, tempImage.size.width,DRAWING_VIEW_HEIGHT)];
            }
        }else{
            if (tempImage.size.height >DRAWING_VIEW_HEIGHT) {
                [self.drawingView setFrame:CGRectMake(0, 0, DRAWING_VIEW_WIDTH,tempImage.size.height)];
            }else{
                [self.drawingView setFrame:CGRectMake(0, 0, DRAWING_VIEW_WIDTH,DRAWING_VIEW_HEIGHT)];
            }
        }
        [self.drawingScrollView setContentSize:CGSizeMake(self.drawingView.frame.size.width, self.drawingView.frame.size.height )];
        [self.drawingView drawImage:tempImage atPoint:point];
    }else{
        [self.drawingView setFrame:CGRectMake(0, 0, DRAWING_VIEW_WIDTH,DRAWING_VIEW_HEIGHT)];
    }
}
//加载手掌禁写区
-(void)addNoInputView
{
    CGRect thumbNailScrollViewFrame = self.thumbNailScrollView.frame;
    if (self.noInputView != nil) {
        [self.thumbNailScrollView setFrame:CGRectMake(thumbNailScrollViewFrame.origin.x, 0, thumbNailScrollViewFrame.size.width, thumbNailScrollViewFrame.size.height+ self.noInputView.frame.size.height)];
        [self.noInputView removeFromSuperview];
        self.noInputView = nil;
    }
    self.noInputView =  [[UIView alloc]initWithFrame:CGRectMake(0, 528, 1024, 240)];
    self.noInputView.alpha = 1;
    [self.noInputView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"NoInputViewBackground"]]];
    [self.view addSubview:self.noInputView];
    self.dragButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.dragButton setFrame:CGRectMake(0,self.noInputView.frame.origin.y -26, 46, 52)];
    self.dragButton.backgroundColor = [UIColor clearColor];
    [self.dragButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    [self.view addSubview:self.dragButton];
    [self.dragButton addTarget:self action:@selector(dragMoving:withEvent: )forControlEvents: UIControlEventTouchDragInside];
    [self.dragButton addTarget:self action:@selector(dragEnded:withEvent: )forControlEvents: UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    [self.thumbNailScrollView setFrame:CGRectMake(thumbNailScrollViewFrame.origin.x, 0, thumbNailScrollViewFrame.size.width, thumbNailScrollViewFrame.size.height-240)];
}
- (void)dragMoving: (UIControl *) c withEvent:ev
{
    CGPoint  center = [[[ev allTouches] anyObject] locationInView:self.view];
    c.center = CGPointMake(c.center.x, center.y);
    CGPoint leftPoint = CGPointMake(center.x - 23, center.y );
    if (!self.isFirstPoint) {
        self.firstPoint = self.noInputView.frame.origin;
        self.isFirstPoint = YES;
    }
    float offset =  self.noInputView.frame.origin.y - leftPoint.y ;
    [UIView animateWithDuration:0.1f animations:^{
        [self.noInputView setFrame:CGRectMake(0, leftPoint.y, self.noInputView.frame.size.width, self.noInputView.frame.size.height + offset)];
    }];
}
- (void)dragEnded: (UIControl *) c withEvent:ev
{
    CGPoint  center = [[[ev allTouches] anyObject] locationInView:self.view];
    c.center = CGPointMake(c.center.x, center.y);
    CGPoint leftPoint = CGPointMake(center.x - 23, center.y);
    CGRect thumbNailScrollViewViewFrame = self.thumbNailScrollView.frame;
    [self.thumbNailScrollView setFrame:CGRectMake(thumbNailScrollViewViewFrame.origin.x, 0, thumbNailScrollViewViewFrame.size.width, self.noInputView.frame.origin.y - self.thumbNailView.frame.origin.y)];
    float offset =  self.noInputView.frame.origin.y - leftPoint.y ;
    [UIView animateWithDuration:0.1f animations:^{
        [self.noInputView setFrame:CGRectMake(0, leftPoint.y, self.noInputView.frame.size.width, self.noInputView.frame.size.height + offset)];
    }];
    self.isFirstPoint = NO;
}
//等比例压缩图片
#pragma mark fitSmallImage
+(UIImage *)fitSmallImage:(UIImage *)image
{
    if (nil == image){
         return nil;
    }
    if (image.size.width<DRAWING_VIEW_WIDTH){
        return image;
    }
    CGSize size = CGSizeMake(DRAWING_VIEW_WIDTH, image.size.height * DRAWING_VIEW_WIDTH/image.size.width);
    UIGraphicsBeginImageContext(size);
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    [image drawInRect:rect];
    UIImage *newing = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newing;
}
#pragma mark ThumbNailImageViewFrame
-(void)setThumbNailImageViewFrame
{
    self.thumbNailImageView.image = self.drawingView.image;
    float height = 170.f*self.drawingView.image.size.height/self.drawingView.image.size.width;
    CGRect imageViewFrame = self.thumbNailImageView.frame;
    [UIView animateWithDuration:0.1f animations:^{
        [self.thumbNailImageView setFrame:CGRectMake(0, 0, imageViewFrame.size.width, height)];
        self.thumbNailScrollView.contentSize = CGSizeMake(self.thumbNailScrollView.contentSize.width, self.thumbNailImageView.frame.size.height);
    }];
}
#pragma mark handleQuestionImageViewPan
//拖拽题图操作
-(void)handelPan:(UIPanGestureRecognizer*)gestureRecognizer{
    if (self.tempImageView == nil) {
         self.tempImageView = [[UIImageView alloc]init];
        self.tempImageView.image = self.questionImageView.image;
        [self.tempImageView setFrame:CGRectMake(0, 0, self.tempImageView.image.size.width, self.tempImageView.image.size.height)];
        [self.view addSubview:self.tempImageView];
    }
    CGPoint curPoint = [gestureRecognizer locationInView:self.view];
    if(gestureRecognizer.state != UIGestureRecognizerStateEnded){
        [self.tempImageView setCenter:curPoint];
    }else{
        [self.tempImageView removeFromSuperview];
        self.tempImageView = nil;
        //判断落点是否在画图区域
        if(CGRectContainsPoint(self.drawingScrollView.frame, curPoint))
        {
            float x = curPoint.x - self.drawingScrollView.frame.origin.x +self.drawingScrollView.contentOffset.x - self.questionImageView.image.size.width/2;
            float y = curPoint.y - self.drawingScrollView.frame.origin.y + self.drawingScrollView.contentOffset.y - self.questionImageView.image.size.height/2;
            [self.drawingView drawImage:self.questionImageView.image atPoint:CGPointMake(x, y)];
            self.currentItem.status = 1;
            [self resetSubmitButtonStatus];
        }else{
            
        }
    }
}

//倒计时
- (void)count_down
{
    [self setThumbNailImageViewFrame];
    int total_time = self.currentTask.timeLimit;
    NSDate* now = [Util localDate];
    int count_down = [now timeIntervalSinceDate:self.currentTask.startDate];
    if (count_down < total_time) {
        count_down = total_time - count_down;
        int minutes = count_down/60;
        int seconds = count_down%60;
        self.timeLabel1.text = [NSString stringWithFormat:@"%i",minutes/100];
        self.timeLabel2.text = [NSString stringWithFormat:@"%i",(minutes - (minutes/100)*100)/10];
        self.timeLabel3.text = [NSString stringWithFormat:@"%i",minutes - ((minutes/10)*10)];
        self.timeLabel4.text = [NSString stringWithFormat:@"%i",seconds/10];
        self.timeLabel5.text = [NSString stringWithFormat:@"%i",seconds%10];
    }else{
        [timer invalidate];
        timer = nil;
        self.timeLabel1.text = [NSString stringWithFormat:@"%i",0];
        self.timeLabel2.text = [NSString stringWithFormat:@"%i",0];
        self.timeLabel3.text = [NSString stringWithFormat:@"%i",0];
        self.timeLabel4.text = [NSString stringWithFormat:@"%i",0];
        self.timeLabel5.text = [NSString stringWithFormat:@"%i",0];
       [self submitAllAnswerWithCompitionHandler:^{
            [Util showAlertViewWithTitle:@"提示" Message:@"答题时间已过！"];
        }];
    }
}
-(void)backToWaitVC
{
    BOOL exits = NO;
    for (UIViewController *viewController in self.navigationController.viewControllers) {
        if ([viewController isKindOfClass:[WaitViewController class]]) {
            [self.navigationController popToViewController:viewController animated:YES];
            exits = YES;
        }
    }
    if (!exits) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        WaitViewController *waitVC = [storyboard instantiateViewControllerWithIdentifier:@"WaitViewController"];
        [self.navigationController pushViewController:waitVC animated:YES];
    }
}
//改变上一题下一题按钮状态
- (void)updateGetQuestionButtonStatus
{
    if (self.currentIndex == 0) {
        self.getLastQuestionButton.enabled = NO;
    }else{
        self.getLastQuestionButton.enabled = YES;
    }
    if (self.currentIndex == self.currentTask.testItemArr.count-1){
        self.getNextQuestionButton.enabled = NO;
    }else{
        self.getNextQuestionButton.enabled = YES;
    }
}
//重置撤销恢复按钮状态
- (void)updateBoardButtonStatus
{
    self.undoButton.enabled = [self.drawingView canUndo];
    self.redoButton.enabled = [self.drawingView canRedo];
}
//按钮操作汇总
- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //undo
            [self.drawingView undoLatestStep];
            [self updateBoardButtonStatus];
            self.currentItem.status = 1;
            [self resetSubmitButtonStatus];
        }
            break;
        case 1:
        {
            //redo
            [self.drawingView redoLatestStep];
            [self updateBoardButtonStatus];
            self.currentItem.status = 1;
            [self resetSubmitButtonStatus];
        }
            break;
        case 2:
        {
            //clear
            [self.drawingView clear];
            [self updateBoardButtonStatus];
            self.currentItem.status = 1;
            [self resetSubmitButtonStatus];
        }
            break;
        case 3:
        {
            //eraser
            [sender setSelected:YES];
            self.oldTool = self.drawingView.drawTool;
            self.drawingView.drawTool = ACEDrawingToolTypePen;
            [self.toolButton setSelected:NO];
            [self.drawingView eraser];
            switch (self.oldTool) {
                case ACEDrawingToolTypePen:
                    [self.toolButton setTitle:@"铅笔" forState:UIControlStateNormal];
                    break;
                case ACEDrawingToolTypeLine:
                    [self.toolButton setTitle:@"直线" forState:UIControlStateNormal];
                    break;
                case ACEDrawingToolTypeRectagleStroke:
                    [self.toolButton setTitle:@"方形" forState:UIControlStateNormal];
                    break;
                case 3:
                    [self.toolButton setTitle:@"方形(填充)" forState:UIControlStateNormal];
                    break;
                case 4:
                    [self.toolButton setTitle:@"圆形" forState:UIControlStateNormal];
                    break;
                case 5:
                    [self.toolButton setTitle:@"圆形(填充)" forState:UIControlStateNormal];
                    break;
            }
        }
            break;
        case 4:
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"选择一个颜色"
                                                                     delegate:self
                                                            cancelButtonTitle:@"取消"
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:@"黑色", @"红色", @"绿色", @"蓝色", nil];
            
            [actionSheet setTag:kActionSheetColor];
            [actionSheet showInView:self.view];
            actionSheet = nil;
        }
            break;
        case 5:
        {
            [sender setSelected:YES];
            [self.eraserButton setSelected:NO];
            self.drawingView.lineWidth = self.oldLineWidth;
            self.drawingView.lineColor = self.oldLineColor;
            self.drawingView.drawTool = self.oldTool;
        }
            break;
        case 6:
        {
            //跳转到PC端答题
            [self submitAllAnswerWithCompitionHandler:^{
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"已跳转回PC端" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                alert = nil;
            }];
        }
            break;
        case 7:
        {
            //改为回到列表
            if ([self saveImageReset:YES]) {
//                self.currentItem.status = 0;
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
            break;
        case 8:
        {
            //加入题库
            UIActionSheet *sheet = [[UIActionSheet alloc]initWithTitle:@"选择题库" delegate:self cancelButtonTitle:Nil destructiveButtonTitle:@"取消" otherButtonTitles:@"难题",@"好题", nil];
            [sheet setTag:kActionSheetAddRepo];
            [sheet showInView:self.view];
            sheet = nil;
        }
            break;
        case 9:
        {
            //拍照解题
            UIActionSheet *sheet;
            // 判断是否支持相机
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                sheet  = [[UIActionSheet alloc] initWithTitle:@"选择图像" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"取消" otherButtonTitles:@"拍照", @"从相册选择", nil];
            }
            else {
                sheet = [[UIActionSheet alloc] initWithTitle:@"选择图像" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"取消" otherButtonTitles:@"从相册选择", nil];
            }
            [sheet setTag:kActionSheetCarmer];
            [sheet showInView:self.view];
            sheet = nil;
        }
            break;
        case 10:
        {
            //submit image,then submit the question info  提交答题
            [self saveImageAndSubmitReset:NO Finished:NO WithComplitionHandler:^{
                self.currentItem.status = 0;
                [self resetSubmitButtonStatus];
            }];
        }
            break;
        case 11:
        {
            //收起题目和展开题目
            [sender setSelected:!sender.isSelected];
            if (sender.isSelected) {
                [UIView animateWithDuration:0.3f animations:^{
                    [self.questionAreaView setFrame:CGRectOffset(self.questionAreaView.frame, 0, -140)];
                    CGRect drawingScrollViewFrame = self.drawingScrollView.frame;
                    [self.drawingScrollView setFrame:CGRectMake(drawingScrollViewFrame.origin.x, drawingScrollViewFrame.origin.y-140, drawingScrollViewFrame.size.width, drawingScrollViewFrame.size.height+140)];
                    [self.toolsView setFrame:CGRectOffset(self.toolsView.frame, 0, -140)];
                    CGRect thumbNailViewFrame = self.thumbNailView.frame;
                    [self.thumbNailView setFrame:CGRectMake(thumbNailViewFrame.origin.x, thumbNailViewFrame.origin.y-140, thumbNailViewFrame.size.width, thumbNailViewFrame.size.height+140)];
                    CGRect thumbNailImageViewFrame = self.thumbNailImageView.frame;
                    [self.thumbNailImageView setFrame:CGRectMake(thumbNailImageViewFrame.origin.x, thumbNailImageViewFrame.origin.y, thumbNailImageViewFrame.size.width, thumbNailImageViewFrame.size.height + 140)];
                    [self.lineWidthSlider setFrame:CGRectOffset(self.lineWidthSlider.frame, 0, -140)];
                } completion:^(BOOL finished) {
                }];
            }else{
                [UIView animateWithDuration:0.3f animations:^{
                    [self.questionAreaView setFrame:CGRectOffset(self.questionAreaView.frame, 0, 140)];
                    CGRect drawingScrollViewFrame = self.drawingScrollView.frame;
                    [self.drawingScrollView setFrame:CGRectMake(drawingScrollViewFrame.origin.x, drawingScrollViewFrame.origin.y+140, drawingScrollViewFrame.size.width, drawingScrollViewFrame.size.height-140)];
                    [self.toolsView setFrame:CGRectOffset(self.toolsView.frame, 0, 140)];
                    CGRect thumbNailViewFrame = self.thumbNailView.frame;
                    [self.thumbNailView setFrame:CGRectMake(thumbNailViewFrame.origin.x, thumbNailViewFrame.origin.y+140, thumbNailViewFrame.size.width, thumbNailViewFrame.size.height-140)];
                    CGRect thumbNailImageViewFrame = self.thumbNailImageView.frame;
                    [self.thumbNailImageView setFrame:CGRectMake(thumbNailImageViewFrame.origin.x, thumbNailImageViewFrame.origin.y, thumbNailImageViewFrame.size.width, thumbNailImageViewFrame.size.height - 140)];
                    [self.lineWidthSlider setFrame:CGRectOffset(self.lineWidthSlider.frame, 0, 140)];
                } completion:^(BOOL finished) {
                }];
            }
        }
            break;
        case 12:
        {
            //last    getLastQuestion //上一题
            if (self.currentItem.status == 1) {
                [self saveImageAndSubmitReset:YES Finished:NO  WithComplitionHandler:^{
                    self.currentItem.status = 0;
                    self.currentIndex -- ;
                    [self initData];
                    [self updateGetQuestionButtonStatus];
                }];
            }else{
                [self resetSetting];
                self.currentIndex -- ;
                [self initData];
                [self updateGetQuestionButtonStatus];
            }
        }
            break;
        case 13:
        {
            //next   getNextQuestion   下一题
            //submit image,then submit the question info
            if (self.currentItem.status == 1) {
                [self saveImageAndSubmitReset:YES Finished:NO WithComplitionHandler:^{
                    self.currentItem.status = 0;
                    self.currentIndex ++;
                    [self initData];
                    [self updateGetQuestionButtonStatus];
                }];
            }else{
                [self resetSetting];
                self.currentIndex ++;
                [self initData];
                [self updateGetQuestionButtonStatus];
            }
        }
            break;
        case 14:
        {
            //调节画笔粗细
            self.lineWidthSlider.hidden = !self.lineWidthSlider.hidden;
        }
            break;
        case 15:
        {
            
        }
            break;
        case 16:
        {
            //加一屏
            [self addDrawingBoardHeight:self.drawingScrollView];
            CGPoint point = self.drawingScrollView.contentOffset;
            [self.drawingScrollView setContentOffset:CGPointMake(0, point.y + 200)];
            self.currentItem.status = 1;
            [self resetSubmitButtonStatus];
        }
            break;
        case 17:
        {
            //选择工具
            self.drawingView.lineWidth = self.oldLineWidth;
            self.drawingView.lineColor = self.oldLineColor;
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"选择一个工具"
                                                                     delegate:self
                                                            cancelButtonTitle:@"取消"
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:@"铅笔", @"直线",
                                          @"方形", @"方形(填充)",
                                          @"圆形", @"圆形(填充)",
                                          nil];
            
            [actionSheet setTag:kActionSheetTool];
            [actionSheet showInView:self.view];
            actionSheet = nil;
        }
            break;
        default:
            break;
    }
}

//缓存图片
#pragma mark SaveImage
- (BOOL)saveQuestionFigureImage:(UIImage*)image
{
    NSData *data;
    if (UIImagePNGRepresentation(image) == nil) {
        data = UIImageJPEGRepresentation(image, 1);
    } else {
        data = UIImagePNGRepresentation(image);
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithString:[self getPath:self.currentTask.taskId]];
    NSLog(@"filePath = %@",filePath);
    [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:Nil error:nil];
    [fileManager createFileAtPath:[filePath stringByAppendingString:[NSString stringWithFormat:@"/%@_figureImage.png",self.currentItem.itemId]] contents:data attributes:nil];
    return YES;
}
- (BOOL)saveImageReset:(BOOL)reset
{
    if (self.drawingView.image != nil) {
        UIImage *tempImage = self.drawingView.image;
        NSData *data;
        if (UIImagePNGRepresentation(tempImage) == nil) {
            data = UIImageJPEGRepresentation(tempImage, 0.00001);
        } else {
            data = UIImagePNGRepresentation(tempImage);
        }
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath = [NSString stringWithString:[self getPath:self.currentTask.taskId]];
        NSLog(@"filePath = %@",filePath);
        [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:Nil error:nil];
        [fileManager createFileAtPath:[filePath stringByAppendingString:[NSString stringWithFormat:@"/%@.png",self.currentItem.itemId]] contents:data attributes:nil];
        if (reset) {
            [self resetSetting];
        }
    }
    return YES;
}

- (NSData *)saveImage
{
    UIImage *tempImage = self.drawingView.image;
    NSData *data;
    if (UIImagePNGRepresentation(tempImage) == nil) {
        data = UIImageJPEGRepresentation(tempImage, 0.00001);
    } else {
        data = UIImagePNGRepresentation(tempImage);
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithString:[self getPath:self.currentTask.taskId]];
    [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:Nil error:nil];
    [fileManager createFileAtPath:[filePath stringByAppendingString:[NSString stringWithFormat:@"/%@.png",self.currentItem.itemId]] contents:data attributes:nil];
    return data;
}

//保存缓存图片并且上传临时答案
- (BOOL)saveImageAndSubmitReset:(BOOL)reset Finished:(BOOL)finished WithComplitionHandler:(void (^)())completionHandler
{
    if (self.drawingView.image != nil) {
        NSData *data = [self saveImage];
        //上传临时答案
        NSString *dataString = [data base64Encoding];
        [self.view addSubview:[waitView getDeafaultView]];
        [[waitView getDeafaultView]startAnimation];
        if (reset) {
            [waitView getDeafaultView].statusLabel.text = @"正在上传答案，并切换题目...";
        }else{
            [waitView getDeafaultView].statusLabel.text = @"正在上传答案...";
        }
        return  [self.network sendRequest:[self.network createUploadResourseRequestWithResData:dataString width:self.drawingView.image.size.width height:self.drawingView.image.size.height] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
           if (isSuccess) {
               if ([[data objectForKey:@"error_code"]intValue] == 0) {
                   [self.network sendRequest:[self.network createSubmitAnswerTmpRequestWithTaskId:self.currentTask.taskId itemId:[NSMutableArray arrayWithObjects:[self.currentItem itemId], nil] ItemAnswer:[NSMutableArray arrayWithObjects:[data objectForKey:@"res_id"], nil] finished:finished] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                       [[waitView getDeafaultView]stopAnimation];
                       [[waitView getDeafaultView]removeFromSuperview];
                       if (isSuccess) {
                           if([[data objectForKey:@"error_code"]intValue] == 0) {
                               if (reset) {
                                   [self resetSetting];
                               }
                               completionHandler();
                           }else{
                               [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                           }
                       }
                       else{
                           [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                       }
                   }];
               } else{
                   [[waitView getDeafaultView]stopAnimation];
                   [[waitView getDeafaultView]removeFromSuperview];
                   [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
               }
           }else{
               [[waitView getDeafaultView]stopAnimation];
               [[waitView getDeafaultView]removeFromSuperview];
               [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
           }
        }];
    }
    return YES;
}
#pragma mark getPath
- (NSString*)getPath:(NSString*)dirName
{
    return [NSString stringWithFormat:@"%@%@",NSTemporaryDirectory(),dirName];
}
#pragma mark submitAllAnswer
-(void)submitAllAnswerWithCompitionHandler:(void(^)())complitionHandler
{
    [self saveImage];
    NSMutableArray *itemArr = [NSMutableArray array];
    for (int i = 0; i < self.currentTask.testItemArr.count ; i ++) {
        item *tempItem = [self.currentTask.testItemArr objectAtIndex:i];
        if (tempItem.status == 1) {
            [itemArr addObject:tempItem];
        }
    }
    [self.view addSubview:[waitView getDeafaultView]];
    [[waitView getDeafaultView]startAnimation];
    [self submit:itemArr compitionHandler:^{
        [[waitView getDeafaultView]stopAnimation];
        [[waitView getDeafaultView]removeFromSuperview];
        complitionHandler();
    }];
}
-(void)submit:(NSMutableArray*)itemArr compitionHandler:(void(^)())complitionHandler
{
    if (itemArr.count > 0) {
        item *tempItem = [itemArr lastObject];
        UIImage *tempImage;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@.png",[self getPath:self.currentTask.taskId],tempItem.itemId];
        if ([fileManager fileExistsAtPath:filePath]) {
            tempImage = [UIImage imageWithContentsOfFile:filePath];
        }
        NSData *data;
        if (UIImagePNGRepresentation(tempImage) == nil) {
            data = UIImageJPEGRepresentation(tempImage, 0.00001);
        } else {
            data = UIImagePNGRepresentation(tempImage);
        }
        NSString *dataString = [data base64Encoding];
        [self.network sendRequest:[self.network createUploadResourseRequestWithResData:dataString width:tempImage.size.width height:tempImage.size.height] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [self.network sendRequest:[self.network createSubmitAnswerTmpRequestWithTaskId:self.currentTask.taskId itemId:[NSMutableArray arrayWithObjects:tempItem.itemId, nil] ItemAnswer:[NSMutableArray arrayWithObjects:[data objectForKey:@"res_id"], nil] finished:NO] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                        if (isSuccess) {
                            if([[data objectForKey:@"error_code"]intValue] == 0) {
                                [itemArr removeLastObject];
                                [self submit:itemArr compitionHandler:complitionHandler];
                            }else{
                                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                            }
                        }
                        else{
                            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                        }
                    }];
                } else{
                    [[waitView getDeafaultView]stopAnimation];
                    [[waitView getDeafaultView]removeFromSuperview];
                    [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                }
            }else{
                [[waitView getDeafaultView]stopAnimation];
                [[waitView getDeafaultView]removeFromSuperview];
                [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
            }
        }];
    }else{
        [self.view addSubview:[waitView getDeafaultView]];
        [[waitView getDeafaultView]startAnimation];
        [self.network sendRequest:[self.network createSubmitAnswerTmpRequestWithTaskId:self.currentTask.taskId itemId:Nil ItemAnswer:nil finished:YES] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            [[waitView getDeafaultView]stopAnimation];
            [[waitView getDeafaultView]removeFromSuperview];
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [Util clearCacheWithTaskId:self.currentTask.taskId];
                    complitionHandler();
                }
            }
        }];
    }
}
#pragma mark calculatePosition
-(void)calculateThumbPosition
{
    CGRect thumbNailBounds = self.thumbNailScrollView.bounds;
    CGRect drawingBounds = self.drawingScrollView.bounds;
    float scale = thumbNailBounds.size.width/drawingBounds.size.width;
    if (drawingBounds.origin.y <= 0) {
        [UIView animateWithDuration:0.1f animations:^{
            [self.thumbNailScrollView setContentOffset:CGPointMake(0, 0)];
        }];
    }else if (drawingBounds.origin.y <= thumbNailBounds.origin.y/scale  && drawingBounds.origin.y +drawingBounds.size.height  <= (thumbNailBounds.origin.y + thumbNailBounds.size.height)/scale) {
        [UIView animateWithDuration:0.1f animations:^{
            [self.thumbNailScrollView setContentOffset:CGPointMake(0, drawingBounds.origin.y*scale)];
        }];
    }else if (drawingBounds.origin.y +drawingBounds.size.height  > (thumbNailBounds.origin.y + thumbNailBounds.size.height)/scale){
        [UIView animateWithDuration:0.1f animations:^{
            [self.thumbNailScrollView setContentOffset:CGPointMake(0, (drawingBounds.origin.y +drawingBounds.size.height)*scale - thumbNailBounds.size.height)];
        }];
    }
}
-(void)calculateDrawingPosition
{
    CGRect thumbNailBounds = self.thumbNailScrollView.bounds;
    CGRect drawingBounds = self.drawingScrollView.bounds;
    float scale = drawingBounds.size.width/thumbNailBounds.size.width;
    if (thumbNailBounds.origin.y <= 0) {
        [UIView animateWithDuration:0.1f animations:^{
            [self.drawingScrollView setContentOffset:CGPointMake(0, 0)];
        }];
    }else if (thumbNailBounds.origin.y <= drawingBounds.origin.y/scale  && thumbNailBounds.origin.y +thumbNailBounds.size.height  <= (drawingBounds.origin.y + drawingBounds.size.height)/scale) {
        [UIView animateWithDuration:0.1f animations:^{
            [self.drawingScrollView setContentOffset:CGPointMake(0, thumbNailBounds.origin.y*scale)];
        }];
    }else if (thumbNailBounds.origin.y +thumbNailBounds.size.height  > (drawingBounds.origin.y + drawingBounds.size.height)/scale){
        [UIView animateWithDuration:0.1f animations:^{
            [self.drawingScrollView setContentOffset:CGPointMake(0, (thumbNailBounds.origin.y +thumbNailBounds.size.height)*scale - drawingBounds.size.height)];
        }];
    }
}
#pragma mark - ACEDrawing View Delegate
- (void)drawingView:(ACEDrawingView *)view didEndDrawUsingTool:(id<ACEDrawingTool>)tool;
{
    [self updateBoardButtonStatus];
    
}

#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.cancelButtonIndex != buttonIndex) {
        if (actionSheet.tag == kActionSheetColor) {
            switch (buttonIndex) {
                case 0:
                    [self.colorButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
                    self.drawingView.lineColor = [UIColor blackColor];
                    break;
                    
                case 1:
                    [self.colorButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
                    self.drawingView.lineColor = [UIColor redColor];
                    break;
                    
                case 2:
                    [self.colorButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
                    self.drawingView.lineColor = [UIColor greenColor];
                    break;
                    
                case 3:
                    [self.colorButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
                    self.drawingView.lineColor = [UIColor blueColor];
                    break;
            }
            self.oldLineColor = self.drawingView.lineColor;
        } else  if(actionSheet.tag == kActionSheetTool){
            [self.toolButton setSelected:YES];
            [self.eraserButton setSelected:NO];
            switch (buttonIndex) {
                case 0:
                    [self.toolButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateSelected];
                    self.drawingView.drawTool = ACEDrawingToolTypePen;
                    break;
                    
                case 1:
                    [self.toolButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateSelected];
                    self.drawingView.drawTool = ACEDrawingToolTypeLine;
                    break;
                    
                case 2:
                    [self.toolButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateSelected];
                    self.drawingView.drawTool = ACEDrawingToolTypeRectagleStroke;
                    break;
                    
                case 3:
                    [self.toolButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateSelected];
                    self.drawingView.drawTool = ACEDrawingToolTypeRectagleFill;
                    break;
                    
                case 4:
                    [self.toolButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateSelected];
                    self.drawingView.drawTool = ACEDrawingToolTypeEllipseStroke;
                    break;
                    
                case 5:
                    [self.toolButton setTitle:[actionSheet buttonTitleAtIndex:buttonIndex] forState:UIControlStateSelected];
                    self.drawingView.drawTool = ACEDrawingToolTypeEllipseFill;
                    break;
            }
        }else if (actionSheet.tag == kActionSheetCarmer){
            // 判断是否支持相机
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                switch (buttonIndex) {
                    case 0:
                        return;
                    case 1: //相机
                    {
                        [self carmer];
                    }
                        break;
                    case 2: //相册
                    {
                        [self photoLabrary];
                    }
                        break;
                }
            }
            else {
                if (buttonIndex == 0) {
                    return;
                } else {
                    [self photoLabrary];
                }
            }
        }else if (actionSheet.tag == kActionSheetAddRepo){
            int type = 0;//1是难题，2是好题
            switch (buttonIndex) {
                case 0:
                    return;
                case 1:
                    type = 1;
                    break;
                case 2:
                    type = 2;
                    break;
                default:
                    break;
            }
            [self.view addSubview:[waitView getDeafaultView]];
            [[waitView getDeafaultView]startAnimation];
            [self.network sendRequest:[self.network createAddRepoItemRequestWithTestItemId:self.currentItem.itemId Type:type] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                [[waitView getDeafaultView]stopAnimation];
                [[waitView getDeafaultView]removeFromSuperview];
                if (isSuccess) {
                    if ([[data objectForKey:@"error_code"]intValue] == 0) {
                        [Util showAlertViewWithTitle:@"提示" Message:@"加入题库完成"];
                    }
                }
            }];
        }
    }
}
-(void)carmer
{
    if (imagePicker == nil) {
        imagePicker = [[UIImagePickerController alloc]init];
    }
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:^{
        
    }];
}
-(void)photoLabrary
{
    if (imagePicker == nil) {
        imagePicker = [[UIImagePickerController alloc]init];
    }
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    else{
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    imagePicker.allowsEditing = YES;
    if (popoverController == nil) {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
        popoverController = popover;
        //popoverController.delegate = self;
    }
    [popoverController presentPopoverFromRect:CGRectMake(0, 0, 300, 300) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
- (IBAction)widthChanged:(UISlider *)sender {
    self.drawingView.lineWidth = sender.value;
    self.oldLineWidth = sender.value;
    [self.lineWidthButton scaleImageWithFloat:sender.value];
}

-(void)addDrawingBoardHeight:(UIScrollView*)scrollView
{
    [scrollView setContentSize:CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height+400)];
    [self.drawingView setFrame: CGRectMake(0,0, scrollView.contentSize.width ,scrollView.contentSize.height)];
    [self.drawingView refreshView];
}

#pragma  mark UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (scrollView.tag == DRAWINGSCROLLVIEW_TAG) {
        if (!self.isScrolled) {
             [self calculateThumbPosition];
            self.isScrolled = YES;
        }
        if (self.isLoading) {
            return;
        }
        self.isDragging = YES;
    }else if (scrollView.tag == THUMBNILSCROLLVIEW_TAG){
        if (!self.isScrolled) {
            [self calculateDrawingPosition];
            self.isScrolled = YES;
        }
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.tag == DRAWINGSCROLLVIEW_TAG) {
        if (!self.isDragShadeImageView) {
        }else{
            
        }
        if (self.isLoading) {
            if (scrollView.contentOffset.y < 0) {
                scrollView.contentInset = UIEdgeInsetsZero;
            }else if (scrollView.contentOffset.y <= LOADING_HEIGHT) {
                scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
            }
        }else if (self.isDragging && scrollView.contentOffset.y > 0 && scrollView.contentOffset.y <=(LOADING_HEIGHT+scrollView.contentSize.height-self.drawingScrollView.frame.size.height)) {
        }
        else if (self.isDragging && scrollView.contentOffset.y >= (LOADING_HEIGHT+scrollView.contentSize.height-self.drawingScrollView.frame.size.height)){
        }
    }else if (scrollView.tag == THUMBNILSCROLLVIEW_TAG){
        
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    self.isScrolled = NO;
    if (scrollView.tag == DRAWINGSCROLLVIEW_TAG) {
        if (self.isDragShadeImageView) {
            self.isDragShadeImageView = NO;
        }
        if (self.isLoading) {
            return;
        }
        self.isDragging = NO;
        if (scrollView.contentOffset.y >= (LOADING_HEIGHT+scrollView.contentSize.height-self.drawingScrollView.frame.size.height)) {
            [self addDrawingBoardHeight:scrollView];
        }
    }else if (scrollView.tag == THUMBNILSCROLLVIEW_TAG){
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [self.drawingView clear];
    self.photo = image;
    [self initData];
    [imagePicker dismissViewControllerAnimated:YES completion:^{
        imagePicker = nil;
    }];
    [popoverController dismissPopoverAnimated:YES];
    popoverController = nil;
    self.currentItem.status = 1;
    [self resetSubmitButtonStatus];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [imagePicker dismissViewControllerAnimated:YES completion:^{
        imagePicker = nil;
    }];
    [popoverController dismissPopoverAnimated:YES];
    popoverController = nil;
}
#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self backToWaitVC];
        }
            break;
            
        default:
            break;
    }
}
#pragma mark ACEDrawingViewDelegate
-(void)drawingView:(ACEDrawingView *)view willBeginDrawUsingTool:(id<ACEDrawingTool>)tool
{
    self.currentItem.status = 1;
    [self resetSubmitButtonStatus];
}
@end
