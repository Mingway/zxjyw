//
//  UrlDefine.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#ifndef DrawingBoard_UrlDefine_h
#define DrawingBoard_UrlDefine_h

#define BASE_URL @"http://demo703.tongyouxuetang.com/dataapi/base/"
#define PAD_URL   @"http://demo703.tongyouxuetang.com/dataapi/pd/"
#define RES_URL    @"http://demo703.tongyouxuetang.com/dataapi/res/"

#define URL_LOGIN                  [NSString stringWithFormat:@"%@login.do",BASE_URL]
#define URL_RELOGIN              [NSString stringWithFormat:@"%@relogin.do",BASE_URL]
#define URL_SUBMIT                 [NSString stringWithFormat:@"%@submitanswertmp.do",BASE_URL]
#define URL_UPLOAD               [NSString stringWithFormat:@"%@add_res.do",RES_URL]
#define URL_TOPAD                  [NSString stringWithFormat:@"%@topd.do",PAD_URL]
#define URL_GET_RESOURCE   [NSString stringWithFormat:@"%@get_res.do",RES_URL]
#define URL_ADD_REPOITEM   [NSString stringWithFormat:@"%@add_repoitem.do",BASE_URL]

#endif