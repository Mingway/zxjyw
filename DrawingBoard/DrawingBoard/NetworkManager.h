//
//  NetworkManager.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NetworkRequest;
@interface NetworkManager : NSObject

- (BOOL)logonUsername:(NSString*)username password:(NSString*)password comletionHandler:(void(^)(BOOL isSuccess,NSDictionary *data,NSString *errorMessage))completionHandler;

-(NetworkRequest*)createReloginRequest;

- (NetworkRequest*)createToPadRequest;

- (NetworkRequest*)createSubmitAnswerTmpRequestWithTaskId:(NSString*)taskId itemId:(NSMutableArray*)itemIdArr ItemAnswer:(NSMutableArray*)answerArr finished:(BOOL)finished;

- (NetworkRequest *)createUploadResourseRequestWithResData:(NSString*)resdata width:(float)width height:(float)height;

- (NetworkRequest*)createAddRepoItemRequestWithTestItemId:(NSString*)testitem_id Type:(int)type;

- (NetworkRequest*)createDownloadImageRequestWithResorseId:(NSString*)res_id;

- (NetworkRequest*)createDownloadImageRequestWithUrl:(NSString*)url;

- (BOOL)sendRequest:(NetworkRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSDictionary *data, NSString *errorMessage))completionHandler;

- (BOOL)downloadReourceRequest:(NetworkRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSData *data, NSString *errorMessage))completionHandler;

@end
