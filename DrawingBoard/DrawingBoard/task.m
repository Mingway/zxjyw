//
//  task.m
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "task.h"
#import "item.h"

@interface task ()
@property (nonatomic, strong)NSDate *startDate;
@property (nonatomic, assign)int timeLimit;
@property (nonatomic, strong)NSString *taskId;
@property (nonatomic, strong)NSString *currentItem;
@property (nonatomic, strong)NSArray *testItemArr;
@property (nonatomic, assign) int course_id;
@property (nonatomic, strong) NSString *course_name;
@property (nonatomic, strong) NSString *task_name;
@end

@implementation task

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)updateWithDic:(NSDictionary*)dic
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSDate *destDate= [dateFormatter dateFromString:[dic objectForKey:@"begin_time"]];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: destDate];
    self.startDate = [destDate  dateByAddingTimeInterval: interval];
    self.timeLimit = [[dic objectForKey:@"time_limit"]intValue];
    self.taskId = [dic objectForKey:@"task_id"];
    self.currentItem = [dic objectForKey:@"curritem"];
    NSMutableArray *tempArr = [NSMutableArray array];
    for (NSDictionary *key in [dic objectForKey:@"testitems"]) {
        item *tempItem = [[item alloc]init];
        [tempItem updateWithDic:key];
        [tempArr addObject:tempItem];
    }
    self.testItemArr = [NSArray arrayWithArray:tempArr];
    self.course_id = [[dic objectForKey:@"course_id"]intValue];
    self.course_name = [dic objectForKey:@"course_name"];
    self.task_name = [dic objectForKey:@"task_name"];
}

@end
