//
//  user.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    NotLogin = 0,
    NotWorking ,
    Working,
}AppStatus;

@interface user : NSObject

@property (nonatomic, strong)NSString *access_token;
@property (nonatomic, strong)NSString *username;
@property (nonatomic, strong)NSString *gender;
@property (nonatomic, strong)NSString *name;
@property (nonatomic, strong)NSURL *profile_image_url;
@property (nonatomic, assign)AppStatus currentStatus;

+ (user*)shareUser;
- (void)updateWithDic:(NSDictionary*)dic;

@end
