//
//  task.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface task : NSObject
@property (nonatomic, readonly)NSDate *startDate;
@property (nonatomic, readonly)int timeLimit;
@property (nonatomic, readonly)NSString *taskId;
@property (nonatomic, readonly)NSString *currentItem;
@property (nonatomic, readonly)NSArray *testItemArr;
@property (nonatomic, readonly) int course_id;
@property (nonatomic, readonly) NSString *course_name;
@property (nonatomic, readonly) NSString *task_name;
- (void)updateWithDic:(NSDictionary*)dic;
@end
