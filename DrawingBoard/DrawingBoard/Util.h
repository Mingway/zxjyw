//
//  Util.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Util : NSObject

+(void)save:(NSString *)access_token;//保存token
+(id)read;//读取token
+(void)deleteUser;//删除token

+(NSString *) md5: (NSString *) inPutText ;//MD5加密
+(void)showAlertViewWithTitle:(NSString*)title Message:(NSString*)messgae;//显示AlertView
+(NSDate*)localDate;
+(void)clearCacheWithTaskId:(NSString*)taskId;
@end
