//
//  Util.m
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "Util.h"
#import "CommonCrypto/CommonDigest.h"
#import "keychainUtils.h"

static NSString * const KEY_IN_KEYCHAIN = @"com.drawingboard.app.allinfo";
static NSString * const KEY_TOKEN = @"com.drawingboard.app.access_token";

@implementation Util

+(void)save:(NSString *)access_token
{
    NSMutableDictionary *usernamepasswordKVPairs = [NSMutableDictionary dictionary];
    [usernamepasswordKVPairs setObject:access_token forKey:KEY_TOKEN];
    [keychainUtils save:KEY_IN_KEYCHAIN data:usernamepasswordKVPairs];
}

+(id)read
{
    NSMutableDictionary *usernamepasswordKVPair = (NSMutableDictionary *)[keychainUtils load:KEY_IN_KEYCHAIN];
    return usernamepasswordKVPair;
}

+(void)deleteUser
{
    [keychainUtils delete:KEY_IN_KEYCHAIN];
}

+(NSString *) md5: (NSString *) inPutText
{
    const char *cStr = [inPutText UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, strlen(cStr), result);
    return [[NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
             result[0], result[1], result[2], result[3],
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]
             ] lowercaseString];
}
+(void)showAlertViewWithTitle:(NSString*)title Message:(NSString*)messgae
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:messgae delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
}
+(NSDate*)localDate
{
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date];
    NSDate *locale = [date  dateByAddingTimeInterval: interval];
    return locale;
}
+(void)clearCacheWithTaskId:(NSString*)taskId
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //document文件夹下用NSDocumentDirectory，cache文件夹下用
    NSArray *fileList ;
    fileList =[fileManager contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@%@",NSTemporaryDirectory(),taskId] error:NULL];
    for (NSString *file in fileList) {
        NSLog(@"file=%@",file);
        NSString *path =[[NSString stringWithFormat:@"%@%@",NSTemporaryDirectory(),taskId]stringByAppendingPathComponent:file];
        [fileManager removeItemAtPath:path error:nil];
        NSLog(@"得到的路径=%@",path);
    }
    [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@%@",NSTemporaryDirectory(),taskId] error:nil];
}
@end
