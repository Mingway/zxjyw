//
//  DrawingBoardViewController.h
//  DrawingBoard
//
//  Created by HelyData on 13-10-31.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class task;
@interface DrawingBoardViewController : UIViewController
@property (nonatomic, strong)task *currentTask;
@property (nonatomic, assign) int currentIndex;//当前题目索引
@property (nonatomic, strong) UIImage *photo;//做题图片
@end
