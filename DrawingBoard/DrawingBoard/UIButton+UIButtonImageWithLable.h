//
//  UIButton+UIButtonImageWithLable.h
//  DrawingBoard
//
//  Created by HelyData on 13-12-2.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (UIButtonImageWithLable)
- (void) setImage:(UIImage *)image withTitle:(NSString *)title forState:(UIControlState)stateType;
- (void) scaleImageWithFloat:(float)num;
@end
