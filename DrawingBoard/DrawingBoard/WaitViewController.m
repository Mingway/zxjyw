//
//  WaitViewController.m
//  DrawingBoard
//
//  Created by HelyData on 13-10-31.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "WaitViewController.h"
#import "NetworkManager.h"
#import "QuestionListViewController.h"
#import "task.h"
#import "Util.h"
#import "user.h"

@interface WaitViewController ()
@property (nonatomic, strong)NetworkManager *network;
- (IBAction)pressBtn:(UIButton *)sender;

@end

@implementation WaitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self pushToQuestionListViewController];
}
- (void)pushToQuestionListViewController
{
    if (self.network == nil) {
        self.network = [[NetworkManager alloc]init];
    }
    [self.network sendRequest:[self.network createToPadRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                QuestionListViewController *nextVC = [board instantiateViewControllerWithIdentifier:@"QuestionListViewController"];
                task *tempTask = [[task alloc]init];
               [tempTask updateWithDic:data];
                nextVC.currentTask = tempTask;
                [self.navigationController pushViewController:nextVC animated:YES];
            }else{
//                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
            }
        }else{
//             [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
        }
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.network = [[NetworkManager alloc]init];
}
-(void)viewWillAppear:(BOOL)animated
{
    [[user shareUser]setCurrentStatus:NotWorking];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            [Util deleteUser];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}
@end
