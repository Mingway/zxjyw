//
//  NetworkRequest.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkRequest : NSObject<NSCopying>

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSMutableDictionary *parameters;

- (BOOL)sendAsyncRequestWithCompletionHandler:(void(^)(BOOL isSuccess, NSDictionary *result, NSString *errorMessage))completionHandler;

- (BOOL)downloadAsyncRequestWithCompletionHandler:(void(^)(BOOL isSuccess, NSData *result, NSString *errorMessage))completionHandler;

@end
