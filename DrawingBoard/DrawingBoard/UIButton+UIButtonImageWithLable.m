//
//  UIButton+UIButtonImageWithLable.m
//  DrawingBoard
//
//  Created by HelyData on 13-12-2.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "UIButton+UIButtonImageWithLable.h"

@implementation UIButton (UIButtonImageWithLable)
- (void) setImage:(UIImage *)image withTitle:(NSString *)title forState:(UIControlState)stateType {
    CGSize titleSize = [title sizeWithFont:[UIFont systemFontOfSize:20.f]];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self setImageEdgeInsets:UIEdgeInsetsMake(10.0,-titleSize.width - 10.0,10.0, -titleSize.width)];
    [self setImage:image forState:stateType];
    [self.titleLabel setContentMode:UIViewContentModeCenter];
    [self.titleLabel setBackgroundColor:[UIColor clearColor]];
    [self.titleLabel setFont:[UIFont systemFontOfSize:20.f]];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
     [self setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0.0,-image.size.width + 20,0.0,0.0)];
    [self setTitle:title forState:stateType];
}
- (void) scaleImageWithFloat:(float)num
{
    self.imageView.contentScaleFactor = (4-num/5)*1.8;
    self.imageView.contentMode = UIViewContentModeCenter;
}
@end
