//
//  itemView.m
//  DrawingBoard
//
//  Created by HelyData on 13-11-30.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "itemView.h"

@implementation itemView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)config
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(loadDrawingView)];
    tap.delegate = self;
    [self.questionWebView addGestureRecognizer:tap];
}
-(void)loadDrawingView
{
     [self.delegate drawWithCurrentIndex:self.currentIndex];
}
- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            UIActionSheet *sheet;
            // 判断是否支持相机
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                sheet  = [[UIActionSheet alloc] initWithTitle:@"选择图像" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"取消" otherButtonTitles:@"拍照", @"从相册选择", nil];
            }
            else {
                sheet = [[UIActionSheet alloc] initWithTitle:@"选择图像" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"取消" otherButtonTitles:@"从相册选择", nil];
            }
            [sheet showInView:[UIApplication sharedApplication].keyWindow];
            sheet = nil;
        }
            break;
        case 1:
        {
            [self.delegate drawWithCurrentIndex:self.currentIndex];
        }
            break;
        default:
            break;
    }
}
#pragma mark UIActionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        switch (buttonIndex) {
            case 0:
                return;
            case 1: //相机
                [self.delegate takePhotoWithCurrentIndex:self.currentIndex];
                break;
            case 2: //相册
                 [self.delegate photoLibraryWithCurrentIndex:self.currentIndex];
                break;
        }
    }
    else {
        if (buttonIndex == 0) {
            return;
        } else {
             [self.delegate photoLibraryWithCurrentIndex:self.currentIndex];
        }
    }
}
#pragma mark UIGestureDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
