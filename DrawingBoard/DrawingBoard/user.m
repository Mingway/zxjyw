//
//  user.m
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "user.h"

static user *userModel = nil;
@implementation user
- (id)init
{
    self = [super init];
    if (self) {
        self.currentStatus = NotLogin;
    }
    return self;
}
+ (user*)shareUser
{
    if (userModel == nil) {
        userModel = [[user alloc]init];
    }
    return userModel;
}

- (void)updateWithDic:(NSDictionary*)dic
{
    self.access_token = [dic objectForKey:@"access_token"];
    self.gender = [dic objectForKey:@"gender"];
    self.name = [dic objectForKey:@"name"];
    self.profile_image_url = [dic objectForKey:@"profile_image_url"];
}

@end
