//
//  itemView.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-30.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol itemViewDelegate <NSObject>

- (void)photoLibraryWithCurrentIndex:(int)index;
- (void)takePhotoWithCurrentIndex:(int)index;
- (void)drawWithCurrentIndex:(int)index;

@end

@interface itemView : UIView<UIActionSheetDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *itemIndexAndTypeLable;
@property (weak, nonatomic) IBOutlet UILabel *itemScoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *havePicImageView;
@property (weak, nonatomic) IBOutlet UIWebView *questionWebView;
@property (nonatomic)id<itemViewDelegate>delegate;
@property (nonatomic, assign) int currentIndex;
- (IBAction)pressBtn:(UIButton *)sender;
-(void)config;
@end
