//
//  NetworkManager.m
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "NetworkManager.h"
#import "Reachability.h"
#import "user.h"
#import "NetworkRequest.h"
#import "UrlDefine.h"
#import "Util.h"

@interface NetworkManager ()

@property (nonatomic, strong)user *userModel;

- (BOOL)isNetworkAvailable;

@end

@implementation NetworkManager

-(user *)userModel
{
    return [user shareUser];
}

- (BOOL)isNetworkAvailable
{
    //链接需要更改
    Reachability *r = [Reachability reachabilityWithHostname:@"demo703.tongyouxuetang.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
            return NO;
            break;
        case ReachableViaWWAN:
            // 使用3G网络
            return YES;
            break;
        case ReachableViaWiFi:
            // 使用WiFi网络
            return YES;
            break;
    }
}

- (BOOL)logonUsername:(NSString *)username password:(NSString *)password comletionHandler:(void (^)(BOOL, NSDictionary *, NSString *))completionHandler
{
    if (!self.isNetworkAvailable) {
        completionHandler(NO,nil,@"请检查网络");
        return NO;
    }
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_LOGIN;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObject:username forKey:@"account"];
    [parameters setObject:[Util md5:[NSString stringWithFormat:@"%@+zxjywusermd5",password]] forKey:@"password"];
    [parameters setObject:@"1" forKey:@"termtype"];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]  != nil && ! [[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] isEqualToString:@""])
    {
         [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"] forKey:@"devtoken"];
    }
    request.parameters = parameters;
    return [request sendAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        completionHandler(isSuccess,result,errorMessage);
        if (isSuccess) {
            if ([[result objectForKey:@"error_code"]intValue] == 0) {
                [self.userModel setCurrentStatus:NotWorking];
                self.userModel.username = username;
                [self.userModel updateWithDic:[result objectForKey:@"data"]];
                [Util save:self.userModel.access_token];
            }
        }
    }];
}

-(NetworkRequest*)createReloginRequest
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_RELOGIN;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    request.parameters = parameters;
    return request;
}

- (NetworkRequest*)createToPadRequest
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_TOPAD;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    request.parameters = parameters;
    return request;
}
- (NetworkRequest*)createSubmitAnswerTmpRequestWithTaskId:(NSString*)taskId itemId:(NSMutableArray*)itemIdArr ItemAnswer:(NSMutableArray*)answerArr finished:(BOOL)finished
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_SUBMIT;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:taskId forKey:@"task_id"];
    for (int i = 0; i < itemIdArr.count; i ++) {
        [parameters setObject:[answerArr objectAtIndex:i] forKey:[NSString stringWithFormat:@"answer_%@",[itemIdArr objectAtIndex:i]]];
    }
    [parameters setObject:[NSString stringWithFormat:@"%i",finished] forKey:@"finished"];
    request.parameters = parameters;
    return request;
}

- (NetworkRequest *)createUploadResourseRequestWithResData:(NSString*)resdata width:(float)width height:(float)height
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_UPLOAD;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:resdata forKey:@"resdata"];
    [parameters setObject:@"png" forKey:@"restype"];
    [parameters setObject:@"2" forKey:@"prop_count"];
    [parameters setObject:[NSString stringWithFormat:@"width\x1c%f",width] forKey:@"prop_1"];
    [parameters setObject:[NSString stringWithFormat:@"height\x1c%f",height] forKey:@"prop_2"];
    request.parameters = parameters;
    return request;
}
- (NetworkRequest*)createAddRepoItemRequestWithTestItemId:(NSString*)testitem_id Type:(int)type
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_ADD_REPOITEM;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    if (self.userModel.access_token != nil) {
        [parameters setObject:self.userModel.access_token forKey:@"access_token"];
    }
    [parameters setObject:testitem_id forKey:@"testitem_id"];
    [parameters setObject:[NSString stringWithFormat:@"%i",type] forKey:@"type"];
    request.parameters = parameters;
    return request;
}
- (NetworkRequest*)createDownloadImageRequestWithResorseId:(NSString*)res_id
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = URL_GET_RESOURCE;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setObject:res_id forKey:@"res_id"];
    request.parameters = parameters;
    return request;
}
- (NetworkRequest*)createDownloadImageRequestWithUrl:(NSString*)url
{
    NetworkRequest *request = [[NetworkRequest alloc]init];
    request.url = url;
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    request.parameters = parameters;
    return request;
}
- (BOOL)downloadReourceRequest:(NetworkRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSData *data, NSString *errorMessage))completionHandler
{
    if (!self.isNetworkAvailable) {
        completionHandler(NO,nil,@"请检查网络");
        return NO;
    }
    return [request downloadAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSData *result, NSString *errorMessage) {
        if (isSuccess) {
            NSLog(@"normal request:%@",result);
            completionHandler(isSuccess,result,errorMessage);
        }else{
            NSLog(@"error message:%@",errorMessage);
            completionHandler(isSuccess,result,errorMessage);
        }
    }];
}
- (BOOL)sendRequest:(NetworkRequest *)request withCompletionHandler:(void (^)(BOOL isSuccess, NSDictionary *data, NSString *errorMessage))completionHandler
{
    if (!self.isNetworkAvailable) {
        completionHandler(NO,nil,@"请检查网络");
        return NO;
    }
    return [request sendAsyncRequestWithCompletionHandler:^(BOOL isSuccess, NSDictionary *result, NSString *errorMessage) {
        if (isSuccess) {
            if ([[result objectForKey:@"error_code"]intValue] == 1) {
                [Util deleteUser];
                UINavigationController *nav = (UINavigationController*)[UIApplication sharedApplication].keyWindow.rootViewController;
                [nav popToRootViewControllerAnimated:YES];
            }else{
                NSLog(@"normal request:%@",result);
                completionHandler(isSuccess,result,errorMessage);
            }
        }else{
            NSLog(@"error message:%@",errorMessage);
            completionHandler(isSuccess,result,errorMessage);
        }
    }];
}
@end
