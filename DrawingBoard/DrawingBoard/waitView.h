//
//  waitView.h
//  DrawingBoard
//
//  Created by HelyData on 13-12-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface waitView : UIView
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

-(void)startAnimation;
-(void)stopAnimation;

+(waitView*)getDeafaultView;

@end
