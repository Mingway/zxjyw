//
//  item.m
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "item.h"

@interface item ()
@property (nonatomic, strong)NSString *itemId;
@property (nonatomic, strong)NSString *answer;//回答的图片res_id
@property (nonatomic, strong)NSString *itemType;//题目类型，证明题，解答题等
@property (nonatomic, assign)int itemIndex;//当前题目在task中的序号
@property (nonatomic, assign)int score;//当前题目分值
@property (nonatomic, strong)NSString *question;//问题内容
@property (nonatomic, strong)NSURL *imageUrl;//题图
@property (nonatomic, assign)int blankNum;//填空个数,根据\a个数计算
@end

@implementation item

- (id)init
{
    self = [super init];
    if (self) {
        self.blankNum = 0;
        self.status = 0;
    }
    return self;
}
-(NSString*)updateOptionsWithStr1:(NSString*)str1 toStr2:(NSString*)str2 htmlString:(NSString*)html_str;
{
    NSRange range = [html_str rangeOfString:str1];
    html_str = [html_str stringByReplacingOccurrencesOfString:str1 withString:str2 options:NSBackwardsSearch range:NSMakeRange(range.location + range.length, html_str.length-range.location - range.length)];
    return html_str;
}
- (void)calculateBlankNum:(NSString*)str
{
    while ([str rangeOfString:@"\\a"].length > 0) {
        str = [ str stringByReplacingCharactersInRange:[str rangeOfString:@"\\a"] withString:@""];
        self.blankNum++;
    };
}
- (void)updateWithDic:(NSDictionary*)dic
{
    self.itemId = [dic objectForKey:@"testitem_id"];
    if (![[dic objectForKey:@"answer"] isEqualToString:@""]) {
        self.answer = [dic objectForKey:@"answer"];
    }
    else
    {
        self.answer = nil;
    }
    self.itemType = [dic objectForKey:@"testitem_type_str"];
     NSString *new_questionContent  = [dic objectForKey:@"question"];
    [self calculateBlankNum:new_questionContent];
    NSLog(@"%@",new_questionContent);
    if([self.itemType isEqualToString:@"单选题"] || [self.itemType isEqualToString:@"多选题"]){
        new_questionContent = [new_questionContent stringByReplacingOccurrencesOfString:@"\\a" withString:@"<br/>A."];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>A." toStr2:@"<br/>B." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>B." toStr2:@"<br/>C." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>C." toStr2:@"<br/>D." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>D." toStr2:@"<br/>E." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>E." toStr2:@"<br/>F." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>F." toStr2:@"<br/>G." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>G." toStr2:@"<br/>H." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>H." toStr2:@"<br/>I." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>I." toStr2:@"<br/>J." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>J." toStr2:@"<br/>K." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>K." toStr2:@"<br/>L." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>L." toStr2:@"<br/>M." htmlString:new_questionContent];
        new_questionContent = [self updateOptionsWithStr1:@"<br/>M." toStr2:@"<br/>N." htmlString:new_questionContent];
    }else{
        new_questionContent = [new_questionContent stringByReplacingOccurrencesOfString:@"\\a" withString:@"_____"];
    }
    NSLog(@"%@",new_questionContent);
    new_questionContent = [self convertImgUrlWithString:new_questionContent];
    self.question = [NSString stringWithFormat:@"<html> <head><title>问题内容</title><script type = \"text/javascript\"src = \"MathJax.js\"></script> </head><body>%@</body>  </html>",new_questionContent];
    self.itemIndex = [[dic objectForKey:@"idx"]intValue];
    self.score = [[dic objectForKey:@"score"]intValue];
}
- (NSString*)convertImgUrlWithString:(NSString*)string
{
    NSScanner *scanner = [NSScanner scannerWithString:string];
    while ([scanner scanUpToString:@"<img" intoString:NULL]) {
        NSString *ImgTagContents;
        if ([scanner scanUpToString:@">" intoString:&ImgTagContents]) {
            // Do something with tag contents
            NSScanner *scanner1 = [NSScanner scannerWithString:ImgTagContents];
            while ([scanner1 scanUpToString:@"src=\"" intoString:NULL]) {
                NSString *srcTagContents;
                if ([scanner1 scanUpToString:@"\" /" intoString:&srcTagContents]) {
                    string  = [string stringByReplacingOccurrencesOfString:srcTagContents withString: [NSString stringWithFormat:@"src=\"http://demo703.tongyouxuetang.com%@",[srcTagContents substringFromIndex:5]]];
                    self.imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://demo703.tongyouxuetang.com%@",[srcTagContents substringFromIndex:5]]];
                }
            }
        }
        else {
            // Do nothing? I think this would be hit on the last time through the loop
        }
    }
    NSLog(@"%@",string);
    return string;
}
@end
