//
//  ViewController.m
//  DrawingBoard
//
//  Created by HelyData on 13-10-31.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "ViewController.h"
#import "NetworkManager.h"
#import "Util.h"
#import "user.h"
#import "waitView.h"
#import "QuestionListViewController.h"
#import "task.h"

static NSString * const KEY_TOKEN = @"com.drawingboard.app.access_token";

@interface ViewController ()
@property (nonatomic, strong) user *userModel;
@property (nonatomic, strong) NetworkManager *network;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (nonatomic, assign) BOOL isShowKeyboard;

- (IBAction)pressBtn:(UIButton *)sender;

@end

@implementation ViewController
-(void)viewWillAppear:(BOOL)animated
{
//    [[user shareUser]setCurrentStatus:NotLogin];
//    //注册通知,监听键盘出现
//    [[NSNotificationCenter defaultCenter]addObserver:self
//                                            selector:@selector(handleKeyboardDidShow:)
//                                                name:UIKeyboardDidShowNotification
//                                              object:nil];
//    //注册通知，监听键盘消失事件
//    [[NSNotificationCenter defaultCenter]addObserver:self
//                                            selector:@selector(handleKeyboardDidHidden:)
//                                                name:UIKeyboardDidHideNotification
//                                              object:nil];
//    [super viewWillAppear:YES];
//    self.isShowKeyboard = NO;
}

//监听事件
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification
{
//    //获取键盘高度
//    NSValue *keyboardRectAsObject=[[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect keyboardRect;
//    [keyboardRectAsObject getValue:&keyboardRect];
    if (!self.isShowKeyboard) {
        [UIView animateWithDuration:0.3f animations:^{
            [[UIApplication sharedApplication].keyWindow setFrame:CGRectOffset([UIApplication sharedApplication].keyWindow.frame, 150, 0)];
        }];
        self.isShowKeyboard = YES;
    }
}

- (void)handleKeyboardDidHidden:(NSNotification*)paramNotification
{
//    //获取键盘高度
//    NSValue *keyboardRectAsObject=[[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect keyboardRect;
//    [keyboardRectAsObject getValue:&keyboardRect];
    if (self.isShowKeyboard) {
        [UIView animateWithDuration:0.3f animations:^{
            [[UIApplication sharedApplication].keyWindow setFrame:CGRectOffset([UIApplication sharedApplication].keyWindow.frame, -150, 0)];
        }];
        self.isShowKeyboard = NO;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIControl *resign = [[UIControl alloc]initWithFrame:self.view.bounds];
    [resign addTarget:self action:@selector(resign) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:resign];
    [self.view sendSubviewToBack:resign];
    self.network = [[NetworkManager alloc]init];
    self.userModel = [user shareUser];
    NSDictionary* storeUserInfo = [Util read];
    if (storeUserInfo != nil) {
        self.userModel.access_token = [storeUserInfo objectForKey:KEY_TOKEN];
        [[UIApplication sharedApplication].keyWindow addSubview:[waitView getDeafaultView]];
        [[waitView getDeafaultView]startAnimation];
        [self.network sendRequest:[self.network createReloginRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            [[waitView getDeafaultView]stopAnimation];
            [[waitView getDeafaultView]removeFromSuperview];
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [self.userModel updateWithDic:[data objectForKey:@"data"]];
                    [Util save:self.userModel.access_token];
                    [self requestToPad];
                }else{
                    [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                }
            }else{
                [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
            }
        }];
    }
}
- (void)resign
{
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)requestToPad
{
    [self.network sendRequest:[self.network createToPadRequest] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
        [[waitView getDeafaultView]stopAnimation];
        [[waitView getDeafaultView]removeFromSuperview];
        if (isSuccess) {
            if ([[data objectForKey:@"error_code"]intValue] == 0) {
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                QuestionListViewController *nextVC = [board instantiateViewControllerWithIdentifier:@"QuestionListViewController"];
                task *tempTask = [[task alloc]init];
                [tempTask updateWithDic:data];
                nextVC.currentTask = tempTask;
                [self.navigationController pushViewController:nextVC animated:YES];
            }else{
                UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *nextVC = [board instantiateViewControllerWithIdentifier:@"WaitViewController"];
                [self.navigationController pushViewController:nextVC animated:YES];
            }
        }else{
        }
    }];
}
- (void)login
{
    if (![self.usernameTextField.text isEqualToString:@""]&&![self.passwordTextField.text isEqualToString:@""]) {
        [self.view addSubview:[waitView getDeafaultView]];
        [[waitView getDeafaultView]startAnimation];
        [self.network logonUsername:self.usernameTextField.text password:self.passwordTextField.text comletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [self.userModel setCurrentStatus:NotWorking];
                    self.userModel.username = self.usernameTextField.text;
                    [self.userModel updateWithDic:[data objectForKey:@"data"]];
                    [Util save:self.userModel.access_token];
                    [self requestToPad];
                }else{
                    [[waitView getDeafaultView]stopAnimation];
                    [[waitView getDeafaultView]removeFromSuperview];
                    [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                }
            }else{
                [[waitView getDeafaultView]stopAnimation];
                [[waitView getDeafaultView]removeFromSuperview];
                [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
            }
        }];
    }else{
        
        [Util showAlertViewWithTitle:@"提示" Message:@"用户名或密码不能为空!"];
    }
}

- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //login
            [self login];
        }
            break;
            
        default:
            break;
    }
}
@end
