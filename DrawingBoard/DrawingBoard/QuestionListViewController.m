//
//  QuestionListViewController.m
//  DrawingBoard
//
//  Created by HelyData on 13-10-31.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "QuestionListViewController.h"
#import "DrawingBoardViewController.h"
#import "task.h"
#import "item.h"
#import "NetworkManager.h"
#import "Util.h"
#import "user.h"
#import "itemView.h"
#import "waitView.h"
#import "WaitViewController.h"

@interface QuestionListViewController ()<UITableViewDataSource,UITableViewDelegate,itemViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
{
    NSTimer *timer;
    UIImagePickerController *imagePicker;
    UIPopoverController *popoverController;
}
@property (nonatomic, strong) user *userModel;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel1;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel2;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel3;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel4;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel5;
@property (weak, nonatomic) IBOutlet UITableView *itemListTableView;
@property (nonatomic, strong)NetworkManager *network;
- (IBAction)pressBtn:(UIButton *)sender;
@property (nonatomic, assign) int currentIndex;
@end

@implementation QuestionListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [timer invalidate];
    timer = nil;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.userModel setCurrentStatus:Working];
    if (timer == nil) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(count_down) userInfo:nil repeats:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.userModel = [user shareUser];
    self.network = [[NetworkManager alloc]init];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(count_down) userInfo:nil repeats:YES];
    self.itemListTableView.delegate = self;
    self.itemListTableView.dataSource = self;
    [self loadData];
}
- (void)loadData
{
    self.studentNameLabel.text = self.userModel.name;
    self.courseLabel.text = self.currentTask.course_name;
    self.taskNameLabel.text = self.currentTask.task_name;
}
- (void)count_down
{
    int total_time = self.currentTask.timeLimit;
    NSDate* now = [Util localDate];
    int count_down = [now timeIntervalSinceDate:self.currentTask.startDate];
    if (count_down < total_time) {
        count_down = total_time - count_down;
        int minutes = count_down/60;
        int seconds = count_down%60;
        self.timeLabel1.text = [NSString stringWithFormat:@"%i",minutes/100];
        self.timeLabel2.text = [NSString stringWithFormat:@"%i",(minutes - (minutes/100)*100)/10];
        self.timeLabel3.text = [NSString stringWithFormat:@"%i",minutes - ((minutes/10)*10)];
        self.timeLabel4.text = [NSString stringWithFormat:@"%i",seconds/10];
        self.timeLabel5.text = [NSString stringWithFormat:@"%i",seconds%10];
    }else{
        [timer invalidate];
        timer = nil;
        self.timeLabel1.text = [NSString stringWithFormat:@"%i",0];
        self.timeLabel2.text = [NSString stringWithFormat:@"%i",0];
        self.timeLabel3.text = [NSString stringWithFormat:@"%i",0];
        self.timeLabel4.text = [NSString stringWithFormat:@"%i",0];
        self.timeLabel5.text = [NSString stringWithFormat:@"%i",0];
        [self backToWaitVC];
        [Util showAlertViewWithTitle:@"提示" Message:@"答题时间已过！"];
        [Util clearCacheWithTaskId:self.currentTask.taskId];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.currentTask.testItemArr.count % 4 == 0) {
        return self.currentTask.testItemArr.count/4;
    }else{
        return self.currentTask.testItemArr.count/4 + 1;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cell_identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (self.currentTask.testItemArr.count >indexPath.row * 4) {
        switch (self.currentTask.testItemArr.count - indexPath.row*4) {
            case 1:
            {
                NSArray *itemNib1 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
                NSArray *defaultNib1 = [[NSBundle mainBundle]loadNibNamed:@"defaultView" owner:nil options:nil];
                NSArray *defaultNib2 = [[NSBundle mainBundle]loadNibNamed:@"defaultView" owner:nil options:nil];
                NSArray *defaultNib3 = [[NSBundle mainBundle]loadNibNamed:@"defaultView" owner:nil options:nil];
                itemView *item1 = [itemNib1 objectAtIndex:0];
                item1.delegate = self;
                [item1 config];
                [item1 setFrame:CGRectMake(16, 4, 230, 278)];
                item1.currentIndex = indexPath.row*4;
                item *tempItem1 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4];
                item1.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem1.itemIndex,tempItem1.itemType];
                item1.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem1.score];
                [item1.questionWebView loadHTMLString:tempItem1.question baseURL:nil];
                UIView *default1 = [defaultNib1 objectAtIndex:0];
                [default1 setFrame:CGRectMake(CGRectGetMaxX(item1.frame)+16, 4, 230, 278)];
                UIView *default2 = [defaultNib2 objectAtIndex:0];
                [default2 setFrame:CGRectMake(CGRectGetMaxX(default1.frame)+16, 4, 230, 278)];
                UIView *default3 = [defaultNib3 objectAtIndex:0];
                [default3 setFrame:CGRectMake(CGRectGetMaxX(default2.frame)+16, 4, 230, 278)];
                [cell addSubview:item1];
                [cell addSubview:default1];
                [cell addSubview:default2];
                [cell addSubview:default3];
            }
                break;
            case 2:
            {
                NSArray *itemNib1 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
                NSArray *itemNib2 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
                NSArray *defaultNib1 = [[NSBundle mainBundle]loadNibNamed:@"defaultView" owner:nil options:nil];
                NSArray *defaultNib2 = [[NSBundle mainBundle]loadNibNamed:@"defaultView" owner:nil options:nil];
                itemView *item1 = [itemNib1 objectAtIndex:0];
                item1.delegate = self;
                [item1 config];
                [item1 setFrame:CGRectMake(16, 4, 230, 278)];
                item1.currentIndex = indexPath.row*4;
                item *tempItem1 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4];
                item1.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem1.itemIndex,tempItem1.itemType];
                item1.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem1.score];
                [item1.questionWebView loadHTMLString:tempItem1.question baseURL:nil];
                itemView *item2 = [itemNib2 objectAtIndex:0];
                item2.delegate = self;
                [item2 config];
                [item2 setFrame:CGRectMake(CGRectGetMaxX(item1.frame)+16, 4, 230, 278)];
                item2.currentIndex = indexPath.row*4+1;
                item *tempItem2 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4+1];
                item2.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem2.itemIndex,tempItem2.itemType];
                item2.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem2.score];
                [item2.questionWebView loadHTMLString:tempItem2.question baseURL:nil];
                UIView *default1 = [defaultNib1 objectAtIndex:0];
                [default1 setFrame:CGRectMake(CGRectGetMaxX(item2.frame)+16, 4, 230, 278)];
                UIView *default2 = [defaultNib2 objectAtIndex:0];
                [default2 setFrame:CGRectMake(CGRectGetMaxX(default1.frame)+16, 4, 230, 278)];
                [cell addSubview:item1];
                [cell addSubview:item2];
                [cell addSubview:default1];
                [cell addSubview:default2];
            }
                break;
            case 3:
            {
                NSArray *itemNib1 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
                NSArray *itemNib2 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
                NSArray *itemNib3 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
                NSArray *defaultNib1 = [[NSBundle mainBundle]loadNibNamed:@"defaultView" owner:nil options:nil];
                itemView *item1 = [itemNib1 objectAtIndex:0];
                item1.delegate = self;
                [item1 config];
                [item1 setFrame:CGRectMake(16, 4, 230, 278)];
                item1.currentIndex = indexPath.row*4;
                item *tempItem1 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4];
                item1.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem1.itemIndex,tempItem1.itemType];
                item1.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem1.score];
                [item1.questionWebView loadHTMLString:tempItem1.question baseURL:nil];
                itemView *item2 = [itemNib2 objectAtIndex:0];
                item2.delegate = self;
                [item2 config];
                [item2 setFrame:CGRectMake(CGRectGetMaxX(item1.frame)+16, 4, 230, 278)];
                item2.currentIndex = indexPath.row*4+1;
                item *tempItem2 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4+1];
                item2.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem2.itemIndex,tempItem2.itemType];
                item2.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem2.score];
                [item2.questionWebView loadHTMLString:tempItem2.question baseURL:nil];
                itemView *item3 = [itemNib3 objectAtIndex:0];
                item3.delegate = self;
                [item3 config];
                [item3 setFrame:CGRectMake(CGRectGetMaxX(item2.frame)+16, 4, 230, 278)];
                item3.currentIndex = indexPath.row*4+2;
                item *tempItem3 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4+2];
                item3.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem3.itemIndex,tempItem3.itemType];
                item3.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem3.score];
                [item3.questionWebView loadHTMLString:tempItem3.question baseURL:nil];
                UIView *default1 = [defaultNib1 objectAtIndex:0];
                [default1 setFrame:CGRectMake(CGRectGetMaxX(item3.frame)+16, 4, 230, 278)];
                [cell addSubview:item1];
                [cell addSubview:item2];
                [cell addSubview:item3];
                [cell addSubview:default1];
            }
                break;
            default:
                break;
        }
    }else{
        NSArray *itemNib1 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
        NSArray *itemNib2 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
        NSArray *itemNib3 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
        NSArray *itemNib4 = [[NSBundle mainBundle]loadNibNamed:@"itemView" owner:nil options:nil];
        itemView *item1 = [itemNib1 objectAtIndex:0];
        item1.delegate = self;
        [item1 config];
        [item1 setFrame:CGRectMake(16, 4, 230, 278)];
        item1.currentIndex = indexPath.row*4;
        item *tempItem1 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4];
        item1.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem1.itemIndex,tempItem1.itemType];
        item1.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem1.score];
        [item1.questionWebView loadHTMLString:tempItem1.question baseURL:nil];
        itemView *item2 = [itemNib2 objectAtIndex:0];
        item2.delegate = self;
        [item2 config];
        [item2 setFrame:CGRectMake(CGRectGetMaxX(item1.frame)+16, 4, 230, 278)];
        item2.currentIndex = indexPath.row*4+1;
        item *tempItem2 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4+1];
        item2.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem2.itemIndex,tempItem2.itemType];
        item2.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem2.score];
        [item2.questionWebView loadHTMLString:tempItem2.question baseURL:nil];
        itemView *item3 = [itemNib3 objectAtIndex:0];
        item3.delegate = self;
        [item3 config];
        [item3 setFrame:CGRectMake(CGRectGetMaxX(item2.frame)+16, 4, 230, 278)];
        item3.currentIndex = indexPath.row*4+2;
        item *tempItem3 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4+2];
        item3.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem3.itemIndex,tempItem3.itemType];
        item3.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem3.score];
        [item3.questionWebView loadHTMLString:tempItem3.question baseURL:nil];
        itemView *item4 = [itemNib4 objectAtIndex:0];
        item4.delegate = self;
        [item4 config];
        [item4 setFrame:CGRectMake(CGRectGetMaxX(item3.frame)+16, 4, 230, 278)];
        item4.currentIndex = indexPath.row*4+3;
        item *tempItem4 = [self.currentTask.testItemArr objectAtIndex:indexPath.row*4+3];
        item4.itemIndexAndTypeLable.text = [NSString stringWithFormat:@"%i、%@",tempItem4.itemIndex,tempItem4.itemType];
        item4.itemScoreLabel.text = [NSString stringWithFormat:@"（%i分）",tempItem4.score];
        [item4.questionWebView loadHTMLString:tempItem4.question baseURL:nil];
        [cell addSubview:item1];
        [cell addSubview:item2];
        [cell addSubview:item3];
        [cell addSubview:item4];
    }
    return cell;
}

#pragma mark itemViewDelegate
-(void)takePhotoWithCurrentIndex:(int)index
{
    self.currentIndex = index;
    if (imagePicker == nil) {
        imagePicker = [[UIImagePickerController alloc]init];
    }
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:^{
        
    }];
}
-(void)photoLibraryWithCurrentIndex:(int)index
{
    self.currentIndex = index;
    if (imagePicker == nil) {
        imagePicker = [[UIImagePickerController alloc]init];
    }
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    else{
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    imagePicker.allowsEditing = YES;
    if (popoverController == nil) {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
        popoverController = popover;
        //popoverController.delegate = self;
    }
    [popoverController presentPopoverFromRect:CGRectMake(0, 0, 300, 300) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(void)drawWithCurrentIndex:(int)index
{
    self.currentIndex = index;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DrawingBoardViewController *draw = [storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
    draw.currentTask = self.currentTask;
    draw.currentIndex = self.currentIndex;
    [self.navigationController pushViewController:draw animated:YES];
}
#pragma mark UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DrawingBoardViewController *draw = [storyboard instantiateViewControllerWithIdentifier:@"DrawingBoardViewController"];
    draw.currentTask = self.currentTask;
    draw.currentIndex = self.currentIndex;
    draw.photo = image;
    [self.navigationController pushViewController:draw animated:YES];
    [imagePicker dismissViewControllerAnimated:YES completion:^{
         imagePicker = nil;
    }];
    [popoverController dismissPopoverAnimated:YES];
    popoverController = nil;
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [imagePicker dismissViewControllerAnimated:YES completion:^{
       imagePicker = nil;
    }];
    [popoverController dismissPopoverAnimated:YES];
    popoverController = nil;
}
- (IBAction)pressBtn:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //跳到PC端答题
            [self submitAllAnswerWithCompitionHandler:^{
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"提示" message:@"已跳转回PC端" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                alert = nil;
            }];
        }
            break;
        default:
            break;
    }
}
-(void)backToWaitVC
{
    BOOL exits = NO;
    for (UIViewController *viewController in self.navigationController.viewControllers) {
        if ([viewController isKindOfClass:[WaitViewController class]]) {
            [self.navigationController popToViewController:viewController animated:YES];
            exits = YES;
        }
    }
    if (!exits) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        WaitViewController *waitVC = [storyboard instantiateViewControllerWithIdentifier:@"WaitViewController"];
        [self.navigationController pushViewController:waitVC animated:YES];
    }
}
- (NSString*)getPath:(NSString*)dirName
{
    return [NSString stringWithFormat:@"%@%@",NSTemporaryDirectory(),dirName];
}
#pragma mark submitAllAnswer
-(void)submitAllAnswerWithCompitionHandler:(void(^)())complitionHandler
{
    NSMutableArray *itemArr = [NSMutableArray array];
    for (int i = 0; i < self.currentTask.testItemArr.count ; i ++) {
        item *tempItem = [self.currentTask.testItemArr objectAtIndex:i];
        if (tempItem.status == 1) {
            [itemArr addObject:tempItem];
        }
    }
    [self.view addSubview:[waitView getDeafaultView]];
    [[waitView getDeafaultView]startAnimation];
    [self submit:itemArr compitionHandler:^{
        [[waitView getDeafaultView]stopAnimation];
        [[waitView getDeafaultView]removeFromSuperview];
        complitionHandler();
    }];
}
-(void)submit:(NSMutableArray*)itemArr compitionHandler:(void(^)())complitionHandler
{
    if (itemArr.count > 0) {
        item *tempItem = [itemArr lastObject];
        UIImage *tempImage;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@.png",[self getPath:self.currentTask.taskId],tempItem.itemId];
        if ([fileManager fileExistsAtPath:filePath]) {
            tempImage = [UIImage imageWithContentsOfFile:filePath];
        }
        NSData *data;
        if (UIImagePNGRepresentation(tempImage) == nil) {
            data = UIImageJPEGRepresentation(tempImage, 0.00001);
        } else {
            data = UIImagePNGRepresentation(tempImage);
        }
        NSString *dataString = [data base64Encoding];
        [self.network sendRequest:[self.network createUploadResourseRequestWithResData:dataString width:tempImage.size.width height:tempImage.size.height] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [self.network sendRequest:[self.network createSubmitAnswerTmpRequestWithTaskId:self.currentTask.taskId itemId:[NSMutableArray arrayWithObjects:tempItem.itemId, nil] ItemAnswer:[NSMutableArray arrayWithObjects:[data objectForKey:@"res_id"], nil] finished:NO] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
                        if (isSuccess) {
                            if([[data objectForKey:@"error_code"]intValue] == 0) {
                                [itemArr removeLastObject];
                                [self submit:itemArr compitionHandler:complitionHandler];
                            }else{
                                [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                            }
                        }
                        else{
                            [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
                        }
                    }];
                } else{
                    [[waitView getDeafaultView]stopAnimation];
                    [[waitView getDeafaultView]removeFromSuperview];
                    [Util showAlertViewWithTitle:@"提示" Message:[data objectForKey:@"error_str"]];
                }
            }else{
                [[waitView getDeafaultView]stopAnimation];
                [[waitView getDeafaultView]removeFromSuperview];
                [Util showAlertViewWithTitle:@"提示" Message:errorMessage];
            }
        }];
    }else{
        [self.view addSubview:[waitView getDeafaultView]];
        [[waitView getDeafaultView]startAnimation];
        [self.network sendRequest:[self.network createSubmitAnswerTmpRequestWithTaskId:self.currentTask.taskId itemId:Nil ItemAnswer:nil finished:YES] withCompletionHandler:^(BOOL isSuccess, NSDictionary *data, NSString *errorMessage) {
            [[waitView getDeafaultView]stopAnimation];
            [[waitView getDeafaultView]removeFromSuperview];
            if (isSuccess) {
                if ([[data objectForKey:@"error_code"]intValue] == 0) {
                    [Util clearCacheWithTaskId:self.currentTask.taskId];
                    complitionHandler();
                }
            }
        }];
    }
}

#pragma mark UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self backToWaitVC];
        }
            break;
            
        default:
            break;
    }
}
@end
