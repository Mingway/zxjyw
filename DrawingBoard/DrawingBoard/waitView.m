//
//  waitView.m
//  DrawingBoard
//
//  Created by HelyData on 13-12-10.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import "waitView.h"
#import <QuartzCore/QuartzCore.h>

static waitView* defaultView = nil;

@interface waitView ()
@property (weak, nonatomic) IBOutlet UIView *showView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;



@end

@implementation waitView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
+(waitView*)getDeafaultView
{
    if (defaultView == nil) {
        NSArray *waitNib = [[NSBundle mainBundle]loadNibNamed:@"waitView" owner:nil options:nil];
        defaultView = [waitNib objectAtIndex:0];
        defaultView.showView.layer.cornerRadius = 5;
    }
    defaultView.statusLabel.text = @"请等待...";
    return defaultView;
}

-(void)startAnimation
{
    [self.activityIndicatorView startAnimating];
}
-(void)stopAnimation
{
    [self.activityIndicatorView stopAnimating];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
