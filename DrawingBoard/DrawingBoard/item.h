//
//  item.h
//  DrawingBoard
//
//  Created by HelyData on 13-11-1.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface item : NSObject
@property (nonatomic, readonly)NSString *itemId;
@property (nonatomic, readonly)NSString *answer;//回答的图片res_id
@property (nonatomic, readonly)NSString *itemType;//题目类型，证明题，解答题等
@property (nonatomic, readonly)int itemIndex;//当前题目在task中的序号
@property (nonatomic, readonly)int score;//当前题目分值
@property (nonatomic, readonly)NSString *question;//问题内容
@property (nonatomic, readonly)NSURL *imageUrl;//题图
@property (nonatomic, readonly)int blankNum;//填空个数
@property (nonatomic, assign) int status;//0未修改，1已修改
- (void)updateWithDic:(NSDictionary*)dic;
@end
