//
//  main.m
//  DrawingBoard
//
//  Created by HelyData on 13-10-31.
//  Copyright (c) 2013年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
