整体说明：

   推送消息的处理都在Appdelete文件中，图片资源在Images.xcassets文件夹中。MathJax.js处理数学公式的js文件。


云辅导平台 ->  CloudPlatform
   
   整体共分五个部分：Model、View、Controller、Network、Util。
   
   Model对应模型层，模型对象的封装
        ->userInfo(用户信息)
        ->question(问题)
        ->coinRecord(积分记录)
        ->exchangeRecord(兑换记录)
  
   View对应视图层
        ->waitView(等待界面视图)
        ->loadCell（积分列表处加载更多的cell
        ->recordCell(录音点评的cell)
        ->exchangePlatformCell(兑换列表的cell，暂时未用)
        ->coinRecordCell(积分记录的cell)
        ->questionListRecordView(问题列表中点评视图)
              -->officialRecordView(推送题目视图)
              -->pratiseRecordView(训练题目视图)
        ->questionListDetailView(问题列表中查看问题视图)
              -->officialQuestionDetailView(推送题目视图)
              -->practiseQuestionDetailView(训练题目视图)
        ->unfoldView(推送题目展开视图)
        ->questionCell(问题列表界面问题的cell)
              -->officialCell(推送题目)
              -->practiseCell(训练题目)
        ->remindCell.xib(针对没有推送题目时，问题列表上方的提醒)。

   Controller对应控制层
        ->navVC(导航控制器)
        ->loginVC（登录视图控制器）
        ->findPassword(忘记密码流程)
        ->register(注册流程)
              -->registerVC(注册界面视图控制器)
              -->addUserInfoVC(注册后的添加用户可工作时间和擅长学科的视图控制器)
        ->activeAndunfreeze(激活和解冻流程)
              -->startVC(开始界面视图控制器)
              -->questionDetailVC(问题详细内容的视图控制器)
              -->recordVC(录音点评的视图控制器)
              -->successfulVC(成功界面视图控制器)
        ->officialQuestion(正式流程)
              -->questionListVC(问题列表视图控制器)
              -->questionUnfoldVC(推送题目展开界面视图控制器，与View层配合使用)
              -->questionListDetailVC(列表题目内容展开视图控制器，与View层配合使用)
              -->questionRecordVC(列表题目录音点评试图控制器，与View层配合使用)
        ->userCenter(用户中心流程)
              -->userCenter(用户中心界面视图控制器)
              -->updatePasswordVC(修改密码视图控制器)
              -->coinCenterVC(积分中心视图控制器)
              -->updateUserInfoVC(修改用户信息视图控制器)
              -->exchangeVC(兑换界面视图控制器)
              -->exchangeDetailVC(兑换详情界面视图控制器)

    
   Network   网络层
        ->Reachability(判断网络状态)
        ->NetRequest(网络请求)
        ->NetworkManager(网络请求的管理)
        ->UrlDefine(接口url定义)

   Util   工具
        ->RSLogger(将Debug信息写入本地文件)
        ->ResourseDefine(常量资源定义)
        ->keychainUtils(钥匙串处理工具类)
        ->UIHyperlinksButton(带下划线的按钮)
        ->ScrollViewWithZoom(图片的缩放ScrollView)
        ->tools(工具类,包含钥匙串的操作，md5，当前日期显示UIAlertView等)
        ->UIButton+Extensions(用于扩大按钮相应区域) 
        ->EGOImage(用于异步加载和缓存图片的第三方库)
        ->PullRefreshTableView(下拉刷新)

    
家长助手  ->  ParentAssistant

   整体共分五个部分：Model、View、Controller、Network、Util。

   Model对应模型层，模型对象的封装
        ->user(用户信息)
        ->student(绑定的学生)
        ->news(新闻)
        ->article(心理测试文章)
        ->articleQuestion(心理测试题目)
        ->task(任务)
        ->course(根据学生进度获取的某一科目信息)
        ->repo(错题)
        ->report(报告)
               -->report(报告)
               -->answer(学生答案情况)
               -->type_stat(题型答题情况)
               -->kpoit_stat(试卷中失分点)
               -->difficulty(某个难度答题情况)
               -->weight(不同权重题目答题情况)
        ->notify(通知)
        ->stat_kpoint(科目状态下得失分点)

   View对应视图层
        ->kpoint_statCell((试卷中失分点的cell)
        ->type_statCell(题型大体情况的cell)
        ->repoCell(错题的cell)
        ->courseCell(学生进度的科目cell)
        ->taskCell(任务的cell)
        ->studentCell(绑定学生的cell)
        ->waitView(等待界面视图)
        ->psychagogyCell(心理文章cell)
        ->newsCell(新闻的cell)

   Controller对应控制层
        ->picker(日期选择视图控制器，用于试卷安排视图)
        ->ViewController(登录和注册选择界面)
        ->loginVC(登录视图控制器)
        ->registerVC(注册视图控制器)
        ->forgetPasswordVC(忘记密码流程)
        ->bindChildrenVC(绑定学生流程，注册之后使用)
        ->studyManagerVC(学习管理视图)
               -->studyManagerVC(学习管理视图控制器)
               -->courseStatusVC(科目状态视图控制器)
               -->repoListVC(错题列表视图控制器)
               -->reportVC(报告视图控制器)
               -->taskCenterVC(学生作业完成情况列表视图控制器,原来叫任务中心)
               -->paperAssignmentVC（试卷安排视图控制器）
               -->chartWebViewVC(图表横屏视图控制器)
        ->educationNewsVC(教育新闻)
               -->educationNewsVC(教育新闻列表视图控制器)
               -->educationNewsDetailVC(教育新闻详情视图控制器)
        ->psychagogyVC(心理测试)
               -->psychagogyVC(心理测试文章列表视图控制器)
               -->psychagogyDetailVC(心理测试文章详情视图控制器)
               -->psychagogyPaperAssignmentVC(心理测试试卷安排视图控制器)
               -->psychagogyQuestionVC(心理测试做题视图控制器)
        ->mineVC(我的)
               -->mineVC(我的视图控制器)
               -->updatePasswordVC(修改密码视图控制器)
               -->bindChildrenVC(绑定学生视图控制器，由我的界面进入)
               -->notifyListVC(消息列表视图控制器)
 
    Network   网络层
        ->Reachability(判断网络状态)
        ->NetRequest(网络请求)
        ->NetworkManager(网络请求的管理)
        ->UrlDefine(接口url定义)

    Util  工具
        ->RSLogger(将Debug信息写入本地文件)
        ->keychainUtils(钥匙串处理工具类)
        ->UIUnderlineButton(带下划线的按钮)
        ->Util(工具类,包含钥匙串的操作，md5，当前日期显示UIAlertView等)
        ->UIButton+Extensions(用于扩大按钮相应区域) 
        ->EGOImage(用于异步加载和缓存图片的第三方库)
        ->EGOTableViewPullRefresh(上拉、下拉刷新)
        ->UINavigationController+Autorotate(用于当行视图横屏)
        ->define(常量资源定义)

ipad画板  ->  DrawingBoard


   整体共分五个部分：Model、View、Controller、Network、Util。

   Model对应模型层，模型对象的封装
        ->user(用户信息)
        ->task(任务)
        ->item(题目)
 

   View对应视图层
        ->waitView(等待界面视图)
        ->itemView(题目列表单个题目视图)

   
   Controller对应控制层
        ->ViewController(登录视图控制器)
        ->WaitViewController(等待视图控制器)
        ->QuestionListViewController(题目列表视图控制器)
        ->DrawingBoardViewController(针对题目的画板视图控制器)


   Network   网络层
        ->Reachability(判断网络状态)
        ->NetRequest(网络请求)
        ->NetworkManager(网络请求的管理)
        ->UrlDefine(接口url定义)

   Util  工具
        ->RSLogger(将Debug信息写入本地文件)
        ->keychainUtils(钥匙串处理工具类)
        ->Util(工具类,包含钥匙串的操作，md5，当前日期显示UIAlertView等)
        ->UIButton+UIButtonImageWithLabel(按钮右边图片左边标题显示)
        ->ACEDrawingView(画图第三方库)

   

    






